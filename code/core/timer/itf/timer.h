/// Header file for the timer device class implemented in the core::timer namespace.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#pragma once

#include "core/peripheral/itf/device.h"

namespace core::timer {

/// Implements a hardware timer device.
///
/// The timer device allows up to 4 independent timers. The interval (in frames) and mode (single
/// shot or repeating) of which can be set independently, but are multiplexed through a single IRQ.
///
/// When one or more timers fire, the timer device IRQ line is raised, and the IRQ_CONTROL register
/// will set the 'IRQ pending' bits for each timer that fired. The IRQ handler can decide to either
/// handle all timers at once and acknowledge the interrupt by clearing all 'IRQ pending bits' at once,
/// or handle them one by one by clearing only the bit for the handled interrupt, which will re-assert
/// the interrupt immediately after returning from the handler.
class Timer : public peripheral::Device
{
  public:
    //
    // Timer device ports
    //

    // IRQ control and status register
    static constexpr WORD IRQ_CONTROL                 = 0x0001;

    // Timer control, interval and counter ports
    static constexpr WORD TIMER_0_CONTROL             = 0x0100;
    static constexpr WORD TIMER_0_INTERVAL            = 0x0101;
    static constexpr WORD TIMER_0_COUNTER             = 0x0102;
    static constexpr WORD TIMER_1_CONTROL             = 0x0110;
    static constexpr WORD TIMER_1_INTERVAL            = 0x0111;
    static constexpr WORD TIMER_1_COUNTER             = 0x0112;
    static constexpr WORD TIMER_2_CONTROL             = 0x0120;
    static constexpr WORD TIMER_2_INTERVAL            = 0x0121;
    static constexpr WORD TIMER_2_COUNTER             = 0x0122;
    static constexpr WORD TIMER_3_CONTROL             = 0x0130;
    static constexpr WORD TIMER_3_INTERVAL            = 0x0131;
    static constexpr WORD TIMER_3_COUNTER             = 0x0132;

    // Start of port range for all timer ports
    static constexpr WORD TIMER_PORTS_BASE            = TIMER_0_CONTROL;
    // Number of ports per timer (note: this is more like a stride, not nearly all ports are used for each timer)
    static constexpr WORD TIMER_NUM_PORTS             = TIMER_1_CONTROL - TIMER_0_CONTROL;

    //
    // Timer device register flag bits
    //

    /// IRQ_CONTROL enabled timers bitmask
    static constexpr WORD IRQ_CONTROL_ENABLE_TIMERS   = 0x0F00;
    /// IRQ_CONTROL timer interrupts pending bitmask
    static constexpr WORD IRQ_CONTROL_TIMERS_PENDING  = 0x000F;
    /// TIMER_x_CONTROL bit mask
    static constexpr WORD TIMER_CONTROL_MODE_BITS     = 0x000F;
    /// TIMER_x_CONTROL disable timer
    static constexpr WORD TIMER_CONTROL_MODE_DISABLED = 0x0000;
    /// TIMER_x_CONTROL one-shot timer mode
    static constexpr WORD TIMER_CONTROL_MODE_ONE_SHOT = 0x0001;
    /// TIMER_x_CONTROL repeat timer mode
    static constexpr WORD TIMER_CONTROL_MODE_REPEAT   = 0x0002;

    //
    // Timer device constants
    //

    /// Number of supported timers
    static constexpr BYTE NUM_TIMERS = 4;

    /// Timer device port descriptions
    inline static const std::map<Port, Device::PortDescription> PORTS = {
      { IRQ_CONTROL,                                   { "irq_control", "IRQ control and status register" } },
      { TIMER_0_CONTROL,                               { "timer_0_control", "Timer 0 control register" } },
      { TIMER_0_INTERVAL,                              { "timer_0_interval", "Timer 0 interval, in frames" } },
      { TIMER_0_COUNTER,                               { "timer_0_counter", "Timer 0 frame counter" } },
      { TIMER_1_CONTROL,                               { "timer_1_control", "Timer 1 control register" } },
      { TIMER_1_INTERVAL,                              { "timer_1_interval", "Timer 1 interval, in frames" } },
      { TIMER_1_COUNTER,                               { "timer_1_counter", "Timer 1 frame counter" } },
      { TIMER_2_CONTROL,                               { "timer_2_control", "Timer 2 control register" } },
      { TIMER_2_INTERVAL,                              { "timer_2_interval", "Timer 2 interval, in frames" } },
      { TIMER_2_COUNTER,                               { "timer_2_counter", "Timer 2 frame counter" } },
      { TIMER_3_CONTROL,                               { "timer_3_control", "Timer 3 control register" } },
      { TIMER_3_INTERVAL,                              { "timer_3_interval", "Timer 3 interval, in frames" } },
      { TIMER_3_COUNTER,                               { "timer_3_counter", "Timer 3 frame counter" } },
    };

    /// Constructor.
    ///
    /// \param id Timer device ID
    /// \param timerIrq IRQ line for timer interrupt
    Timer(const Device::ID id, const BYTE timerIrq);

    /// Step timer device by a single frame.
    ///
    /// This will update all enabled timer counters and set the timer IRQ control flags
    /// for all timers that should fire.
    void frame();

    //
    // Device base class interface
    //

    /// Reset GPU to default state.
    void reset() override;

    /// Return pending interrupts.
    ///
    /// For the timer device, there is only one IRQ line used for all timers. The interrupt
    /// handler should check the \a IRQ_CONTROL_TIMERS_PENDING bits of the \a IRQ_CONTROL
    /// register to see which timers fired and write 0 bits for each timer to acknowledge
    /// the interrupt.
    ///
    /// \return bitfield indicating pending interrupt IRQ lines
    virtual WORD pendingInterrupts() const override;

  private:
    BYTE _timerIrq;
};

}