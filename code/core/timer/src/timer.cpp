/// Implementation file for the core::timer::Timer class.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#include "core/timer/itf/timer.h"

namespace core::timer {

// Constructor
Timer::Timer(const Device::ID id, const BYTE timerIrq)
:
  Device(id, PORTS, (1 << timerIrq)),
  _timerIrq(timerIrq)
{
  // Reset timer device to default (powerup) state
  reset();
}

// Step timer device by one frame
void Timer::frame()
{
  for (int i = 0; i < NUM_TIMERS; i++)
  {
    const WORD timer_control_port = TIMER_0_CONTROL + i*TIMER_NUM_PORTS;
    const WORD timer_interval_port = TIMER_0_INTERVAL + i*TIMER_NUM_PORTS;
    const WORD timer_counter_port = TIMER_0_COUNTER + i*TIMER_NUM_PORTS;

    const WORD timer_control = in(timer_control_port);
    const WORD timer_mode = timer_control & TIMER_CONTROL_MODE_BITS;

    if (timer_mode != TIMER_CONTROL_MODE_DISABLED)
    {
      const WORD timer_interval = in(timer_interval_port);

      WORD timer_counter = in(timer_counter_port);

      if (++timer_counter >= timer_interval)
      {
        // One-shot timers are disabled after firing
        if (timer_mode == TIMER_CONTROL_MODE_ONE_SHOT)
        {
          out(timer_control_port, (timer_control & ~TIMER_CONTROL_MODE_BITS) | TIMER_CONTROL_MODE_DISABLED);
        }

        // Set timer IRQ pending for the this timer.
        out(IRQ_CONTROL, in(IRQ_CONTROL) | (1 << i));

        // Reset timer counter
        timer_counter = 0;
      }

      out(timer_counter_port, timer_counter);
    }
  }
}

// Reset timer device to powerup state
void Timer::reset()
{
  out(IRQ_CONTROL, 0);

  for (int i = 0; i < NUM_TIMERS; i++)
  {
    out(TIMER_0_CONTROL + i*TIMER_NUM_PORTS, TIMER_CONTROL_MODE_DISABLED);
    out(TIMER_0_INTERVAL + i*TIMER_NUM_PORTS, 0);
    out(TIMER_0_COUNTER + i*TIMER_NUM_PORTS, 0);
  }
}

// Return pending interrupts. For the timer device, there is only one IRQ line used for all timers,
// which is asserted if at least one timer fired and has not been acknowledged yet.
WORD Timer::pendingInterrupts() const
{
  const WORD timers_fired = in(IRQ_CONTROL) & IRQ_CONTROL_TIMERS_PENDING;
  const WORD  timers_enabled = (in(IRQ_CONTROL) & IRQ_CONTROL_ENABLE_TIMERS) >> 8;

  return ((timers_fired & timers_enabled) != 0 ? (1 << _timerIrq) : 0);
}

}
