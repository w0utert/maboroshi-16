/// Implementation file for the core::gpu::Gpu class and other GPU-related interfaces.
///
/// \note all of the rendering code is currently the most naive reference implementation
///   imaginable, no effort has been done to optimize anything. Also only a tiny subset
///   of GPU features and rendering corner cases is properly implemented.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#include "core/gpu/itf/gpu.h"

#include "default_palette.h"
#include "default_font_8x8.h"

#include <tbb/parallel_for.h>

#include <range/v3/view/iota.hpp>
#include <range/v3/view/cartesian_product.hpp>
#include <range/v3/view/transform.hpp>
#include <range/v3/view/enumerate.hpp>

#include <simde/x86/sse3.h>
#include <simde/x86/sse4.1.h>

#include <array>

using namespace std::chrono;

using namespace ranges;

namespace core::gpu {

namespace {

// Expand TRGB1555 color value to RGBA8888
constexpr utils::ImageRgba::RGBA8888 RGBA8888(const WORD trgb1555)
{
  return {
    static_cast<BYTE>(((trgb1555 & 0b0111110000000000) >> 10) << 3),
    static_cast<BYTE>(((trgb1555 & 0b0000001111100000) >> 5) << 3),
    static_cast<BYTE>(((trgb1555 & 0b0000000000011111) << 3)),
    static_cast<BYTE>(trgb1555 >> 15 == 0 ? 0xFF : 0x00)
  };
}

// Naive rotate, flip x, and/or flip-y transform of 8x8 block of pixels. This can be optimized
// significantly using SSE or (better yet) AVX2.
void transform_8x8(std::array<Gpu::Pixel, 64> &block, const bool rotate, const bool flipX, const bool flipY)
{
  if (flipX)
  {
    Gpu::Pixel *row = block.data();

    for (int i = 0; i < 8; i++)
    {
      for (int j = 0; j < 4; j++)
      {
        std::swap(row[j], row[7-j]);
      }

      row += 8;
    }
  }

  if (flipY)
  {
    for (int j = 0; j < 8; j++)
    {
      for (int i = 0; i < 4; i++)
      {
        std::swap(block[i*8 + j], block[(7-i)*8 + j]);
      }
    }
  }

  if (rotate)
  {
    const std::array<Gpu::Pixel, 64> tmp = block;

    for (int i = 0; i < 8; i++)
    {
      for (int j = 0; j < 8; j++)
      {
        block[j*8 + (7 - i)] = tmp[i*8 + j];
      }
    }
  }
}

// Generator that returns x,y pixel grid offsets for a tile/sprite composed of multiple 8x8 subtiles,
// possibly transformed by a 90-degree rotation, x-flip and/or y-flip.
//
// The multi-tile rendering code uses this to be able to iterate sub-tiles in their original linear
// order, using the generated pixel offset to place them at their correct transformed display location.
// The 8x8 subtile rendering code itself will apply the same rotate, flipX, flipY transform to the tile
// pixel data to achieve the final transformed rendering of the mult-tile.
struct GenerateSubtileOffsets
{
  public:
    struct Vec2i { int x; int y; };

    GenerateSubtileOffsets(const int size, const int scale, const bool rotate, const bool flipX, const bool flipY)
    :
      GenerateSubtileOffsets({ size, size }, scale, rotate, flipX, flipY)
    {
    }

    GenerateSubtileOffsets(const Vec2i &size, const int scale, const bool rotate, const bool flipX, const bool flipY)
    {
      const Transform &transform = SUBTILE_TRANSFORMS[rotate | (flipX << 1) | (flipY << 2)];

      const int dxy = Gpu::SUBTILE_SIZE * scale;

      _origin = { transform.origin.x * (size.x - 1) * dxy, transform.origin.y * (size.y - 1) * dxy};
      _axis_i = { transform.axes.i.x * dxy, transform.axes.i.y * dxy };
      _axis_j = { transform.axes.j.x * dxy, transform.axes.j.y * dxy };
    }

    std::pair<int, int> operator()(const std::tuple<int, int> &ij) const
    {
      auto &[i, j] = ij;

      const int ox = _origin.x + _axis_j.x * j + _axis_j.y * i;
      const int oy = _origin.y + _axis_i.x * j + _axis_i.y * i;

      return { ox, oy };
    }

  private:
    struct Transform
    {
      Vec2i origin;
      struct { Vec2i j; Vec2i i; } axes;
    };

    // Transform table that contains the normalized coordinate system origin and axis
    // vectors for generating the offsets for all possible transforms.
    static constexpr std::array<Transform, 8> SUBTILE_TRANSFORMS = {{
      { { 0, 0 }, { {  1,  0 }, {  0,  1 } } },   // rotate = 0, flip x = 0, flip y = 0
      { { 1, 0 }, { {  0, -1 }, {  1,  0 } } },   // rotate = 1, flip x = 0, flip y = 0
      { { 1, 0 }, { { -1,  0 }, {  0,  1 } } },   // rotate = 0, flip x = 1, flip y = 0
      { { 1, 1 }, { {  0, -1 }, { -1,  0 } } },   // rotate = 1, flip x = 1, flip y = 0
      { { 0, 1 }, { {  1,  0 }, {  0, -1 } } },   // rotate = 0, flip x = 0, flip y = 1
      { { 0, 0 }, { {  0,  1 }, {  1,  0 } } },   // rotate = 1, flip x = 0, flip y = 1
      { { 1, 1 }, { { -1 , 0 }, {  0, -1 } } },   // rotate = 0, flip x = 1, flip y = 1
      { { 0, 1 }, { {  0,  1 }, { -1,  0 } } },   // rotate = 1, flip x = 1, flip y = 1
    }};

    Vec2i _origin;
    Vec2i _axis_i;
    Vec2i _axis_j;
};

}

// Constructor
Gpu::Gpu(const Device::ID id, const unsigned width, const unsigned int height, const BYTE vblankIrq)
:
  Device(id, PORTS, (1 << vblankIrq), NUM_VRAM_PAGES),
  _width(width), _height(height),
  _vblankIrq(vblankIrq)
{
  // Allocate layer backing stores and frame buffer
  for (std::unique_ptr<utils::ImageRgba::RGBA8888[]> &layer_buffer : _layerBuffers)
  {
    layer_buffer = std::make_unique<utils::ImageRgba::RGBA8888[]>(width * height);
  }

  _frontBuffer = std::make_unique<utils::ImageRgba::RGBA8888[]>(width * height);

  // Create TBB task arena that will be used to concurrently render enabled layers and initialize
  // it. TBB can do all of this implicitly/lazily, but that introduces unwanted initialization
  // overhead when rendering the first frame, which is annoying when profiling the rendering code.
  _taskArena = std::make_unique<tbb::task_arena>(4);
  _taskArena->initialize();

  // Reset GPU to initial (powerup) state
  reset();
}

// Reset GPU to initial (powerup) state
void Gpu::reset()
{
  // Zero all memory
  _memory->clear();

  for (std::unique_ptr<utils::ImageRgba::RGBA8888[]> &layer_buffer : _layerBuffers)
  {
    std::fill_n(layer_buffer.get(), _width * _height, utils::ImageRgba::RGBA8888(0, 0, 0));
  }

  std::fill_n(_frontBuffer.get(), _width * _height, utils::ImageRgba::RGBA8888(0, 0, 0));

  // Initialize ROM region
  std::copy(DEFAULT_PALETTE.begin(), DEFAULT_PALETTE.end(), _memory->getAddress(ROM_PAGE, DEFAULT_PALETTE_ROM_OFFSET));
  std::copy(DEFAULT_FONT_8x8.begin(), DEFAULT_FONT_8x8.end(), _memory->getAddress<std::array<WORD, 4>>(ROM_PAGE, DEFAULT_FONT_8x8_OFFSET));

  // Disable vblank interrupt
  out(IRQ_CONTROL, 0);

  // Initialize layers to initial state. All layers will be configured as 'standard 8x8 text mode',
  for (int layer = 0; layer < 4; layer++)
  {
    out(getLayerPort(layer, LAYER_TILE_MODE), TileMode(Tile::SIZE_8_PIXELS, TileWrapMode::NONE, TileWrapMode::NONE).bits);
    out(getLayerPort(layer, LAYER_NUM_TILES_X), DEFAULT_NUM_TILES_X);
    out(getLayerPort(layer, LAYER_NUM_TILES_Y), DEFAULT_NUM_TILES_Y);
    out(getLayerPort(layer, LAYER_CLEAR_MODE), ClearMode::CLEAR_BACKGROUND);
    out(getLayerPort(layer, LAYER_BACKGROUND_COLOR), 0);
    out(getLayerPort(layer, LAYER_FOREGROUND_COLOR), 1);
    out(getLayerPort(layer, LAYER_OFFSET_X), 0);
    out(getLayerPort(layer, LAYER_OFFSET_Y), 0);
    out(getLayerPort(layer, LAYER_TILE_MAP_PAGE), DEFAULT_TILE_MAP_PAGE);
    out(getLayerPort(layer, LAYER_TILE_MAP_OFFSET), DEFAULT_TILE_MAP_OFFSET);
    out(getLayerPort(layer, LAYER_COLOR_MAP_PAGE), DEFAULT_COLOR_MAP_PAGE);
    out(getLayerPort(layer, LAYER_COLOR_MAP_OFFSET), DEFAULT_COLOR_MAP_OFFSET);
    out(getLayerPort(layer, LAYER_TILE_ATLAS_PAGE), ROM_PAGE);
    out(getLayerPort(layer, LAYER_TILE_ATLAS_OFFSET), DEFAULT_FONT_8x8_OFFSET);
    out(getLayerPort(layer, LAYER_PALETTE_PAGE), ROM_PAGE);
    out(getLayerPort(layer, LAYER_PALETTE_OFFSET), DEFAULT_PALETTE_ROM_OFFSET);
  }

  // Enable only layer 0 will be enabled.
  out(getLayerPort(0, LAYER_MODE), LayerMode::MONOCHROME_TILE);
  out(getLayerPort(1, LAYER_MODE), LayerMode::NONE);
  out(getLayerPort(2, LAYER_MODE), LayerMode::NONE);
  out(getLayerPort(3, LAYER_MODE), LayerMode::NONE);
}

// Unsafe but fast 8x8 blit that does not perform any bounds checking or clipping. This is
// used for blitting sprites and tiles that are fully inside the view bounds.
void Gpu::unsafeBlt8x8(const Gpu::Pixel * const src, const int layer, const int x, const int y, const int scale) const
{
  const Gpu::Pixel *s = src;
  Gpu::Pixel *d = _layerBuffers[layer].get() + (y  * _width) + x;

  __m128i zero_128i = _mm_set1_epi32(0);

  switch (scale)
  {
    case 1:
    {
      // Unscaled blit without clipping is just a row-by-row copy
      for (int row = 0; row < 8; row++)
      {
        __m128i p0_3 = _mm_loadu_si128(reinterpret_cast<const __m128i *>(s));
        __m128i m0_3 = _mm_cmplt_epi32(p0_3, zero_128i);
        _mm_maskmoveu_si128(p0_3, m0_3, reinterpret_cast<char *>(d));

        __m128i p4_7 = _mm_loadu_si128(reinterpret_cast<const __m128i *>(s + 4));
        __m128i m4_7 = _mm_cmplt_epi32(p4_7, zero_128i);
        _mm_maskmoveu_si128(p4_7, m4_7, reinterpret_cast<char *>(d + 4));

        s += 8;
        d += _width;
      }

      break;
    }

    case 2:
    {
      __m128i *d_128 = reinterpret_cast<__m128i *>(d);

      for (int row = 0; row < 8; row++)
      {
        // Each row is duplicated twice. For each row, use SSE to expand 2 source pixels to 4 destination
        // pixels. The inner loop is unrolled, to draw 4x2 source pixels to 16 destination pixels.
        for (int i = 0; i < 2; i++)
        {
          __m128i p0 = _mm_set_epi32(s[1].rgba, s[1].rgba, s[0].rgba, s[0].rgba);
          __m128i m0 = _mm_cmplt_epi32(p0, zero_128i);
          _mm_maskmoveu_si128(p0, m0, reinterpret_cast<char *>(d_128 + 0));

          __m128i p1 = _mm_set_epi32(s[3].rgba, s[3].rgba, s[2].rgba, s[2].rgba);
          __m128i m1 = _mm_cmplt_epi32(p1, zero_128i);
          _mm_maskmoveu_si128(p1, m1, reinterpret_cast<char *>(d_128 + 1));

          __m128i p2 = _mm_set_epi32(s[5].rgba, s[5].rgba, s[4].rgba, s[4].rgba);
          __m128i m2 = _mm_cmplt_epi32(p2, zero_128i);
          _mm_maskmoveu_si128(p2, m2, reinterpret_cast<char *>(d_128 + 2));

          __m128i p3 = _mm_set_epi32(s[7].rgba, s[7].rgba, s[6].rgba, s[6].rgba);
          __m128i m3 = _mm_cmplt_epi32(p3, zero_128i);
          _mm_maskmoveu_si128(p3, m3, reinterpret_cast<char *>(d_128 + 3));

          d_128 += _width / 4;
        }

        s += 8;
      }

      break;
    }

    case 4:
    {
      __m128i *d_128 = reinterpret_cast<__m128i *>(d);

      for (int row = 0; row < 8; row++)
      {
        // Each row is duplicated 4 times.
        for (int i = 0; i < 4; i++)
        {
          for (int pixel = 0; pixel < 8; pixel++)
          {
            // Use SSE to expand 1 source pixel to 4 destination pixels, calculate alpha store mask and write
            __m128i p = _mm_set1_epi32(s[pixel].rgba);
            __m128i m = _mm_cmplt_epi32(p, zero_128i);
            _mm_maskmoveu_si128(p, m, reinterpret_cast<char *>(d_128 + pixel));
          }

          d_128 += _width / 4;
        }

        s += 8;
      }

      break;
    }

    default:
      throw std::logic_error(fmt::format("Invalid blit scale factor: {0}", scale));
  }
}

// Safe but slower 8x8 blit that includes bounds checking and clipping against the view size
void Gpu::safeBlt8x8(const Gpu::Pixel * const src, const int layer, const int x, const int y, const int scale) const
{
  constexpr int TILE_SIZE = 8;

  const bool outside = ((x + TILE_SIZE*scale) <= 0) || ((y + TILE_SIZE*scale) <= 0) || (x >= _width) || (y >= _height);

  if (outside)
  {
    return;
  }

  // Calculate number of destination rows/cols to skip, and from that how many to draw
  const int skip_rows = (y >= 0 ? 0 : -y);
  const int num_rows = std::min(_height - y, (TILE_SIZE * scale) - skip_rows);

  const int skip_cols = (x >= 0 ? 0 : -x);
  const int num_cols = std::min(_width - x, (TILE_SIZE * scale) - skip_cols);

  Gpu::Pixel *d = _layerBuffers[layer].get() + (std::max(0, y)  * _width) + std::max(0, x);

  switch (scale)
  {
    case 1:
    {
      // Unscaled blit with clipping is fairly straightforward, we can just skip the calculated
      // number of rows and (for each row) columns, as they map 1-to-1 to source pixels.
      const Gpu::Pixel *s = src + TILE_SIZE*skip_rows + skip_cols;

      for (int i = 0; i < num_rows; i++)
      {
        for (int j = 0; j < num_cols; j++)
        {
          const Pixel &p = s[j];

          if (p.a())
          {
            d[j] = p;
          }
        }

        d += _width;
        s += 8;
      }

      break;
    }

    case 2:
    case 4:
    {
      // 2x or 4x scaled tile. This code path is currently the primary 'slow fallback' path. To
      // keep things simple, we loop through the destination pixels, and for each pixel covered
      // calculate the corresponding source pixel. This means multiple shifts, adds and a
      // multiplication per destination pixel. This could be optimized significantly by iterating
      // source pixels instead, then duplicating and masking off-screen destinations using SSE.
      const int shift = (scale == 2 ? 1 : 2);

      for (int i = 0; i < num_rows; i++)
      {
        for (int j = 0; j < num_cols; j++)
        {
          const Pixel &p = src[(((skip_rows + i) >> shift) * 8) + ((skip_cols + j) >> shift)];

          if (p.a())
          {
            *(d + j) = p;
          }
        }

        d += _width;
      }
    }

    default:
      break;
  }
}

// Blit 8x8 block of pixel data to pixel location in backing store for specified layer
void Gpu::blt8x8(const Pixel * const src, const int layer, const int x, const int y, const int scale) const
{
  const int blt_dst_size = 8 * scale;

  const bool inside = (x >= 0) && (y >= 0) && ((x + blt_dst_size) <= _width) && ((y + blt_dst_size) <= _height);

  if (inside)
  {
    // Blit destination fully inside frame boundaries, no clipping necessary
    unsafeBlt8x8(src, layer, x, y, scale);
  }
  else
  {
    // Fallback path for blits that straddle the frame boundaries
    safeBlt8x8(src, layer, x, y, scale);
  }
}

// Render single 8x8 monochrome tile
void Gpu::renderMonochromeTile(
  const int layer,
  const int ox, const int oy,
  const unsigned int scale,
  const BYTE * const tileData,
  const utils::ImageRgba::RGBA8888 &fg,
  const utils::ImageRgba::RGBA8888 &bg,
  const bool rotate,
  const bool flipX,
  const bool flipY) const
{
  std::array<Pixel, 64> tile_pixels;

  Pixel *dst = tile_pixels.data();

  for (int i = 0; i < 8; i++)
  {
    const BYTE tile_row = tileData[i];

    for (int j = 0; j < 8; j++)
    {
      const utils::ImageRgba::RGBA8888 &color = ((tile_row & (1 << (7 - j))) != 0 ? fg : bg);

      *dst++ = color;
    }
  }

  if (rotate || flipX || flipY)
  {
    transform_8x8(tile_pixels, rotate, flipX, flipY);
  }

  blt8x8(tile_pixels.data(), layer, ox, oy, scale);
}

// Render single 8x8 multicolor tile
void Gpu::renderMulticolorTile(
  const int layer,
  const int ox, const int oy,
  const unsigned int scale,
  const BYTE * const tileData,
  const unsigned short * const paletteData,
  const bool rotate,
  const bool flipX,
  const bool flipY) const
{
  std::array<Pixel, 64> tile_pixels;

  Pixel *dst = tile_pixels.data();

  for (int i = 0; i < 8; i++)
  {
    for (int j = 0; j < 8; j++)
    {
      const BYTE palette_index = tileData[i*8 + j];
      const auto color = RGBA8888(paletteData[palette_index]);

      *dst++ = color;
    }
  }

  if (rotate || flipX || flipY)
  {
    transform_8x8(tile_pixels, rotate, flipX, flipY);
  }

  blt8x8(tile_pixels.data(), layer, ox, oy, scale);
}

// Render tile layer (monochrome only, for the moment)
void Gpu::renderTileLayer(const int layer) const
{
  const WORD layer_mode = in(getLayerPort(layer, LAYER_MODE));

  if ((layer_mode != LayerMode::MONOCHROME_TILE) && (layer_mode != LayerMode::COLOR_MAPPED_TILE) && (layer_mode != LayerMode::INDEXED_TILE))
  {
    throw std::runtime_error(fmt::format("Invalid layer mode {0} for tile layer rendering", layer_mode));
  }

  const TileMode tile_mode = in(getLayerPort(layer, LAYER_TILE_MODE));

  const int tile_subtiles = 1 << tile_mode.tileSize;
  const int tile_scale_factor = 1 << tile_mode.scale;
  const int tile_size = tile_subtiles * Gpu::SUBTILE_SIZE;
  const int tile_render_size = tile_size * tile_scale_factor;

  const int screen_tiles_x = (_width / tile_render_size) + (_width % tile_render_size != 0 ? 1 : 0);
  const int screen_tiles_y = (_height / tile_render_size) + (_height % tile_render_size != 0 ? 1 : 0);

  const int map_tiles_x = in(getLayerPort(layer, LAYER_NUM_TILES_X));
  const int map_tiles_y = in(getLayerPort(layer, LAYER_NUM_TILES_Y));

  const int map_pixels_x = map_tiles_x * tile_size;
  const int map_pixels_y = map_tiles_y * tile_size;

  const auto tile_atlas = _memory->getAddress<BYTE>(in(getLayerPort(layer, LAYER_TILE_ATLAS_PAGE)), in(getLayerPort(layer, LAYER_TILE_ATLAS_OFFSET)));
  const auto tile_map = _memory->getAddress<Tile>(in(getLayerPort(layer, LAYER_TILE_MAP_PAGE)), in(getLayerPort(layer, LAYER_TILE_MAP_OFFSET)));

  const auto color_map = _memory->getAddress<WORD>(in(getLayerPort(layer, LAYER_COLOR_MAP_PAGE)), in(getLayerPort(layer, LAYER_COLOR_MAP_OFFSET)));
  const auto palette = _memory->getAddress<WORD>(in(getLayerPort(layer, LAYER_PALETTE_PAGE)), in(getLayerPort(layer, LAYER_PALETTE_OFFSET)));

  const WORD layer_bg_color_index = in(getLayerPort(layer, LAYER_BACKGROUND_COLOR));
  const WORD layer_fg_color_index = in(getLayerPort(layer, LAYER_FOREGROUND_COLOR));

  const utils::ImageRgba::RGBA8888 layer_bg_color = RGBA8888(palette[layer_bg_color_index]);
  const utils::ImageRgba::RGBA8888 layer_fg_color = RGBA8888(palette[layer_fg_color_index]);

  // Calculate tile grid offset and size to render, depending on the layer offset, map size, and crop/wrap mode
  const auto layer_offset_x = in<int>(getLayerPort(layer, LAYER_OFFSET_X));
  const auto layer_offset_y = in<int>(getLayerPort(layer, LAYER_OFFSET_Y));

  const bool have_offset_x = (layer_offset_x != 0);
  const bool have_offset_y = (layer_offset_y != 0);

  const bool periodic_x = (tile_mode.wrapX == TileWrapMode::PERIODIC);
  const bool periodic_y = (tile_mode.wrapY == TileWrapMode::PERIODIC);

  // To simplify the rest of the calculations, make sure that in case of a periodic tile layer
  // we use a positive offset. This is possible since we can just add any multiple of the tile
  // map size to the offset when we consider it periodic
  const int periodic_offset_x = (have_offset_x ? (layer_offset_x % map_pixels_x) : (layer_offset_x % map_pixels_x) + map_pixels_x);
  const int periodic_offset_y = (have_offset_y ? (layer_offset_y % map_pixels_y) : (layer_offset_y % map_pixels_y) + map_pixels_y);

  // Calculate origin of render coordinate system (in pixels) and tile maps (in tile indexes). In case of
  // a periodic layer with a non-zero layer offset, move the render origin by 1 tile left & up and render
  // two extra rows and columns of tiles, to ensure partially offscreen tiles are rendered.
  const auto render_origin_x = (periodic_x && have_offset_x ? (periodic_offset_x % tile_render_size) - tile_render_size : layer_offset_x);
  const auto render_origin_y = (periodic_y && have_offset_y ? (periodic_offset_y % tile_render_size) - tile_render_size : layer_offset_y);

  const int tile_origin_i = (periodic_y ? ((render_origin_y - periodic_offset_y) / tile_size) : 0);
  const int tile_origin_j = (periodic_x ? ((render_origin_x - periodic_offset_x) / tile_size) : 0);

  const int grid_tiles_x = (periodic_x ? screen_tiles_x + 2 : std::min(screen_tiles_x, map_tiles_x));
  const int grid_tiles_y = (periodic_y ? screen_tiles_y + 2 : std::min(screen_tiles_y, map_tiles_y));

  // Iterate and render all map tiles covering the screen area
  for (int i = 0; i < grid_tiles_y; i++)
  {
    for (int j = 0; j < grid_tiles_x; j++)
    {
      const int tile_i = ((tile_origin_i + i) + map_tiles_y) % map_tiles_y;
      const int tile_j = ((tile_origin_j + j) + map_tiles_x) % map_tiles_x;

      const int tile_x = render_origin_x + (j * tile_render_size);
      const int tile_y = render_origin_y + (i * tile_render_size);

      const Tile &tile = tile_map[tile_i * map_tiles_x + tile_j];

      if (tile.skip)
      {
        continue;
      }

      // Initialize generator range for subtile offsets, used for rendering tiles composed of multiple subtiles
      const auto subtile_offsets =
        views::cartesian_product(views::iota(0, tile_subtiles), views::iota(0, tile_subtiles)) |
        views::transform(GenerateSubtileOffsets(tile_subtiles, tile_scale_factor, tile.rotate, tile.flipX, tile.flipY));

      if ((layer_mode == LayerMode::MONOCHROME_TILE) || (layer_mode == LayerMode::COLOR_MAPPED_TILE))
      {
        // Monochrome tile using either the layer background/foreground color, or the foreground/background
        // color indicated by the colormap entry for the tile.
        utils::ImageRgba::RGBA8888 fg, bg;

        if (layer_mode == LayerMode::MONOCHROME_TILE)
        {
          bg = layer_bg_color;
          fg = layer_fg_color;
        }
        else
        {
          const WORD color_bits = color_map[i * map_tiles_x + j];

          bg = RGBA8888(palette[color_bits >> 8]);
          fg = RGBA8888(palette[color_bits & 0xFF]);
        }

        if (tile.invert)
        {
          std::swap(fg, bg);
        }

        for (const auto &[subtile_index, subtile_offset] : views::enumerate(subtile_offsets))
        {
          const BYTE *subtile_data = &tile_atlas[(tile.tile + subtile_index) * 8];

          renderMonochromeTile(
            layer,
            tile_x + subtile_offset.first, tile_y + subtile_offset.second,
            tile_scale_factor, subtile_data, fg, bg,
            tile.rotate, tile.flipX, tile.flipY);
        }
      }
      else
      {
        // Indexed-color tile
        for (const auto &[subtile_index, subtile_offset] : views::enumerate(subtile_offsets))
        {
          const BYTE *subtile_data = &tile_atlas[(tile.tile + subtile_index) * 64];

          renderMulticolorTile(
            layer,
            tile_x + subtile_offset.first, tile_y + subtile_offset.second,
            tile_scale_factor, subtile_data, palette,
            tile.rotate, tile.flipX, tile.flipY);
        }
      }
    }
  }
}

// Render a sprite layer
void Gpu::renderSpriteLayer(const int layer) const
{
  constexpr int TILE_SIZE = 8;
  constexpr int TILE_STRIDE = TILE_SIZE*TILE_SIZE;

  const auto palette_data = _memory->getAddress<const WORD>(in(getLayerPort(layer, LAYER_PALETTE_PAGE)), in(getLayerPort(layer, LAYER_PALETTE_OFFSET)));

  const auto sprite_data = _memory->getAddress<const BYTE>(in(getLayerPort(layer, LAYER_TILE_ATLAS_PAGE)), in(getLayerPort(layer, LAYER_TILE_ATLAS_OFFSET)));
  const auto sprite_attributes = _memory->getAddress<const Sprite>(in(SPRITE_ATTRIBUTES_PAGE), in(SPRITE_ATTRIBUTES_OFFSET));

  const BoundingBox screen_bbox = { 0, 0, _width, _height };

  std::for_each(sprite_attributes, sprite_attributes + MAX_SPRITES, [this, layer, palette_data, sprite_data, &screen_bbox](const Sprite &sprite)
  {
    if (sprite.control.enable)
    {
      // Sprites can be rectangular, having a different row x column (i, j) size. If they
      // are rotated, the sprite display size (x, y) will be transposed
      int ni = 1 << sprite.tiles.height;
      int nj = 1 << sprite.tiles.width;

      int nx = (sprite.control.rotate ? ni : nj);
      int ny = (sprite.control.rotate ? nj : ni);

      const int scale = 1 << sprite.control.scale;

      const BoundingBox sprite_bbox = { sprite.x, sprite.y, nx*TILE_SIZE*scale, ny*TILE_SIZE*scale };

      // Setup generator range for sprite subtile render offsets for the sprite rotation & flip settings
      const auto subtile_offsets =
        views::cartesian_product(views::iota(0, ni), views::iota(0, nj)) |
        views::transform(GenerateSubtileOffsets({ nx, ny }, scale, sprite.control.rotate, sprite.control.flipX, sprite.control.flipY));

      // Very quick & dirty handling of sprite wrapping. If the sprite straddles the screen boundaries and it's
      // configured to wrap in X and/or Y, we just render it multiple times, at appropriate X/Y screen pitch
      // offsets. This is not super-nice but not super crappy either, as it allows keeping the low-level
      // sprite tile drawing code simple and wrapped sprites should usually be considered a rare corner case.
      const bool inside = screen_bbox.contains(sprite_bbox);

      const bool wrap_x = !inside && sprite.control.wrapX;
      const bool wrap_y = !inside && sprite.control.wrapY;

      for (int instance_y = (wrap_y ? -1 : 0); instance_y <= (wrap_y ? 1 : 0); instance_y++)
      {
        for (int instance_x = (wrap_x ? -1 : 0); instance_x <= (wrap_x ? 1 : 0); instance_x++)
        {
          const BoundingBox instance_bbox = sprite_bbox.translated(instance_x * _width, instance_y * _height);

          if (instance_bbox.overlaps(screen_bbox))
          {
            for (const auto &[subtile_index, subtile_offset] : views::enumerate(subtile_offsets))
            {
              const BYTE * const tile_data = sprite_data + (sprite.tiles.index + subtile_index) * TILE_STRIDE;

              renderMulticolorTile(
                layer,
                instance_bbox.minX + subtile_offset.first, instance_bbox.minY + subtile_offset.second,
                scale, tile_data, palette_data,
                sprite.control.rotate, sprite.control.flipX, sprite.control.flipY);
            }
          }
        }
      }
    }
  });
}

// Composit active layers. This will alpha-blend all enabled layers to the framebuffer
void Gpu::compositLayers() const
{
  // Gather backing stores for all active layers and count them
//  std::array<const __m128i *, 4> src = {};

  const __m128i *src[4] = { nullptr, nullptr, nullptr, nullptr };

  for (int n = 0, layer = 0; layer < 4; layer++)
  {
    if (in<LayerMode::Enum>(getLayerPort(layer, LAYER_MODE)) != LayerMode::NONE)
    {
      src[n++] = reinterpret_cast<const __m128i *>(_layerBuffers[layer].get());
    }
  }

  const int num_active_layers = std::count_if(src, src + 4, [](const __m128i *p) { return (p != nullptr); });

  // Compositing the active layers is a very straightforward blend operation using the
  // pixel alpha channel to mask out transparent pixels. It is implemented as follows:
  // for every enabled layer, load 4 pixels at once, create an alpha mask from them,
  // then blend the non-transparent pixels into the destination pixel using the alpha
  // mask. To prevent per-pixel branches, the blend 'kernel' is expanded 4 times for
  // each possible number of active layers.
  const auto dst = reinterpret_cast<__m128i *>(_frontBuffer.get());

  const __m128i zero_128i = _mm_set1_epi32(0);

  switch (num_active_layers)
  {
    case 1:
    {
      for (int n = 0; n < (_width * _height) / 4; n++)
      {
        __m128i p = _mm_loadu_si128(dst + n);

        p = _mm_blendv_epi8(p, src[0][n], _mm_cmplt_epi32(src[0][n], zero_128i));

        _mm_storeu_si128(dst + n, p);
      }

      break;
    }

    case 2:
    {
      for (int n = 0; n < (_width * _height) / 4; n++)
      {
        __m128i p = _mm_loadu_si128(dst);

        p = _mm_blendv_epi8(p, src[0][n], _mm_cmplt_epi32(src[0][n], zero_128i));
        p = _mm_blendv_epi8(p, src[1][n], _mm_cmplt_epi32(src[1][n], zero_128i));

        _mm_storeu_si128(dst, p);
      }

      break;
    }

    case 3:
    {
      for (int n = 0; n < (_width * _height) / 4; n++)
      {
        __m128i p = _mm_loadu_si128(dst + n);

        p = _mm_blendv_epi8(p, src[0][n], _mm_cmplt_epi32(src[0][n], zero_128i));
        p = _mm_blendv_epi8(p, src[1][n], _mm_cmplt_epi32(src[1][n], zero_128i));
        p = _mm_blendv_epi8(p, src[2][n], _mm_cmplt_epi32(src[2][n], zero_128i));

        _mm_storeu_si128(dst + n, p);
      }

      break;
    }

    case 4:
    {
      for (int n = 0; n < (_width * _height) / 4; n++)
      {
        __m128i p = _mm_loadu_si128(dst + n);

        p = _mm_blendv_epi8(p, src[0][n], _mm_cmplt_epi32(src[0][n], zero_128i));
        p = _mm_blendv_epi8(p, src[1][n], _mm_cmplt_epi32(src[1][n], zero_128i));
        p = _mm_blendv_epi8(p, src[2][n], _mm_cmplt_epi32(src[2][n], zero_128i));
        p = _mm_blendv_epi8(p, src[3][n], _mm_cmplt_epi32(src[3][n], zero_128i));

        _mm_storeu_si128(dst + n, p);
      }

      break;
    }

    default:
      break;
  }
}

// Render single frame
void Gpu::frame()
{
  // Lambda function that renders a single layer. This will be called concurrently for all 4 layers.
  const auto render_layer = [this](int layer)
  {
    const auto layer_mode = in<LayerMode::Enum>(getLayerPort(layer, LAYER_MODE));

    if (layer_mode != LayerMode::NONE)
    {
      const WORD * const layer_palette = _memory->getAddress(in(getLayerPort(layer, LAYER_PALETTE_PAGE)), in(getLayerPort(layer, LAYER_PALETTE_OFFSET)));

      const WORD bg_index = in(getLayerPort(layer, LAYER_BACKGROUND_COLOR));
      const auto bg = RGBA8888(*(layer_palette + bg_index));

      // Clear background
      if (in(getLayerPort(layer, LAYER_CLEAR_MODE)) == ClearMode::CLEAR_BACKGROUND)
      {
        std::fill_n(_layerBuffers[layer].get(), (_width * _height), bg);
      }

      switch (layer_mode)
      {
        case LayerMode::MONOCHROME_TILE:
        case LayerMode::COLOR_MAPPED_TILE:
        case LayerMode::INDEXED_TILE:
          renderTileLayer(layer);
          break;
        case LayerMode::SPRITE:
          renderSpriteLayer(layer);
          break;
        case LayerMode::NONE:
          break;
      }
    }
  };

  // Render 4 layers in parallel
  _taskArena->execute([this, render_layer] {
    const high_resolution_clock::time_point start = high_resolution_clock::now();

    tbb::parallel_for(0, 4, render_layer);

    _renderTime = duration_cast<microseconds>(high_resolution_clock::now() - start);
  });

  // Composit layers (alpha blend all enabled layers to frame buffer)
  compositLayers();

  // Scanout complete: generate v-blank interrupt if enabled
  if (in(IRQ_CONTROL) & IRQ_CONTROL_ENABLE_VBLANK)
  {
    out(IRQ_CONTROL, in(IRQ_CONTROL) | IRQ_CONTROL_VBLANK_PENDING);
  }
}

// Return last rendered frame buffer data
Frame Gpu::getFrame() const
{
  /// \todo Right now we just copy out the front buffer, but this copy can be avoided
  ///   if we actually implement double-buffering in the future.
  utils::ImageRgba image(_width, _height);

  std::copy(_frontBuffer.get(), _frontBuffer.get() + _width*_height, image.data.begin());

  return { image };
}

// Return frame time for last rendered frame, in microseconds
std::chrono::microseconds Gpu::getRenderTime() const
{
  return _renderTime;
}

// Return pending interrupts. For the Gpu device, there is only one IRQ line for the vblank interrupt
WORD Gpu::pendingInterrupts() const
{
  return ((in(IRQ_CONTROL) & IRQ_CONTROL_VBLANK_PENDING) != 0 ? (1 << _vblankIrq) : 0);
}

}
