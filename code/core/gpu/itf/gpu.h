/// Header file for the main interface classes in the core::gpu namespace, which together implement the maborosh-16 GPU.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#pragma once

#include "core/common/itf/memory_map.h"

#include "core/peripheral/itf/device.h"

#include "utils/itf/utils.h"

#include <tbb/task_arena.h>

#include <chrono>

namespace core::gpu {

/// Layer mode
namespace LayerMode
{
  enum Enum
  {
    NONE               = 0,        ///< Disable layer
    MONOCHROME_TILE    = 1,        ///< Monochrome (1 bit per pixel) tile layer with fixed foreground & background colors
    COLOR_MAPPED_TILE  = 2,        ///< Monochrome (1 bit per pixel) tile layer with color map
    INDEXED_TILE       = 3,        ///< 8-bit indexed-color tile layer
    SPRITE             = 4,        ///< Sprite layer
  };
};

/// Background mode
namespace ClearMode
{
  enum Enum
  {
    NONE              = 0,         ///< No background (do nothing)
    CLEAR_BACKGROUND  = 1,         ///< Clear background with layer background color
  };
};

/// Tile wrap mode
namespace TileWrapMode
{
  enum Enum
  {
    NONE              = 0,         ///< Do not draw anything for pixels outside tile map
    PERIODIC          = 1,         ///< Periodic extension of the tile map to fill the whole screen
  };
};

/// Simple pixel bounding box type.
///
/// Pixel bounding boxes are inclusive, ie: a bounding box with origin (4, 2) and size (3, 2)
/// will cover the pixel range ([4:7], [2:4]).
struct BoundingBox
{
  int minX;     ///< Bounding box upper-left corner X-coordinate
  int minY;     ///< Bounding box upper-left corner Y-coordinate
  int sizeX;    ///< Bounding box width
  int sizeY;    ///< Bounding box height

  /// \return bounding box bottom-right X-coordinate
  int getMaxX() const { return minX + sizeX; };

  /// \return bounding box bottom-right X-coordinate
  int getMaxY() const { return minY + sizeY; };

  /// Check if the passed bounding \p box is fully inside (non-intersecting, but potentially touching) this bounding box.
  ///
  /// \param box bounding box to check
  /// \return whether the passed \p box is fully inside this bounding box
  bool contains(const BoundingBox &box) const
  {
    return (box.minX >= minX) && (box.minY >= minY) && (box.getMaxX() <= getMaxX()) && (box.getMaxY() <= getMaxY());
  }

  /// Check if this bounding box contains or intersects the passed bounding \p box.
  ///
  /// \param box bounding box to check
  /// \return whether this bounding box contains or intersects the passed bounding box
  bool overlaps(const BoundingBox &box) const
  {
    return (box.minX <= getMaxX()) && (box.minY <= getMaxY()) && (box.getMaxX() >= minX) && (box.getMaxY() >= minY);
  }

  /// Return translated version of this bounding box
  ///
  /// \param tx translation in X
  /// \param ty translation in Y
  /// \return this bounding box, translated by the passed translation
  BoundingBox translated(const int tx, const int ty) const
  {
    return { minX + tx, minY + ty, sizeX, sizeY };
  };
};


/// Tile properties (1 machine word per tile).
union Tile
{
  // Constants for tile attributes/flags
  static constexpr BYTE SIZE_8_PIXELS = 0;         ///< Single-tile
  static constexpr BYTE SIZE_16_PIXELS = 1;        ///< 2x2 subtiles
  static constexpr BYTE SIZE_32_PIXELS = 2;        ///< 4x4 subtiles

  static constexpr BYTE SCALE_1X = 0;              ///< No scaling
  static constexpr BYTE SCALE_2X = 1;              ///< 2x2 pixel size
  static constexpr BYTE SCALE_4X = 2;              ///< 4x4 pixel size

  /// Initialize tile structure.
  ///
  /// \param tile tile number
  /// \param skip skip tile (do not draw)
  Tile(const WORD tile, const bool skip)
  :
    bits(0)
  {
    this->tile = tile;
    this->skip = skip;
  }

  BitRange<WORD, WORD, 0, 10> tile;                ///< Tile number
  BitRange<WORD, bool, 10, 1> skip;                ///< Skip tile (do not draw)
  BitRange<WORD, bool, 11, 1> invert;              ///< Invert tile colors (monochrome tiles only)
  BitRange<WORD, bool, 12, 1> rotate;              ///< Rotate tile 90 degrees clockwise
  BitRange<WORD, bool, 13, 1> flipX;               ///< Flip tile horizontally
  BitRange<WORD, bool, 14, 1> flipY;               ///< Flip tile vertically
  BitRange<WORD, BYTE, 15, 1> reserved;            ///< Reserved for future use
  WORD bits;                                       ///< All tile bits
} __attribute__((packed));


/// Tile mode
union TileMode
{
  /// Initialize from tile mode bits
  ///
  /// \param bits tile mode bits
  TileMode(const WORD bits)
  :
    bits(bits)
  {
  }

  /// Initialize tile mode structure
  ///
  /// \param tileSizeX Tile size, 0 = 8 pixels, 1 = 16 pixels, 2 = 32 pixels
  /// \param wrapX tile wrap mode in x direction
  /// \param wrapY tile wrap mode in y direction
  /// \param scale tile scaling
  TileMode(
    const BYTE tileSize,
    const TileWrapMode::Enum wrapX = TileWrapMode::NONE,
    const TileWrapMode::Enum wrapY = TileWrapMode::NONE,
    const BYTE scale = Tile::SCALE_1X)
  :
    bits(0)
  {
    this->tileSize = tileSize;
    this->wrapX = wrapX;
    this->wrapY = wrapY;
    this->scale = scale;
  }

  BitRange<WORD, BYTE, 0, 3> tileSize;             ///< Tile size in bits, 0 = 8 pixels, 1 = 16 pixels, 2 = 32 pixels
  BitRange<WORD, TileWrapMode::Enum, 3, 2> wrapX;  ///< Tile wrap mode in x direction
  BitRange<WORD, TileWrapMode::Enum, 5, 2> wrapY;  ///< Tile wrap mode in y direction
  BitRange<WORD, BYTE, 7, 2> scale;                ///< Tile scaling  0 = 1x, 1 = 2x, 2 = 4x
  BitRange<WORD, BYTE, 9, 7> reserved;             ///< Reserved for future use
  WORD bits;                                       ///< All tile mode bits
} __attribute__((packed));

/// Sprite attributes (4 machine words per sprite).
struct Sprite
{
  // Constants for sprite attribute values/flags
  static constexpr BYTE SIZE_8_PIXELS = 0;         ///< Single-tile sprite
  static constexpr BYTE SIZE_16_PIXELS = 1;        ///< 2x2 subtile sprite
  static constexpr BYTE SIZE_32_PIXELS = 2;        ///< 4x4 subtile sprite

  static constexpr BYTE SCALE_1X = 0;              ///< Render without scaling
  static constexpr BYTE SCALE_2X = 1;              ///< 2x2 pixel scaling
  static constexpr BYTE SCALE_4X = 2;              ///< 4x4 pixel scaling

  /// Structure type to hold sprite control bits
  union Control
  {
    BitRange<WORD, bool, 0, 1> enable;             ///< Enable sprite
    BitRange<WORD, BYTE, 1, 2> scale;              ///< Sprite scale: 0 = 1x, 1 = 2x, 2 = 4x
    BitRange<WORD, bool, 3, 1> wrapX;              ///< Wrap offscreen sprite pixels in X-direction instead of clipping them
    BitRange<WORD, bool, 4, 1> wrapY;              ///< Wrap offscreen sprite pixels in Y-direction instead of clipping them
    BitRange<WORD, bool, 5, 1> rotate;             ///< Rotate sprite by 90 degrees clockwise
    BitRange<WORD, bool, 6, 1> flipX;              ///< Flip sprite in x-direction
    BitRange<WORD, bool, 7, 1> flipY;              ///< Flip sprite in y-direction
    BitRange<WORD, WORD, 8, 8> reserved;           ///< Reserved for future use
    WORD bits;                                     ///< All sprite control bits
  } __attribute__((packed));

  /// Structure type to hold tile bits
  union Tiles
  {
    BitRange<WORD, WORD, 0, 10> index;             ///< Starting index of 8x8 tile
    BitRange<WORD, BYTE, 10, 2> width;             ///< Sprite width: 0 = 1 tile, 1 = 2 tiles, 2 = 4 tiles
    BitRange<WORD, BYTE, 12, 2> height;            ///< Sprite height: 0 = 1 tile, 1 = 2 tiles, 2 = 4 tiles
    BitRange<WORD, WORD, 14, 2> reserved;          ///< Reserved for future use
    WORD bits;                                     ///< All tile bits
  } __attribute__((packed));

  SWORD x;                                         ///< Sprite x-position in pixels
  SWORD y;                                         ///< Sprite y-position in pixels

  Control control;                                 ///< Sprite control bits
  Tiles tiles;                                     ///< Sprite tile bits
} __attribute__((packed));

/// Structure to hold information about a completed frame.
///
/// This currently just stores a 32-bit RGBA image, but may be extended with
/// additional frame data later (e.g. things like frame sequence number, render
/// time, these kinds of things)
struct Frame
{
  utils::ImageRgba image;                          ///< Frame image
};

/// Implements the maboroshi-16 GPU.
///
/// In its current state, the GPU implementation is pretty much useless. The
/// GPU only exposes some I/O ports to change the background color for single
/// graphics layer, and only supports a single layer type (monochrome tile mode).
///
/// \todo Document the GPU capabilities once they evolve to something useful.
class Gpu : public peripheral::Device
{
  public:
    /// Pixel format for the frame buffer and layer backing stores. Note that though this
    /// type is named 'RGBA8888', the memory layout is actually ARGB (ie: alpha channel in MSB)
    using Pixel = utils::ImageRgba::RGBA8888;

    //
    // GPU ports
    //

    static constexpr WORD IRQ_CONTROL                 = 0x0001;

    // Sprite attribute memory page and offset in VRAM
    static constexpr WORD SPRITE_ATTRIBUTES_PAGE      = 0x0060;
    static constexpr WORD SPRITE_ATTRIBUTES_OFFSET    = 0x0061;

    // Start of port range for all layer ports
    static constexpr WORD LAYER_PORTS_BASE            = 0x0080;
    // Number of ports per layer
    static constexpr WORD LAYER_NUM_PORTS             = 0x0020;

    // Base port for each individual layer
    static constexpr std::array<WORD, 4> LAYER_N_PORTS = {
      LAYER_PORTS_BASE,
      LAYER_PORTS_BASE + 1 * LAYER_NUM_PORTS,
      LAYER_PORTS_BASE + 2 * LAYER_NUM_PORTS,
      LAYER_PORTS_BASE + 3 * LAYER_NUM_PORTS
    };

    // Offsets relative to base port for each layer
    static constexpr WORD LAYER_MODE                  = 0x0000;
    static constexpr WORD LAYER_CLEAR_MODE            = 0x0001;
    static constexpr WORD LAYER_BACKGROUND_COLOR      = 0x0002;
    static constexpr WORD LAYER_FOREGROUND_COLOR      = 0x0003;
    static constexpr WORD LAYER_OFFSET_X              = 0x0004;
    static constexpr WORD LAYER_OFFSET_Y              = 0x0005;
    static constexpr WORD LAYER_TILE_MAP_PAGE         = 0x0008;
    static constexpr WORD LAYER_TILE_MAP_OFFSET       = 0x0009;
    static constexpr WORD LAYER_TILE_MODE             = 0x000A;
    static constexpr WORD LAYER_NUM_TILES_X           = 0x000B;
    static constexpr WORD LAYER_NUM_TILES_Y           = 0x000C;
    static constexpr WORD LAYER_TILE_ATLAS_PAGE       = 0x001A;
    static constexpr WORD LAYER_TILE_ATLAS_OFFSET     = 0x001B;
    static constexpr WORD LAYER_COLOR_MAP_PAGE        = 0x001C;
    static constexpr WORD LAYER_COLOR_MAP_OFFSET      = 0x001D;
    static constexpr WORD LAYER_PALETTE_PAGE          = 0x001E;
    static constexpr WORD LAYER_PALETTE_OFFSET        = 0x001F;

    //
    // GPU constants
    //

    /// ROM page
    static constexpr BYTE ROM_PAGE = 0;

    /// Number of 64KB VRAM pages
    static constexpr BYTE NUM_VRAM_PAGES = 4;
    /// VRAM size, in machine words
    static constexpr DWORD VRAM_SIZE = NUM_VRAM_PAGES << 16;

    /// Maximum number of sprites per layer
    static constexpr BYTE MAX_SPRITES = 64;

    /// Subtile size in pixels
    static constexpr BYTE SUBTILE_SIZE = 8;

    //
    // GPU register flag bits
    //

    /// IRQ_CONTROL enable vblank interrupt flag
    static constexpr WORD IRQ_CONTROL_ENABLE_VBLANK         = 0x0100;
    /// IRQ_CONTROL vblank interrupt pending flag
    static constexpr WORD IRQ_CONTROL_VBLANK_PENDING        = 0x0001;

    //
    // Initial GPU state defaults
    //

    /// Default palette offset in ROM
    static constexpr WORD DEFAULT_PALETTE_ROM_OFFSET        = 0x0100;
    /// Default 8x8 font offset in ROM
    static constexpr WORD DEFAULT_FONT_8x8_OFFSET           = 0x0200;

    /// Default sprite attribute page in VRAM, after GPU reset
    static constexpr WORD DEFAULT_SPRITE_ATTRIBUTES_PAGE    = 0x01;
    /// Default sprite attribute offset in VRAM, after GPU reset
    static constexpr WORD DEFAULT_SPRITE_ATTRIBUTES_OFFSET  = 0x0000;

    /// Default layer 0 tile map page in VRAM, after GPU reset
    static constexpr WORD DEFAULT_TILE_MAP_PAGE             = 0x01;
    /// Default layer 0 tile map offset in VRAM, after GPU reset
    static constexpr WORD DEFAULT_TILE_MAP_OFFSET           = 0x2000;
    /// Default layer 0 tile map width
    static constexpr WORD DEFAULT_NUM_TILES_X               = 64;
    /// Default layer 0 tile map height
    static constexpr WORD DEFAULT_NUM_TILES_Y               = 36;
    /// Default layer 0 color map page in VRAM, after GPU reset
    static constexpr WORD DEFAULT_COLOR_MAP_PAGE            = 0x01;
    /// Default layer 0 color map offset in VRAM, after GPU reset
    static constexpr WORD DEFAULT_COLOR_MAP_OFFSET          = 0x4000;

    /// GPU port descriptions
    inline static const std::map<Port, Device::PortDescription> PORTS = {
      { IRQ_CONTROL,                                   { "irq_control", "IRQ control and status register" } },

      { SPRITE_ATTRIBUTES_PAGE,                        { "sprite_attributes_page", "Sprite attribute table segment in VRAM" }},
      { SPRITE_ATTRIBUTES_OFFSET,                      { "sprite_attributes_offset", "Sprite attributes offet in VRAM" }},

      { LAYER_N_PORTS[0] + LAYER_MODE,                 { "layer_0_mode", "Layer 0 mode (see LayerMode for specification)" } },
      { LAYER_N_PORTS[0] + LAYER_CLEAR_MODE,           { "layer_0_clear_mode", "Layer 0 clear mode (see ClearMode for specification)" } },
      { LAYER_N_PORTS[0] + LAYER_BACKGROUND_COLOR,     { "layer_0_background_color", "Layer 0 background color palette index" } },
      { LAYER_N_PORTS[0] + LAYER_FOREGROUND_COLOR,     { "layer_0_foreground_color", "Layer 0 foreground color palette index (monochrome tile mode only)" } },
      { LAYER_N_PORTS[0] + LAYER_OFFSET_X,             { "layer_0_offset_x", "Layer 0 x-offset" } },
      { LAYER_N_PORTS[0] + LAYER_OFFSET_Y,             { "layer_0_offset_y", "Layer 0 y-offset" } },
      { LAYER_N_PORTS[0] + LAYER_TILE_MAP_PAGE,        { "layer_0_tile_map_page", "Layer 0 tile map address page in VRAM" } },
      { LAYER_N_PORTS[0] + LAYER_TILE_MAP_OFFSET,      { "layer_0_tile_map_offset", "Layer 0 tile map address offset in VRAM" } },
      { LAYER_N_PORTS[0] + LAYER_TILE_MODE,            { "layer_0_tile_mode", "Layer 0 tile mode (only used for tile layers, see TileMode for specification)" } },
      { LAYER_N_PORTS[0] + LAYER_NUM_TILES_X,          { "layer_0_num_tiles_x", "Layer 0 number of tiles in per row" } },
      { LAYER_N_PORTS[0] + LAYER_NUM_TILES_Y,          { "layer_0_num_tiles_y", "Layer 0 number of tiles rows per column" } },
      { LAYER_N_PORTS[0] + LAYER_TILE_ATLAS_PAGE,      { "layer_0_tile_atlas_page", "Layer 0 tile atlas address page in VRAM" } },
      { LAYER_N_PORTS[0] + LAYER_TILE_ATLAS_OFFSET,    { "layer_0_tile_atlas_offset", "Layer 0 tile atlas address offset in VRAM" } },
      { LAYER_N_PORTS[0] + LAYER_COLOR_MAP_PAGE,       { "layer_0_color_map_page", "Layer 0 color map address page in VRAM (monochrome tile mode only)" } },
      { LAYER_N_PORTS[0] + LAYER_COLOR_MAP_OFFSET,     { "layer_0_color_map_offset", "Layer 0 color map address offset in VRAM (monochrome tile mode only)" } },
      { LAYER_N_PORTS[0] + LAYER_PALETTE_PAGE,         { "layer_0_palette_page", "Layer 0 palette address page in VRAM" } },
      { LAYER_N_PORTS[0] + LAYER_PALETTE_OFFSET,       { "layer_0_palette_offset", "Layer 0 palette address offset in VRAM" } },

      { LAYER_N_PORTS[1] + LAYER_MODE,                 { "layer_1_mode", "Layer 1 mode (see LayerMode for specification)" } },
      { LAYER_N_PORTS[1] + LAYER_CLEAR_MODE,           { "layer_1_clear_mode", "Layer 1 clear mode (see ClearMode for specification)" } },
      { LAYER_N_PORTS[1] + LAYER_BACKGROUND_COLOR,     { "layer_1_background_color", "Layer 1 background color palette index" } },
      { LAYER_N_PORTS[1] + LAYER_FOREGROUND_COLOR,     { "layer_1_foreground_color", "Layer 1 foreground color palette index (monochrome tile mode only)" } },
      { LAYER_N_PORTS[1] + LAYER_OFFSET_X,             { "layer_1_offset_x", "Layer 1 x-offset" } },
      { LAYER_N_PORTS[1] + LAYER_OFFSET_Y,             { "layer_1_offset_y", "Layer 1 y-offset" } },
      { LAYER_N_PORTS[1] + LAYER_TILE_MAP_PAGE,        { "layer_1_tile_map_page", "Layer 1 tile map address page in VRAM" } },
      { LAYER_N_PORTS[1] + LAYER_TILE_MAP_OFFSET,      { "layer_1_tile_map_offset", "Layer 1 tile map address offset in VRAM" } },
      { LAYER_N_PORTS[1] + LAYER_TILE_MODE,            { "layer_1_tile_mode", "Layer 1 tile mode (only used for tile layers, see TileMode for specification)" } },
      { LAYER_N_PORTS[1] + LAYER_NUM_TILES_X,          { "layer_1_num_tiles_x", "Layer 1 number of tiles in per row" } },
      { LAYER_N_PORTS[1] + LAYER_NUM_TILES_Y,          { "layer_1_num_tiles_y", "Layer 1 number of tiles rows per column" } },
      { LAYER_N_PORTS[1] + LAYER_TILE_ATLAS_PAGE,      { "layer_1_tile_atlas_page", "Layer 1 tile atlas address page in VRAM" } },
      { LAYER_N_PORTS[1] + LAYER_TILE_ATLAS_OFFSET,    { "layer_1_tile_atlas_offset", "Layer 1 tile atlas address offset in VRAM" } },
      { LAYER_N_PORTS[1] + LAYER_COLOR_MAP_PAGE,       { "layer_1_color_map_page", "Layer 1 color map address page in VRAM (monochrome tile mode only)" } },
      { LAYER_N_PORTS[1] + LAYER_COLOR_MAP_OFFSET,     { "layer_1_color_map_offset", "Layer 1 color map address offset in VRAM (monochrome tile mode only)" } },
      { LAYER_N_PORTS[1] + LAYER_PALETTE_PAGE,         { "layer_1_palette_page", "Layer 1 palette address page in VRAM" } },
      { LAYER_N_PORTS[1] + LAYER_PALETTE_OFFSET,       { "layer_1_palette_offset", "Layer 1 palette address offset in VRAM" } },

      { LAYER_N_PORTS[2] + LAYER_MODE,                 { "layer_2_mode", "Layer 1 mode (see LayerMode for specification)" } },
      { LAYER_N_PORTS[2] + LAYER_CLEAR_MODE,           { "layer_2_clear_mode", "Layer 2 clear mode (see ClearMode for specification)" } },
      { LAYER_N_PORTS[2] + LAYER_BACKGROUND_COLOR,     { "layer_2_background_color", "Layer 2 background color palette index" } },
      { LAYER_N_PORTS[2] + LAYER_FOREGROUND_COLOR,     { "layer_2_foreground_color", "Layer 2 foreground color palette index (monochrome tile mode only)" } },
      { LAYER_N_PORTS[2] + LAYER_OFFSET_X,             { "layer_2_offset_x", "Layer 2 x-offset" } },
      { LAYER_N_PORTS[2] + LAYER_OFFSET_Y,             { "layer_2_offset_y", "Layer 2 y-offset" } },
      { LAYER_N_PORTS[2] + LAYER_TILE_MAP_PAGE,        { "layer_2_tile_map_page", "Layer 2 tile map address page in VRAM" } },
      { LAYER_N_PORTS[2] + LAYER_TILE_MAP_OFFSET,      { "layer_2_tile_map_offset", "Layer 2 tile map address offset in VRAM" } },
      { LAYER_N_PORTS[2] + LAYER_TILE_MODE,            { "layer_2_tile_mode", "Layer 2 tile mode (only used for tile layers, see TileMode for specification)" } },
      { LAYER_N_PORTS[2] + LAYER_NUM_TILES_X,          { "layer_2_num_tiles_x", "Layer 2 number of tiles in per row" } },
      { LAYER_N_PORTS[2] + LAYER_NUM_TILES_Y,          { "layer_2_num_tiles_y", "Layer 2 number of tiles rows per column" } },
      { LAYER_N_PORTS[2] + LAYER_TILE_ATLAS_PAGE,      { "layer_2_tile_atlas_page", "Layer 2 tile atlas address page in VRAM" } },
      { LAYER_N_PORTS[2] + LAYER_TILE_ATLAS_OFFSET,    { "layer_2_tile_atlas_offset", "Layer 2 tile atlas address offset in VRAM" } },
      { LAYER_N_PORTS[2] + LAYER_COLOR_MAP_PAGE,       { "layer_2_color_map_page", "Layer 2 color map address page in VRAM (monochrome tile mode only)" } },
      { LAYER_N_PORTS[2] + LAYER_COLOR_MAP_OFFSET,     { "layer_2_color_map_offset", "Layer 2 color map address offset in VRAM (monochrome tile mode only)" } },
      { LAYER_N_PORTS[2] + LAYER_PALETTE_PAGE,         { "layer_2_palette_page", "Layer 2 palette address page in VRAM" } },
      { LAYER_N_PORTS[2] + LAYER_PALETTE_OFFSET,       { "layer_2_palette_offset", "Layer 2 palette address offset in VRAM" } },

      { LAYER_N_PORTS[3] + LAYER_MODE,                 { "layer_3_mode", "Layer 1 mode (see LayerMode for specification)" } },
      { LAYER_N_PORTS[3] + LAYER_CLEAR_MODE,           { "layer_3_clear_mode", "Layer 3 clear mode (see ClearMode for specification)" } },
      { LAYER_N_PORTS[3] + LAYER_BACKGROUND_COLOR,     { "layer_3_background_color", "Layer 3 background color palette index" } },
      { LAYER_N_PORTS[3] + LAYER_FOREGROUND_COLOR,     { "layer_3_foreground_color", "Layer 3 foreground color palette index (monochrome tile mode only)" } },
      { LAYER_N_PORTS[3] + LAYER_OFFSET_X,             { "layer_3_offset_x", "Layer 3 x-offset" } },
      { LAYER_N_PORTS[3] + LAYER_OFFSET_Y,             { "layer_3_offset_y", "Layer 3 y-offset" } },
      { LAYER_N_PORTS[3] + LAYER_TILE_MAP_PAGE,        { "layer_3_tile_map_page", "Layer 3 tile map address page in VRAM" } },
      { LAYER_N_PORTS[3] + LAYER_TILE_MAP_OFFSET,      { "layer_3_tile_map_offset", "Layer 3 tile map address offset in VRAM" } },
      { LAYER_N_PORTS[3] + LAYER_TILE_MODE,            { "layer_3_tile_mode", "Layer 3 tile mode (only used for tile layers, see TileMode for specification)" } },
      { LAYER_N_PORTS[3] + LAYER_NUM_TILES_X,          { "layer_3_num_tiles_x", "Layer 3 number of tiles in per row" } },
      { LAYER_N_PORTS[3] + LAYER_NUM_TILES_Y,          { "layer_3_num_tiles_y", "Layer 3 number of tiles rows per column" } },
      { LAYER_N_PORTS[3] + LAYER_TILE_ATLAS_PAGE,      { "layer_3_tile_atlas_page", "Layer 3 tile atlas address page in VRAM" } },
      { LAYER_N_PORTS[3] + LAYER_TILE_ATLAS_OFFSET,    { "layer_3_tile_atlas_offset", "Layer 3 tile atlas address offset in VRAM" } },
      { LAYER_N_PORTS[3] + LAYER_COLOR_MAP_PAGE,       { "layer_3_color_map_page", "Layer 3 color map address page in VRAM (monochrome tile mode only)" } },
      { LAYER_N_PORTS[3] + LAYER_COLOR_MAP_OFFSET,     { "layer_3_color_map_offset", "Layer 3 color map address offset in VRAM (monochrome tile mode only)" } },
      { LAYER_N_PORTS[3] + LAYER_PALETTE_PAGE,         { "layer_3_palette_page", "Layer 3 palette address page in VRAM" } },
      { LAYER_N_PORTS[3] + LAYER_PALETTE_OFFSET,       { "layer_3_palette_offset", "Layer 3 palette address offset in VRAM" } },
    };

    /// Constructor.
    ///
    /// \param id GPU device ID
    /// \param width framebuffer width
    /// \param height framebuffer height
    /// \param vblankIrq IRQ line for vblank interrupt
    Gpu(const Device::ID id, const unsigned int width, const unsigned int height, const BYTE vblankIrq);

    /// Render single frame.
    void frame();

    /// Return current (=most recent) rendered frame.
    ///
    /// \return current frame
    Frame getFrame() const;

    /// \return render time for last frame, in microseconds
    std::chrono::microseconds getRenderTime() const;

    /// Get layer port from layer number and offset relative to layer base port.
    ///
    /// \param layer layer number
    /// \param port port offset
    /// \throws std::logic_error if the layer number or port offset are out of range
    /// \return
    constexpr WORD getLayerPort(const int layer, const WORD port) const
    {
      if ((layer < 0) || (layer >= 4) || (port >= LAYER_NUM_PORTS))
      {
        throw std::logic_error("Cannot get layer port: invalid layer number and/or port offset");
      }

      return LAYER_N_PORTS[layer] + port;
    };

    //
    // Device base class interface
    //

    /// Reset GPU to default state.
    void reset() override;

    /// Return pending interrupts.
    ///
    /// The GPU device currently only uses a single IRQ line, for the vblank interrupt.
    ///
    /// \return bitfield indicating pending interrupt IRQ lines
    virtual WORD pendingInterrupts() const override;

  private:
    void safeBlt8x8(const Pixel * const src, const int layer, const int x, const int y, const int scale) const;
    void unsafeBlt8x8(const Pixel * const src, const int layer, const int x, const int y, const int scale) const;

    void blt8x8(const Pixel * const src, const int layer, const int x, const int y, const int scale = 1) const ;

    void renderMonochromeTile(
      const int layer,
      const int ox, const int oy,
      const unsigned int scale,
      const BYTE * const tileData,
      const utils::ImageRgba::RGBA8888 &fg,
      const utils::ImageRgba::RGBA8888 &bg,
      const bool rotate,
      const bool flipX,
      const bool flipY) const;

    void renderMulticolorTile(
      const int layer,
      const int ox, const int oy,
      const unsigned int scale,
      const BYTE * const tileData,
      const unsigned short * const paletteData,
      const bool rotate,
      const bool flipX,
      const bool flipY) const;

    void renderTileLayer(const int layer) const;
    void renderSpriteLayer(const int layer) const;

    void compositLayers() const;

    int _width;
    int _height;

    BYTE _vblankIrq;

    std::unique_ptr<tbb::task_arena> _taskArena;

    std::array<std::unique_ptr<utils::ImageRgba::RGBA8888[]>, 4> _layerBuffers;
    std::unique_ptr<utils::ImageRgba::RGBA8888[]> _frontBuffer;

    std::chrono::microseconds _renderTime;
};

}
