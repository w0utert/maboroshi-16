/// Header file for the core::peripheral::Device class, which is the base
/// class for all peripheral devices.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#pragma once

#include "core/common/itf/memory_map.h"

#include <map>
#include <memory>

#include <boost/optional.hpp>

namespace core::peripheral {

/// Base class for peripheral devices.
///
/// This implements properties and facilities common to all devices, such
/// as port-mapped I/O, access to device local memory, interrupt requests, etc.
class Device
{
  public:
    /// Type alias for device IDs
    using ID = BYTE;
    /// Type alias for device port numbers
    using Port = WORD;

    /// Structure to hold port information
    struct PortDescription
    {
      /// Mnemonic (short name) for the port
      std::string mnemonic;
      /// Port description (mainly for generating IO/memory maps)
      std::string description;
    };

    /// Constructor.
    ///
    /// \param id device ID
    /// \param ports device port specification
    /// \param irqMask bitfield indicating the IRQ lines available to the device
    /// \param memoryPages number of device memory pages, if any
    Device(
      const ID id,
      const std::map<Port, PortDescription> &ports,
      const WORD irqMask = 0,
      const boost::optional<BYTE> memoryPages = boost::none)
    :
      _id(id),
      _ports(ports),
      _irqMask(irqMask),
      _memory(new MemoryMap(memoryPages.value_or(0))),
      _io(std::vector<WORD>(std::numeric_limits<Port>::max() + 1))
    {
    }

    /// Destructor
    virtual ~Device() = default;

    /// Write value to port.
    ///
    /// \param port port to write to
    /// \param value value to write
    /// \throw std::logic_error if the port is not valid for this device
    template<typename T>
    void out(const Port port, T value)
    {
      _io[port] = value;
    }

    /// Read value from port.
    ///
    /// \param port port to read from
    /// \return value read from port
    /// \throw std::logic_error if the port is not valid for this device
    template<typename T = core::WORD>
    T in(const Port port) const
    {
      core::WORD v = _io[port];

      // 8-bit port read: mask out high byte
      if constexpr (std::is_same<T, core::BYTE>() || std::is_same<T, core::SBYTE>())
      {
        v &= 0xFF;
      }

      // Signed port-read: ensure proper sign extension in case the return type is wider than a machine word,
      // by casting the port value to a signed word before returning
      if constexpr (std::is_signed<T>())
      {
        return static_cast<SWORD>(v);
      }
      else
      {
        return static_cast<T>(v);
      }
    }

    /// Get device ID
    ///
    /// \return device id
    ID getId() const
    {
      return _id;
    }

    /// Return port definitions.
    ///
    /// \return port definitions
    const std::map<Port, PortDescription> &getPorts() const
    {
      return _ports;
    }

    /// Get addressable device memory, if the device has any.
    ///
    /// \return device memory, or \a nullptr if the device has no local memory
    MemoryMap *getMemory()
    {
      return _memory.get();
    }

    /// Return pending interrupt requests.
    ///
    /// This needs to be implemented by subclasses, to return a bitmask that indicates
    /// a 'pending interrupt' status based on the device state, e.g. based on the value
    /// of its internal registers. Each 1 bit in the mask indicates that the device has
    /// a pending IRQ with the corresponding number, for example 0x05 would indicate the
    /// device has pending IRQ's 1 and 4.
    ///
    /// The base class implementation always returns 0, so devices that don't generate
    /// any interrupts do not have to override this.
    ///
    /// \return bitmask indicating pending interrupts
    virtual WORD pendingInterrupts() const
    {
      return 0;
    }

    /// Reset device to powerup state.
    ///
    /// This abstract interface needs to be implemented by all devices
     virtual void reset() = 0;

  protected:
    ID _id;

    std::map<Port, PortDescription> _ports;
    WORD _irqMask;
    std::unique_ptr<MemoryMap> _memory;
    std::vector<WORD> _io;
};

}
