/// Defines the BitRange template class, which is used for safe/portable bit-fields.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#pragma once

namespace core {

/// Utility class that can be used to define safe/portable bitfields.
///
/// This class works around the problem that the memory layout and bit order
/// of native C++ bitfields is not standardized, and completely at the discretion
/// of the compiler. In particular, some compilers order the bits in a struct
/// containing bitfields differently depending on the endianness of the target
/// architecture. This poses problems when trying to interpret some bitfield
/// stored as a machine word written/manipulated by emulated code, or when
/// casting bitfields to a C++ structure type to allow convenient access to
/// the individual bits/bit ranges of the bitfield (as opposed to manually
/// masking and shifting the bits).
///
/// The intended usage of this class is within the currentNamespace of a union with the size of
/// the bitfield storage type, and any number of bitfield ranges defined using the
/// BitRange template, which acts as a proxy for getting/setting bitfield ranges, hiding
/// all the bit shifting & masking, and allowing sign extension for bitfield ranges
/// that are defined with a signed field type.
///
/// Example:
///
/// \code{.cpp}
///   union MyBitField
///   {
///     WORD bits;
///     BitRange<WORD, bool, 0, 1> b;
///     BitRange<WORD, int, 1, 10> i;
///     BitRange<WORD, unsigned int, 11, 5> u;
///   }
///
///   MyBitField bf = {};
///   b.i = -20;
///   const int i = b.i;
/// \endcode
///
/// \tparam T storage type, typically BYTE or WORD
/// \tparam FT field return type
/// \tparam LSB start-bit of range
/// \tparam SIZE size of range, in bits
template<typename T, typename FT, int LSB, int SIZE>
class BitRange
{
  public:
    static constexpr T SRC_MASK = ((1 << SIZE) - 1);
    static constexpr T DST_MASK = SRC_MASK << LSB;
    static constexpr T SIGN_BIT = 1 << (SIZE - 1);
    static constexpr T SHIFT = LSB;

    /// Assign bitfield range.
    ///
    /// If the field type is wider than the bitfield range itself, any high \p bits that don't fit
    /// the bitfield will be silently discarded.
    ///
    /// \param bits bitfield bits
    constexpr void operator=(const FT bits) { _b = (_b & ~DST_MASK) | ((static_cast<T>(bits) & SRC_MASK) << SHIFT); }

    /// Extract value represented by range bits and return as field return type.
    ///
    /// This will sign-extend the bitfield value if the field type is signed,
    /// otherwise it will leave unused/excess high bits in the return field type zeroed out.
    ///
    /// \return value represented by the range bits
    constexpr operator FT() const
    {
      return as<FT>();
    }

    /// Dereference bit range.
    ///
    /// This is basically shorthand for static_cast<FT>(*this), in other words, it extracts
    /// the value represented by the range bits, and returns it as the field return type.
    ///
    /// \return value represented by the range bits.
    constexpr FT operator*() const
    {
      return as<FT>();
    }

    /// Extract value represented by range bits and cast it to the type indicated by the
    /// template parameter.
    ///
    /// \tparam U type to cast the extracted bit range value to
    /// \return value represented by the range bits
    template<typename U>
    constexpr U as() const
    {
      const T v = (_b & DST_MASK) >> SHIFT;

      // If the range field type is signed, sign extend it to the return value type. Otherwise
      // just left-pad with zeros to the return value type.
      if constexpr (std::is_signed<FT>::value)
      {
        return static_cast<U>((v ^ SIGN_BIT) - SIGN_BIT);
      }
      else
      {
        return static_cast<U>(v);
      }
    }

  private:
    T _b;
} __attribute__((packed));

}
