/// Common types used throughout the codebase.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#pragma once

#include "fmt/format.h"
#include "fmt/ostream.h"

#include "utils/itf/stream_operators.h"

#include <cstdint>
#include <cstddef>
#include <stdexcept>
#include <variant>

#pragma GCC system_header

namespace core {

/// 8-bit unsigned integer type
using BYTE = uint8_t;
/// Signed 8-bit integer
using SBYTE = int8_t;
/// 16-bit machine word
using WORD = uint16_t;
/// Signed 16-bit machine word
using SWORD = int16_t;
/// 32-bit machine double-word
using DWORD = uint32_t;
/// Signed 32-bit machine double-word
using SDWORD = int32_t;

// Forward declare cpu::Register enumeration type to break circular dependency
namespace cpu {

enum class Register;

}

/// Structure type to indicate long pointer (3-bit segment + 16-bit offset).
struct LongPointer
{
  /// Default constructor.
  LongPointer()
  :
    segment(0), offset(0)
  {
  }

  /// Constructor.
  ///
  /// \param segment 3-bit pointer segment
  /// \param offset 16-bit pointer offset
  /// \throws std::runtime_error if the \p segment and/or \p offset values are out of range
  constexpr LongPointer(const int64_t segment, const WORD offset)
  :
    segment(segment), offset(offset)
  {
    if ((segment & ~0b111) != 0)
    {
      throw std::logic_error(fmt::format("invalid long pointer: {0:#04x}:{1:#06x}", segment, offset));
    }
  }

  /// Constructor for long pointer into zero page
  ///
  /// \param offset 16-bit pointer offset
  /// \throws std::runtime_error if the \p offset value is out of range
  constexpr LongPointer(const WORD offset)
    :
    LongPointer(0, offset)
  {
  }

  /// Compare for ordering.
  ///
  /// \param rhs right-hand side for comparison
  /// \return *this < rhs
  bool operator<(const LongPointer &rhs) const
  {
    return (this->linear() < rhs.linear());
  }

  /// Compare for less-than or equal
  ///
  /// \param rhs right-hand side for comparison
  /// \return *this <= rhs
  bool operator<=(const LongPointer &rhs) const
  {
    return (*this < rhs) || (*this == rhs);
  }

  /// Compare for equality.
  ///
  /// \param rhs right-hand side for comparison
  /// \return *this == rhs
  bool operator==(const LongPointer &rhs) const
  {
    return (this->linear() == rhs.linear());
  }

  /// Compare for non-equality.
  ///
  /// \param rhs right-hand side for comparison
  /// \return *this != rhs
  bool operator!=(const LongPointer &rhs) const
  {
    return !(*this == rhs);
  }

  /// Create new pointer by adding the passed offset in machine words from this pointer.
  ///
  /// \param words number of words to offset to this pointer by
  /// \return pointer created by offsetting this pointer with specified offset
  constexpr LongPointer operator+(const int words) const
  {
    const size_t l = linear() + words;

    return LongPointer((BYTE) (l  >> 16), (WORD) (l & 0xFFFF));
  }

  /// Create new pointer by subtracting the passed offset in machine words from this pointer.
  ///
  /// \param words number of words to offset to this pointer by
  /// \return pointer created by offsetting this pointer with specified offset
  constexpr LongPointer operator-(const int words) const
  {
    return *this + -words;
  }

  /// Return pointer difference, in number of instructions.
  ///
  /// \param other other pointer to calculate pointer difference from this pointer to
  /// \return pointer difference betweeh this pointer and the \p other pointer, as (*this - other)
  constexpr int operator-(const LongPointer &other) const
  {
    return (this->linear() - other.linear());
  }

  /// Return linear address created from pointer segment and offset.
  ///
  /// \return linear address created from pointer segment and offset
  constexpr size_t linear() const
  {
    return ((size_t) segment << 16) | offset;
  }

  /// 3-bit memory segment
  BYTE segment;
  /// 16-bit offset into segment
  WORD offset;
};



/// Structure that stores an indirect address.
///
/// Indirect addresses can take two forms: 'normal form' and 'zero-page form', which
/// are built from a 3-bit immediate multiplier, a base register, a 10-bit signed offset.
///
/// The normal form is [multiplier*base register +/- offset], and refers to a memory
/// address in either the source segment or the destination segment indicated by the
/// CPU segment register, depending on whether the indirect address is used as a source or
/// destination operand.
///
/// The zero-page form is [0.base register +/- offset], in this case the 3-bit multiplier
/// is set to zero, and the address resolves to the zero-page, without scaling the base
/// register (ie: the multiplier value 0 is not used).
struct IndirectAddress
{
  /// Default contructor.
  constexpr IndirectAddress()
  :
    multiplier(1), base(static_cast<cpu::Register>(0)), offset(0), backward(0)
  {
  }

  /// Construct indirect address from multiplier, base register and signed offset
  ///
  /// \param segment 3-bit multiplier
  /// \param base base register
  /// \param offset 10-bit signed offset
  IndirectAddress(const BYTE multiplier, const cpu::Register base, const SWORD offset)
  :
    multiplier(multiplier), base(base), offset(std::abs(offset)), backward(offset < 0)
  {
    if (multiplier >= (1 << 3))
    {
      throw std::logic_error(fmt::format("invalid multiplier {0} for indirect address, must be [0..8)", multiplier));
    }

    if (std::abs(offset) >= (1 << 9))
    {
      throw std::logic_error(fmt::format("invalid offset {0} for indirect address, must be [-{1}..{1}]", offset, (1 << 9) - 1));
    }
  }

  /// Construct zero-page indirect address from base register and 10-bit signed offset.
  ///
  /// \param base base register
  /// \param offset 10-bit signed offset
  IndirectAddress(const cpu::Register base, const SWORD offset)
  :
    multiplier(0), base(base), offset(std::abs(offset)), backward(offset < 0)
  {
    if (std::abs(offset) >= (1 << 9))
    {
      throw std::logic_error(fmt::format("invalid offset {0} for indirect address, must be [-{1}..{1}]", offset, (1 << 9) - 1));
    }
  }

  /// 3-bit multiplier
  BYTE multiplier;
  /// Base address register
  cpu::Register base;
  /// Offset relative to (multiplier * base address)
  WORD offset;
  /// Whether the offset should be added (forward) or subtracted (backward)
  bool backward;
};

/// I/O port move operation (device, port, source/destination register)
struct PortMove
{
  using Port = std::variant<WORD, cpu::Register>;

  /// Construct port move with immediate port operand.
  ///
  /// \param device device for port-move
  /// \param port source/destination port
  /// \param value register to read/write from/to port
  PortMove(const BYTE device, const WORD port, const cpu::Register value)
  :
    indirect(false),
    device(device),
    port(port),
    reg(value)
  {
  }

  /// Construct port move with indirect (register) port operand.
  ///
  /// \param device device for port-move
  /// \param port register holding source/destination port
  /// \param value register to read/write from/to port
  PortMove(const BYTE device, const cpu::Register port, const cpu::Register value)
  :
    indirect(true),
    device(device),
    port(port),
    reg(value)
  {
  }

  /// Indicates whether this is an indirect port move (port specified by register)
  bool indirect;
  /// Device ID
  BYTE device;
  /// Port
  Port port;
  /// Source/destination register
  cpu::Register reg;
};

}
