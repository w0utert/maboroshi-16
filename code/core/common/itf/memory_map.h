/// Header file for the core::MemoryMap class, which is used to represent memory regions.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#pragma once

#include "core/common/itf/types.h"

#include <third_party/fmt/format.h>

#include <vector>

namespace core {

class MemoryMap
{
  public:
    /// Constructor that allocates and clears (zeros) the requested number of 64KB \p pages.
    ///
    /// \param pages number of 64KB pages to allocate.
    MemoryMap(const BYTE pages)
    :
      _pages(pages)
    {
      _memory.resize(static_cast<uint32_t>(pages) << 16);

      clear();
    }

    /// Clear (zero) all pages of the memory map.
    void clear()
    {
      std::fill(_memory.begin(), _memory.end(), 0.0);
    }

    /// Get pointer to zero-page memory.
    ///
    /// \return pointer to zero-page memory
    /// \throw std::runtime error if the page is out of range for this memory map
    WORD *getZeroPage()
    {
      return getPage(0);
    }

    /// Get pointer to start numbered \p page.
    ///
    /// \return pointer to start of the passed \p page
    /// \throw std::runtime error if the page is out of range for this memory map
    template<typename T = WORD>
    T *getPage(const uint8_t page)
    {
      if (page >= _pages)
      {
        throw std::runtime_error(fmt::format("Memory map page index {0} out of bounds", page));
      }

      return reinterpret_cast<T *>(&_memory[static_cast<uint32_t>(page) << 16]);
    }

    /// Get pointer at \p offset into a \p page of memory in this memory map.
    ///
    /// \param page memory page
    /// \param offset offset into page
    /// \return pointer to the memory at the specified \p page and \p offset
    /// \throw std::runtime_error if the \p page is out of range for this memory map
    template<typename T = WORD>
    T* getAddress(const uint8_t page, const WORD offset)
    {
      return reinterpret_cast<T *>(getPage(page) + offset);
    }

    /// Get number of pages in memory map.
    ///
    /// \return number of pages in memory map
    BYTE getNumPages() const
    {
      return _pages;
    }

  private:
    uint8_t _pages;
    std::vector<WORD> _memory;
};

}
