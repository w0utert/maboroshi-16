/// Implementation file for the core::input::Controllers device.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#include "core/input/itf/controller.h"

namespace core::input {

// Constructor
Controllers::Controllers(const Device::ID id)
:
  Device(id, PORTS)
{
  // Reset controller device to default (powerup) state
  reset();
}

// Reset timer device to powerup state
void Controllers::reset()
{
  out(CONTROLLER_0_STATE, 0);
  out(CONTROLLER_0_CHANGED, 0);
  out(CONTROLLER_1_STATE, 0);
  out(CONTROLLER_1_CHANGED, 0);
}

// Set controller state
void Controllers::setControllerState(const platform::Machine::ControllerState &controller0, const platform::Machine::ControllerState &controller1)
{
  // Helper function to create controller button state bitfield
  const auto controller_bits = [](const platform::Machine::ControllerState &state) -> WORD
  {
    return
      (state.a ? A_BUTTON_BIT : 0) |
      (state.b ? B_BUTTON_BIT : 0) |
      (state.x ? X_BUTTON_BIT : 0) |
      (state.y ? Y_BUTTON_BIT : 0) |
      (state.start ? START_BUTTON_BIT : 0) |
      (state.select ? SELECT_BUTTON_BIT : 0);
  };

  // Set controller button state & changed registers according to passed controller state
  const WORD prev_state_0 = in(CONTROLLER_0_STATE);
  const WORD prev_state_1 = in(CONTROLLER_1_STATE);

  const WORD next_state_0 = controller_bits(controller0);
  const WORD next_state_1 = controller_bits(controller1);

  out(CONTROLLER_0_STATE, next_state_0);
  out(CONTROLLER_0_CHANGED, (prev_state_0 ^ next_state_0) & ALL_BUTTON_BITS);

  out(CONTROLLER_1_STATE, next_state_1);
  out(CONTROLLER_1_CHANGED, (prev_state_1 ^ next_state_1) & ALL_BUTTON_BITS);
}

}
