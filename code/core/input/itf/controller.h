/// Header file for the controller (gamepad) device.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#pragma once

#include "core/peripheral/itf/device.h"

#include "platform/itf/machine.h"

namespace core::input {

///
class Controllers : public peripheral::Device
{
  public:
    // Controllers state I/O ports
    static constexpr WORD CONTROLLER_0_STATE             = 0x0000;
    static constexpr WORD CONTROLLER_0_CHANGED           = 0x0001;
    static constexpr WORD CONTROLLER_1_STATE             = 0x0010;
    static constexpr WORD CONTROLLER_1_CHANGED           = 0x0011;

    // Start of port range for all controller ports
    static constexpr WORD CONTROLLER_PORTS_BASE         = CONTROLLER_0_STATE;
    // Number of ports per controller (note: this is more like a stride, not nearly all ports are used for each controller)
    static constexpr auto CONTROLLER_NUM_PORTS          = CONTROLLER_1_STATE - CONTROLLER_0_STATE;

    // Number of supported controllers
    static constexpr auto NUM_CONTROLLERS               = 2;

    //
    // Conntroller device constants & flag bits
    //

    static constexpr WORD A_BUTTON_BIT                  = 1 << 0;
    static constexpr WORD B_BUTTON_BIT                  = 1 << 1;
    static constexpr WORD X_BUTTON_BIT                  = 1 << 2;
    static constexpr WORD Y_BUTTON_BIT                  = 1 << 3;
    static constexpr WORD START_BUTTON_BIT              = 1 << 4;
    static constexpr WORD SELECT_BUTTON_BIT             = 1 << 5;
    static constexpr WORD ALL_BUTTON_BITS               = 0b111111;

    /// Timer device port descriptions
    inline static const std::map<Port, Device::PortDescription> PORTS = {
      { CONTROLLER_0_STATE,                               { "controller_0_state", "Controllers 0 button state" } },
      { CONTROLLER_0_CHANGED,                             { "controller_0_changed", "Indicates which controller 0 buttons changed state since last frame" } },
      { CONTROLLER_1_STATE,                               { "controller_1_state", "Controllers 1 button state" } },
      { CONTROLLER_1_CHANGED,                             { "controller_1_changed", "Indicates which controller 1 buttons changed state since last frame" } },
    };

    /// Constructor.
    ///
    /// \param id device ID
    Controllers(const Device::ID id);

    /// Reset controller device to default state.
    void reset() override;

    /// Set controller state.
    ///
    /// \param controller0 controller 0 state
    /// \param controller1 controller 1 state
    void setControllerState(const platform::Machine::ControllerState &controller0, const platform::Machine::ControllerState &controller1);
};

}