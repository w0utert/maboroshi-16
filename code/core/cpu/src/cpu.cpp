#include "core/cpu/itf/cpu.h"

/// Implements the main maboroshi-16 CPU-related features.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#include "core/cpu/itf/exceptions.h"
#include "core/cpu/itf/instruction.h"

#include "core/peripheral/itf/device.h"

#include <unordered_map>
#include <algorithm>
#include <numeric>
#include <cmath>

namespace core::cpu {

// Include implementation files for each of the CPU instructions. Each is implemented in
// a seperate file to avoid having to create one single-file monstrosity, but we still
// want the compile to be able to inline them so having separate translation units for
// each of them is not an option. Instead we just #include the code directly here.

#include "instructions/add.cpp"
#include "instructions/adc.cpp"
#include "instructions/and.cpp"
#include "instructions/asr.cpp"
#include "instructions/b.cpp"
#include "instructions/bank.cpp"
#include "instructions/call.cpp"
#include "instructions/clr.cpp"
#include "instructions/cmp.cpp"
#include "instructions/dec.cpp"
#include "instructions/div.cpp"
#include "instructions/divs.cpp"
#include "instructions/halt.cpp"
#include "instructions/in.cpp"
#include "instructions/inc.cpp"
#include "instructions/j.cpp"
#include "instructions/ldsp.cpp"
#include "instructions/lsl.cpp"
#include "instructions/lsr.cpp"
#include "instructions/mov.cpp"
#include "instructions/movl.cpp"
#include "instructions/movh.cpp"
#include "instructions/mul.cpp"
#include "instructions/muls.cpp"
#include "instructions/neg.cpp"
#include "instructions/not.cpp"
#include "instructions/or.cpp"
#include "instructions/out.cpp"
#include "instructions/pop.cpp"
#include "instructions/pops.cpp"
#include "instructions/push.cpp"
#include "instructions/pushs.cpp"
#include "instructions/ret.cpp"
#include "instructions/rsv.cpp"
#include "instructions/rti.cpp"
#include "instructions/set.cpp"
#include "instructions/stsp.cpp"
#include "instructions/sub.cpp"
#include "instructions/sbb.cpp"
#include "instructions/wait.cpp"
#include "instructions/xor.cpp"

//
// Public interface methods to get read-only references to registers, memory, flags, and other CPU state.
//

const Cpu::State &Cpu::getState() const
{
  return _state;
}

LongPointer Cpu::getInstructionPointer() const
{
  return _state.ip;
}

WORD Cpu::getStackPointer() const
{
  return _state.sp;
}

std::tuple<BYTE, BYTE> Cpu::getSegmentRegister() const
{
  return { _state.sr & 0xFF, _state.sr >> 8 };
}

WORD Cpu::getRegister(const Register r) const
{
  return const_cast<Cpu *>(this)->reg(r);
}

const WORD &Cpu::getMemory(const WORD o) const
{
  return const_cast<Cpu *>(this)->mem(o);
}

const WORD &Cpu::getMemory(const LongPointer p) const
{
  return const_cast<Cpu *>(this)->mem(p);
}

const WORD &Cpu::getMemory(const IndirectAddress ia, const OperandSegment operandSegment) const
{
  return const_cast<Cpu *>(this)->mem(translate(ia, operandSegment));
}

BYTE Cpu::getFlags() const
{
  return _state.flags.bits;
}

bool Cpu::getFlag(const Flag::Bits flag) const
{
  return (_state.flags.bits & static_cast<BYTE>(flag)) != 0;
}

uint64_t Cpu::getPerformanceCounter() const
{
  return _state.performanceCounter;
}

std::vector<WORD> &Cpu::getRam()
{
  return _ram;
}

// Connect peripheral device to CPU
void Cpu::connectDevice(peripheral::Device *device)
{
  const peripheral::Device::ID id = device->getId();

  if (_devices[id] != nullptr)
  {
    throw std::runtime_error(fmt::format("Device ID {0:#04x} already connected to bus", id));
  }

  _devices[id] = device;
}

// Switch memory bank
void Cpu::switchBank(const peripheral::Device::ID &id, const BYTE page, const BYTE addressPage)
{
  if ((addressPage < NUM_RAM_PAGES) || (addressPage >= NUM_ADDRESS_PAGES))
  {
    throw std::runtime_error(fmt::format("Error mapping address page {0:#04x}, page not mappable or out of range", addressPage));
  }

  peripheral::Device *device = (id < _devices.size() ?  _devices[id] : nullptr);

  if (!device)
  {
    throw std::runtime_error(fmt::format("Error mapping device memory for device {0:#04x}, invalid device", id));
  }

  MemoryMap *device_memory = device->getMemory();

  if (!device_memory || (page >= device_memory->getNumPages()))
  {
    throw std::runtime_error(fmt::format("Error mapping device {0:#04x} memory page {1:#04x}, source page out of range", device->getId(), page));
  }

  _addressPages[addressPage] = device_memory->getPage(page);
}

// Return CPU debugger
Debugger &Cpu::getDebugger()
{
  return _debugger;
}

// Set IRQ flags based on the 'pending interrupt' status of all connected devices
void Cpu::setIrqFlags()
{
  _state.irq = std::accumulate(_devices.begin(), _devices.end(), 0, [](const DWORD flags, const peripheral::Device *device)
  {
    return flags | (device != nullptr ? device->pendingInterrupts() : 0);
  });
}

// Constructor
Cpu::Cpu()
{
  std::fill(_devices.begin(), _devices.end(), nullptr);

  /// \todo memory map + allocated memory regions should be passed in from a platform component
  _ram.resize(RAM_SIZE);

  // Reset CPU to initial state
  reset();
}

// Reset CPU
void Cpu::reset()
{
  _state = {};

  std::fill(_ram.begin(), _ram.end(), 0);

  // Initialize 'MMU', resetting any mapped device RAM pages and mapping the low pages in the address range to system RAM.
  std::fill(_addressPages.begin(), _addressPages.end(), nullptr);

  for (BYTE ram_page = 0; ram_page < NUM_RAM_PAGES; ram_page++)
  {
    _addressPages[ram_page] = _ram.data() + (ram_page << 16);
  }
}

// Fetch next instruction
Instruction Cpu::fetch(const bool peek)
{
  const Instruction instruction = { _ram[_state.ip.linear()], _ram[_state.ip.linear() + 1] };

  if (!peek)
  {
    _state.ip = _state.ip + 2;
  }

  return instruction;
}

// Service IRQ with passed number
void Cpu::serviceIrq(BYTE irq)
{
  if ((_state.sp - 3) < (Cpu::STACK_OFFSET - Cpu::STACK_SIZE))
  {
    /// \todo this should throw a custom CPU exception type
    throw std::runtime_error("StackOverflow");
  }

  // Special-case handling if the next instruction in the instruction stream is a 'WAIT' instruction. If this
  // is the case, and the current IRQ flags include any of the IRQ's the WAIT instruction is blocking
  // on, we advance the instruction pointer to skip it.
  const Instruction next_instruction = fetch(true);

  if ((next_instruction.opcode == Opcode::WAIT) && ((next_instruction.immediate & _state.irq) != 0))
  {
    _state.ip = _state.ip + 2;
  }

  // Save CPU state (push flags, push return address)
  mem(_state.sp - 0) = _state.flags.bits;
  mem(_state.sp - 1) = _state.ip.offset;
  mem(_state.sp - 2) = _state.ip.segment;

  _state.sp = _state.sp - 3;

  // Set IRQ disable flag to mute all IRQ until the handler for the current IRQ returns
  _state.flags.bits = _state.flags.bits | Flag::DISABLE_IRQ;

  // Get the IRQ vector and set the instruction pointer to it
  _state.ip = { mem(IRQ_TABLE_OFFSET + 2*irq), mem(IRQ_TABLE_OFFSET + 2*irq + 1) };
}

// Advance CPU state
void Cpu::tick(const size_t ticks)
{
  // Update the IRQ flags for devices that have a pending interrupt request. This ensures
  // control is transferred to the service routine for the lowest-numbered IRQ before
  // executing any instructions.
  setIrqFlags();

  // If there are pending interrupt requests, transfer control to the service routine for the
  // IRQ with the lowest number.
  while (!getFlag(Flag::DISABLE_IRQ) && (_state.irq != 0))
  {
    // Service IRQ (save IP & CPU flags and set IP to the service routine)
    const BYTE irq_num = __builtin_ffs(_state.irq) - 1;

    serviceIrq(irq_num);
  }

  // Step CPU for the requested number of instructions
  for (size_t n = 0; n < ticks; n++)
  {
    // If debugger is enabled, throw a Break exception if a breakpoint was hit, or if in single-step mode
    switch (_debugger.getMode())
    {
      case Debugger::Mode::SINGLE_STEP:
      {
        if (!_lastBreak || (_lastBreak.value() != _state.ip))
        {
          _debugger.stop();

          _lastBreak = _state.ip;

          throw Break();
        }

        break;
      }

      case Debugger::Mode::RUN_TO_BREAKPOINT:
      {
        if (_debugger.isBreakpoint(_state.ip))
        {
          if (!_lastBreak || (_lastBreak.value() != _state.ip))
          {
            _debugger.stop();

            _lastBreak = _state.ip;

            throw Break();
          }
          else
          {
            _lastBreak.reset();
          }
        }

        break;
      }

      case Debugger::Mode::DISABLED:
        break;
    }

    // Instruction fetch
    Instruction instruction = fetch();

    // Instruction 'decode' and dispatch
    const Opcode opcode = instruction.opcode;

    switch (opcode)
    {
      case Opcode::ADC:
      case Opcode::ADCI:
      case Opcode::ADCM:
      case Opcode::ADCE:
        adc(instruction);
        break;

      case Opcode::ADD:
      case Opcode::ADDI:
      case Opcode::ADDM:
      case Opcode::ADDE:
        add(instruction);
        break;

      case Opcode::ASR:
      case Opcode::ASRI:
      case Opcode::ASRM:
      case Opcode::ASRE:
        asr(instruction);
        break;

      case Opcode::AND:
      case Opcode::ANDI:
      case Opcode::ANDM:
      case Opcode::ANDE:
        and_(instruction);
        break;

      case Opcode::NOP:
        break;

      case Opcode::B:
        b(instruction);
        break;

      case Opcode::BANK:
        bank(instruction);
        break;

      case Opcode::CLR:
        clr(instruction);
        break;

      case Opcode::CALLM:
      case Opcode::CALLV:
        call(instruction);
        break;

      case Opcode::CMP:
      case Opcode::CMPI:
      case Opcode::CMPM:
      case Opcode::CMPE:
        cmp(instruction);
        break;

      case Opcode::DEC:
      case Opcode::DECM:
      case Opcode::DECE:
        dec(instruction);
        break;

      case Opcode::DIV:
        div(instruction);
        break;

      case Opcode::DIVS:
        divs(instruction);
        break;

      case Opcode::HALT:
        halt(instruction);
        break;

      case Opcode::INI:
      case Opcode::INR:
        in(instruction);
        break;

      case Opcode::INC:
      case Opcode::INCM:
      case Opcode::INCE:
        inc(instruction);
        break;

      case Opcode::JM:
      case Opcode::JV:
        j(instruction);
        break;

      case Opcode::LDSP:
        ldsp(instruction);
        break;

      case Opcode::LSL:
      case Opcode::LSLI:
      case Opcode::LSLM:
      case Opcode::LSLE:
        lsl(instruction);
        break;

      case Opcode::LSR:
      case Opcode::LSRI:
      case Opcode::LSRM:
      case Opcode::LSRE:
        lsr(instruction);
        break;

      case Opcode::MOV:
      case Opcode::MOVI:
      case Opcode::MOVE:
      case Opcode::MOVM:
        mov(instruction);
        break;

      case Opcode::MOVH:
      case Opcode::MOVHI:
      case Opcode::MOVHE:
      case Opcode::MOVHM:
        movh(instruction);
        break;

      case Opcode::MOVL:
      case Opcode::MOVLI:
      case Opcode::MOVLE:
      case Opcode::MOVLM:
        movl(instruction);
        break;

      case Opcode::MUL:
        mul(instruction);
        break;

      case Opcode::MULS:
        muls(instruction);
        break;

      case Opcode::NEG:
        neg(instruction);
        break;

      case Opcode::NOT:
        not_(instruction);
        break;

      case Opcode::OR:
      case Opcode::ORI:
      case Opcode::ORE:
      case Opcode::ORM:
        or_(instruction);
        break;

      case Opcode::OUTI:
      case Opcode::OUTR:
        out(instruction);
        break;

      case Opcode::POP:
        pop(instruction);
        break;

      case Opcode::POPS:
        pops(instruction);
        break;

      case Opcode::PUSH:
        push(instruction);
        break;

      case Opcode::PUSHS:
        pushs(instruction);
        break;

      case Opcode::RET:
        ret(instruction);
        break;

      case Opcode::RTI:
        rti(instruction);
        break;

      case Opcode::RSV:
        rsv(instruction);
        break;

      case Opcode::SET:
        set(instruction);
        break;

      case Opcode::STSP:
        stsp(instruction);
        break;

      case Opcode::SBB:
      case Opcode::SBBI:
      case Opcode::SBBE:
      case Opcode::SBBM:
        sbb(instruction);
        break;

      case Opcode::SUB:
      case Opcode::SUBI:
      case Opcode::SUBE:
      case Opcode::SUBM:
        sub(instruction);
        break;

      case Opcode::XOR:
      case Opcode::XORI:
      case Opcode::XORM:
      case Opcode::XORE:
        xor_(instruction);
        break;

      case Opcode::WAIT:
        // The 'WAIT' instruction is a special case. Since the run loop assumes no new interrupts occur until the
        // the number of cycles for which it was called have expired, the wait instruction can simply block (move the
        // instruction pointer back 1 instruction) and break out of the current run loop invocation, which is
        // done by setting the number of cycles executed to the number of cycles the run loop was called for.
        wait(instruction);
        n = ticks;
        break;

      default:
        /// \todo this should throw a custom CPU exception type
        throw std::runtime_error(fmt::format("Invalid opcode: {0:#04x}", (BYTE) opcode));
    }

    _state.performanceCounter++;
  }
}

//
// Private methods
//

/// \cond INTERNAL

/// Translate indirect address to direct memory address (long pointer).
///
/// \param ia indirect address to translate
/// \return long pointer to direct memory address
LongPointer Cpu::translate(const IndirectAddress ia, const OperandSegment operandSegment) const
{
  const BYTE multiplier = ia.multiplier;
  const SDWORD base = const_cast<Cpu *>(this)->reg(ia.base);
  const SDWORD offset = (ia.backward ? -ia.offset : ia.offset);

  // A multiplier of 0 is treated as a special case, to indicate zero-page addressing mode,
  // ie: [r0 + 10] addresses a location in the zero page.
  if (multiplier == 0)
  {
    return LongPointer(0, (base + offset) & 0xFFFF);
 }
  else
  {
    const BYTE segment = (operandSegment == OperandSegment::SRC ? (_state.sr & 0xFF) : (_state.sr >> 8));

    return LongPointer(segment, (multiplier * base + offset) & 0xFFFF);
  }
}

/// Return reference to named register.
///
/// \param r register to get reference to
/// \return reference to register
WORD &Cpu::reg(const Register r)
{
  return _state.registers.all[static_cast<BYTE>(r)];
}

/// Return reference to memory address at specified \p offset in zero page.
///
/// \param o zero-page offset of memory address to get reference to
/// \return reference to zero-page memory address
WORD &Cpu::mem(const WORD o)
{
  return *(_addressPages[0] + o);
}

/// Return reference to memory address.
///
/// \param p long pointer to memory address to get reference to
/// \return reference to memory address
WORD &Cpu::mem(const LongPointer p)
{
  if (_addressPages[p.segment] != nullptr)
  {
    return *(_addressPages[p.segment] + p.offset);
  }
  else
  {
    throw std::runtime_error(fmt::format("Bus error: read/write to invalid page {0:#04x}", p.segment));
  }
}

/// Extract source/destination operands for unary operation encoded by the passed \p instruction.
///
/// The operand for a unary operation is used both as the source and destination operand, and is
/// returned as a modifiable reference to either a register or a memory location.
///
/// \param instruction instruction to get the source/destination operand from
/// \return a reference to the source/destination operand
WORD *Cpu::getUnarySrcDstOperands(const Instruction &instruction)
{
  WORD *src_dst = nullptr;

  switch (instruction.addressMode)
  {
    case AddressMode::REGISTER:
      src_dst = &reg(instruction.unaryOperand.as<Register>());
      break;

    case AddressMode::MEMORY:
      src_dst = &mem(instruction.longPointer());
      break;

    case AddressMode::INDIRECT_ADDRESS:
      // Translating unary indirect address operands always uses the destination segment
      src_dst = &mem(translate(instruction.indirectAddress(), OperandSegment::DST));
      break;

    case AddressMode::IMMEDIATE:
      // Unary operand on immediate does not make any sense
      break;
  }

  return src_dst;
}

/// Extract source and destination operands for binary operation encoded by the passed \p instruction.
///
/// The destination operand is returned as a modifiable reference, and can be a register or a
/// memory location, depending on the instruction direction.
///
/// \param instruction instruction to get destination and value operands from
/// \return tuple consisting of a reference to the destination operand, and the value operand
std::tuple<WORD *, WORD> Cpu::getBinarySrcDstOperands(const Instruction &instruction)
{
  const Instruction::Direction direction = instruction.binaryOperands.direction;

  // Get destination and value
  WORD *dst = nullptr;
  WORD value = 0;

  switch (instruction.addressMode)
  {
    case AddressMode::REGISTER:
    {
      dst = &reg(instruction.binaryOperands.o0.as<Register>());
      value = reg(instruction.binaryOperands.o1.as<Register>());

      break;
    }

    case AddressMode::IMMEDIATE:
    {
      dst = &reg(instruction.binaryOperands.o0.as<Register>());
      value = instruction.immediate;

      break;
    }

    case AddressMode::INDIRECT_ADDRESS:
    {
      const IndirectAddress ia = instruction.indirectAddress();
      const Register r = instruction.binaryOperands.o0.as<Register>();

      if (direction == Instruction::Direction::MEM_SRC)
      {
        std::tie(dst, value) = std::pair<WORD *, WORD>(&mem(translate(ia, OperandSegment::DST)), reg(r));
      }
      else if (direction == Instruction::Direction::DST_MEM)
      {
        std::tie(dst, value) = std::pair<WORD *, WORD>(&reg(r), mem(translate(ia, OperandSegment::SRC)));
      }

      break;
    }

    case AddressMode::MEMORY:
    {
      WORD * const o0 = &mem(instruction.longPointer());
      WORD * const o1 = &reg(instruction.binaryOperands.o0.as<Register>());

      if (direction == Instruction::Direction::MEM_SRC)
      {
        std::tie(dst, value) = std::pair<WORD *, WORD>(o0, *o1);
      }
      else if (direction == Instruction::Direction::DST_MEM)
      {
        std::tie(dst, value) = std::pair<WORD *, WORD>(o1, *o0);
      }

      break;
    }
  }

  return { dst, value };
}

/// \endcond

//
// Stream operators
//

namespace Flag {

std::ostream &operator<<(std::ostream &out, const Bits &f)
{
  static const std::unordered_map<Bits, std::string> FLAGS = {
    { Flag::NONE, "0" },
    { Flag::CARRY, "C" },
    { Flag::ZERO, "Z" },
    { Flag::OVERFLOW, "O" },
    { Flag::NEGATIVE, "N" },
    { Flag::DISABLE_IRQ, "I" },
    { Flag::ALL_ARITHMETIC, "A" },
    { Flag::ALL, "*" },
  };

  out << FLAGS.find(f)->second;

  return out;
}

}

}


