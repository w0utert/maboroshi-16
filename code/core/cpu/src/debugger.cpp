/// Implements debugger API functions.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#include "core/cpu/itf/debugger.h"

namespace core::cpu {

Debugger::Debugger()
:
  _mode(Mode::DISABLED), _state(State::RUNNING)
{
}

// Set breakpoint.
void Debugger::setBreakpoint(const LongPointer &address)
{
  _breakpoints[address.linear()] = address;
}

// Delete breakpoint.
void Debugger::deleteBreakpoint(const LongPointer &address)
{
  _breakpoints.erase(address.linear());
}

// Query breakpoint
bool Debugger::isBreakpoint(const LongPointer &address) const
{
  return (_breakpoints.count(address.linear()) != 0);
}

// Stop program
void Debugger::stop()
{
  _state = State::STOPPED;
}

// Resume program
void Debugger::resume()
{
  _mode = Mode::RUN_TO_BREAKPOINT;
  _state = State::RUNNING;
}

// Single-step program
void Debugger::step()
{
  _mode = Mode::SINGLE_STEP;
  _state = State::RUNNING;
}

}
