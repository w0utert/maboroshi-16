/// Return from interrupt.
///
/// A return-from-interrupt is pretty much identical to a return-from-subroutine, the
/// only difference being that besides the return addres, also the CPU flags are popped
/// and restored.
///
/// ### Mnemonic
///
/// ``rti``
///
/// ### Opcodes
///
/// ``rti``
///
/// ### Syntax
///
/// ``rti``
///
/// ### Examples
///
/// ``rti``
///
/// ### Operation
///
/// ```
/// if (CPU.sp + 3 > STACK_OFFSET)
/// {
///   throw StackUnderflow
/// }
///
/// segment = [CPU.sp + 1]
/// offset = [CPU.sp + 2]
/// flags = [CPU.sp + 3]
///
/// CPU.ip = (segment:offset)
/// CPU.flags = flags
/// CPU.sp = CPU.sp + 3
/// CPU.irq = 'bitmask indicating devices with pending interrupts'
/// ```
///
/// #### Flags
///
/// |C|Z|N|O|
/// |-|-|-|-|
/// |*|*|*|*|
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
void Cpu::rti(const Instruction &rti)
{
  if (_state.sp + 3 > Cpu::STACK_OFFSET)
  {
    /// \todo throw custom exception type
    throw std::runtime_error("StackUnderflow");
  }

  _state.ip = { mem(_state.sp + 1) & 0xFF, mem(_state.sp + 2) };
  _state.flags.bits =  mem(_state.sp + 3);
  _state.sp = _state.sp + 3;

  // Update IRQ flags based on devices that still have pending interrupt requests. The service
  // routine exited by this RTI instruction should have acknowledged the its source interrupt
  // to prevent the service routine getting called again immediately after the RTI instruction.
  setIrqFlags();
}
