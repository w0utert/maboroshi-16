/// Logical shift right of destination (reg, mem, ia) over distance (reg, u16, mem, ia).
///
/// Zero bits will be shifted into the most significant bits of the result.
///
/// Notes:
///   * Shift distance is interpreted as an unsigned value.
///   * If the shift distance is zero, neither the destination register nor the CPU flags are
///     affected.
///
/// ### Mnemonic
///
/// ``lsr``
///
/// ### Opcodes
///
/// ``lsr, lsri, lsrm, lsre``
///
/// ### Addressing modes
///
///   - reg &larr; reg
///   - reg &larr; imm16
///   - reg &harr; mem
///   - reg &harr; ia
///
/// ### Syntax
///
/// <pre>lsr dst<sub>reg,mem,ia</sub>, shift<sub>reg,mem,ia,u16</sub></pre>
///
/// ### Examples
///
/// <pre>
/// lsr r0, r1
/// lsr r3, #4
/// lsr (0x01:0x0200), r5
/// lsr r0, [0x01:r1 + 2]
/// </pre>
///
/// ### Operation
///
/// ```
/// if (shift != 0)
/// {
///   u16 dst = (dst >> shift)
///
///   CPU.flags.Z = (dst == 0)
///   CPU.flags.N = 0
///   CPU.flags.O = 0
///   CPU.flags.C = 0
/// }
/// ```
///
/// ### Flags
///
/// |C|Z|N|O|
/// |-|-|-|-|
/// |0|*|0|0|
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
void Cpu::lsr(const Instruction &lsr)
{
  WORD *dst;
  WORD shift;

  std::tie(dst, shift) = getBinarySrcDstOperands(lsr);

  if ((dst != nullptr) && (shift != 0))
  {
    *dst = (*dst >> shift);

    // Update CPU flags
    _state.flags.zero = (*dst == 0);
    _state.flags.negative = 0;
    _state.flags.overflow = 0;
    _state.flags.carry = 0;
  }
}

