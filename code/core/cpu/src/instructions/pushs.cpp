/// Push CPU state.
///
/// This will push all CPU registers and flags to the stack.
///
/// ### Mnemonic
///
/// ``pushs``
///
/// ### Opcodes
///
/// ``pushs``
///
/// ### Syntax
///
/// <pre>pushs src<sub>reg</sub></pre>
///
/// ### Examples
///
/// <pre>
/// pushs
/// </pre>
///
/// ### Operation
///
/// ```
/// if ((Cpu.sp - 9) < Cpu.STACK_OFFSET - Cpu.STACK_SIZE)
/// {
///   throw StackOverflow
/// }
///
/// [Cpu.sp - 0] = Cpu.registers.r0
/// [Cpu.sp - 1] = Cpu.registers.r1
/// [Cpu.sp - 2] = Cpu.registers.r2
/// [Cpu.sp - 3] = Cpu.registers.r3
/// [Cpu.sp - 4] = Cpu.registers.r4
/// [Cpu.sp - 5] = Cpu.registers.r5
/// [Cpu.sp - 6] = Cpu.registers.r6
/// [Cpu.sp - 7] = Cpu.registers.r7
/// [Cpu.sp - 8] = u16(Cpu.flags)
///
/// Cpu.sp = Cpu.sp - 9
/// ```
///
/// ### Flags
///
/// |C|Z|N|O|
/// |-|-|-|-|
/// |-|-|-|-|
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
void Cpu::pushs(const Instruction &pushs)
{
  /// \todo this should throw a custom CPU exception type
  if ((_state.sp - 9) < (Cpu::STACK_OFFSET - Cpu::STACK_SIZE))
  {
    throw std::runtime_error("StackOverflow");
  }

  mem(_state.sp - 0) = _state.registers.r0;
  mem(_state.sp - 1) = _state.registers.r1;
  mem(_state.sp - 2) = _state.registers.r2;
  mem(_state.sp - 3) = _state.registers.r3;
  mem(_state.sp - 4) = _state.registers.r4;
  mem(_state.sp - 5) = _state.registers.r5;
  mem(_state.sp - 6) = _state.registers.r6;
  mem(_state.sp - 7) = _state.registers.r7;
  mem(_state.sp - 8) = static_cast<WORD>(_state.flags.bits);

  _state.sp = _state.sp - 9;
}