/// Signed integer divide.
///
/// This divides a signed 32-bit value by an signed 16-bit value, producing a 32-bit result and
/// a 16-bit remainder, both signed. The two registers containing the high- and low-word of the numerator
/// will be overwritten by the quotient of the division. This instruction uses the carry flag to indicate
/// that the result high-word contains any significant bits of the division result.
///
/// ### Mnemonic
///
/// ``divs``
///
/// ### Opcodes
///
/// ``divs``
///
/// ### Syntax
///
/// <pre>divs src_dst_low<sub>reg</sub>, src_dist_high<sub>reg</sub>, denominator<sub>reg</sub>, remainder<sub>reg</sub></pre>
///
/// ### Examples
///
/// <pre>
/// divs r0, r1, r5, r6
/// </pre>
///
/// ### Operation
///
/// ```
/// if (denominator == 0)
/// {
///   throw ArithmeticError
/// }
///
/// s32 numerator = ((s32) src_dst_high << 16) | src_dst_low
/// s32 quotient = numerator / (s32) denominator
///
/// u16 src_dst_low = quotient & 0xFFFF
/// u16 src_dst_high = quotient >> 16
/// s16 remainder = numerator % denominator
///
/// Cpu.Flags.Z = (quotient == 0)
/// Cpu.Flags.C = ((quotient >> 16) != 0) && ((quotient & 0xFFFF8000) != 0xFFFF8000)
/// Cpu.Flags.N = signbit(quotient)
/// Cpu.Flags.O = 0
/// ```
///
/// #### Flags
///
/// |C|Z|N|O|
/// |-|-|-|-|
/// |*|*|*|0|
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
void Cpu::divs(const Instruction &divs)
{
  const auto o0 = divs.quaternaryOperands.o0.as<Register>();
  const auto o1 = divs.quaternaryOperands.o1.as<Register>();
  const auto o2 = divs.quaternaryOperands.o2.as<Register>();
  const auto o3 = divs.quaternaryOperands.o3.as<Register>();

  const WORD numerator_low = reg(o0);
  const WORD numerator_high = reg(o1);

  const SDWORD numerator = static_cast<SDWORD>((numerator_high << 16) | numerator_low);
  const SWORD denominator = reg(o2);

  if (denominator == 0)
  {
    /// \todo this should throw a custom CPU exception type
    throw std::runtime_error("ArithmeticError");
  }

  const SDWORD quotient = numerator / denominator;
  const SWORD remainder = numerator % denominator;

  const WORD quotient_low = quotient & 0xFFFF;
  const WORD quotient_high = quotient >> 16;

  reg(o0) = quotient_low;
  reg(o1) = quotient_high;
  reg(o3) = remainder;

  _state.flags.zero = (quotient == 0);
  _state.flags.carry = (quotient_high != 0) && ((quotient & 0xFFF8000) != 0xFFF8000);
  _state.flags.negative = std::signbit<SDWORD>(quotient);
  _state.flags.overflow = false;
}
