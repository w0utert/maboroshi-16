/// Set CPU flag.
///
/// ### Mnemonic
///
/// ``set``
///
/// ### Opcodes
///
/// ``set``
///
/// ### Syntax
///
/// ``SET flags<sub>imm8,flags</sub>``
///
/// ### Examples
///
/// <pre>
/// set #2
/// set %0
/// set %A
/// set %CO
/// </pre>
///
/// ### Operation
///
/// ```
/// CPU.flags = CPU.flags | flag
/// ```
///
/// ### Flags
///
/// |C|Z|N|O|
/// |-|-|-|-|
/// |*|*|*|*|
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
void Cpu::set(const Instruction &set)
{
  _state.flags.bits |= set.unaryOperand;
}

