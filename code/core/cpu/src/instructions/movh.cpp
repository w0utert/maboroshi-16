/// Move high-byte of source (reg, imm16, mem, ia) to destination (reg, mem, ia)
///
/// Byte-move can operate in 'direct byte move' mode (move high-byte of source into
/// high byte of destination), or 'swapped byte move' mode (move high-byte of source
/// into low-byte of destination).
///
/// ### Mnemonic
///
/// ``movh``
///
/// ### Opcodes
///
/// ``movh, movhi, movhm, movhe``
///
/// ### Addressing modes
///
///   - reg &larr; reg
///   - reg &larr; imm16
///   - reg &harr; mem
///   - reg &harr; ia
///
/// ### Syntax
///
/// <pre>movh dst<sub>reg,mem,ia</sub>, src<sub>reg,imm16,mem,ia</sub></pre>, swapped<sub>bool</sub>
///
/// ### Examples
///
/// <pre>
/// movh r0, r1, true
/// movh r3, #0x1234, false
/// movh (0x01:0x0200), r5, false
/// movh r0, [0x01:r1 + 2], true
/// </pre>
///
/// ### Operation
///
/// ```
/// if (swapped)
/// {
///   dst = (dst & 0xFF00) | ((src & 0xFF) >> 8)
/// }
/// else {
///   dst = (dst & 0x00FF) | (src & 0xFF00)
/// }
///
/// CPU.flags.C = 0
/// CPU.flags.Z = (dst == 0)
/// CPU.flags.N = (dst < 0)
/// ```
///
/// ### Flags
///
/// |C|Z|N|O|
/// |-|-|-|-|
/// |0|*|*|-|
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
void Cpu::movh(const Instruction &movh)
{
  WORD *dst;
  WORD value;

  std::tie(dst, value) = getBinarySrcDstOperands(movh);

  if (dst != nullptr)
  {
    const bool swapped = movh.binaryOperands.modifier;
    const WORD result = (swapped ? (*dst & 0xFF00) | ((value & 0xFF00) >> 8) : (*dst & 0x00FF) | (value & 0xFF00));

    *dst = result;

    // Update CPU flags
    _state.flags.zero = (result == 0);
    _state.flags.negative = std::signbit<SWORD>(result);
    _state.flags.overflow = false;
    _state.flags.carry = false;
  }
}

