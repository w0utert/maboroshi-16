/// Unsigned integer multiply.
///
/// This multiplies the 16-bit values of the lhs and rhs register operand,
/// and stores the 32-bit result in the low-, and high result registers. The
/// source and destination operands are interpreted as unsigned. The CPU carry
/// flag is used to indicate the high-word of the multiplication result is
/// non-zero.
///
/// ### Mnemonic
///
/// ``mul``
///
/// ### Opcodes
///
/// ``mul``
///
/// ### Syntax
///
/// <pre>mul lhs<sub>reg</sub>, rhs<sub>reg</sub>, result_low<sub>reg</sub>, result_high<sub>reg</sub></pre>
///
/// ### Examples
///
/// <pre>
/// mul r0, r1, r5, r6
/// </pre>
///
/// ### Operation
///
/// ```
/// u32 p = u32(o0) * u32(o1)
///
/// u16 result_low = p & 0xFFFF
/// u16 result_high = p >> 16
///
/// Cpu.flags.Z = (p == 0)
/// Cpu.flags.C = ((p & 0xFFFF0000) != 0)
/// Cpu.flags.N = 0
/// Cpu.flags.O = 0
/// ```
///
/// #### Flags
///
/// |C|Z|N|O|
/// |-|-|-|-|
/// |*|*|0|0|
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
void Cpu::mul(const Instruction &mul)
{
  const auto o0 = mul.quaternaryOperands.o0.as<Register>();
  const auto o1 = mul.quaternaryOperands.o1.as<Register>();
  const auto o2 = mul.quaternaryOperands.o2.as<Register>();
  const auto o3 = mul.quaternaryOperands.o3.as<Register>();

  const uint32_t lhs = reg(o0);
  const uint32_t rhs = reg(o1);

  const uint32_t result = lhs * rhs;

  reg(o2) = static_cast<WORD>(result & 0xFFFF);
  reg(o3) = static_cast<WORD>((result >> 16) & 0xFFFF);

  _state.flags.zero = (result == 0);
  _state.flags.carry = ((result & 0xFFFF0000) != 0);
  _state.flags.negative = false;
  _state.flags.overflow = false;
}

