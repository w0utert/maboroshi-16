/// Push register.
///
/// ### Mnemonic
///
/// ``push``
///
/// ### Opcodes
///
/// ``push``
///
/// ### Syntax
///
/// <pre>push src<sub>reg</sub></pre>
///
/// ### Examples
///
/// <pre>
/// push r0
/// </pre>
///
/// ### Operation
///
/// ```
/// if (Cpu.sp == Cpu.STACK_OFFSET - Cpu.STACK_SIZE)
/// {
///   throw StackOverflow
/// }
///
/// [Cpu.sp] = src
///
/// Cpu.sp = Cpu.sp - 1
/// ```
///
/// ### Flags
///
/// |C|Z|N|O|
/// |-|-|-|-|
/// |-|-|-|-|
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
void Cpu::push(const Instruction &push)
{
  /// \todo this should throw a custom CPU exception type
  if (_state.sp == (Cpu::STACK_OFFSET - Cpu::STACK_SIZE))
  {
    throw std::runtime_error("StackOverflow");
  }

  mem(_state.sp) = reg(push.unaryOperand.as<Register>());

  _state.sp = _state.sp - 1;
}