/// Pop CPU state.
///
/// This will pop the CPU registers and flags from the stack, in the order pushed
/// by the PUSHS instruction.
///
/// ### Mnemonic
///
/// ``pops``
///
/// ### Opcodes
///
/// ``pops``
///
/// ### Syntax
///
/// <pre>pops dst<sub>reg</sub></pre>
///
/// ### Examples
///
/// <pre>
/// pops
/// </pre>
///
/// ### Operation
///
/// ```
/// if (Cpu.sp + 9 > Cpu.STACK_OFFSET)
/// {
///   throw StackUnderflow
/// }
///
/// Cpu.flags = u8([Cpu.sp + 1] & 0x00FF)
/// Cpu.registers.r7 = [Cpu.sp + 2]
/// Cpu.registers.r6 = [Cpu.sp + 3]
/// Cpu.registers.r5 = [Cpu.sp + 4]
/// Cpu.registers.r4 = [Cpu.sp + 5]
/// Cpu.registers.r3 = [Cpu.sp + 6]
/// Cpu.registers.r2 = [Cpu.sp + 7]
/// Cpu.registers.r1 = [Cpu.sp + 8]
/// Cpu.registers.r0 = [Cpu.sp + 9]
///
/// Cpu.sp = Cpu.sp + 9
/// ```
///
/// ### Flags
///
/// |C|Z|N|O|
/// |-|-|-|-|
/// |-|-|-|-|
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
void Cpu::pops(const Instruction &pops)
{
  /// \todo this should throw a custom CPU exception type
  if ((_state.sp + 9) > Cpu::STACK_OFFSET)
  {
    throw std::runtime_error("StackUnderflow");
  }

  _state.flags.bits = static_cast<BYTE>(mem(_state.sp + 1) & 0x00FF);

  _state.registers.r7 = mem(_state.sp + 2);
  _state.registers.r6 = mem(_state.sp + 3);
  _state.registers.r5 = mem(_state.sp + 4);
  _state.registers.r4 = mem(_state.sp + 5);
  _state.registers.r3 = mem(_state.sp + 6);
  _state.registers.r2 = mem(_state.sp + 7);
  _state.registers.r1 = mem(_state.sp + 8);
  _state.registers.r0 = mem(_state.sp + 9);

  _state.sp = _state.sp + 9;
}
