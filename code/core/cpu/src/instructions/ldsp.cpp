/// Load special purpose register.
///
/// This instruction loads the stack point (sp) or the segment register (sr) into a
/// general-purpose register. Does not affect any CPU flags.
///
/// ### Mnemonic
///
/// ``ldsp``
///
/// ### Opcodes
///
/// ``ldsp``
///
/// ### Syntax
///
/// <pre>ldsp dst<sub>reg</sub>, src<sub>special purpose register</sub></pre>
///
/// ### Examples
///
/// ``ldsp r0, sp``
/// ``ldsp r1, sr``
///
/// ### Operation
///
/// ```
/// if (src == sp)
/// {
///   dst = CPU.sp
/// }
/// else if (src == sr)
/// {
///   dst = CPU.sr
/// }
/// ```
///
/// ### Flags
///
/// |C|Z|N|O|
/// |-|-|-|-|
/// |-|-|-|-|
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
void Cpu::ldsp(const Instruction &ldsp)
{
  const auto dst = ldsp.binaryOperands.o0.as<Register>();
  const auto src = ldsp.binaryOperands.o1.as<SpecialPurposeRegister>();

  switch (src)
  {
    case SpecialPurposeRegister::SP:
      reg(dst) = _state.sp;
      break;
    case SpecialPurposeRegister::SR:
      reg(dst) = _state.sr;
      break;
  }
}

