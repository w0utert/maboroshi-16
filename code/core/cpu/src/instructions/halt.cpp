/// Halt processor.
///
/// This instruction is for debugging purposes only. When the CPU executes a HALT instruction, it
/// will throw a core::cpu::Halted exception, which can be trapped by client code to e.g. inspect
/// CPU/memory state, terminate the program, etc. After executing a HALT instruction, program
/// execution can be resumed normally using the standard CPU clock functions core::Cpu::tick(), etc.
///
/// ### Mnemonic
///
/// ``halt```
///
/// ### Opcodes
///
/// ``halt``
///
/// ### Syntax
///
/// <pre>HALT</pre>
///
/// ### Examples
///
/// <pre>
/// halt
/// </pre>
///
/// ### Operation
///
/// ```
/// throw Halted()
/// ```
///
/// ### Flags
///
/// |C|Z|N|O|
/// |-|-|-|-|
/// |-|-|-|-|
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
void Cpu::halt(const Instruction &halt)
{
  throw Halted();
}

