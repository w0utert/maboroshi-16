/// Read I/O port.
///
/// ### Mnemonic
///
/// ``in``
///
/// ### Opcodes
///
/// ``in``
///
/// ### Syntax
///
/// <pre>in device<sub>imm4</sub>, port<sub>imm9</sub>, dst<sub>reg</sub>
/// <pre>in device<sub>imm4</sub>, port<sub>reg</sub>, dst<sub>reg</sub>
///
/// ### Examples
///
/// <pre>
/// in 0x04, 0x1000, r0
/// in 0x04, r2, r1
/// </pre>
///
/// ### Operation
///
/// ```
/// if (type(port) == imm9)
/// {
///   *dst = $(device, port)
/// }
/// else
/// {
///   if (*port > imm9.max)
///   {
///     throw BusError;
///   }
///
///   *dst = $(device, *port)
/// }
/// ```
///
/// ### Flags
///
/// |C|Z|N|O|
/// |-|-|-|-|
/// |0|*|*|0|
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
void Cpu::in(const Instruction &in)
{
  const bool is_indirect = (*in.opcode == Opcode::INR);

  const WORD port = (is_indirect ? reg(in.portMove.port.as<Register>()) : in.portMove.port);

  const peripheral::Device *device = (in.portMove.device < _devices.size() ? _devices[in.portMove.device] : nullptr);

  if ((port >= (1 << PORT_BITS)) || (device == nullptr))
  {
    throw std::runtime_error(fmt::format("Bus error: read from invalid device port {0:#04x}:{1:#06x}", in.portMove.device.as<BYTE>(), port));
  }

  const WORD v = device->in(port);

  reg(in.portMove.reg) = v;

  _state.flags.zero = (v == 0);
  _state.flags.carry = false;
  _state.flags.negative = std::signbit<SDWORD>(v);
  _state.flags.overflow = false;
}

