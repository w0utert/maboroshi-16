/// Move low-byte of source (reg, imm16, mem, ia) to destination (reg, mem, ia)
///
/// Byte-move can operate in 'direct byte move' mode (move low-byte of source into
/// low byte of destination), or 'swapped byte move' mode (move low-byte of source
/// into high-byte of destination).
///
/// ### Mnemonic
///
/// ``movl``
///
/// ### Opcodes
///
/// ``movl, movli, movlm, movle``
///
/// ### Addressing modes
///
///   - reg &larr; reg
///   - reg &larr; imm16
///   - reg &harr; mem
///   - reg &harr; ia
///
/// ### Syntax
///
/// <pre>movl dst<sub>reg,mem,ia</sub>, src<sub>reg,imm16,mem,ia</sub></pre>, swapped<sub>bool</sub>
///
/// ### Examples
///
/// <pre>
/// movl r0, r1, true
/// movl r3, #0x1234, false
/// movl (0x01:0x0200), r5, false
/// movl r0, [0x01:r1 + 2], true
/// </pre>
///
/// ### Operation
///
/// ```
/// if (swapped)
/// {
///   dst = (dst & 0x00FF) | ((src & 0x00FF) << 8)
/// }
/// else {
///   dst = (dst & 0xFF00) | (src & 0x00FF)
/// }
///
/// CPU.flags.C = 0
/// CPU.flags.Z = (dst == 0)
/// CPU.flags.N = (dst < 0)
/// ```
///
/// ### Flags
///
/// |C|Z|N|O|
/// |-|-|-|-|
/// |0|*|*|-|
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
void Cpu::movl(const Instruction &movl)
{
  WORD *dst;
  WORD value;

  std::tie(dst, value) = getBinarySrcDstOperands(movl);

  if (dst != nullptr)
  {
    const bool swapped = movl.binaryOperands.modifier;
    const WORD result = (swapped ? (*dst & 0x00FF) | ((value & 0x00FF) << 8) : (*dst & 0xFF00) | (value & 0x00FF));

    *dst = result;

    // Update CPU flags
    _state.flags.zero = (result == 0);
    _state.flags.negative = std::signbit<SWORD>(result);
    _state.flags.overflow = false;
    _state.flags.carry = false;
  }
}

