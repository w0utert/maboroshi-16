/// Unsigned integer divide.
///
/// This divides an unsigned 32-bit value by an unsigned 16-bit value, producing a 32-bit result and
/// a 16-bit remainder. The two registers containing the high- and low-word of the numerator will be
/// overwritten by the quotient of the division. This instruction uses the carry flag to indicate that
/// the result high-word is non-zero (ie: contains any significant bits of the division result).
///
/// ### Mnemonic
///
/// ``div``
///
/// ### Opcodes
///
/// ``div``
///
/// ### Syntax
///
/// <pre>div src_dst_low<sub>reg</sub>, src_dist_high<sub>reg</sub>, denominator<sub>reg</sub>, remainder<sub>reg</sub></pre>
///
/// ### Examples
///
/// <pre>
/// div r0, r1, r5, r6
/// </pre>
///
/// ### Operation
///
/// ```
/// if (denominator == 0)
/// {
///   throw ArithmeticError
/// }
///
/// u32 numerator = ((u32) src_dst_high << 16) | src_dst_low
/// u32 quotient = numerator / (u32) denominator
///
/// u16 src_dst_low = quotient & 0xFFFF
/// u16 src_dst_high = quotient >> 16
/// u16 remainder = numerator % denominator
///
/// Cpu.Flags.Z = (quotient == 0)
/// Cpu.Flags.C = ((quotient >> 16) != 0)
/// Cpu.Flags.N = 0
/// Cpu.Flags.O = 0
/// ```
///
/// #### Flags
///
/// |C|Z|N|O|
/// |-|-|-|-|
/// |*|*|0|0|
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
void Cpu::div(const Instruction &div)
{
  const auto o0 = div.quaternaryOperands.o0.as<Register>();
  const auto o1 = div.quaternaryOperands.o1.as<Register>();
  const auto o2 = div.quaternaryOperands.o2.as<Register>();
  const auto o3 = div.quaternaryOperands.o3.as<Register>();

  const WORD numerator_low = reg(o0);
  const WORD numerator_high = reg(o1);

  const DWORD numerator = (static_cast<DWORD>(numerator_high) << 16) | numerator_low;
  const WORD denominator = reg(o2);

  if (denominator == 0)
  {
    /// \todo this should throw a custom CPU exception type
    throw std::runtime_error("ArithmeticError");
  }

  const DWORD quotient = numerator / denominator;
  const WORD remainder = numerator % denominator;

  const WORD quotient_low = static_cast<WORD>(quotient & 0xFFFF);
  const WORD quotient_high = static_cast<WORD>(quotient >> 16);

  reg(o0) = quotient_low;
  reg(o1) = quotient_high;
  reg(o3) = remainder;

  _state.flags.zero = (quotient == 0);
  _state.flags.carry = (quotient_high != 0);
  _state.flags.negative = false;
  _state.flags.overflow = false;
}

