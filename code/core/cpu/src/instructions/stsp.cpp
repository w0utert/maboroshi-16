/// Restore special purpose register.
///
/// This instruction restores the stack point (sp) or the segment register (sr) from a
/// general-purpose register. Does not affect any CPU flags.
///
/// ### Mnemonic
///
/// ``stsp``
///
/// ### Opcodes
///
/// ``stsp``
///
/// ### Syntax
///
/// <pre>stsp dst<sub>special purpose register</sub>, src<sub>reg</sub></pre>
///
/// ### Examples
///
/// ``stsp sp, r0``
/// ``stsp sr, r1``
///
/// ### Operation
///
/// ```
/// if (dst == sp)
/// {
///   if (src < Cpu.STACK_OFFSET - Cpu::STACK_SIZE)
///   {
///     throw StackUnderflow
///   }
///
///   CPU.sp = src
/// }
/// else
/// {
///   CPU.sr = src
/// }
///
/// ```
///
/// ### Flags
///
/// |C|Z|N|O|
/// |-|-|-|-|
/// |-|-|-|-|
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
void Cpu::stsp(const Instruction &stsp)
{
  const auto dst = stsp.binaryOperands.o0.as<SpecialPurposeRegister>();
  const auto src = stsp.binaryOperands.o1.as<Register>();

  switch (dst)
  {
    case SpecialPurposeRegister::SP:
    {
      const WORD new_sp = reg(src);

      if (new_sp < Cpu::STACK_OFFSET - Cpu::STACK_SIZE)
      {
        throw std::runtime_error("StackUnderflow");
      }

      _state.sp = new_sp;
      break;
    }

    case SpecialPurposeRegister::SR:
    {
      _state.sr = reg(src);
      break;
    }
  }
}