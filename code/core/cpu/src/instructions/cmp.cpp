/// Compare lhs value (reg, mem, ia) value to rhs value (reg, imm16, mem, ia).
///
/// The result of this operation is that the CPU flags will be set according to the
/// result of subtracting the rhs value from the lhs value.
///
/// ### Mnemonic
///
/// ``cmp``
///
/// ### Opcodes
///
/// ``cmp, cmpi, cmpm, cmpe``
///
/// ### Addressing modes
///
///   - reg &larr; reg
///   - reg &larr; imm16
///   - reg &harr; mem
///   - reg &harr; ia
///
/// ### Syntax
///
/// <pre>cmp dst<sub>reg,mem,ia</sub>, rhs<sub>reg,imm16,mem,ia</sub></pre>
///
/// ### Examples
///
/// <pre>
/// cmp r0, r1
/// cmp r1, #0x6969
/// cmp (0x01:0x0200), r5
/// cmp r0, [0x01:r1 + 2]
/// </pre>
///
/// ### Operation
///
/// ```
/// diff = dst - rhs
/// CPU.flags.C = (rhs > dst)
/// CPU.flags.Z = (dst == rhs)
/// CPU.flags.N = signbit(dst - rhs)
/// CPU.flags.O = (signbit(dst) != signbit(rhs)) && (signbit(dst) != signbit(dst - rhs))
/// ```
///
/// ### Flags
///
/// |C|Z|N|O|
/// |-|-|-|-|
/// |*|*|*|*|
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
void Cpu::cmp(const Instruction &cmp)
{
  WORD *lhs;
  WORD rhs;

  std::tie(lhs, rhs) = getBinarySrcDstOperands(cmp);

  if (lhs != nullptr)
  {
    const WORD diff = *lhs - rhs;

    const bool lhs_sign = std::signbit<int16_t>(*lhs);
    const bool rhs_sign = std::signbit<int16_t>(rhs);
    const bool diff_sign = std::signbit<int16_t>(diff);

    _state.flags.zero = (diff == 0);
    _state.flags.negative = diff_sign;
    _state.flags.carry = (rhs > *lhs);
    _state.flags.overflow = (lhs_sign != rhs_sign) && (lhs_sign != diff_sign);
  }
}
