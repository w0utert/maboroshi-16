/// Negate register.
///
/// If the value of the source register is 0x8000 (-32768) its negation is
/// not representable in 16 bits, in which case the source register is
/// unmodified and the overflow bit is set.
///
/// ### Mnemonic
///
/// ``neg``
///
/// ### Opcodes
///
/// ``neg``
///
/// ### Syntax
///
/// <pre>neg src_dst<sub>reg</sub></pre>
///
/// ### Examples
///
/// <pre>
/// neg r0
/// </pre>
///
/// ### Operation
///
/// ```
/// s16 src = src_dst
///
/// if (src != 0x8000)
/// {
///   src_dst = -src_dst
/// }
///
/// Cpu.flags.Z = (src_dst == 0)
/// Cpu.flags.N = (src_dst < 0)
/// Cpu.flags.O = (src == 0x8000)
/// Cpu.flags.C = 0
/// ```
///
/// ### Flags
///
/// |C|Z|N|O|
/// |-|-|-|-|
/// |0|*|*|*|
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
void Cpu::neg(const Instruction &neg)
{
  const auto r = neg.unaryOperand.as<Register>();

  const SWORD value = static_cast<SWORD>(reg(r));
  const SWORD negated_value = -value;

  reg(r) = static_cast<WORD>(-value);

  _state.flags.zero = (negated_value == 0);
  _state.flags.negative = (negated_value < 0);
  _state.flags.overflow = (static_cast<WORD>(value) == 0x8000);
  _state.flags.carry = 0;
}

