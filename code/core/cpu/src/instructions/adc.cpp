/// Add value (reg, imm16, mem, ia) and carry flag to destination (reg, mem, ia).
///
/// This operation is usually used to add 32-bit values by first adding the low-words using
/// ADD, then adding the high-words using ADC.
///
/// ### Mnemonic
///
/// ``adc``
///
/// ### Opcodes
///
/// ``adc, adci, adcm, adce``
///
/// ### Addressing modes
///
///   - reg &larr; reg
///   - reg &larr; imm16
///   - reg &harr; mem
///   - reg &harr; ia
///
/// ### Syntax
///
/// <pre>adc dst<sub>reg,mem,ia</sub>, value<sub>reg,imm16,mem,ia</sub></pre>
///
/// ### Examples
///
/// <pre>
/// adc r0, r1
/// adc r3, #0x1234
/// adc (0x01:0x0200), r5
/// adc r0, [0x01:r1 + 2]
/// </pre>
///
/// ### Operation
///
/// ```
/// u16 dst_before = dst
/// u32 dst_32 = u32(dst) + u32(value) + (CPU.flags.C ? 1 : 0)
/// u16 dst = dst_32 & 0xFFFF
///
/// CPU.flags.C = (dst_32 > 0xFFFF)
/// CPU.flags.Z = (dst == 0)
/// CPU.flags.N = signbit(dst)
/// CPU.flags.O = (signbit(dst_before) == signbit(value)) && (signbit(dst) != signbit(dst_before))
/// ```
///
/// ### Flags
///
/// |C|Z|N|O|
/// |-|-|-|-|
/// |0|*|*|*|
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
void Cpu::adc(const Instruction &adc)
{
  WORD *dst;
  WORD value;

  std::tie(dst, value) = getBinarySrcDstOperands(adc);

  if (dst != nullptr)
  {
    const WORD dst_before = *dst;
    const DWORD result_32 = static_cast<DWORD>(*dst) + static_cast<DWORD>(value) + (_state.flags.carry ? 1 : 0);
    const WORD result = static_cast<WORD>(result_32 & 0xFFFF);

    *dst = result;

    // Update CPU flags
    _state.flags.zero = (result == 0);
    _state.flags.negative = std::signbit<SWORD>(result);
    _state.flags.overflow =
      (std::signbit<SWORD>(dst_before) == std::signbit<SWORD>(value)) &&
      (std::signbit<SWORD>(result) != std::signbit<SWORD>(dst_before));
    _state.flags.carry = (result_32 > 0xFFFF);
  }
}

