/// Logical XOR of destination (reg, mem, ia) and value (reg, imm16, mem, ia).
///
/// ### Mnemonic
///
/// ``xor``
///
/// ### Opcodes
///
/// ``xor, xori, xorm, xore``
///
/// ### Addressing modes
///
///   - reg &larr; reg
///   - reg &larr; imm16
///   - reg &harr; mem
///   - reg &harr; ia
///
/// ### Syntax
///
/// <pre>xor dst<sub>reg,mem,ia</sub>, value<sub>reg,imm16,mem,ia</sub></pre>
///
/// ### Examples
///
/// <pre>
/// xor r0, r1
/// xor r3, #0x1234
/// xor (0x01:0x0200), r5
/// xor r0, [0x01:r1 + 2]
/// </pre>
///
/// ### Operation
///
/// ```
/// dst = dst ^ value
///
/// CPU.flags.C = 0
/// CPU.flags.Z = (dst == 0)
/// CPU.flags.N = signbit(dst)
/// CPU.flags.O = 0
/// ```
///
/// ### Flags
///
/// |C|Z|N|O|
/// |-|-|-|-|
/// |0|*|*|0|
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
void Cpu::xor_(const Instruction &xor_)
{
  WORD *dst;
  WORD value;

  std::tie(dst, value) = getBinarySrcDstOperands(xor_);

  if (dst != nullptr)
  {
    const WORD result = (*dst ^ value);

    *dst = result;

    // Update CPU flags
    _state.flags.zero = (result == 0);
    _state.flags.negative = std::signbit<SWORD>(result);
    _state.flags.overflow = 0;
    _state.flags.carry = 0;
  }
}

