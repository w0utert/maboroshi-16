/// Reserve stack frame.
///
/// ### Mnemonic
///
/// ``rsv``
///
/// ### Opcodes
///
/// ``rsv``
///
/// ### Syntax
///
/// <pre>rsv size<sub>imm16</sub></pre>
///
/// ### Examples
///
/// ``rsv #100``
///
/// ### Operation
///
/// ```
/// if (Cpu.sp - size < Cpu.STACK_OFFSET - Cpu.STACK_SIZE)
/// {
///   throw StackOverflow
/// }
///
/// Cpu.sp = Cpu.sp - size
/// ```
///
/// ### Flags
///
/// |C|Z|N|O|
/// |-|-|-|-|
/// |-|-|-|-|
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
void Cpu::rsv(const Instruction &rsv)
{
  const WORD size = rsv.immediate;

  /// \todo this should throw a custom CPU exception type
  if (_state.sp - size < (Cpu::STACK_OFFSET - Cpu::STACK_SIZE))
  {
    throw std::runtime_error("StackOverflow");
  }

  _state.sp = _state.sp - size;
}

