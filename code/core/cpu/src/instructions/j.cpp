/// Unconditional jump to direct memory location or effective address.
///
/// ### Mnemonic
///
/// ``j``
///
/// ### Opcodes
///
/// ``jm, jv``
///
/// ### Syntax
///
/// <pre>J dst<sub>mem,vec</sub></pre>
///
/// ### Examples
///
/// <pre>
/// j (0x01:0x0200)
/// j <0x00:0x0400>
/// </pre>
///
/// ### Operation
///
/// ```
/// CPU.ip = dst
/// ```
///
/// #### Flags
///
/// |C|Z|N|O|
/// |-|-|-|-|
/// |-|-|-|-|
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
void Cpu::j(const Instruction &j)
{
  if (*j.opcode == Opcode::JM)
  {
    // Direct jump to memory location
    _state.ip = j.longPointer();
  }
  else if (j.opcode == Opcode::JV)
  {
    // Indirect jump to vector
    const LongPointer vector = j.longPointer();

    const WORD offset = mem(vector);
    const WORD segment = mem(vector + 1);

    _state.ip = LongPointer(segment, offset);
  }
}
