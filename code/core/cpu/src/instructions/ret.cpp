/// Return from subroutine.
///
/// ### Mnemonic
///
/// ``ret``
///
/// ### Opcodes
///
/// ``ret``
///
/// ### Syntax
///
/// ``ret``
///
/// ### Examples
///
/// ``ret``
///
/// ### Operation
///
/// ```
/// if (CPU.sp + 2 > STACK_OFFSET)
/// {
///   throw StackUnderflow
/// }
///
/// segment = [CPU.sp + 1]
/// offset = [CPU.sp + 2]
///
/// CPU.ip = (segment:offset)
/// CPU.sp = CPU.sp + 2
/// ```
///
/// #### Flags
///
/// |C|Z|N|O|
/// |-|-|-|-|
/// |-|-|-|-|
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
void Cpu::ret(const Instruction &ret)
{
  if (_state.sp + 2 > Cpu::STACK_OFFSET)
  {
    /// \todo throw custom exception type
    throw std::runtime_error("StackUnderflow");
  }

  _state.ip = { mem(_state.sp + 1) & 0xFF, mem(_state.sp + 2) };
  _state.sp = _state.sp + 2;
}
