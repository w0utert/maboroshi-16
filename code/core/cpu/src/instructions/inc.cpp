/// Increase operand.
///
/// The operand that is used as both the source and destination of the increment instruction is
/// interpreted as a signed value, and CPU flags N, Z and O are set according to the signed result.
///
/// ### Mnemonic
///
/// ``inc``
///
/// ### Opcodes
///
/// ``inc, incm, ince``
///
/// ### Syntax
///
/// <pre>inc src_dst<sub>reg,mem,ia</sub></pre>
///
/// ### Examples
///
/// <pre>
/// inc r0
/// inc r1, (0x01:0x0200)
/// inc r2, [0x01:r1 + 2]
/// </pre>
///
/// ### Operation
///
/// ```
/// s16 src = src_dst
///
/// src_dst = src_dst + 1
///
/// Cpu.flags.Z = (src_dst == 0)
/// Cpu.flags.N = (src_dst < 0)
/// Cpu.flags.O = !signbit(src) && signbit(dst)
/// ```
///
/// ### Flags
///
/// |C|Z|N|O|
/// |-|-|-|-|
/// |-|*|*|*|
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
void Cpu::inc(const Instruction &inc)
{
  WORD * const src_dst = getUnarySrcDstOperands(inc);

  const WORD value = *src_dst;
  const WORD result = value + 1;

  *src_dst = result;

  _state.flags.zero = (result == 0);
  _state.flags.negative = std::signbit<SWORD>(result);
  _state.flags.overflow = !std::signbit<SWORD>(value) && std::signbit<SWORD>(result);
}

