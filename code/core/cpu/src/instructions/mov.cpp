/// Move value from source (reg, imm16, mem, ia) to destination (reg, mem, ia).
///
/// ### Mnemonic
///
/// ``mov``
///
/// ### Opcodes
///
/// ``mov, movi, movm, move``
///
/// ### Addressing modes
///
///   - reg &larr; reg
///   - reg &larr; imm16
///   - reg &harr; mem
///   - reg &harr; ia
///
/// ### Syntax
///
/// <pre>mov dst<sub>reg,mem,ia</sub>, src<sub>reg,imm16,mem,ia</sub></pre>
///
/// ### Examples
///
/// <pre>
/// mov r0, r1
/// mov r3, #0x1234
/// mov (0x01:0x0200), r5
/// mov r0, [0x01:r1 + 2]
/// </pre>
///
/// ### Operation
///
/// ```
/// dst = src
///
/// CPU.flags.C = 0
/// CPU.flags.Z = (dst == 0)
/// CPU.flags.N = (dst < 0)
/// ```
///
/// ### Flags
///
/// |C|Z|N|O|
/// |-|-|-|-|
/// |0|*|*|-|
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
void Cpu::mov(const Instruction &mov)
{
  WORD *dst;
  WORD value;

  std::tie(dst, value) = getBinarySrcDstOperands(mov);

  if (dst != nullptr)
  {
    *dst = value;

    // Update CPU flags
    _state.flags.zero = (value == 0);
    _state.flags.negative = std::signbit<SWORD>(value);
    _state.flags.overflow = false;
    _state.flags.carry = false;
  }
}

