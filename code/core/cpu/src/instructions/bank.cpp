/// Switch bank (map device RAM page into a page of CPU-visible address space).
///
/// ### Mnemonic
///
/// ``bank``
///
/// ### Opcodes
///
/// ``bank``
///
/// ### Syntax
///
/// <pre>bank device_src_dst<sub>reg</sub>
///
/// ### Examples
///
/// <pre>
/// bank 0x02, r0
/// </pre>
///
/// ### Operation
///
/// ```
/// u8 device = *device_src_dst[8:15]
/// u4 src_page = *device_src_dst[4:7]
/// u4 dst_page = *device_src_dst[0:3]
///
/// if ('invalid device page, or destination page')
/// {
///   throw BusError
/// }
///
/// 'map device page into destination page in CPU-visible address space'
/// ```
///
/// ### Flags
///
/// |C|Z|N|O|
/// |-|-|-|-|
/// |-|-|-|-|
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
void Cpu::bank(const Instruction &bank)
{
  const WORD device_src_dst = reg(bank.unaryOperand.as<Register>());

  const auto device = static_cast<BYTE>((device_src_dst & 0xFF00) >> 8);
  const auto src_page = static_cast<BYTE>((device_src_dst & 0x00F0) >> 4);
  const auto dst_page = static_cast<BYTE>(device_src_dst & 0x000F);

  switchBank(device, src_page, dst_page);
}

