/// Call subroutine.
///
/// ### Mnemonic
///
/// ``call``
///
/// ### Opcodes
///
/// ``callm, callv``
///
/// ### Syntax
///
/// <pre>call dst<sub>mem,vec</sub></pre>
///
/// ### Examples
///
/// <pre>
/// call (0x01:0x0200)
/// call <0x01:0x8000>
/// </pre>
///
/// ### Operation
///
/// ```
/// if (CPU.sp - 2 < Cpu.STACK_OFFSET - Cpu.STACK_SIZE)
/// {
///   throw StackOverflow
/// }
///
/// [CPU.sp] = CPU.ip:offset
/// [CPU.sp - 1] = CPU.ip:segment
///
/// CPU.sp = CPU.sp - 2
/// CPU.ip = dst
/// ```
///
/// #### Flags
///
/// |C|Z|N|O|
/// |-|-|-|-|
/// |-|-|-|-|
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
void Cpu::call(const Instruction &call)
{
  if (_state.sp - 2 < (Cpu::STACK_OFFSET - Cpu::STACK_SIZE))
  {
    /// \todo throw custom exception type
    throw std::runtime_error("StackOverflow");
  }

  mem(_state.sp) = _state.ip.offset;
  mem(_state.sp - 1) = _state.ip.segment;

  _state.sp = _state.sp - 2;

  if (call.opcode == Opcode::CALLM)
  {
    _state.ip = call.longPointer();
  }
  else if (call.opcode == Opcode::CALLV)
  {
    const LongPointer vector = call.longPointer();

    const WORD segment = mem(vector);
    const WORD offset = mem(vector + 1);

    _state.ip = LongPointer(segment, offset);
  }
}
