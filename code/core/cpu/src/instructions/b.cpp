/// Branch if condition encoded in instruction evaluates to true.
///
/// The evaluation of the branch condition is fully determined by the state of
/// the CPU flags, as set by preceding instructions, and can be any of the following:
///
///   - ``z``: branch if equal/zero flag set (``Z``)
///   - ``nz``: branch if not equal/zero flag clear (``!Z``)
///   - ``o``: branch if overflow flag set (``O``)
///   - ``no``: branch if overflow flag clear (``!O``)
///   - ``c``: branch if carry flag set (``C``)
///   - ``nc``: branch if carry flag clear (``!C``)
///   - ``g``: branch if greater than, signed (``N,O,!Z`` or ``!N,!O,!Z``)
///   - ``ge``: branch if greater than or equal, signed (``N,O`` or ``!N,!O``)
///   - ``l``: branch if less than or equal, signed (N,!V or ``!N,V``)
///   - ``le``: branch if less than or equal, signed (``Z`` or ``N,!V`` or ``!N,V``)
///   - ``ug``: branch if greater than, unsigned (``!C,!Z``)
///   - ``uge``: branch if greater than or equal, unsigned (``!C``)
///   - ``ul``: branch if less than, unsigned (``C``)
///   - ``ule``: branch if less than or equal, unsigned (``C,Z``)
///
/// As can be seen from the condition table, there are some duplicate conditions, namely
/// ``c == ul`` and ``nc == ule``. For simplicity and clarity they are
/// encoded in the opcode using different bit patterns.
///
/// The branch distance is encoded as a signed 16-bit immediate, indicating the branch
/// distance as a number of instructions and the branch direction. Note that instructions
/// are always encoded using 2 machine words, so the CPU instruction pointer will be
/// offset by the branch distance multiplied by two in case the branch is executed.
///
/// ### Mnemonic
///
/// ``b``
///
/// ### Opcodes
///
/// ``b``
///
/// ### Syntax
///
/// <pre>b condition, distance<sub>imm16s</sub></pre>
///
/// ### Examples
///
/// <pre>
/// b nz, +120
/// b ule, -100
/// </pre>
///
/// ### Operation
///
/// ```
/// if (condition)
/// {
///   CPU.ip = CPU.ip + distance * 2
/// }
/// ```
///
/// ### Flags
///
/// |C|Z|N|O|
/// |-|-|-|-|
/// |-|-|-|-|
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
void Cpu::b(const Instruction &b)
{
  // Branch table that indicates for all possible CPU flag states, which of
  // the supported branch conditions are satisfied. The decision to branch or
  // not branch can be made by simply looking up the combination of the CPU
  // flags and the branch condition encoded in the instructions.
  static const BYTE BRANCH_TABLE[16][16] =
  {
    // Z,  NZ,   O,  NO,   C,  NC,   G,  GE,   L,  LE,  UG, UGE,  UL, ULE,   X,   X
    {  0,   1,   0,   1,   0,   1,   1,   1,   0,   0,   1,   1,   0,   0,   0,   0 }, // ----
    {  0,   1,   0,   1,   1,   0,   1,   1,   0,   0,   0,   0,   1,   1,   0,   0 }, // ---C
    {  1,   0,   0,   1,   0,   1,   0,   1,   0,   1,   0,   1,   0,   1,   0,   0 }, // --Z-
    {  1,   0,   0,   1,   1,   0,   0,   1,   0,   1,   0,   0,   1,   1,   0,   0 }, // --ZC
    {  0,   1,   0,   1,   0,   1,   0,   0,   1,   1,   1,   1,   0,   0,   0,   0 }, // -N--
    {  0,   1,   0,   1,   1,   0,   0,   0,   1,   1,   0,   0,   1,   1,   0,   0 }, // -N-C
    {  1,   0,   0,   1,   0,   1,   0,   0,   1,   1,   0,   1,   0,   1,   0,   0 }, // -NZ-
    {  1,   0,   0,   1,   1,   0,   0,   0,   1,   1,   0,   0,   1,   1,   0,   0 }, // -NZC
    {  0,   1,   1,   0,   0,   1,   0,   0,   1,   1,   1,   1,   0,   0,   0,   0 }, // O---
    {  0,   1,   1,   0,   1,   0,   0,   0,   1,   1,   0,   0,   1,   1,   0,   0 }, // O--C
    {  1,   0,   1,   0,   0,   1,   0,   0,   1,   1,   0,   1,   0,   1,   0,   0 }, // O-Z-
    {  1,   0,   1,   0,   1,   0,   0,   0,   1,   1,   0,   0,   1,   1,   0,   0 }, // O-ZC
    {  0,   1,   1,   0,   0,   1,   1,   1,   0,   0,   1,   1,   0,   0,   0,   0 }, // ON--
    {  0,   1,   1,   0,   1,   0,   1,   1,   0,   0,   0,   0,   1,   1,   0,   0 }, // ON-C
    {  1,   0,   1,   0,   0,   1,   0,   1,   0,   1,   0,   1,   0,   1,   0,   0 }, // ONZ-
    {  1,   0,   1,   0,   1,   0,   0,   1,   0,   1,   0,   0,   1,   1,   0,   0 }  // ONZC
  };

  const BYTE flags = _state.flags.bits & 0b00001111;
  const BYTE condition = static_cast<BYTE>(*b.branchCondition) & 0b00001111;

  if (BRANCH_TABLE[flags][condition])
  {
    const int16_t signed_distance = b.relativeAddress.as<int16_t>() * 2;

    _state.ip = _state.ip + signed_distance;
  }
}

