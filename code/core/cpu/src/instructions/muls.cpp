/// Signed integer multiply.
///
/// This multiplies the 16-bit values of the lhs and rhs register operand,
/// and stores the 32-bit result in the low-, and high result registers. The
/// source and destination operands are interpreted as signed values. The CPU
/// carry flag is used to indicate whether the result high-word contains any
/// significant bits of the result.
///
/// ### Mnemonic
///
/// ``muls``
///
/// ### Opcodes
///
/// ``muls``
///
/// ### Syntax
///
/// <pre>muls lhs<sub>reg</sub>, rhs<sub>reg</sub>, result_low<sub>reg</sub>, result_high<sub>reg</sub></pre>
///
/// ### Examples
///
/// <pre>
/// muls r0, r1, r5, r6
/// </pre>
///
/// ### Operation
///
/// ```
/// s32 p = s32(o0) * s32(o1)
///
/// u16 result_low = p & 0xFFFF
/// u16 result_high = p >> 16
///
/// Cpu.flags.Z = (p == 0)
/// Cpu.flags.C = ((p & 0xFFFF0000) != 0) && ((p & 0xFFFF8000) != 0xFFFF8000)
/// Cpu.flags.N = 0
/// Cpu.flags.O = 0
/// ```
///
/// #### Flags
///
/// |C|Z|N|O|
/// |-|-|-|-|
/// |*|*|0|0|
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
void Cpu::muls(const Instruction &muls)
{
  const auto o0 = muls.quaternaryOperands.o0.as<Register>();
  const auto o1 = muls.quaternaryOperands.o1.as<Register>();
  const auto o2 = muls.quaternaryOperands.o2.as<Register>();
  const auto o3 = muls.quaternaryOperands.o3.as<Register>();

  const int32_t lhs = static_cast<int16_t>(reg(o0));
  const int32_t rhs = static_cast<int16_t>(reg(o1));

  const int32_t result = lhs * rhs;

  reg(o2) = static_cast<WORD>(result & 0xFFFF);
  reg(o3) = static_cast<WORD>((result >> 16) & 0xFFFF);

  _state.flags.zero = (result == 0);
  _state.flags.carry = ((result & 0xFFFF0000) != 0) && ((result & 0xFFFF8000) != 0xFFFF8000);
  _state.flags.negative = std::signbit<int32_t>(result);
  _state.flags.overflow = false;
}

