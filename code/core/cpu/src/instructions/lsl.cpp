/// Logical shift left of destination (reg, mem, ia) over distance (reg, imm16, mem, ia).
///
/// There is no distinction between logical (unsigned) and arithmetic (signed) shift when
/// shifting left: zero bits will be shifted into the least significant bits.
///
/// Notes:
///   * Shift distance is interpreted as an unsigned value.
///   * If the shift distance is zero, neither the destination register nor the CPU flags are
///     affected.
///
/// ### Mnemonic
///
/// ``lsl``
///
/// ### Opcodes
///
/// ``lsl, lsli, lslm, lsle``
///
/// ### Addressing modes
///
///   - reg &larr; reg
///   - reg &larr; imm16
///   - reg &harr; mem
///   - reg &harr; ia
///
/// ### Syntax
///
/// <pre>lsl dst<sub>reg,mem,ia</sub>, shift<sub>reg,imm16,mem,ia</sub></pre>
///
/// ### Examples
///
/// <pre>
/// lsl r0, r1
/// lsl r3, #4
/// lsl (0x01:0x0200), r5
/// lsl r0, [0x01:r1 + 2]
/// </pre>
///
/// ### Operation
///
/// ```
/// if (shift != 0)
/// {
///   u32 result = ((u32) dst) << shift
///   u16 dst = (result & 0xFFFF)
///
///   CPU.flags.C = ((result & 0x10000) != 0)
///   CPU.flags.Z = (dst == 0)
///   CPU.flags.N = signbit(dst)
///   CPU.flags.O = 0
/// }
/// ```
///
/// ### Flags
///
/// |C|Z|N|O|
/// |-|-|-|-|
/// |*|*|*|0|
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
void Cpu::lsl(const Instruction &lsl)
{
  WORD *dst;
  WORD shift;

  std::tie(dst, shift) = getBinarySrcDstOperands(lsl);

  if ((dst != nullptr) && (shift != 0))
  {
    const DWORD result = static_cast<DWORD>(*dst) << shift;

    *dst = static_cast<WORD>(result & 0xFFFF);

    // Update CPU flags
    _state.flags.zero = (*dst == 0);
    _state.flags.negative = std::signbit<SWORD>(*dst);
    _state.flags.overflow = 0;
    _state.flags.carry = ((result & 0x10000) != 0);
  }
}

