/// Write I/O port.
///
/// ### Mnemonic
///
/// ``out``
///
/// ### Opcodes
///
/// ``outi, outr``
///
/// ### Syntax
///
/// <pre>out device<sub>imm4</sub>, port<sub>imm9</sub>, src<sub>reg</sub>
/// <pre>out device<sub>imm4</sub>, port<sub>reg</sub>, src<sub>reg</sub>
///
/// ### Examples
///
/// <pre>
/// out 0x04, 0x1000, r0
/// out 0x02, r2, r3
/// </pre>
///
/// ### Operation
///
/// ```
/// if (type(port) == imm9)
/// {
///   $(device, port) = *src
/// }
/// else
/// {
///   if (*port > imm9.max)
///   {
///     throw BusError;
///   }
///
///   $(device, *port) = *src;
/// }
/// ```
///
/// ### Flags
///
/// |C|Z|N|O|
/// |-|-|-|-|
/// |-|-|-|-|
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
void Cpu::out(const Instruction &out)
{
  const bool is_indirect = (out.opcode == Opcode::OUTR);

  const WORD port = (is_indirect ? reg(out.portMove.port.as<Register>()) : out.portMove.port);

  peripheral::Device * const device = (out.portMove.device < _devices.size() ? _devices[out.portMove.device] : nullptr);

  if ((port >= (1 << PORT_BITS)) || (device == nullptr))
  {
    throw std::runtime_error(fmt::format("Bus error: write to invalid device port {0:#04x}:{1:#06x}", out.portMove.device.as<core::BYTE>(), port));
  }

  device->out(port, reg(out.portMove.reg));
}

