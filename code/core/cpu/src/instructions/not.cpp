/// Logical inverse of register.
///
/// ### Mnemonic
///
/// ``not``
///
/// ### Opcodes
///
/// ``not``
///
/// ### Syntax
///
/// <pre>not src_dst<sub>reg</sub></pre>
///
/// ### Examples
///
/// <pre>
/// not r0
/// </pre>
///
/// ### Operation
///
/// ```
/// src_dst = ~src_dst
///
/// Cpu.flags.Z = (src_dst == 0)
/// Cpu.flags.N = ((s16) src_dst < 0)
/// Cpu.flags.O = 0
/// Cpu.flags.C = 0
/// ```
///
/// ### Flags
///
/// |C|Z|N|O|
/// |-|-|-|-|
/// |0|*|*|0|
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
void Cpu::not_(const Instruction &not_)
{
  const auto r = not_.unaryOperand.as<Register>();

  const WORD v = ~reg(r);

  reg(r) = v;

  _state.flags.zero = (v == 0);
  _state.flags.negative = std::signbit<SWORD>(v);
  _state.flags.overflow = 0;
  _state.flags.carry = 0;
}
