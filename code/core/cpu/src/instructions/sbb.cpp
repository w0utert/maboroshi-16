/// Subtracts value (reg, imm16, mem, ia) and borrow flag from destination (reg, mem, ia).
///
/// The SBB instruction treats the carry flag as a borrow flag: it will calculate a - (b + Cpu.Flags.CARRY),
/// and set the carry flag if (a < b), clear it if (a >= b).
///
/// This operation is typically used to subtract 32-bit words by subtracting the low-words using a regular
/// SUB instruction, then subtract the high words using SBB.
///
/// ### Mnemonic
///
/// ``sbb``
///
/// ### Opcodes
///
/// ``sbb, sbbi, sbbm, sbbe``
///
/// ### Addressing modes
///
///   - reg &larr; reg
///   - reg &larr; imm16
///   - reg &harr; mem
///   - reg &harr; ia
///
/// ### Syntax
///
/// <pre>sbb dst<sub>reg,mem,ia</sub>, value<sub>reg,imm16,mem,ia</sub></pre>
///
/// ### Examples
///
/// <pre>
/// sbb r0, r1
/// sbb r3, #0x1234
/// sbb (0x01:0x0200), r5
/// sbb r0, [0x01:r1 + 2]
/// </pre>
///
/// ### Operation
///
/// ```
/// u32 a = u32(dst)
/// u32 b = u32(value) + (CPU.flags.C ? 1 : 0)
/// u32 result_32 = a - b
/// u16 result = result_32 & 0xFFFF
///
/// CPU.flags.C = (a < b)
/// CPU.flags.Z = (result == 0)
/// CPU.flags.N = signbit(result)
/// CPU.flags.O = (signbit(a) != signbit(b)) && (signbit(result) != signbit(a))
/// ```
///
/// ### Flags
///
/// |C|Z|N|O|
/// |-|-|-|-|
/// |0|*|*|*|
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
void Cpu::sbb(const Instruction &sbb)
{
  WORD *dst;
  WORD value;

  std::tie(dst, value) = getBinarySrcDstOperands(sbb);

  if (dst != nullptr)
  {
    const DWORD a = static_cast<DWORD>(*dst);
    const DWORD b = (static_cast<DWORD>(value) + (_state.flags.carry ? 1 : 0));

    const DWORD result_32 = a - b;
    const WORD result = static_cast<WORD>(result_32 & 0xFFFF);

    *dst = result;

    // Update CPU flags
    _state.flags.zero = (result == 0);
    _state.flags.negative = std::signbit<SWORD>(result);
    _state.flags.overflow =
      (std::signbit<SWORD>(a) != std::signbit<SWORD>(b)) &&
      (std::signbit<SWORD>(result) != std::signbit<SWORD>(a));
    _state.flags.carry = (a < b);
  }
}

