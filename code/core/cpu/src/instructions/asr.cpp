/// Arithmetic shift right of destination (reg, mem, ia) over distance (reg, u16, mem, ia).
///
/// Arithmetic shift will shift zeros or ones into the most-significant bits of the result,
/// depending on the sign of the value being shifted: 0 bits for positive values, 1 bits
/// for negative values.
///
/// Notes:
///   * Shift distance is interpreted as an unsigned value.
///   * If the shift distance is zero, neither the destination register nor the CPU flags are
///     affected.
///
/// ### Mnemonic
///
/// ``asr``
///
/// ### Opcodes
///
/// ``asr, asri, asrm, asre``
///
/// ### Addressing modes
///
///   - reg &larr; reg
///   - reg &larr; imm16
///   - reg &harr; mem
///   - reg &harr; ia
///
/// ### Syntax
///
/// <pre>asr dst<sub>reg,mem,ia</sub>, shift<sub>reg,mem,ia,u16</sub></pre>
///
/// ### Examples
///
/// <pre>
/// asr r0, r1
/// asr r3, #4
/// asr (0x01:0x0200), r5
/// asr r0, [0x01:r1 + 2]
/// </pre>
///
/// ### Operation
///
/// ```
/// if (shift != 0)
/// {
///   s16 dst = ((s16) dst >> shift)
///
///   CPU.flags.Z = (dst == 0)
///   CPU.flags.N = signbit(dst)
///   CPU.flags.O = 0
///   CPU.flags.C = 0
/// }
/// ```
///
/// ### Flags
///
/// |C|Z|N|O|
/// |-|-|-|-|
/// |0|*|*|0|
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
void Cpu::asr(const Instruction &asr)
{
  WORD *dst;
  WORD shift;

  std::tie(dst, shift) = getBinarySrcDstOperands(asr);

  if ((dst != nullptr) && (shift != 0))
  {
    const SWORD result = static_cast<SWORD>(*dst) >> shift;

    *dst = static_cast<WORD>(result);

    // Update CPU flags
    _state.flags.zero = (*dst == 0);
    _state.flags.negative = std::signbit(result);
    _state.flags.overflow = 0;
    _state.flags.carry = 0;
  }
}

