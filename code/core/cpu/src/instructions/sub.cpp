/// Subtract value (reg, imm16, mem, ia) to destination (reg, mem, ia).
///
/// ### Mnemonic
///
/// ``sub``
///
/// ### Opcodes
///
/// ``sub, subi, subm, sube``
///
/// ### Addressing modes
///
///   - reg &larr; reg
///   - reg &larr; imm16
///   - reg &harr; mem
///   - reg &harr; ia
///
/// ### Syntax
///
/// <pre>sub dst<sub>reg,mem,ia</sub>, value<sub>reg,imm16,mem,ia</sub></pre>
///
/// ### Examples
///
/// <pre>
/// sub r0, r1
/// sub r3, #0x1234
/// sub (0x01:0x0200), r5
/// sub r0, [0x01:r1 + 2]
/// </pre>
///
/// ### Operation
///
/// ```
/// u32 a = u32(dst)
/// u32 b = u32(value)
/// u32 result_32 = a - b
/// u16 result = result_32 & 0xFFFF
///
/// CPU.flags.C = (a < b)
/// CPU.flags.Z = (result == 0)
/// CPU.flags.N = signbit(result)
/// CPU.flags.O = (signbit(a) != signbit(b)) && (signbit(result) != signbit(a))
/// ```
///
/// ### Flags
///
/// |C|Z|N|O|
/// |-|-|-|-|
/// |0|*|*|*|
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
void Cpu::sub(const Instruction &sbc)
{
  WORD *dst;
  WORD value;

  std::tie(dst, value) = getBinarySrcDstOperands(sbc);

  if (dst != nullptr)
  {
    const DWORD a = static_cast<DWORD>(*dst);
    const DWORD b = static_cast<DWORD>(value);
    const DWORD result_32 = a - b;
    const WORD result = static_cast<WORD>(result_32 & 0xFFFF);

    *dst = result;

    // Update CPU flags
    _state.flags.zero = (result == 0);
    _state.flags.negative = std::signbit<SWORD>(result);
    _state.flags.overflow =
      (std::signbit<SWORD>(a) != std::signbit<SWORD>(b)) &&
      (std::signbit<SWORD>(result) != std::signbit<SWORD>(a));
    _state.flags.carry = (result_32 > 0xFFFF);
  }
}

