/// Wait for interrupt.
///
/// Using this intruction, the CPU can be halted until any one of a set of interrupts occurs. The
/// interrupts to wait for are specified as a bit mask, ie, eg: bit 5 and 6 set means 'wait for
/// IRQ 5 or 6'.
///
/// NOTE:
/// Be careful using this instruction where the DISABLE_IRQ flag is not guaranteed to be cleared,
/// as this will block the CPU indefinitely!
///
/// ### Mnemonic
///
/// ``wait``
///
/// ### Opcodes
///
/// ``wait``
///
/// ### Syntax
///
/// <pre>wait irqs<sub>imm16</sub>
///
/// ### Examples
///
/// <pre>
/// wait 0x02
/// </pre>
///
/// ### Operation
///
/// ```
/// if ((CPU.irq & irqs) == 0)
/// {
///   state.ip = state.ip - 2;
/// }
/// ```
///
/// ### Flags
///
/// |C|Z|N|O|
/// |-|-|-|-|
/// |-|-|-|-|
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
void Cpu::wait(const Instruction &wait)
{
  // The implementation of the wait instruction is very simple, it effectively always halts the
  // CPU by moving back the instruction pointer by 1 instruction, without actually checking any
  // interrupt state. This is based on the assumption that that now new interrupts can be generated
  // during a single invocation of the CPU run loop. Instead the interrupts will be deferred to and
  // handled at the next run loop invocation, which has special-case handling if the next instruction
  // is a WAIT instruction, skipping over it if the pending interrupts before going back into the
  // run loop match the WAIT instruction operand bitmask.
  _state.ip = _state.ip - 2;
}

