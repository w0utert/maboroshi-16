/// Clear CPU flag.
///
/// ### Mnemonic
///
/// ``clr``
///
/// ### Opcodes
///
/// ``clr``
///
/// ### Syntax
///
/// ``clr flags<sub>imm8, flags</sub>``
///
/// ### Examples
///
/// <pre>
/// clr #2
/// clr %A
/// clr %Z
/// clr %CO
/// </pre>
///
/// ### Operation
///
/// ```
/// CPU.flags = CPU.flags & ~flag
/// ```
///
/// ### Flags
///
/// |C|Z|N|O|
/// |-|-|-|-|
/// |*|*|*|*|
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
void Cpu::clr(const Instruction &clr)
{
  _state.flags.bits &= ~clr.unaryOperand;
}

