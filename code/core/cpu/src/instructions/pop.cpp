/// Pop register.
///
/// ### Mnemonic
///
/// ``pop``
///
/// ### Opcodes
///
/// ``pop``
///
/// ### Syntax
///
/// <pre>POP dst<sub>reg</sub></pre>
///
/// ### Examples
///
/// <pre>
/// pop r0
/// </pre>
///
/// ### Operation
///
/// ```
/// if (Cpu.sp == Cpu.STACK_OFFSET)
/// {
///   throw StackUnderflow
/// }
///
/// Cpu.sp = Cpu.sp + 1
///
/// dst = [Cpu.sp]
/// ```
///
/// ### Flags
///
/// |C|Z|N|O|
/// |-|-|-|-|
/// |-|-|-|-|
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
void Cpu::pop(const Instruction &pop)
{
  /// \todo this should throw a custom CPU exception type
  if (_state.sp == Cpu::STACK_OFFSET)
  {
    throw std::runtime_error("StackUnderflow");
  }

  _state.sp = _state.sp + 1;

  reg(pop.unaryOperand.as<Register>()) = mem(_state.sp);
}
