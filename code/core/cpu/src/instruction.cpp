/// Implements functions related to the Instruction structure that represents CPU machine code instructions.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#include "core/cpu/itf/instruction.h"

#include "instruction_layouts.cpp"

namespace core::cpu {

std::ostream &operator<<(std::ostream &out, const Instruction &instruction)
{
  out << instruction.operation();

  return out;
}

}

