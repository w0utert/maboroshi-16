/// Types and utility functions to represent CPU instructions.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#pragma once

#include "core/common/itf/types.h"
#include "core/common/itf/bit_range.h"

#include "core/cpu/itf/cpu.h"
#include "core/cpu/itf/opcodes.h"

// Suppress GCC warnings about bitfields in the Instruction struct not fitting all values of
// the underlying type of the enum classes used, even though they fit all *valid* enum values.
#pragma GCC system_header

// The structures used to represent the instruction bitfield need to be packed (no padding or alignment).
#pragma pack(push, 1)

namespace core::cpu {

/// Structure type to represent CPU instructions, and to create & access
/// the bit patterns encoding the instruction opcode & operands.
///
/// Instructions are always 32-bit DWORD bitfields, that can have a few different layouts
/// depending on the instruction opcode and operands. For example, the instruction low-word
/// could encode an immediate, a relative address, the offset part of a direct address,
/// etc. The only bits that are fixed for all instruction layout are the instruction opcode,
/// which is encoded in the upper 8 bits of the instruction bitfield.
///
/// To provide easy and safe/portable access to the instruction bitfield, it is represented as
/// a union of bit ranges, one bit range for every element of every possible instruction
/// layout. Each of these bit-ranges is of type BitRange, which acts as a proxy to the element
/// bits and performs the necessary shifting & masking when getting or setting elements of
/// the instruction bitfield. These BitRange instances are all backed by the same 32-bit (DWORD)
/// value, which can also be addressed directly through the \a bits member.
struct Instruction
{
  /// Instruction size in machine words. All instructions have the same word size
  static constexpr unsigned int WORD_SIZE = 2;

  /// Structure to represent an instruction layout
  ///
  /// This is used to be able to look up the operands of an instruction based on its opcode. The main
  /// use case for this is disassembling instructions in the debugger. A mapping of opcodes to instruction
  /// layouts is defined as the Instruction::INSTRUCTION_LAYOUTS class variable.
  struct Layout
  {
    /// Enumeration type for operand types
    enum class OperandType : uint8_t
    {
      NONE = 0,
      FLAGS,
      IMM8,
      IMM16,
      REG,
      SP_REG,
      LONGPOINTER,
      DIRECT_PORT_MOVE,
      INDIRECT_PORT_MOVE,
      BRANCH_CONDITION,
      RELATIVE_ADDRESS,
      INDIRECT_ADDRESS,
      VECTOR,
      DIRECTION,
      MODIFIER
    };

    /// Construct instruction layout from up to MAX_OPERANDS operand types
    ///
    /// \param operands instruction layout operands
    template<typename... Args, std::enable_if_t<sizeof...(Args) <= 5> * = nullptr>
    constexpr Layout(const Args... operands)
    :
      operands({ operands... })
    {

    }

    /// Cast to uint64_t, to allow using Instruction::Layout instances to be used as labels in switch statements.
    ///
    /// \return 64-bit hash code for the instruction layout
    constexpr operator uint64_t() const
    {
      return
        (static_cast<uint64_t>(operands[0]) << 0) |
        (static_cast<uint64_t>(operands[1]) << 8) |
        (static_cast<uint64_t>(operands[2]) << 16) |
        (static_cast<uint64_t>(operands[3]) << 24) |
        (static_cast<uint64_t>(operands[4]) << 32);
    }

    // Comparison operators to allow using Instruction::Layout as sorted map keys
    bool operator<(const Layout &other) const { return (this->operands < other.operands); }
    bool operator==(const Layout &other) const { return (this->operands == other.operands); }

    /// Insruction layout operands
    std::array<OperandType, 5> operands;
  };

  /// Instruction layouts, mapping opcodes to the operands they take
  static const std::map<Opcode, Layout> INSTRUCTION_LAYOUTS;

  /// Structure type used to indicate subdivisions of the instruction bitfield
  struct BitField
  {
    /// Bit offset into instruction bitfield
    BYTE offset;
    /// Number of bits
    BYTE size;
  };

  /// Enumeration type indicating the direction of instructions that take
  /// source and destination operands.
  enum class Direction : BYTE
  {
    /// First operand is destination register, second operand is source memory location (absolute or indirect address)
    DST_MEM = 0b0,
    /// First operand is destination memory location (absolute or indirect address), second operand is source register
    MEM_SRC = 0b1,
  };

  /// Enumeration type for the 'byte-move' modifier bit for the MOVL and MOVH instructions
  enum class ByteMoveMode : bool
  {
    /// Direct move (low byte -> low byte, or high byte -> high byte)
    DIRECT = false,
    /// Swapped move (low byte -> high byte, or high byte -> low byte)
    SWAPPED = true,
  };

  /// \return opcode operation bits
  Operation operation() const
  {
    const auto o = opcode.as<BYTE>();

    return static_cast<Operation>((o & 0x80) == 0 ? o : o & ~0b11);
  }

  /// Return size of instruction, in machine words
  ///
  /// \return size of instruction, in machine words
  static constexpr size_t size()
  {
    return sizeof(Instruction) / sizeof(core::WORD);
  };

  /// Instruction bitfield.
  union
  {
    /// Full instruction bitfield
    DWORD bits;

    /// Instruction low-word
    BitRange<DWORD, WORD, 0, 16> low;
    /// Instruction high-word
    BitRange<DWORD, WORD, 16, 16> high;
    /// Instruction opcode
    BitRange<DWORD, Opcode, 24, 8> opcode;
    /// Instruction opcode address mode bits
    BitRange<DWORD, AddressMode, 24, 2> addressMode;
    /// 8-bit unary operand
    BitRange<DWORD, BYTE, 16, 8> unaryOperand;
    /// Branch condition operand
    BitRange<DWORD, BranchCondition, 16, 8> branchCondition;
    /// 16-bit immediate operand
    BitRange<DWORD, WORD, 0, 16> immediate;
    /// Relative address (16-bit signed value)
    BitRange<DWORD, SWORD, 0, 16> relativeAddress;

    /// 3-bit binary operands with (optional) direction and modifier bits
    struct
    {
      union
      {
        BitRange<DWORD, BYTE, 16, 3> o0;
        BitRange<DWORD, BYTE, 19, 3> o1;
        BitRange<DWORD, Direction, 22, 1> direction;
        BitRange<DWORD, bool, 23, 1> modifier;
      };
    } binaryOperands;

    /// 3-bit quaternary operands
    struct
    {
      union
      {
        BitRange<DWORD, BYTE, 0, 3> o0;
        BitRange<DWORD, BYTE, 3, 3> o1;
        BitRange<DWORD, BYTE, 6, 3> o2;
        BitRange<DWORD, BYTE, 9, 3> o3;
      };
    } quaternaryOperands;

    /// Direct address operand (3-bit segment plus 16-bit offset)
    struct
    {
      union
      {
        BitRange<DWORD, BYTE, 19, 3> segment;
        BitRange<DWORD, WORD, 0, 16> offset;
      };

      /// \return direct address bits as \a LongPointer structure
      LongPointer operator()() const
      {
        return { segment, offset };
      }
    } longPointer;

    /// Indirect address operand (base address register, 9-bit offset, offset sign bit)
    struct
    {
      union
      {
        BitRange<DWORD, BYTE, 0, 3> segment;
        BitRange<DWORD, cpu::Register, 3, 3> base;
        BitRange<DWORD, WORD, 6, 9> offset;
        BitRange<DWORD, bool, 15, 1> sign;
      };

      /// \return indirect address bits as \a IndirectAddress structure
      IndirectAddress operator()() const
      {
        const SWORD signed_offset =  (sign ? -offset : offset);

        return { segment, base, signed_offset };
      }
    } indirectAddress;

    /// Port move operand (4-bit device id, 9-bit port number or register holding port number, source register)
    struct
    {
      union
      {
        BitRange<DWORD, BYTE, 0, 4> device;
        BitRange<DWORD, WORD, 4, 9> port;
        BitRange<DWORD, Register, 13, 3> reg;
      };
    } portMove;
  };

  /// Default constructor.
  ///
  /// This will create a NOP instruction.
  Instruction()
  :
    Instruction(Opcode::NOP)
  {
  }

  /// Construct instruction directly from bitfield.
  ///
  /// \param bits instruction bits, in host byte order
  Instruction(const DWORD bits)
  :
    bits(bits)
  {
  }

  /// Create instruction from low & high word bits.
  ///
  /// \param low low-word bits
  /// \param high high-word bits
  Instruction(const WORD low, const WORD high)
  :
    bits(0)
  {
    this->low = low;
    this->high = high;
  }

  /// Create instruction without arguments.
  ///
  /// \param opcode instruction opcode
  Instruction(const Opcode opcode)
  :
    bits(0)
  {
    this->opcode = opcode;
  }

  /// Create instruction with 8-bit unary operand
  ///
  /// \param opcode instruction opcode
  /// \param operand operand
  Instruction(const Opcode opcode, const BYTE operand)
  :
    Instruction(opcode)
  {
    this->unaryOperand = static_cast<BYTE>(operand);
  }

  /// Create instruction with CPU flag.
  ///
  /// \param opcode instruction opcode
  /// \param flag CPU flag operand
  Instruction(const Opcode opcode, const Flag::Bits flag)
  :
    Instruction(opcode, static_cast<BYTE>(flag))
  {
  }

  /// Create instruction with unary register operand.
  ///
  /// \param opcode instruction opcode
  /// \param reg register operand
  Instruction(const Opcode opcode, const Register reg)
  :
    Instruction(opcode, static_cast<BYTE>(reg))
  {
  }

  /// Create binary-operand instruction with source and destination register operands.
  ///
  /// \param opcode instruction opcode
  /// \param o0 destination register operand
  /// \param o1 source register operand
  /// \param modifier operation modifier bits
  Instruction(const Opcode opcode, const Register o0, const Register o1, const bool modifier = false)
  :
    Instruction(opcode)
  {
    binaryOperands.o0 = static_cast<BYTE>(o0);
    binaryOperands.o1 = static_cast<BYTE>(o1);
    binaryOperands.modifier = modifier;
  }

  /// Create instruction with 4 register operands encoded in the instruction high-word.
  ///
  /// The interpretation of these is specific to each opcode that uses it, e.g. MUL will
  /// multiply o0 by o1 and store the result in o2:o3, while DIV will divide o0:o1 by o2
  /// and store the result in o3. These instructions do not use the addressing mode and
  /// direction bits, which will be cleared for all of them.
  ///
  /// \param opcode instruction opcode
  /// \param o0 first register operand
  /// \param o1 first register operand
  /// \param o2 first register operand
  /// \param o3 first register operand
  Instruction(const Opcode opcode, const Register o0, const Register o1, const Register o2, const Register o3)
  :
    Instruction(opcode)
  {
    this->quaternaryOperands.o0 = static_cast<BYTE>(o0);
    this->quaternaryOperands.o1 = static_cast<BYTE>(o1);
    this->quaternaryOperands.o2 = static_cast<BYTE>(o2);
    this->quaternaryOperands.o3 = static_cast<BYTE>(o3);
  }

  /// Create port-move instruction.
  ///
  /// \param opcode instruction opcode
  /// \param portMove port-move operands
  Instruction(const Opcode opcode, const PortMove &portMove)
  :
    Instruction(opcode)
  {
    this->portMove.device = portMove.device;
    this->portMove.reg = portMove.reg;

    if (!portMove.indirect)
    {
      this->portMove.port = std::get<WORD>(portMove.port);
    }
    else
    {
      this->portMove.port = static_cast<WORD>(std::get<Register>(portMove.port));
    }
  }

  /// Create branch instruction.
  ///
  /// \param opcode instruction opcode
  /// \param branchCondition branch condition
  /// \param relativeAddress relative address for branch
  Instruction(const Opcode opcode, const BranchCondition branchCondition, const SWORD relativeAddress)
  :
    Instruction(opcode)
  {
    this->branchCondition = branchCondition;
    this->relativeAddress = relativeAddress;
  }

  /// Create instruction with only an immediate operand
  ///
  /// \param opcode instruction opcode
  /// \param imm 16-bit immediate operand
  Instruction(const Opcode opcode, const WORD imm)
  :
    Instruction(opcode)
  {
    this->immediate = imm;
  }

  /// Create binary-operand instruction with destination register and immediate operands.
  ///
  /// \param opcode instruction opcode
  /// \param o0 destination register
  /// \param imm 16-bit immediate operand
  /// \param modifier operation modifier bits
  Instruction(const Opcode opcode, const Register o0, const WORD imm, const bool modifier = false)
  :
    Instruction(opcode)
  {
    this->immediate = imm;
    this->binaryOperands.o0 = static_cast<BYTE>(o0);
    this->binaryOperands.modifier = modifier;
  }

  /// Create unary-operand instruction with long pointer operand.
  ///
  /// \param opcode instruction opcode
  /// \param address address, as long pointer
  Instruction(const Opcode opcode, const LongPointer &address)
  :
    Instruction(opcode)
  {
    this->longPointer.segment = address.segment;
    this->longPointer.offset = address.offset;
  }

  /// Create binary-operand instruction with register (destination) and long pointer (source) operands.
  ///
  /// \param opcode instruction opcode
  /// \param dst destination register
  /// \param src source address, as long pointer
  /// \param modifier instruction modifier bit
  Instruction(const Opcode opcode, const Register dst, const LongPointer &src, const bool modifier = false)
  :
    Instruction(opcode, src)
  {
    binaryOperands.o0 = static_cast<BYTE>(dst);
    binaryOperands.direction = Direction::DST_MEM;
    binaryOperands.modifier = modifier;
  }

  /// Create binary-operand instruction with long pointer (destination) and register (source) operands.
  ///
  /// \param opcode instruction opcode
  /// \param dst destination address, as long pointer
  /// \param src source register
  /// \param modifier instruction modifier bit
  Instruction(const Opcode opcode, const LongPointer &dst, const Register src, const bool modifier = false)
  :
    Instruction(opcode, dst)
  {
    binaryOperands.o0 = static_cast<BYTE>(src);
    binaryOperands.direction = Direction::MEM_SRC;
    binaryOperands.modifier = modifier;
  }

  /// Create unary-operand instruction with indirect address operand.
  ///
  /// \param opcode instruction opcode
  /// \param indirectAddress indirect address operand
  Instruction(const Opcode opcode, const IndirectAddress &indirectAddress)
  :
    Instruction(opcode)
  {
    this->indirectAddress.segment = indirectAddress.multiplier;
    this->indirectAddress.offset = indirectAddress.offset;
    this->indirectAddress.base = indirectAddress.base;
    this->indirectAddress.sign = indirectAddress.backward;
  }

  /// Create binary-operand instruction with register (destination) and indirect address (source) operands.
  ///
  /// \param opcode instruction opcode
  /// \param dst destination register operand
  /// \param indirectAddress indirect address operand
  /// \param modifier optional instruction modifier bit
  Instruction(const Opcode opcode, const Register dst, const IndirectAddress &indirectAddress, bool modifier = false)
  :
    Instruction(opcode, indirectAddress)
  {
    this->binaryOperands.o0 = static_cast<BYTE>(dst);
    this->binaryOperands.direction = Direction::DST_MEM;
    this->binaryOperands.modifier = modifier;
  }

  /// Create binary-operand instruction with indirect address (destination) and register (source) operands.
  ///
  /// \param opcode instruction opcode
  /// \param dst destination register
  /// \param indirectAddress indirect address operand
  /// \param modifier optional instruction modifier bit
  Instruction(const Opcode opcode, const IndirectAddress &indirectAddress, const Register src, bool modifier = false)
  :
    Instruction(opcode, indirectAddress)
  {
    this->binaryOperands.o0 = static_cast<BYTE>(src);
    this->binaryOperands.direction = Direction::MEM_SRC;
    this->binaryOperands.modifier = modifier;
  }


  /// Create binary-operand instruction with register (destination) and special purpose register (source) operands.
  ///
  /// \param opcode instruction opcode
  /// \param dst destination register operand
  /// \param src source special purpose register operand
  Instruction(const Opcode opcode, const Register dst, const SpecialPurposeRegister src)
  :
    Instruction(opcode)
  {
    this->binaryOperands.o0 = static_cast<BYTE>(dst);
    this->binaryOperands.o1 = static_cast<BYTE>(src);
  }

  /// Create binary-operand instruction with special purpose register (source) and register (destination) operands.
  ///
  /// \param opcode instruction opcode
  /// \param dst destination special purpose register operand
  /// \param src source register operand
  Instruction(const Opcode opcode, const SpecialPurposeRegister dst, const Register src)
  :
    Instruction(opcode)
  {
    this->binaryOperands.o0 = static_cast<BYTE>(dst);
    this->binaryOperands.o1 = static_cast<BYTE>(src);
  }
};

/// Stream operator for instructions, outputs a disassembly of the passed \p instruction to the output stream.
///
/// \param instruction instruction to write
/// \param out stream to write the \p instruction disassembly to
/// \return the output stream, after writing the instruction disassembly to it
std::ostream &operator<<(std::ostream &out, const Instruction &instruction);

}

#pragma pack(pop)
