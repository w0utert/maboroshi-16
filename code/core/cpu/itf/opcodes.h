/// Defines the opcode table for the Maboroshi-16 CPU
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#pragma once

#include "core/cpu/itf/cpu.h"

#include <stdexcept>
#include <iostream>

namespace core::cpu {

/// Enumeration type for the operation bits used for OpCodes. These are split
/// into two categories: operations without an addressing mode (MSB is 0),
/// and operations with an addressing mode (MSB is 1). Operations that support
/// multiple addressing modes reserve their 2 low bits to store the addressing
/// mode, which is of type core::cpu::AddressMode.
enum class Operation
{
  // Operations without addressing mode bits or opcode mode bits
  NOP   = 0b00000000, ///< No operation
  RET   = 0b00000010, ///< Return from subroutine
  RTI   = 0b00000011, ///< Return from interrupt
  CLR   = 0b00000100, ///< Clear CPU flag
  SET   = 0b00000101, ///< Set CPU flag
  NEG   = 0b00000110, ///< Negate register operand
  NOT   = 0b00000111, ///< Logic inverse of register operand
  PUSH  = 0b00001000, ///< Push register
  POP   = 0b00001001, ///< Pop register
  PUSHS = 0b00001010, ///< Push CPU state
  POPS  = 0b00001011, ///< Pop CPU state
  B     = 0b00010000, ///< Relative conditional branch
  DIV   = 0b00100000, ///< Integer divide (unsigned)
  MUL   = 0b00100001, ///< Integer multiplication (unsigned)
  DIVS  = 0b00100010, ///< Integer divide (signed)
  MULS  = 0b00100011, ///< Integer multiplication (signed)
  LDSP  = 0b00100100, ///< Load special purpose register
  STSP  = 0b00100110, ///< Store special purpose register
  RSV   = 0b00110000, ///< Reserve stack frame
  BANK  = 0b01000010, ///< Switch bank (map device RAM into CPU-visible address space)
  WAIT  = 0b01111110, ///< Wait for interrupt
  HALT  = 0b01111111, ///< Halt CPU

  // Operations that support multiple addressing modes in bits 0-1 of the opcode
  MOV   = 0b10000000, ///< Move value
  CMP   = 0b10001000, ///< Compare
  ADD   = 0b10001100, ///< Add
  ADC   = 0b10010000, ///< Add operand carry
  SUB   = 0b10010100, ///< Subtract
  SBB   = 0b10011000, ///< Subtract with borrow
  ASR   = 0b10100000, ///< Arithmetic shift right
  AND   = 0b10100100, ///< Logical AND
  OR    = 0b10101000, ///< Logical OR
  XOR   = 0b10101100, ///< Logical XOR
  LSL   = 0b10110000, ///< Logical shift left
  LSR   = 0b10110100, ///< Logical shift right
  INC   = 0b10111000, ///< Increase operand
  DEC   = 0b10111100, ///< Decrease operand
  MOVL  = 0b11001000, ///< Move low byte
  MOVH  = 0b11010000, ///< Move high byte
  IN    = 0b11100000, ///< Read I/O port
  OUT   = 0b11100100, ///< Write I/O port

  // Operations that support multiple jump modes in bits 0-1 of the opcode
  CALL  = 0b11110100, ///< Call subroutine
  J     = 0b11111000, ///< Unconditional jump

  // Extended opcode (currently unused, but reserved bit pattern)
  EXT   = 0b11111111, ///< Extended opcode
};

/// Enumeration type for addressing modes
enum class AddressMode
{
  REGISTER          = 0b00, ///< Operate on source register
  IMMEDIATE         = 0b01, ///< Operate on 16-bit immediate
  INDIRECT_ADDRESS  = 0b10, ///< Operate on indirect address [multiplier * [base register] + offset]
  MEMORY            = 0b11, ///< Operate on memory address (segment:offset)
};

/// Enumeration type for jump modes
enum class JumpMode : BYTE
{
  DIRECT            = 0b00, ///< Direct memory address
  VECTOR            = 0b10, ///< Vector destination (load location segment:offset from DWORD at memory address)
};

/// Enumeration type for branch conditions
enum class BranchCondition
{
  Z   =  0, ///< Branch on zero flag set
  NZ  =  1, ///< Branch on zero flag clear
  O   =  2, ///< Branch if overflow flag set
  NO  =  3, ///< Branch if overflow flag clear
  C   =  4, ///< Branch if carry flag set
  NC  =  5, ///< Branch if carry flag clear

  G   =  6, ///< Branch if right-hand side operand greater than, signed
  GE  =  7, ///< Branch if right-hand side operand greater than or equal, signed
  L   =  8, ///< Branch if right-hand side operand less than, signed
  LE  =  9, ///< Branch if right-hand side operand less than or equal, signed

  UG  = 10, ///< Branch if right-hand side operand greater than, unsigned
  UGE = 11, ///< Branch if right-hand side operand greater than or equal, unsigned
  UL  = 12, ///< Branch if right-hand side operand less than, unsigned
  ULE = 13, ///< Branch if right-hand side operand less than or equal, unsigned
};

/// Make opcode without addressing mode.
///
/// \param operation opcode operation
/// \return 8-bit opcode for the operation
/// \throws std::logic_error if the operation bitfield has its MSB set
constexpr BYTE OPCODE(const Operation operation)
{
  if (((BYTE) operation & 0b10000000) != 0b00000000)
  {
    throw std::logic_error("Invalid operation bitfield for operation without address mode bits");
  }

  return (BYTE) operation;
}

/// Make opcode with addressing mode (bits 0-1 of the opcode)
///
/// \param operation opcode operation
/// \param addressMode addressing mode
/// \return 8-bit opcode for the operation
/// \throws std::logic_error if the operation bitfield does not have its MSB set, or
///   has any of its low-2 bits set (these are reserved for the addressing mode)
constexpr BYTE OPCODE(const Operation operation, const AddressMode addressMode)
{
  if ((static_cast<BYTE>(operation) & 0b10000011) != 0b10000000)
  {
    throw std::logic_error("Invalid operation bitfield for operation with address mode bits");
  }

  return static_cast<BYTE>(operation) | static_cast<BYTE>(addressMode);
}

/// Make opcode with jump mode.
///
/// \param operation opcode operation
/// \param jumpMode jump mode
/// \return 8-bit opcode for the operation
/// \throws std::logic_error if the 2 most significant bits of the operation bitfield
///    are not 01b, or if the operation bitfield has any of its low-2 bits set (these
///    are reserved for the jump mode)
constexpr BYTE OPCODE(const Operation operation, const JumpMode jumpMode)
{
  if ((static_cast<BYTE>(operation) & 0b01000011) != 0b01000000)
  {
    throw std::logic_error("Invalid operation bitfield for operation with jump mode bits");
  }

  return (BYTE) operation | (BYTE) jumpMode;
}

/// Enumeration type holding the opcode table.
enum class Opcode
{
  /// No operation
  NOP   = OPCODE(Operation::NOP),
  /// Halt CPU
  HALT  = OPCODE(Operation::HALT),
  /// Wait for interrupt
  WAIT = OPCODE(Operation::WAIT),

  /// Clear interrupt flag
  CLR   = OPCODE(Operation::CLR),
  /// Set interrupt flag
  SET   = OPCODE(Operation::SET),
  /// Return from subroutine
  RET   = OPCODE(Operation::RET),
  /// Return from interrupt
  RTI   = OPCODE(Operation::RTI),
  /// Push register
  PUSH  = OPCODE(Operation::PUSH),
  /// Pop register
  POP   = OPCODE(Operation::POP),
  /// Push CPU state
  PUSHS = OPCODE(Operation::PUSHS),
  /// Pop CPU state
  POPS  = OPCODE(Operation::POPS),
  /// Negate register
  NEG   = OPCODE(Operation::NEG),
  /// Logical NOT of register
  NOT   = OPCODE(Operation::NOT),
  /// Increase register
  INC   = OPCODE(Operation::INC, AddressMode::REGISTER),
  /// Decrease register
  DEC   = OPCODE(Operation::DEC, AddressMode::REGISTER),

  /// Move from register
  MOV   = OPCODE(Operation::MOV, AddressMode::REGISTER),
  /// Compare with register
  CMP   = OPCODE(Operation::CMP, AddressMode::REGISTER),
  /// Add register
  ADD   = OPCODE(Operation::ADD, AddressMode::REGISTER),
  /// Add register with carry
  ADC   = OPCODE(Operation::ADC, AddressMode::REGISTER),
  /// Subtract register
  SUB   = OPCODE(Operation::SUB, AddressMode::REGISTER),
  /// Subtract register with borrow
  SBB   = OPCODE(Operation::SBB, AddressMode::REGISTER),
  /// Arithmetic shift right with register
  ASR   = OPCODE(Operation::ASR, AddressMode::REGISTER),
  /// Logical AND with register
  AND   = OPCODE(Operation::AND, AddressMode::REGISTER),
  /// Logical OR with register
  OR    = OPCODE(Operation::OR, AddressMode::REGISTER),
  /// Logical XOR with register
  XOR   = OPCODE(Operation::XOR, AddressMode::REGISTER),
  /// Logical shift left with register
  LSL   = OPCODE(Operation::LSL, AddressMode::REGISTER),
  /// Logical shift right with register
  LSR   = OPCODE(Operation::LSR, AddressMode::REGISTER),
  /// Move low-byte from register
  MOVL  = OPCODE(Operation::MOVL, AddressMode::REGISTER),
  /// Move high-byte from register
  MOVH  = OPCODE(Operation::MOVH, AddressMode::REGISTER),

  /// Branch
  B     = OPCODE(Operation::B),

  /// Integer multiplication (unsigned)
  MUL   = OPCODE(Operation::MUL),
  /// Integer multiplication (signed)
  MULS  = OPCODE(Operation::MULS),
  /// Integer division (unsigned)
  DIV   = OPCODE(Operation::DIV),
  /// Integer division (signed)
  DIVS  = OPCODE(Operation::DIVS),

  /// Load stack pointer
  LDSP  = OPCODE(Operation::LDSP),
  /// Store stack pointer
  STSP  = OPCODE(Operation::STSP),
  /// Reserve stack frame
  RSV   = OPCODE(Operation::RSV),

  /// Read I/O port indicated by immediate operand
  INI   = OPCODE(Operation::IN, AddressMode::IMMEDIATE),
  /// Read I/O port indicated by register operand
  INR   = OPCODE(Operation::IN, AddressMode::REGISTER),
  /// Write I/O port indicated by immediate operand
  OUTI  = OPCODE(Operation::OUT, AddressMode::IMMEDIATE),
  /// Write I/O port indicated by register operand
  OUTR  = OPCODE(Operation::OUT, AddressMode::REGISTER),

  /// Switch bank
  BANK  = OPCODE(Operation::BANK),

  /// Increase value at memory location
  INCM  = OPCODE(Operation::INC, AddressMode::MEMORY),
  /// Increase value at indirect address
  INCE  = OPCODE(Operation::INC, AddressMode::INDIRECT_ADDRESS),
  /// Decrease value at memory location
  DECM  = OPCODE(Operation::DEC, AddressMode::MEMORY),
  /// Increase value at indirect address
  DECE  = OPCODE(Operation::DEC, AddressMode::INDIRECT_ADDRESS),

  /// Unconditional direct jump to memory location
  JM    = OPCODE(Operation::J, JumpMode::DIRECT),
  /// Unconditional indirect jump to vector
  JV    = OPCODE(Operation::J, JumpMode::VECTOR),

  /// Direct subroutine call to memory location
  CALLM = OPCODE(Operation::CALL, JumpMode::DIRECT),
  /// Indirect subroutine call to vector
  CALLV = OPCODE(Operation::CALL, JumpMode::VECTOR),

  /// Load immediate
  MOVI  = OPCODE(Operation::MOV, AddressMode::IMMEDIATE),
  /// Load from indirect address
  MOVE  = OPCODE(Operation::MOV, AddressMode::INDIRECT_ADDRESS),
  /// Load from memory
  MOVM  = OPCODE(Operation::MOV, AddressMode::MEMORY),

  /// Compare with immediate
  CMPI  = OPCODE(Operation::CMP, AddressMode::IMMEDIATE),
  /// Compare with value at indirect address
  CMPE  = OPCODE(Operation::CMP, AddressMode::INDIRECT_ADDRESS),
  /// Compare to value at memory location
  CMPM  = OPCODE(Operation::CMP, AddressMode::MEMORY),

  /// Add immediate
  ADDI  = OPCODE(Operation::ADD, AddressMode::IMMEDIATE),
  /// Add value at indirect adddress
  ADDE  = OPCODE(Operation::ADD, AddressMode::INDIRECT_ADDRESS),
  /// Add value at memory location
  ADDM  = OPCODE(Operation::ADD, AddressMode::MEMORY),

  /// Add immediate with carry
  ADCI  = OPCODE(Operation::ADC, AddressMode::IMMEDIATE),
  /// Add value at indirect adddress with carry
  ADCE  = OPCODE(Operation::ADC, AddressMode::INDIRECT_ADDRESS),
  /// Add value at memory location with carry
  ADCM  = OPCODE(Operation::ADC, AddressMode::MEMORY),

  /// Subtract immediate
  SUBI  = OPCODE(Operation::SUB, AddressMode::IMMEDIATE),
  /// Subtract value at indirect adddress
  SUBE  = OPCODE(Operation::SUB, AddressMode::INDIRECT_ADDRESS),
  /// Subtract value at memory location
  SUBM  = OPCODE(Operation::SUB, AddressMode::MEMORY),

  /// Subtract immediate with borrow
  SBBI  = OPCODE(Operation::SBB, AddressMode::IMMEDIATE),
  /// Subtract value at indirect adddress with borrow
  SBBE  = OPCODE(Operation::SBB, AddressMode::INDIRECT_ADDRESS),
  /// Subtract value at memory location with borrow
  SBBM  = OPCODE(Operation::SBB, AddressMode::MEMORY),

  /// Arithmetic shift right with immediate
  ASRI  = OPCODE(Operation::ASR, AddressMode::IMMEDIATE),
  /// Arithmetic shift right by value at indirect adddress
  ASRE  = OPCODE(Operation::ASR, AddressMode::INDIRECT_ADDRESS),
  /// Arithmetic shift right by value at memory location
  ASRM  = OPCODE(Operation::ASR, AddressMode::MEMORY),

  /// Logical shift left with immediate
  LSLI  = OPCODE(Operation::LSL, AddressMode::IMMEDIATE),
  /// Logical shift left by value at indirect adddress
  LSLE  = OPCODE(Operation::LSL, AddressMode::INDIRECT_ADDRESS),
  /// Logical shift left by value at memory location
  LSLM  = OPCODE(Operation::LSL, AddressMode::MEMORY),

  /// Logical shift right with immediate
  LSRI  = OPCODE(Operation::LSR, AddressMode::IMMEDIATE),
  /// Logical shift right by value at indirect adddress
  LSRE  = OPCODE(Operation::LSR, AddressMode::INDIRECT_ADDRESS),
  /// Logical shift right by value at memory location
  LSRM  = OPCODE(Operation::LSR, AddressMode::MEMORY),

  /// Logical AND with immediate
  ANDI  = OPCODE(Operation::AND, AddressMode::IMMEDIATE),
  /// Logical AND with value at indirect adddress
  ANDE  = OPCODE(Operation::AND, AddressMode::INDIRECT_ADDRESS),
  /// Logical AND with value at memory location
  ANDM  = OPCODE(Operation::AND, AddressMode::MEMORY),

  /// Logical OR with immediate
  ORI  = OPCODE(Operation::OR, AddressMode::IMMEDIATE),
  /// Logical OR with value at indirect adddress
  ORE  = OPCODE(Operation::OR, AddressMode::INDIRECT_ADDRESS),
  /// Logical OR with value at memory location
  ORM  = OPCODE(Operation::OR, AddressMode::MEMORY),

  /// Logical XOR with immediate
  XORI  = OPCODE(Operation::XOR, AddressMode::IMMEDIATE),
  /// Logical XOR with value at indirect adddress
  XORE  = OPCODE(Operation::XOR, AddressMode::INDIRECT_ADDRESS),
  /// Logical XOR with value at memory location
  XORM  = OPCODE(Operation::XOR, AddressMode::MEMORY),

  /// Move low byte of immediate
  MOVLI = OPCODE(Operation::MOVL, AddressMode::IMMEDIATE),
  /// Move low byte of value at indirect address
  MOVLE = OPCODE(Operation::MOVL, AddressMode::INDIRECT_ADDRESS),
  /// Move low byte of value at memory location
  MOVLM = OPCODE(Operation::MOVL, AddressMode::MEMORY),

  /// Move high byte of immediate
  MOVHI = OPCODE(Operation::MOVH, AddressMode::IMMEDIATE),
  /// Move high byte of value at indirect address
  MOVHE = OPCODE(Operation::MOVH, AddressMode::INDIRECT_ADDRESS),
  /// Move high byte of value at memory location
  MOVHM = OPCODE(Operation::MOVH, AddressMode::MEMORY),
};

}
