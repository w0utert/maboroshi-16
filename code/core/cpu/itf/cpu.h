/// Header file for the main CPU-related features of the maboroshi-16.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#pragma once

#include "core/common/itf/types.h"
#include "core/common/itf/bit_range.h"

#include "core/cpu/itf/debugger.h"

#include "core/peripheral/itf/device.h"

#include <tuple>
#include <vector>
#include <array>
#include <stdexcept>
#include <optional>

// Workaround for a stupid #define in one of the macOS standard library headers (math.h), which clashes
// with one of our enumeration values (Cpu::Flags::Bits::OVERFLOW)
#undef OVERFLOW

namespace core::cpu {

// Forward declaration for the enumeration type for CPU opcodes. These are
// defined in opcodes.h to keep the Cpu class interface readable.
enum class Opcode;

// Forward declaration of the CPU instructions structure
struct Instruction;

/// Enumeration type for CPU registers.
///
/// The maboroshi-16 CPU has 8 general-purpose registers that are each 16 bits
/// wide. The general-purpose registers are all created equal: none of them has
/// any special significance for any specific instructions, and any of the 8
/// available registers can be used in any instruction that takes a general-purpose
/// register as operand(s).
enum class Register
{
  R0 = 0,
  R1 = 1,
  R2 = 2,
  R3 = 3,
  R4 = 4,
  R5 = 5,
  R6 = 6,
  R7 = 7,
};

/// Enumeration class for special-purpose registers
enum class SpecialPurposeRegister
{
  SP = 0,   ///< Stack pointer
  SR = 1,   ///< Segment register
};

/// Operand segment indicator
enum class OperandSegment
{
  SRC = 0,  ///< Use source segment
  DST = 1,  ///< Use destination segment
};

/// Namespace for CPU flag bits. These are defined as an enumeration type, but using a scoped enum is
/// inconvenient as it disallows doing bitwise arithmetic using flag bits.
namespace Flag
{
  enum Bits : BYTE
  {
    /// No flags
    NONE = 0,
    /// Carry bit, set/cleared by addition instructions to indicate the result did/did not
    /// exceeded the machine word range, or when shifting out bits.
    CARRY = 1 << 0,
    /// Zero bit, set/cleared by instructions that produce a value, to indicate whether the value was 0,
    /// or by compare operations to indicate operands where equal
    ZERO = 1 << 1,
    /// Negative bit, set/cleared by instructions that produce a value, to indicate whether the
    /// value was negative, or by comparison operations to indicate the left operand compared
    /// less than the right operation
    NEGATIVE = 1 << 2,
    /// Overflow bit, set/cleared by operations that flip the MSB (sign bit) because of arithmetic overflow
    OVERFLOW = 1 << 3,
    /// IRQ disable flag, if set interrupt requests are ignored
    DISABLE_IRQ = 1 << 7,
    /// All arithmetic flags, useful to clear/set using SET or clear, after instructions that modify them
    ALL_ARITHMETIC = 0b00001111,
    /// All flags, useful to clear/set all flags using SET or CLR instructions
    ALL =  0b10001111,
  };

  std::ostream &operator<<(std::ostream &out, const Bits &r);
}

/// Implements the maboroshi-16 CPU.
///
/// \todo the memory map (address space, RAM regions, I/O regions) should be defined
///   outside the currentNamespace of the Cpu class itself, it is a machine property and not a CPU
///   property. Currently all address space/RAM sizes, address translation, I/O etc. are
///   implemented directly as interfaces of the Cpu class.
class Cpu
{
  public:
    /// CPU Hz (16 Mhz)
    static constexpr DWORD HZ = 16000000;

    /// Total address page bits (3 bits, 8 pages)
    static constexpr BYTE ADDRESS_PAGE_BITS = 3;
    /// Number of address pages
    static constexpr BYTE NUM_ADDRESS_PAGES = (1 << ADDRESS_PAGE_BITS);
    /// System RAM page bits (2 bits, lower 4 pages of address space)
    static constexpr BYTE RAM_PAGE_BITS = ADDRESS_PAGE_BITS - 1;
    /// Number or RAM pages
    static constexpr BYTE NUM_RAM_PAGES = (1 << RAM_PAGE_BITS);
    /// RAM size in machine words.
    static constexpr DWORD RAM_SIZE = NUM_RAM_PAGES * 0x10000;

    /// Initial instruction pointer into main RAM region after CPU reset
    static constexpr LongPointer IP_0 = { 0x1, 0x0000 };
    /// Initial segment register (0x0101, ie: source and destination segment both 0x01)
    static constexpr WORD SR_0 = { 0x0101 };
    /// Stack size in machine words (1K words)
    static constexpr WORD STACK_SIZE = 1 << 10;
    /// Stack offset into zero-page RAM region. The stack grows downwards, ie: pushing a value decreases the stack pointer
    static constexpr WORD STACK_OFFSET = 0xFFFF;

    /// Offset of IRQ table in zero page. Each entry in the table consists of 2 machine words that hold a (segment, offset) vector
    static constexpr WORD IRQ_TABLE_OFFSET = 0x1000;

    /// Peripheral bus ID bits. This determines the highest valid peripheral device ID.
    static constexpr BYTE BUS_ID_BITS = 4;
    /// Number of peripheral device ID's
    static constexpr BYTE NUM_BUS_IDS = (1 << BUS_ID_BITS);
    /// Port bits. This determines the highest valid port number.
    static constexpr BYTE PORT_BITS = 9;
    /// Maxmimum number of ports per devioce
    static constexpr WORD NUM_PORTS = (1 << PORT_BITS);

    /// Processor state.
    ///
    /// The current processor state is fully encoded by the members of this structure,
    /// which currently includes instruction & stack pointers, CPU flags and the register
    /// file.
    struct State
    {
      /// Default constructor that sets the CPU state to initial (powerup) valus
      State()
      :
        ip(IP_0),
        sp(STACK_OFFSET),
        sr(SR_0),
        flags({}),
        registers({}),
        irq(0),
        performanceCounter(0)
      {
      }

      /// Instruction pointer
      LongPointer ip;
      /// Stack pointer. The stack is always located in the zero-page, so the stack pointer
      /// is stored as a 16-bit word instead of a long pointer. The stack grows downwards (stack
      /// pointer decreases on push, increased on pop). The stack pointer can also be manipulated
      /// directly through the \a LDSP and \a STSP instruction.
      WORD sp;
      /// Segment register. This special-purpose register controls the source and destination
      /// segment for instructions that take indirect address operands. The high byte indicates the
      /// segment to use for destination addresses, the low byte indicates the segment for
      /// source addresses. This register is manipulated through the \a LDSP and \a STSP
      /// instruction.
      WORD sr;

      /// Processor flags
      struct
      {
        union
        {
          /// All flag bits
          BYTE bits;

          /// Carry flag bit
          BitRange<BYTE, bool, 0, 1> carry;
          /// Zero flag bit
          BitRange<BYTE, bool, 1, 1> zero;
          /// Negative flag bit
          BitRange<BYTE, bool, 2, 1> negative;
          /// Overflow flag bit
          BitRange<BYTE, bool, 3, 1> overflow;
          /// IRQ disable flag bit
          BitRange<BYTE, bool, 7, 1> disableIrq;
        };
      } flags;

      /// Register file
      union
      {
        struct
        {
          WORD r0;
          WORD r1;
          WORD r2;
          WORD r3;
          WORD r4;
          WORD r5;
          WORD r6;
          WORD r7;
        };

        std::array<WORD, 8> all;

      } registers;

      /// Device IRQ flags
      DWORD irq;

      /// Performance counter
      uint64_t performanceCounter;

   };

    /// Default constructor.
    ///
    /// This leaves the CPU in the initial state.
    Cpu();

    /// Reset CPU.
    ///
    /// This leaves the CPU in its initial state:
    ///
    ///   * \a ip register points to 0x00001000
    ///   * \a sp register points to 0x00000800
    ///   * \a sr register is set to 0x0101 (source and destination segment both 0x01)
    ///   * all CPU flags are cleared
    ///   * all general-purpose registers have value 0x0000
    ///
    void reset();

    /// Advance CPU state by at most the specified number of instructions.
    ///
    /// The actual number of ticks executed may be less than the passed number of \p ticks
    /// if the debugger was active and the CPU hit a breakpoint, in which case a Break
    /// exception is thrown. The CPU performance counter can be used to inspect how many
    /// ticks were executed up to when the breakpoint was hit.
    ///
    /// This will check and service any pending interrupt requests once when called, but
    /// assumes no new interrupts are generated until the next invocation. In other words,
    /// the machine control loop should make sure not to advance the CPU in increments
    /// longer than the desired interrupt resolution
    ///
    /// \param ticks number of instructions to advance CPU state by, defaults to 1.
    /// \throws Break if the debugger was active and the CPU hit a breakpoint
    void tick(const size_t ticks = 1);

    /// \return CPU state
    const State &getState() const;

    /// \return Instruction pointer
    LongPointer getInstructionPointer() const;

    /// \return Stack pointer
    WORD getStackPointer() const;

    /// \return segment register as (source segment, destination segment) tuple
    std::tuple<BYTE, BYTE> getSegmentRegister() const;

    /// Return value of named register.
    ///
    /// \param r register to return
    /// \return register value
    WORD getRegister(const Register r) const;

    /// Return reference to memory address at specified \p offset in zero page.
    ///
    /// \param o zero-page offset of memory address to get reference to
    /// \return reference to zero-page memory address
    const WORD &getMemory(const WORD o) const;

    /// Return value at direct memory location.
    ///
    /// \param p memory location for which to get value
    /// \return value at memory location
    const WORD &getMemory(const LongPointer p) const;

    /// Return value at indirect address.
    ///
    /// An indirect address has the form [multiplier*[base register] + offset]. In its 'normal form' it
    /// indicates a location in either the current source segment or destination segment, as configured
    /// through the CPU segment register. The \p segmentOperand parameter is used to indicate which of
    /// these segments the indirect address should be resolved to (source or destination).
    ///
    /// In the 'zero-page form', where the multiplier is zero, an indirect address always addresses a
    /// location in the zero page, and scaling of the base register is not possible.
    ///
    /// \param ia indirect address for which to get value
    /// \param operandSegment whether to resolve the indirect address to the source or destination segment
    /// \return value at indirect address
    const WORD &getMemory(const IndirectAddress ia, const OperandSegment segmentOperand) const;

    /// Return full 8 bits of CPU flags register.
    ///
    /// \return CPU flags register
    BYTE getFlags() const;

    /// Query specific CPU flag bit.
    ///
    /// \param flag to query
    /// \return whether the passed flag bit is set
    bool getFlag(const Flag::Bits flag) const;

    /// \return CPU performance counter (elapsed ticks since reset)
    uint64_t getPerformanceCounter() const;

    /// \return reference to base of system RAM
    std::vector<WORD> &getRam();

    /// Connect device to CPU I/O bus.
    ///
    /// \todo TODO this is temporary/transitional, to be able to incrementally
    ///   move all device I/O and memory access logic into a 'bus' class that
    ///   abstracts these things, and can be passed into the Cpu class on construction
    ///   to connect the CPU to peripherals and RAM.
    ///
    /// \param device device to connect
    /// \throw std::runtime_error
    void connectDevice(peripheral::Device *device);

    /// Switch memory bank.
    ///
    /// This will map the passed \p page of \p device memory into the specified \p addressPage,
    /// enabling it for reads and writes, replacing any previously mapped or device RAM page.
    ///
    /// Only high pages in the address space above the pages mapped to system RAM are mappable,
    /// which are currently hard-coded to page 4, 5, 6 and 7.
    ///
    /// \todo TODO this is temporary/transitional, to be able to incrementally
    ///   move all device I/O and memory access logic into a 'bus' class that
    ///   abstracts these things, and can be passed into the Cpu class on construction
    ///   to connect the CPU to peripherals and RAM.
    ///
    /// \param id source page peripheral device id
    /// \param page source page
    /// \param addressPage destination page in CPU address space
    /// \throw std::runtime_error if the page is not mappable (invalid device \p id or device \p page
    ///   or destination \p addressPage out of range)
    void switchBank(const peripheral::Device::ID &id, const BYTE page, const BYTE addressPage);

    /// \return a reference to the CPU debugger
    Debugger &getDebugger();

  private:
    State _state;

    std::vector<WORD> _ram;

    std::array<peripheral::Device *, NUM_BUS_IDS> _devices;

    std::array<WORD *, NUM_ADDRESS_PAGES> _addressPages;

    Debugger _debugger;
    std::optional<LongPointer> _lastBreak;

    // Memory translation functions

    LongPointer translate(const IndirectAddress ia, const OperandSegment operandSegment) const;

    // Read/write register/memory accessors

    WORD &reg(const Register r);
    WORD &mem(const WORD o);
    WORD &mem(const LongPointer p);

    WORD *getUnarySrcDstOperands(const Instruction &instruction);
    std::tuple<WORD *, WORD> getBinarySrcDstOperands(const Instruction &instruction);

    // CPU control functions

    Instruction fetch(const bool peek = false);

    void setIrqFlags();
    void serviceIrq(BYTE irq);

    // Instruction set implementation

    void add(const Instruction &add);
    void adc(const Instruction &add);
    void and_(const Instruction &and_);
    void asr(const Instruction &asr);
    void b(const Instruction &b);
    void bank(const Instruction &bank);
    void call(const Instruction &call);
    void clr(const Instruction &clr);
    void cmp(const Instruction &cmp);
    void dec(const Instruction &dec);
    void div(const Instruction &div);
    void divs(const Instruction &divs);
    void in(const Instruction &in);
    void inc(const Instruction &inc);
    void halt(const Instruction &halt);
    void j(const Instruction &j);
    void ldsp(const Instruction &ldsp);
    void lsl(const Instruction &lsl);
    void lsr(const Instruction &lsr);
    void mov(const Instruction &mov);
    void movh(const Instruction &movh);
    void movl(const Instruction &movl);
    void mul(const Instruction &mul);
    void muls(const Instruction &muls);
    void neg(const Instruction &neg);
    void not_(const Instruction &not_);
    void or_(const Instruction &or_);
    void out(const Instruction &out);
    void pop(const Instruction &pop);
    void pops(const Instruction &pops);
    void push(const Instruction &push);
    void pushs(const Instruction &pushs);
    void ret(const Instruction &ret);
    void rsv(const Instruction &rsv);
    void rti(const Instruction &rti);
    void set(const Instruction &set);
    void stsp(const Instruction &stsp);
    void sub(const Instruction &sub);
    void sbb(const Instruction &sbb);
    void wait(const Instruction &wait);
    void xor_(const Instruction &sub);
};

}
