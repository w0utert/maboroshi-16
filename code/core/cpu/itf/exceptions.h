/// Declares exception types thrown from CPU-related functions.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#pragma once

#include <exception>

namespace core::cpu {

/// CPU halted
class Halted : public std::exception {};

/// Cpu breakpoint hit
class Break : public std::exception {};

}
