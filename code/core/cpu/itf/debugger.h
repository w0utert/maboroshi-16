/// Header file for the Debugger class, the main interface for all CPU debugging facilities.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#pragma once

#include "core/common/itf/types.h"

#include <unordered_map>

namespace core::cpu {

/// Main interface class for the CPU debugging facilities.
///
/// Currently this is not much more than an object that can be used to set and query breakpoints
/// directly using LongPpinter addresses, and to set the debugger mode (disabled, single-step
/// or run-until-breakpoint). Later, this should probably be extended with facilities to relate code
/// addresses to debug symbols etc.
class Debugger
{
  public:
    /// Enumeration type used to indicate debugger mode of operation
    enum class Mode
    {
      DISABLED,             ///< Disable debugger
      SINGLE_STEP,          ///< Enable single-step debugging mode
      RUN_TO_BREAKPOINT     ///< Enable run-until-breakpoint mode
    };

    /// Enumeration type to indicate debugger state
    enum class State
    {
      RUNNING,              ///< Running
      STOPPED,              ///< Stopped
    };

    Debugger();

    /// Set breakpoint.
    ///
    /// \param address address to set breakpoint at
    void setBreakpoint(const LongPointer &address);

    /// Delete breakpoint.
    ///
    /// \param address address of breakpoint to delete
    void deleteBreakpoint(const LongPointer &address);

    /// Query breakpoint.
    ///
    /// \param address address for which to query breakpoint
    /// \return whether a breakpoint is set at the current \p address
    bool isBreakpoint(const LongPointer &address) const;

    /// Stop program
    void stop();

    /// Resume running.
    ///
    /// Would have preferred to call this 'continue' but that's a reserved keyword :-(
    void resume();

    /// Single-step instruction
    void step();

    /// \return current debugger mode of operation
    Mode getMode() const { return _mode; }

    /// Set debugger mode
    ///
    /// \param mode debugger mode
    void setMode(const Mode mode) { _mode = mode; }

    /// \return current debuggser state
    State getState() const { return _state; }

  private:
    Mode _mode;
    State _state;

    std::unordered_map<size_t, LongPointer> _breakpoints;
};

}