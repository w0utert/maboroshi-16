# Find libpng library and headers
#
# This uses the following environment variables to find libpng:
#
#   - PNG_ROOT
#
# Sets the following variables:
#
#   - PNG_FOUND
#   - PNG_LOCATION
#   - PNG_INCLUDE_DIR
#   - PNG_LIBRARY

find_package (PackageHandleStandardArgs)

find_path (PNG_LOCATION include/png.h $ENV{PNG_ROOT})

if (PNG_LOCATION)
  find_library(PNG_LIBRARY png PATHS ${PNG_LOCATION}/lib)
  set (PNG_INCLUDE_DIR ${PNG_LOCATION}/include)
endif ()

find_package_handle_standard_args (PNG DEFAULT_MSG PNG_LIBRARY PNG_INCLUDE_DIR)

