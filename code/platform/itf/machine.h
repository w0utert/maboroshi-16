/// Header file for the Machine class, the main interface for creating and controlling the
/// maboroshi-16 system.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#pragma once

#include "core/common/itf/types.h"

#include "core/gpu/itf/gpu.h"
#include "core/cpu/itf/cpu.h"

#include <tbb/task_group.h>

#include <memory>
#include <array>
#include <map>
#include <chrono>

namespace core::timer { class Timer; };
namespace core::input { class Controllers; };

namespace platform {

/// Main interface class for creating and controlling the maboroshi-16.
///
/// \todo comment once this converges to something resembling a functional interface
class Machine
{
  public:
    /// GPU device ID, and also the single IRQ line it uses
    static constexpr core::peripheral::Device::ID GPU_DEVICE_ID = 0x02;
    /// Timer device ID, and also the single IRQ line it uses
    static constexpr core::peripheral::Device::ID TIMER_DEVICE_ID = 0x03;
    /// Controllers device ID
    static constexpr core::peripheral::Device::ID CONTROLLERS_DEVICE_ID = 0x04;

    /// Number of controllers
    static constexpr auto NUM_CONTROLLERS = 2;

    /// Structure to hold machine constants used to configure things like
    /// CPU frequency, screen refresh rate, memory sizes, etc.
    struct Constants
    {
      /// Default constructor, initializes machine constants to sensible defaults
      Constants()
      :
        cpuHz(16'000'000), frameRate(60),
        screenWidth(64*8), screenHeight(36*8)
      {
      };

      /// CPU frequency in Herz
      unsigned int cpuHz;
      /// Machine framerate
      unsigned int frameRate;

      /// Screen width in pixels
      unsigned int screenWidth;
      /// Screen height in pixels
      unsigned int screenHeight;
    };

    /// Input state for a single controller
    struct ControllerState
    {
      ControllerState()
      :
        a(false), b(false), x(false), y(false), start(false), select(false)
      {
      }

      bool a;
      bool b;
      bool x;
      bool y;
      bool start;
      bool select;
    };

    /// Machine runtime statistics.
    struct Statistics
    {
      std::chrono::microseconds frameTime;
      double speedup;
      std::chrono::microseconds gpuTime;
    };

    /// Frame scheduler utility class.
    ///
    /// The name of this class is slightly misleading because it does not actually schedule
    /// anything itself, but since it is used from the machine loop to schedule per-frame
    /// tasks, it is close enough ;-)
    ///
    /// What this class does, is split the total number of CPU cycles per frame into intervals
    /// at which emulation events occur (e.g. v-blank start, scan-out start, etc), then provide an
    /// interface to step the emulator main loop until next emulation event, and know how much
    /// emulation time remains after that. This allows the host program, which is throttled
    /// by the display rate, to drive the emulator main loop in time increments below or above
    /// the emulator frame-time, and run at a different frame rate compared to the emulated system
    /// without affecting emulation speed (not speeding up the emulated program when running on
    /// a 144Hz screen, for example).
    class Scheduler
    {
      public:
        /// Enumeration type for per-frame events
        enum class Event
        {
          FRAME_START,
          VBLANK_START,
          SCANOUT_START,
          FRAME_END,
          NONE
        };

        /// Default constructor
        Scheduler() : _cpu(nullptr) { };

        /// Constructor.
        ///
        /// \param cpu CPU instance controlled by the scheduler
        /// \param ticksPerFrame number of CPU ticks in a single emulated frame
        Scheduler(core::cpu::Cpu * const cpu, const int64_t ticksPerFrame);

        /// Advance CPU by the passed number of \p ticks, or until the start of the next scheduler
        /// phase if this crosses the end of the current scheduler phase.
        ///
        /// \param ticks number of ticks to advance
        /// \return a pair indicating the actual number of ticks advanced, and any events generated
        ///   as a result of advancing to a next phase.
        std::pair<int64_t, std::vector<Event>> tick(const int64_t ticks);

      private:
        core::cpu::Cpu *_cpu;

        std::map<int64_t, std::vector<Event>> _phases;
        std::optional<int64_t> _currentTicks;
    };

    /// Constructor.
    ///
    /// \param constants machine constants
    Machine(const Constants &constants);

    /// Destructor
    ~Machine();

    /// \return machine constants
    const Constants &getConstants() const { return _constants; }

    /// \return machine runtime statistics
    const Statistics &getStatistics() const { return _statistics; }

    /// \return machine CPU device
    core::cpu::Cpu *getCpu() { return _cpu.get(); }

    /// Emulate passed number of microseconds (emulator time).
    ///
    /// \param time number of microseconds to emulate.
    void tick(const std::chrono::microseconds time);

    /// Set controller state for numbered controller.
    ///
    /// \param controller controller index (0-based)
    /// \param state controller button state
    /// \throw std::runtime_error if the controller index is out-of range.
    void setControllerState(const unsigned int controller, const ControllerState &state);

    /// \return latest video frame produced by the GPU.
    core::gpu::Frame getFrame();

  private:
    Constants _constants;
    Statistics _statistics;
    Scheduler _scheduler;

    std::unique_ptr<core::cpu::Cpu> _cpu;
    std::unique_ptr<core::gpu::Gpu> _gpu;
    std::unique_ptr<core::timer::Timer> _timer;
    std::unique_ptr<core::input::Controllers> _controllers;

    std::array<ControllerState, NUM_CONTROLLERS> _controllerState;

    tbb::task_group _frameTask;
};

}
