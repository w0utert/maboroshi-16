/// Implementes the Machine class, the main interface for creating and controlling the maboroshi-16.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#include "platform/itf/machine.h"

#include "core/timer/itf/timer.h"
#include "core/input/itf/controller.h"

#include <chrono>

using namespace std::chrono;

using namespace core;
using namespace core::cpu;
using namespace core::peripheral;

namespace platform {

// Initialize frame scheduler
Machine::Scheduler::Scheduler(core::cpu::Cpu * const cpu, const int64_t ticksPerFrame)
:
  _cpu(cpu)
{
  const int64_t vblank_ticks = ticksPerFrame / 4;
  const int64_t scanout_ticks = ticksPerFrame - vblank_ticks;

  _phases = {
    { 0, { Event::FRAME_START, Event::VBLANK_START } },
    { vblank_ticks, { Event::SCANOUT_START } },
    { vblank_ticks + scanout_ticks, { Event::FRAME_END } }
  };
}

// Advance scheduler by the passed number of ticks, or up to the next emulation event (whichever comes first),
// returning the number of ticks 'spent' and the emulator event that occurred before the end of the passed
// time interval (if any)
std::pair<int64_t, std::vector<Machine::Scheduler::Event>> Machine::Scheduler::tick(const int64_t ticks)
{
  if (!_currentTicks)
  {
    // Current phase is 'pre-frame'. Move to start of first phase and return associated events
    _currentTicks = 0;

    return { 0, _phases.begin()->second };
  }
  else
  {
    // Get iterator to current phase
    const auto next_phase = _phases.upper_bound(*_currentTicks);

    // Advance CPU by the requested number of ticks, or by the number of ticks left in the current phase, whichever is less
    const int64_t phase_ticks_left = next_phase->first - *_currentTicks;
    const int64_t max_ticks = std::min(ticks, phase_ticks_left);

    const int64_t ticks_before = _cpu->getPerformanceCounter();

    _cpu->tick(max_ticks);

    // We use the CPU performance counter to see how many ticks were actually emulated, which may be less then the number
    // we requested, for example in case a CPU exception occurred (none of these are currently handled though)
    const int64_t spent = _cpu->getPerformanceCounter() - ticks_before;

    *_currentTicks += spent;

    // Check if we reached the beginning of the next phase. If so, return the events associated with it, If
    // we reached the end of the frame, reset the scheduler for the next frame.
    if (*_currentTicks == next_phase->first)
    {
      if (std::next(next_phase) == _phases.end())
      {
        _currentTicks.reset();
      }

      return { spent, next_phase->second };
    }
    else
    {
      return { spent, {} };
    }
  }
}

// Constructor
Machine::Machine(const Machine::Constants &constants)
:
  _constants(constants), _statistics({})
{
  _cpu = std::make_unique<core::cpu::Cpu>();
  _gpu = std::make_unique<core::gpu::Gpu>(GPU_DEVICE_ID, constants.screenWidth, constants.screenHeight, GPU_DEVICE_ID);
  _timer = std::make_unique<core::timer::Timer>(TIMER_DEVICE_ID, TIMER_DEVICE_ID);
  _controllers = std::make_unique<core::input::Controllers>(CONTROLLERS_DEVICE_ID);

  _cpu->connectDevice(_gpu.get());
  _cpu->connectDevice(_timer.get());
  _cpu->connectDevice(_controllers.get());

  _scheduler = Scheduler(_cpu.get(), constants.cpuHz / constants.frameRate);
}

// Destructor
Machine::~Machine() = default;

void Machine::tick(const std::chrono::microseconds time)
{
  // Return immediately without touching any CPU state if the debugger is active and currently stopped
  if ((_cpu->getDebugger().getMode() != Debugger::Mode::DISABLED) && (_cpu->getDebugger().getState() == Debugger::State::STOPPED))
  {
    return;
  }

  const high_resolution_clock::time_point frame_start = high_resolution_clock::now();

  double ticks_per_microsecond = _constants.cpuHz / 1'000'000.0;

  auto ticks_remaining = static_cast<int64_t>(time.count() * ticks_per_microsecond);

  while (ticks_remaining != 0)
  {
    auto [ ticks_spent, events ] = _scheduler.tick(ticks_remaining);

    for (const Scheduler::Event event : events)
    {
      switch (event)
      {
        case Scheduler::Event::FRAME_START:
        {
          // Frame start, copy controller input state tot controller device
          _controllers->setControllerState(_controllerState[0], _controllerState[1]);
          break;
        }

        case Scheduler::Event::SCANOUT_START:
        {
          // Scanout start. Kick off asynchronous GPU task to render a single frame, and step all other frame-based devices
          _frameTask.run([this]() { _gpu->frame(); });

          _timer->frame();
          break;
        }

        case Scheduler::Event::FRAME_END:
        {
          // Frame end, block until the GPU finished rendering the current frame
          _frameTask.wait();

          _statistics.frameTime = duration_cast<microseconds>(high_resolution_clock::now() - frame_start);
          _statistics.gpuTime = _gpu->getRenderTime();
          _statistics.speedup = (double) time.count() / _statistics.frameTime.count();
        }

        default:
          break;
      }
    }

    ticks_remaining -= ticks_spent;
  }
}

// Set controller state
void Machine::setControllerState(const unsigned int controller, const ControllerState &state)
{
  if (controller >= _controllerState.size())
  {
    throw std::runtime_error(fmt::format("Controller index {0} out of range", controller));
  }

  _controllerState[controller] = state;
}

// Return last video frame
core::gpu::Frame Machine::getFrame()
{
  return _gpu->getFrame();
}

}
