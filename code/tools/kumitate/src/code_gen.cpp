/// Implements the code generation phase of the Kumitate assembler.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#include "code_gen.h"
#include "opcode_signatures.h"

#include "tools/kumitate/itf/assembler.h"

#include <functional>
#include <numeric>

namespace tools::kumitate::codegen {

// Apply code generation functor.
core::cpu::Instruction CodeGen::operator()(const parser::Instruction &instruction) const
{
  const Signature signature(instruction.operands);

  const auto opcode_signature = OPCODE_SIGNATURES.find({ instruction.operation, signature });

  if (opcode_signature != OPCODE_SIGNATURES.end())
  {
    const core::cpu::Opcode opcode = opcode_signature->second;

    switch (signature)
    {
      // Instruction without operands
      case Signature(Operand::Type::NONE):
      {
        return { opcode };
      }

      // Unary instruction with register operand
      case Signature(Operand::Type::REG):
      {
        const auto reg = boost::get<core::cpu::Register>(instruction.operands[0].value);

        return { opcode, reg };
      }

      // Unary instruction with immediate operand
      case Signature(Operand::Type::IMMEDIATE):
      {
        const auto imm16 = static_cast<core::WORD>(boost::get<int64_t>(instruction.operands[0].value));

        return { opcode, imm16 };
      }

      // Unary instruction with flag bits operand
      case Signature(Operand::Type::FLAGS):
      {
        const auto flags = boost::get<parser::Flags>(instruction.operands[0].value);

        return { opcode, flags.bits };
      }

      // Unary instruction with direct address operand
      case Signature(Operand::Type::DIRECT_ADDRESS):
      {
        const auto address = boost::get<parser::DirectAddress>(instruction.operands[0].value);

        return { opcode, core::LongPointer(address.segment, address.offset) };
      }

      // Unary instruction with indirect address operand
      case Signature(Operand::Type::INDIRECT_ADDRESS):
      {
        return { opcode, boost::get<parser::IndirectAddress>(instruction.operands[0].value) };
      }

      // Unary instruction with vector-address operand
      case Signature(Operand::Type::VECTOR):
      {
        const auto vector = boost::get<parser::JumpVector>(instruction.operands[0].value);
        const auto address = boost::get<parser::DirectAddress>(vector);

        return { opcode, core::LongPointer(address.segment, address.offset) };
      }

      // Register-register instruction
      case Signature(Operand::Type::REG, Operand::Type::REG):
      {
        const auto reg0 = boost::get<core::cpu::Register>(instruction.operands[0].value);
        const auto reg1 = boost::get<core::cpu::Register>(instruction.operands[1].value);

        return { opcode, reg0, reg1 };
      }

      // Special purpose register, register instruction
      case Signature(Operand::Type::SP_REG, Operand::Type::REG):
      {
        const auto reg0 = boost::get<core::cpu::SpecialPurposeRegister>(instruction.operands[0].value);
        const auto reg1 = boost::get<core::cpu::Register>(instruction.operands[1].value);

        return { opcode, reg0, reg1 };
      }

      // Register, special purpose register instruction
      case Signature(Operand::Type::REG, Operand::Type::SP_REG):
      {
        const auto reg0 = boost::get<core::cpu::Register>(instruction.operands[0].value);
        const auto reg1 = boost::get<core::cpu::SpecialPurposeRegister>(instruction.operands[1].value);

        return { opcode, reg0, reg1 };
      }

      // Register-register instruction with modifier
      case Signature(Operand::Type::REG, Operand::Type::REG, Operand::Type::BOOLEAN):
      {
        const auto reg0 = boost::get<core::cpu::Register>(instruction.operands[0].value);
        const auto reg1 = boost::get<core::cpu::Register>(instruction.operands[1].value);
        const auto b = boost::get<bool>(instruction.operands[2].value);

        return { opcode, reg0, reg1, b };
      }

      // 2-operand instruction with register and immediate
      case Signature(Operand::Type::REG, Operand::Type::IMMEDIATE):
      {
        const auto reg = boost::get<core::cpu::Register>(instruction.operands[0].value);
        const auto imm16 = static_cast<core::WORD>(boost::get<int64_t>(instruction.operands[1].value));

        return { opcode, reg, imm16 };
      }

      // 2-operand instruction with register, immediate and modifier
      case Signature(Operand::Type::REG, Operand::Type::IMMEDIATE, Operand::Type::BOOLEAN):
      {
        const auto reg = boost::get<core::cpu::Register>(instruction.operands[0].value);
        const auto imm16 = static_cast<core::WORD>(boost::get<int64_t>(instruction.operands[1].value));
        const auto b = boost::get<bool>(instruction.operands[2].value);

        return { opcode, reg, imm16, b };
      }

      // 2-operand instruction with register and direct address
      case Signature(Operand::Type::REG, Operand::Type::DIRECT_ADDRESS):
      {
        const auto reg = boost::get<core::cpu::Register>(instruction.operands[0].value);
        const auto address = boost::get<parser::DirectAddress>(instruction.operands[1].value);

        return { opcode, reg, core::LongPointer(address.segment, address.offset) };
      }

      // 2-operand instruction with register, direct address and modifier
      case Signature(Operand::Type::REG, Operand::Type::DIRECT_ADDRESS, Operand::Type::BOOLEAN):
      {
        const auto reg = boost::get<core::cpu::Register>(instruction.operands[0].value);
        const auto address = boost::get<parser::DirectAddress>(instruction.operands[1].value);
        const auto b = boost::get<bool>(instruction.operands[2].value);

        return { opcode, reg, core::LongPointer(address.segment, address.offset), b };
      }

      // 2-operand instruction with register and indirect address
      case Signature(Operand::Type::REG, Operand::Type::INDIRECT_ADDRESS):
      {
        const auto reg = boost::get<core::cpu::Register>(instruction.operands[0].value);
        const auto ia = boost::get<parser::IndirectAddress>(instruction.operands[1].value);

        return { opcode, reg, ia };
      }

      // 2-operand instruction with register, indirect address and modifier
      case Signature(Operand::Type::REG, Operand::Type::INDIRECT_ADDRESS, Operand::Type::BOOLEAN):
      {
        const auto reg = boost::get<core::cpu::Register>(instruction.operands[0].value);
        const auto ia = boost::get<parser::IndirectAddress>(instruction.operands[1].value);
        const auto b = boost::get<bool>(instruction.operands[2].value);

        return { opcode, reg, ia, b };
      }

      // 2-operand instruction with direct address and register
      case Signature(Operand::Type::DIRECT_ADDRESS, Operand::Type::REG):
      {
        const auto address = boost::get<parser::DirectAddress>(instruction.operands[0].value);
        const auto reg = boost::get<core::cpu::Register>(instruction.operands[1].value);

        return { opcode, core::LongPointer(address.segment, address.offset), reg };
      }

      // 2-operand instruction with direct address, register and modifier
      case Signature(Operand::Type::DIRECT_ADDRESS, Operand::Type::REG, Operand::Type::BOOLEAN):
      {
        const auto address = boost::get<parser::DirectAddress>(instruction.operands[0].value);
        const auto reg = boost::get<core::cpu::Register>(instruction.operands[1].value);
        const auto b = boost::get<bool>(instruction.operands[2].value);

        return { opcode, core::LongPointer(address.segment, address.offset), reg, b };
      }

      // 2-operand instruction with indirect address and register
      case Signature(Operand::Type::INDIRECT_ADDRESS, Operand::Type::REG):
      {
        const auto ia = boost::get<parser::IndirectAddress>(instruction.operands[0].value);
        const auto reg = boost::get<core::cpu::Register>(instruction.operands[1].value);

        return { opcode, ia, reg };
      }

      // 2-operand instruction with indiret address, register and modifier
      case Signature(Operand::Type::INDIRECT_ADDRESS, Operand::Type::REG, Operand::Type::BOOLEAN):
      {
        const auto ia = boost::get<parser::IndirectAddress>(instruction.operands[0].value);
        const auto reg = boost::get<core::cpu::Register>(instruction.operands[1].value);
        const auto b = boost::get<bool>(instruction.operands[2].value);

        return { opcode, ia, reg, b };
      }

      // Branch instruction
      case Signature(Operand::Type::BRANCH_CONDITION, Operand::Type::DIRECT_ADDRESS):
      {
        const auto bc = boost::get<core::cpu::BranchCondition>(instruction.operands[0].value);
        const auto address = boost::get<parser::DirectAddress>(instruction.operands[1].value);

        // Calculate current instruction pointer, which will be one instruction past the branch
        // instruction, and branch destination address.
        const core::LongPointer instruction_pointer = instruction.address + core::cpu::Instruction::size();

        // Calculate branch distance in machine words, then divide by the instruction size and
        // to get the branch distance in instructions.
        const core::SWORD word_distance = static_cast<int64_t>(core::LongPointer(address.segment, address.offset).linear() - instruction_pointer.linear());
        const core::SWORD instruction_distance = (word_distance / core::cpu::Instruction::size());

        return { opcode, bc, instruction_distance };
      }

      // Quaternary instruction
      case Signature(Operand::Type::REG, Operand::Type::REG, Operand::Type::REG, Operand::Type::REG):
      {
        const auto reg0 = boost::get<core::cpu::Register>(instruction.operands[0].value);
        const auto reg1 = boost::get<core::cpu::Register>(instruction.operands[1].value);
        const auto reg2 = boost::get<core::cpu::Register>(instruction.operands[2].value);
        const auto reg3 = boost::get<core::cpu::Register>(instruction.operands[3].value);

        return { opcode, reg0, reg1, reg2, reg3 };
      }

      // Port move instruction (immediate)
      case Signature(Operand::Type::IMMEDIATE, Operand::Type::IMMEDIATE, Operand::Type::REG):
      {
        const auto device = static_cast<core::BYTE>(boost::get<int64_t>(instruction.operands[0].value));
        const auto port = static_cast<core::WORD>(boost::get<int64_t>(instruction.operands[1].value));
        const auto reg = boost::get<core::cpu::Register>(instruction.operands[2].value);

        return { opcode, core::PortMove(device, port, reg) };
      }

      // Port move instruction (indirect)
      case Signature(Operand::Type::IMMEDIATE, Operand::Type::REG, Operand::Type::REG):
      {
        const auto device = static_cast<core::BYTE>(boost::get<int64_t>(instruction.operands[0].value));
        const auto port = boost::get<core::cpu::Register>(instruction.operands[1].value);
        const auto reg = boost::get<core::cpu::Register>(instruction.operands[2].value);

        return { opcode, core::PortMove(device, port, reg) };
      }

      default:
        break;
    }
  }

  // If we get to this point, the instruction has an invalid combination of operation and operands.
  throw InvalidSignature("Invalid signature", instruction);
}

// Apply DataGen functor to get bytes for parsed line of byte byteData
std::vector<core::WORD> DataGen::operator()(const parser::ByteData &byteData) const
{
  const unsigned int line_size = parser::DataLine::LineSize()(byteData);

  std::vector<core::WORD> word_data(line_size);

  for (size_t i = 0; i < line_size; i++)
  {
    // Note that the parser stores 64 bits for every parsed byte to be able to
    // validate that the parsed values fit into 8-bits, hence the static casts here.
    const auto high = static_cast<core::BYTE>(byteData.bytes[i * 2]);
    const auto low = static_cast<core::BYTE>((i*2 + 1) < byteData.bytes.size() ? byteData.bytes[i * 2 + 1] : 0);

    word_data[i] = (high << 8)  | low;
  }

  return word_data;
}

// Apply DataGen functor to get bytes for parsed line of word wordData
std::vector<core::WORD> DataGen::operator()(const parser::WordData &wordData) const
{
  const unsigned int line_size = parser::DataLine::LineSize()(wordData);

  std::vector<core::WORD> word_data(line_size, 0);

  // Note that the parser stores 64 bits for every parsed word to be able to
  // validate that the parsed values fit into 8-bits, hence the std::transform and
  // static cast here
  std::transform(wordData.words.begin(), wordData.words.end(), word_data.begin(), [](const auto &wd)
  {
    return static_cast<core::WORD>(wd);
  });

  return word_data;
}

// Apply DataGen functor to get byte data for parsed string data
std::vector<core::WORD> DataGen::operator()(const parser::StringData &stringData) const
{
  class StringDataGenerator : boost::static_visitor<void>
  {
    public:
      StringDataGenerator()
      :
        _data({ 0 }),
        _attribute(0)
      {
      }

      void operator()(const char &c)
      {
        _data.push_back((_attribute << 8) | c);
        _data[0]++;
      }

      void operator()(const parser::StringData::Attribute &a)
      {
        _attribute = static_cast<core::BYTE>(a.value);
      }

      const std::vector<core::WORD> &data() { return _data; }

    private:
      std::vector<core::WORD> _data;
      core::BYTE _attribute;
  };

  StringDataGenerator string_data_generator;

  std::for_each(stringData.data.begin(), stringData.data.end(), boost::apply_visitor(string_data_generator));

  return string_data_generator.data();
}

// Generate assembly from parsed AST
std::unique_ptr<utils::Assembly> generate(const parser::AST &ast, const AssemblerOptions &options)
{
  // Generate code chunks from code sections
  std::vector<utils::Assembly::CodeChunk> code_chunks;

  for (const auto &section : ast.codeSections)
  {
    const core::LongPointer base(section.base.segment, section.base.offset);

    std::vector<core::cpu::Instruction> instructions(section.instructions.size());

    try
    {
      std::transform(section.instructions.begin(), section.instructions.end(), instructions.begin(), codegen::CodeGen());
    }
    catch (const InvalidSignature &e)
    {
      const utils::Assembly::DebugInfo::LineInfo &line_info = ast.debugInfo.getLineInfo(e.instruction.address);

      throw AssemblerException(
        "Code generation error",
        fmt::format("Invalid operands for '{0}' instruction", e.instruction.operation),
        ast.debugInfo.getFile(line_info.file), line_info.line);
    }

    code_chunks.push_back({ base, instructions });
  }

  // Generate data chunks from data sections
  std::vector<utils::Assembly::DataChunk> data_chunks;

  for (const auto &section : ast.dataSections)
  {
    const core::LongPointer base(section.base.segment, section.base.offset);
    const std::vector<parser::DataLine> &data_lines = section.lines;

    std::vector<core::WORD> data;

    // Calculate chunk size so we can pre-allocate the chunk data once
    const int chunk_size = std::accumulate(data_lines.begin(), data_lines.end(), 0, [](const int n, const parser::DataLine &line)
    {
      return n + line.size();
    });

    data.reserve(chunk_size);

    // Get byte data for each code section and copy it to the chunk data
    for (const parser::DataLine &data_line : section.lines)
    {
      const std::vector<core::WORD> line_data = boost::apply_visitor(codegen::DataGen(), data_line.data);

      data.insert(data.end(), line_data.begin(), line_data.end());
    }

    // Create data chunk from assembled instruction bitfields
    data_chunks.push_back({ base, data });
  }

  // Create assembly from the generated chunks
  const auto debug_info = (options.strip ? std::nullopt : std::make_optional<utils::Assembly::DebugInfo>(ast.debugInfo));

  return std::make_unique<utils::Assembly>(code_chunks, data_chunks, debug_info);
}

}
