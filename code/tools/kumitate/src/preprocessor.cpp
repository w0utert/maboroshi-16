/// Implements the preprocessor functions of the kumitate assembler.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#include "tools/kumitate/itf/preprocessor.h"

#include "tools/kumitate/itf/assembler.h"

#include "utils/itf/tile_set.h"

#include <fmt/format.h>

#include <boost/algorithm/string.hpp>

#include <iostream>
#include <filesystem>
#include <regex>

namespace tools::kumitate::preprocessor {

// Preprocess context. This holds preprocessor settings & environment, and
// tracks parsed preprocessor state such as #defined variable and #includes.
struct Context
{
  // Include search paths
  std::vector<std::string> includePaths;

  // Include files that have already been expanded
  std::set<std::string> includes;
  // Defines parsed from the source text and the values to replace them with
  std::map<std::string, std::string> defines;

  // Include file trail (for outputting filename annotations and error reporting)
  std::vector<std::pair<std::string, int>> trail;

  // Return reference to current file
  const std::string &currentFile() const { return trail.back().first; }

  // Return current line number
  int currentLine() const { return trail.back().second; }

  // Next line
  void nextLine() { trail.back().second++; }
};

// Forward declare the private preprocessor interface, which is called recursively from expand_include
static std::string preprocess(std::istream &source, Context &context, const std::string &name);

// Regex match string, returning the specified group after trimming whitespace
static bool trimmed_matches(const std::string s, const std::regex re, const std::vector<int> &groups, std::vector<std::string> &m)
{
  std::smatch match;

  if (!std::regex_match(s, match, re))
  {
    return false;
  }

  m = std::vector<std::string>(groups.size());

  std::transform(groups.begin(), groups.end(), m.begin(), [&match](const int group)
  {
    std::string trimmed_matches = match[group];
    boost::trim(trimmed_matches);

    return trimmed_matches;
  });

  return true;
}

// Search include paths for a file matching the supplied include file base name and return its absolute path
static std::string get_include_filename(const std::string include, const Context &context)
{
  // Iterate include paths until we find the file in one of them
  for (const std::string &include_path : context.includePaths)
  {
    const std::string include_filename = std::filesystem::path(fmt::format("{0}/{1}", include_path, include)).lexically_normal();

    // Check if include file exists, and if so expand it
    if (std::filesystem::exists(include_filename))
    {
      return include_filename;
    }
  }

  // If we end up here the include file was not found in any of the include pathes
  throw AssemblerException("Preprocessor error", fmt::format("Include '{0}' not found", include), context.currentFile(), context.currentLine());
}

// Expand include file. This will read and preprocess the include file, unless it
// was already #included earlier, in which case it is simply skipped.
static std::optional<std::string> expand_include(const std::string &include, Context &context)
{
  // Iterate include paths until we find the file in one of them
  const std::string include_filename = get_include_filename(include, context); // std::filesystem::path(fmt::format("{0}/{1}", include_path, include)).lexically_normal();

  // Skip include if it was already included before
  if (context.includes.count(include_filename))
  {
    return std::nullopt;
  }

  // Open and preprocess the include file
  std::ifstream include_source(include_filename);

  if (!include_source.is_open())
  {
    throw AssemblerException("Preprocessor error", fmt::format("Failed to open include file '{0}'", include_filename), context.currentFile(), context.currentLine());
  }

  std::ostringstream preprocessed_include;

  preprocessed_include << preprocess(include_source, context, include_filename);

  // Write filename annotation to mark the end of the include
  preprocessed_include << fmt::format("[={0}]", context.currentFile()) << std::endl;

  // Track the include file name to ensure it gets skipped if it is included again
  context.includes.insert(include_filename);

  return preprocessed_include.str();
}

// Expand tile PNG file to byte data
static std::string expand_tiles(const std::string &tiles, const int tileWidth, const int tileHeight, const Context &context)
{
  std::ostringstream expanded_tile_data;

  std::unique_ptr<const utils::TileSet> tile_set = utils::TileSet::fromPng(get_include_filename(tiles, context), tileWidth, tileHeight);

  auto [ tile_width, tile_height ] = tile_set->getTileSize();

  for (size_t tile_index = 0; tile_index < tile_set->getNumTiles(); tile_index++)
  {
    const uint8_t * const tile_data = tile_set->getTileData(tile_index);

    for (int ty = 0; ty < tile_height; ty++)
    {
      expanded_tile_data << fmt::format("[@{0:06}]{1:4} .byte ", context.currentLine(), "");

      for (int tx = 0; tx < tile_width; tx++)
      {
        const std::string separator = ((tx + 1) < tile_width ? ", " : "\n");

        expanded_tile_data << fmt::format("{0:#04x}{1}", tile_data[ty * tile_width + tx], separator);
      }
    }
  }

  return expanded_tile_data.str();
}

// Expand palette stored in PNG file to pack word data
static std::string expand_palette(const std::string &tiles, Context &context)
{
  std::ostringstream expanded_palette_data;

  const utils::Palette palette = utils::TileSet::paletteFromPng(get_include_filename(tiles, context));

  for (const utils::Palette::ARGB1888 &color : palette.data)
  {
    expanded_palette_data << fmt::format("[@{0:06}]{1:4} .word {2:#06x}\n", context.currentLine(), "", color.pack());
  }

  return expanded_palette_data.str();
}

// Preprocess source text
static std::string preprocess(std::istream &source, Context &context, const std::string &name)
{
  static const std::string IDENTIFIER("([a-zA-Z0-9_\\.]+)");

  static const std::regex INCLUDE_REGEX("#include[[:blank:]]+\"([^\"]+)\"");
  static const std::regex TILES_REGEX("#tiles[[:blank:]]+\"([^\"]+)\",[[:blank:]]([[:digit:]]+),[[:blank:]]([[:digit:]]+)");
  static const std::regex PALETTE_REGEX("#palette[[:blank:]]+\"([^\"]+)\"");
  static const std::regex DEFINE_REGEX(fmt::format("#define[[:blank:]]+{0}[[:blank:]]+([[:print:]]+)", IDENTIFIER));
  static const std::regex TEXT_REGEX("([^#;][^;]*)(:?;.+)?");
  static const std::regex COMMENT_REGEX(";.*");
  static const std::regex VARIABLE_REGEX(fmt::format("\\$({0})", IDENTIFIER));

  // The output stream to write preprocessed output to
  std::ostringstream preprocessed_output;

  // The preprocessor main loop is extremely simple: we iterate the input stream line by
  // line, categorizing each line using simple regex matching, then depending on the line
  // type either preprocess the line and write the result to the output stream or ignore it.
  //
  // Lines are currently categorized as any one of the following:
  //
  //   1. Preprocessor directives
  //
  //      The relevant parameters of the directive is extracted and the directive is handled,
  //      which will either modify some preprocessor context state (e.g. #define directives), or
  //      produce an output (e.g. #include directives).
  //
  //   2. Text lines
  //
  //      The contain actual source code text such as instructions, labels, assembler directives,
  //      etc. Preprocessing these will strip possible trailing comments, trim leading & trailing
  //      whitespace, and resolve and replace referenced #define variables.
  //
  //   3. Anything else
  //
  //      This is currently limited to comments, which are simply ignored.
  //
  // The final result written to the output stream is an amalgamation of all the assembler source
  // code referenced by the input file, free of any preprocessor directives or #defined variables,
  // ready to be parsed by the assembler.
  //

  std::string line;

  // Push current file to file trail and write start of file annotation
  context.trail.push_back({ name, 1 });

  preprocessed_output << fmt::format("[={0}]", name) << std::endl;

  while (std::getline(source, line))
  {
    boost::trim(line);

    if (!line.empty())
    {
      std::vector<std::string> m;
      std::string preprocessed_line;

      if (trimmed_matches(line, INCLUDE_REGEX, { 1 }, m))
      {
        // #include directive: expand (load and preprocess) the referenced include and write the result to the output stream
        preprocessed_output << expand_include(m[0], context).value_or("");
      }
      else if (trimmed_matches(line, DEFINE_REGEX, { 1, 2 }, m))
      {
        // #define directive: register the defined variable and value in the preprocessor context
        const std::string variable = m[0];
        const std::string value = m[1];

        const auto existing_define = context.defines.find(variable);

        if ((existing_define != context.defines.end()) && (existing_define->second != value))
        {
          throw std::runtime_error(fmt::format("Invalid redefinition for '{0}', previously defined as {1}", variable, existing_define->second));
        }

        context.defines[m[0]] = m[1];
      }
      else if (trimmed_matches(line, TILES_REGEX, { 1, 2, 3 }, m))
      {
        // #tiles directive: load tiles from .png and output corresponding byte data
        const std::string tiles = m[0];
        const int tile_width = std::stoi(m[1]);
        const int tile_height = std::stoi(m[2]);

        preprocessed_output << expand_tiles(tiles, tile_width, tile_height, context);
      }
      else if (trimmed_matches(line, PALETTE_REGEX, { 1}, m))
      {
        // #palette directive: load palette from .png and output corresponding pack word data
        const std::string palette = m[0];

        preprocessed_output << expand_palette(palette, context);
      }
      else if (trimmed_matches(line, TEXT_REGEX, { 1 }, m))
      {
        // Source text: strip trailing comments, and resolve & replace any possible #define variable references
        const std::string text = m[0];

        std::string preprocessed_line = text;

        auto matched_variable = std::sregex_iterator(text.begin(), text.end(), VARIABLE_REGEX);

        while (matched_variable != std::sregex_iterator())
        {
          const std::string variable = matched_variable->str(1);

          const auto define = context.defines.find(variable);

          if (define == context.defines.end())
          {
            throw AssemblerException("Preprocessor error", fmt::format("Undefined variable reference '{0}'", variable), context.currentFile(), context.currentLine());
          }

          preprocessed_line = std::regex_replace(preprocessed_line, std::regex("\\$" + variable), define->second);

          ++matched_variable;
        }

        preprocessed_output << fmt::format("[@{0:06}]{1:4}", context.currentLine(), "") << preprocessed_line << std::endl;
      }
      else
      {
        // Handle other line types. These should simply be ignored, but we try to match them to a known line
        // type (currently only comment lines) anyway to ensure the preprocessor is not failing to recognize
        // any line type that we are supposed to handle.
        const bool is_comment = std::regex_match(line, COMMENT_REGEX);

        if (!is_comment)
        {
          throw AssemblerException("Preprocessor error", "Invalid syntax", context.currentFile(), context.currentLine());
        }
      }
    }

    context.nextLine();
  }

  // Done processing this file, remove it from the file trail
  context.trail.pop_back();

  return preprocessed_output.str();
}

// Preprocess input source and output preprocessed result
std::string preprocess(std::istream &source, const std::vector<std::string> &includePaths, const std::string &name)
{
  Context context = { .includePaths = includePaths };

  return preprocess(source, context, name);
}

}
