/// Contains the Boost.Spirit EBNF grammar for kumitate .kasm source files.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#pragma once

#include "core/common/itf/types.h"

#include "utils/itf/assembly.h"

#include "core/cpu/itf/cpu.h"
#include "core/cpu/itf/opcodes.h"
#include "core/cpu/itf/instruction.h"

#include <boost/variant.hpp>
#include <boost/mpl/vector.hpp>
#include <boost/mpl/find.hpp>

#include <numeric>

namespace tools::kumitate::parser {

// Tag type for attaching parser state to the Boost.Spirit parser context
struct parser_state_tag {};

/// Parser state.
///
/// This class tracks parser state resulting from parse rule side-effects, which can not easily
/// be encoded directly in the rule attributes. Examples are file/line information parsed from
/// annotations, currently open scope, debug information such as label addresses, etc.
class ParserState
{
  public:
    /// Create new namespace inside the currently open namespace.
    ///
    /// \param ns namespace to push, if not specified, an anonymous namespace is created
    /// \return fully-qualified name of the created namespace
    /// \throw std::runtime_error when trying to open a new namespace inside an anonymous namespace, which is not allowed
    std::string pushNamespace(const std::optional<std::string> &ns = std::nullopt);

    /// Pop namespace
    void popNamespace();

    /// \return currently open namespace, fully qualified
    std::string getCurrentNamespace() const;

    /// Get current output address.
    ///
    /// The parser tracks the address the next code-, or data line that is parsed will end
    /// up in the assembly generated from the input. Any time a code- or data line is parsed,
    /// this address is advanced by means of the advanceOutputAddress() method.
    ///
    /// \return current address
    core::LongPointer getOutputAddress() const;

    /// Advance output address by the passed number of machine \p words.
    ///
    /// \param words number of machine words by which to advance output address
    void advanceOutputAddress(const int words);

    /// Start new section
    ///
    /// \param base section base address
    void createSection(const core::LongPointer &base);

    /// Last parsed line number annotation
    int line;
    /// Last parsed filename annotation
    std::string file;

    /// Accumulated debug info (labels, parsed files, line number information)
    utils::Assembly::DebugInfo debugInfo;

  private:
    std::vector<std::pair<std::string, core::LongPointer>> _currentNamespace;

    core::LongPointer _sectionBase;
    int _sectionOffset;
};

/// Structure to hold a parsed indirect address (multiplier * base register + offset)
struct IndirectAddress
{
  IndirectAddress()
  :
    base(core::cpu::Register::R0)
  {
  }

  operator core::IndirectAddress() const
  {
    return core::IndirectAddress(multiplier.value_or(1), base, offset.value_or(0));
  }

  /// Multiplier
  boost::optional<int64_t> multiplier;
  /// Base register
  core::cpu::Register base;
  /// Offset
  boost::optional<int64_t> offset;
};

/// Structure to hold parsed direct addresses (long pointers) operand
struct DirectAddress
{
  /// Default constructor
  ///
  /// By default an uninitialized DirectAddress points to core::cpu::Cpu::IP_0 (instruction
  /// pointer after CPU reset), so code sections without a base address are interpreted
  /// to be loaded at that address.
  DirectAddress()
  :
    DirectAddress(core::LongPointer(core::cpu::Cpu::IP_0.segment, core::cpu::Cpu::IP_0.offset))
  {
  }

  /// Create from long pointer.
  ///
  /// \param address long pointer address from which to initialize the DirectAddress operand
  DirectAddress(const core::LongPointer &address)
  :
    segment(address.segment), offset(address.offset)
  {
  }

  /// Cast to long pointer.
  ///
  /// \return the direct address cast to core::LongPointer type.
  operator core::LongPointer() const
  {
    return { segment, static_cast<core::WORD>(offset) };
  }

  /// Segment
  int64_t segment;
  /// Offset
  int64_t offset;
};

/// Structure to hold label expressions.
///
/// Depending on the (absence of) a label suffix, a label expression refers
/// an explicit memory location (DirectAddress) or an immediate.
struct LabelExpression
{
  /// Enumeration type for allowed label suffixes
  enum class Suffix
  {
    NONE,
    SEGMENT,
    OFFSET
  };

  /// Constructor
  LabelExpression()
  :
    suffix(Suffix::NONE), offset(0)
  {
  }

  /// Symbol name
  std::string symbol;
  /// LabelExpression suffix
  Suffix suffix;
  /// Constant offset to add to resolved label value
  int64_t offset;
};

/// Structure to hold flag operands
struct Flags
{
  /// Default constructor
  Flags()
  :
    bits(0)
  {
  }

  /// Logical or with flag bits \p b.
  ///
  /// This is here to simplify the parser semantic action that is exuted when parsing
  /// CPU flag operands, which can be a sequence of multiple flags.
  ///
  /// \param b flag bits to add to the current flag bits
  void operator|=(const core::cpu::Flag::Bits &b)
  {
    bits |= b;
  }

  /// Flag bits
  uint8_t bits;
};

/// Variant type to hold jump vector operand, which can be a label expression or a direct address.
using JumpVector = boost::variant<LabelExpression, DirectAddress>;

/// Structure to hold parsed instructions consisting of a CPU operation plus zero or more operands.
struct Instruction
{
  /// Structure to hold instruction operands, as (type, value) pairs
  struct Operand
  {
    /// Variant value types used for operands
    using ValueTypes = boost::mpl::vector<
      boost::blank,
      LabelExpression,
      bool,
      int64_t,
      Flags,
      DirectAddress,
      IndirectAddress,
      JumpVector,
      core::cpu::Register,
      core::cpu::SpecialPurposeRegister,
      core::cpu::BranchCondition>;

    /// The variant type used to represent operands
    using Value = boost::make_variant_over<ValueTypes>::type;

    /// Helper template get the index of the passed operand value type into the Operand variant
    template<typename T>
    using ValueTypeIndex = typename boost::mpl::find<ValueTypes, T>::type::pos;

    /// Enumeration type to indentify operand types. The enumeration values are linked to the respective
    /// index of their value type in the variant that stores operand values.
    enum class Type
    {
      /// Unused operand
      NONE = ValueTypeIndex<boost::blank>::value,
      /// LabelExpression
      LABEL_EXPRESSION = ValueTypeIndex<LabelExpression>::value,
      /// Boolean
      BOOLEAN = ValueTypeIndex<bool>::value,
      /// Immediate
      IMMEDIATE = ValueTypeIndex<int64_t>::value,
      /// Flags
      FLAGS = ValueTypeIndex<Flags>::value,
      /// Register
      REG = ValueTypeIndex<core::cpu::Register>::value,
      /// Special purpose register
      SP_REG = ValueTypeIndex<core::cpu::SpecialPurposeRegister>::value,
      /// Direct address (segment:offset)
      DIRECT_ADDRESS = ValueTypeIndex<DirectAddress>::value,
      /// Indirect address (segment:[base] +/- offset)
      INDIRECT_ADDRESS = ValueTypeIndex<IndirectAddress>::value,
      /// Branch condition
      BRANCH_CONDITION = ValueTypeIndex<core::cpu::BranchCondition>::value,
      /// Jump vector
      VECTOR = ValueTypeIndex<JumpVector>::value,
    };

    /// Return operand type
    ///
    /// \return operand type
    Type getType() const
    {
      return static_cast<Type>(value.which());
    }

    /// Operand value
    Value value;
  };

  /// Stucture to store the signature (combination of operand types) of a parsed instruction. This structure
  /// has a constexpr constructor and cast operator to uint64_t, so it can be used directly in e.g. switch
  /// statements and as map keys.
  struct Signature
  {
    /// Constructor
    ///
    /// \param t0 first operand type, defaults to \a Operand::Type::NONE
    /// \param t1 second operand type, defaults to \a Operand::Type::NONE
    /// \param t2 third operand type, defaults to \a Operand::Type::NONE
    /// \param t3 fourth operand type, defaults to \a Operand::Type::NONE
    constexpr Signature(
      const Operand::Type t0 = Operand::Type::NONE,
      const Operand::Type t1 = Operand::Type::NONE,
      const Operand::Type t2 = Operand::Type::NONE,
      const Operand::Type t3 = Operand::Type::NONE)
    :
      operandTypes({ t0, t1, t2, t3 })
    {
    }

    /// Construct signature from variable-sized operand vector
    Signature(const std::vector<Operand> &operands)
    :
      operandTypes({ Operand::Type::NONE, Operand::Type::NONE, Operand::Type::NONE, Operand::Type::NONE })
    {
      for (size_t i = 0; i < std::min<size_t>(4, operands.size()); i++)
      {
        operandTypes[i] = static_cast<Operand::Type>(operands[i].value.which());
      }
    }

    /// Cast Signature instance to uint64_t.
    ///
    /// This allows the structure to be used directly in e.g. switch statements and as map keys.
    ///
    /// \return 64-bit 'hash' for the signature
    constexpr operator uint64_t() const
    {
      return
        static_cast<uint64_t>(operandTypes[0]) |
        static_cast<uint64_t>(operandTypes[1]) << 8 |
        static_cast<uint64_t>(operandTypes[2]) << 16 |
        static_cast<uint64_t>(operandTypes[3]) << 24;
    }

    /// Signature operand types
    std::array<Operand::Type, 4> operandTypes;
  };

  /// Instruction address
  core::LongPointer address;
  /// Instruction operation
  core::cpu::Operation operation;
  /// Instruction operands
  std::vector<Operand> operands;
};

/// Byte data. To avoid truncation of data while parsing and to be able to perform overflow
/// detection in the semantic pass, we store all parsed data elements as 64-bit signed values. This is obviously
/// wasteful but considering the tiny amount of memory of the target machine (1 MB as currently designed), this
/// is not really a concern.
struct ByteData
{
  std::vector<int64_t> bytes;
};

/// Word data. Again we store 64-bits to avoid truncation and allow overflow handling after parsing
struct WordData
{
  std::vector<int64_t> words;
};

///
struct StringData
{
  struct Attribute
  {
    int64_t value;
  };

  using Element = boost::variant<char, Attribute>;

  std::vector<Element> data;
};

/// Variant type to hold a single line of parsed data.
struct DataLine
{
  /// Visitor functor that calculates the data line size in machine words, taking
  /// into account possible padding to ensure word alignment.
  struct LineSize : boost::static_visitor<unsigned int>
  {
    unsigned int operator()(const ByteData &byteData) const
    {
      return (byteData.bytes.size() + (byteData.bytes.size() % sizeof(core::WORD))) / 2;
    }

    unsigned int operator()(const WordData &wordData) const
    {
      return wordData.words.size();
    }

    unsigned int operator()(const StringData &stringData) const
    {
      struct CountCharacters : boost::static_visitor<int>
      {
        int operator()(const char &c) const { return 1; }
        int operator()(const StringData::Attribute &a) const { return 0; }
      };

      const int num_characters = std::accumulate(stringData.data.begin(), stringData.data.end(), 0, [](const int n, const auto &e)
      {
        return n + boost::apply_visitor(CountCharacters(), e);
      });

      return 1 + num_characters;
    }
  };

  /// Return data line size, including possible padding to ensure machine word alignment.
  ///
  /// \return data line size, in machine words
  size_t size() const
  {
    return boost::apply_visitor(LineSize(), data);
  }

  /// Line address
  core::LongPointer address;
  /// Line data
  boost::variant<ByteData, WordData, StringData> data;
};

/// Structure to hold parsed code section
struct CodeSection
{
  /// Section base address
  DirectAddress base;
  /// Parsed instructions
  std::vector<Instruction> instructions;
};

/// Structure to hold parsed data section
struct DataSection
{
  /// Section base address
  DirectAddress base;
  /// Parsed data, line by line
  std::vector<DataLine> lines;
};

/// Variant type over all parser section types
using Section = boost::variant<CodeSection, DataSection>;

/// Data type for the 'AST' (abstract syntax tree) resulting from parsing preprocessed .kasm source text
struct AST
{
  /// Constructor.
  ///
  /// \param sections parsed code & data sections
  AST(const std::vector<boost::variant<CodeSection, DataSection>> &sections, const utils::Assembly::DebugInfo &debugInfo)
  :
    debugInfo(debugInfo)
  {
    struct SeparateSections : boost::static_visitor<void>
    {
      SeparateSections(AST *ast) : ast(ast) {};

      void operator()(const CodeSection &section) { ast->codeSections.push_back(section); };
      void operator()(const DataSection &section) { ast->dataSections.push_back(section); };

      AST *ast;
    };

    SeparateSections separate_sections(this);

    std::for_each(sections.begin(), sections.end(), [&separate_sections](const auto &section) { boost::apply_visitor(separate_sections, section); });
  }

  /// Code sections
  std::vector<CodeSection> codeSections;
  /// Data sections
  std::vector<DataSection> dataSections;

  /// Debug information
  utils::Assembly::DebugInfo debugInfo;
};

/// Parse preprocessed .kasm source text.
///
/// This will parse the passed source into a vector of code & data sections, which can be
/// considered an 'AST' for the source code. After parsing, label resolution is performed
/// on the AST, replacing label expressions by absolute addresses.
///
/// \param source preprocessed .kasm source text
/// \return parsed code and data sections (the 'AST' for the input \p source code)
AST parse(const std::string &source);

}
