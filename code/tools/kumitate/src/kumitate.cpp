/// Implements the kumitate assembler command-line interface.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#include "tools/kumitate/itf/assembler.h"

#include "third_party/cxxopts/cxxopts.hpp"
#include "third_party/fmt/format.h"

#include <cstdlib>
#include <sstream>
#include <memory>

/// Entry point for the kumitate assembler command-line interface.
///
/// \param argc number of command line arguments
/// \param argv command line arguments
/// \return exit code
int main(int argc, char **argv)
{
  cxxopts::Options options("kumitate", "\nkumitate: assembler for the maboroshi-16 fantasy console\n");

  options
    .positional_help("input output")
    .show_positional_help();

  options.add_options()
    ("i,input", "input assembly source file (.kasm)", cxxopts::value<std::string>())
    ("o,output", "output object file (.mabo)", cxxopts::value<std::string>())
    ("positional", "additional positional arguments", cxxopts::value<std::vector<std::string>>())
    ("help", "print help");

  options.parse_positional({ "input", "output", "positional" });

  const auto result = options.parse(argc, argv);

  if (result.count("help") || (result.count("input") == 0) || (result.count("output") == 0))
  {
    std::cout << options.help() << std::endl;
    std::exit(EXIT_SUCCESS);
  }

  const std::string input_filename = result["input"].as<std::string>();
  const std::string output_filename = result["output"].as<std::string>();

  try
  {
    // Read input source file
    std::ifstream input_file(input_filename, std::ios_base::in);

    if (!input_file.is_open())
    {
      throw fmt::format("failed to open input file '{0}' for reading", input_filename);
    }

    // Assemble source file
    std::unique_ptr<utils::Assembly> assembly;

    try
    {
      assembly = tools::kumitate::assemble(input_file, {}, { "." }, input_filename);
    }
    catch (const tools::kumitate::AssemblerException &e)
    {
      throw fmt::format("{0}:\n  what: {1}\n  file: {2}\n  line: {3}\n", e.what(), e.msg, e.file, e.line);
    }

    // Write output file
    std::ofstream output_file(output_filename, std::ios_base::out | std::ios_base::trunc | std::ios_base::binary);

    if (!output_file.is_open())
    {
      throw fmt::format("failed to open file '{0}' for writing", output_filename);
    }

    assembly->write(output_file);
  }
  catch (const std::string &e)
  {
    std::cout << std::endl <<  e  << std::endl << std::endl;
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
