/// Contains the Boost.Spirit X3 EBNF grammar for kumitate .kasm source files.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#pragma once

#include "parser.h"

#include <boost/lexical_cast.hpp>
#include <boost/spirit/home/x3.hpp>
#include <boost/fusion/include/at_c.hpp>
#include <boost/fusion/include/adapt_struct.hpp>

BOOST_FUSION_ADAPT_STRUCT(tools::kumitate::parser::DirectAddress, segment, offset)
BOOST_FUSION_ADAPT_STRUCT(tools::kumitate::parser::IndirectAddress, multiplier, base, offset)
BOOST_FUSION_ADAPT_STRUCT(tools::kumitate::parser::LabelExpression, symbol, suffix, offset)
BOOST_FUSION_ADAPT_STRUCT(tools::kumitate::parser::Instruction::Operand, value)
BOOST_FUSION_ADAPT_STRUCT(tools::kumitate::parser::CodeSection, base, instructions)
BOOST_FUSION_ADAPT_STRUCT(tools::kumitate::parser::ByteData, bytes)
BOOST_FUSION_ADAPT_STRUCT(tools::kumitate::parser::WordData, words)
BOOST_FUSION_ADAPT_STRUCT(tools::kumitate::parser::StringData, data)
BOOST_FUSION_ADAPT_STRUCT(tools::kumitate::parser::StringData::Attribute, value)
BOOST_FUSION_ADAPT_STRUCT(tools::kumitate::parser::DataSection, base, lines)

namespace tools::kumitate::parser::grammar {

namespace x3 = boost::spirit::x3;

//
// Grammar symbol tables
//

/// Symbol table for CPU operation mnemonics
struct Mnemonic : x3::symbols<core::cpu::Operation>
{
  Mnemonic()
  {
    static const std::vector<core::cpu::Operation> ALL_OPERATIONS = {
      core::cpu::Operation::ADC,
      core::cpu::Operation::ADD,
      core::cpu::Operation::AND,
      core::cpu::Operation::ASR,
      core::cpu::Operation::B,
      core::cpu::Operation::BANK,
      core::cpu::Operation::CALL,
      core::cpu::Operation::CLR,
      core::cpu::Operation::CMP,
      core::cpu::Operation::DEC,
      core::cpu::Operation::DIV,
      core::cpu::Operation::DIVS,
      core::cpu::Operation::HALT,
      core::cpu::Operation::IN,
      core::cpu::Operation::INC,
      core::cpu::Operation::J,
      core::cpu::Operation::LDSP,
      core::cpu::Operation::LSL,
      core::cpu::Operation::LSR,
      core::cpu::Operation::MOV,
      core::cpu::Operation::MOVH,
      core::cpu::Operation::MOVL,
      core::cpu::Operation::MUL,
      core::cpu::Operation::MULS,
      core::cpu::Operation::NEG,
      core::cpu::Operation::NOP,
      core::cpu::Operation::NOT,
      core::cpu::Operation::OR,
      core::cpu::Operation::OUT,
      core::cpu::Operation::POP,
      core::cpu::Operation::POPS,
      core::cpu::Operation::PUSH,
      core::cpu::Operation::PUSHS,
      core::cpu::Operation::RET,
      core::cpu::Operation::RTI,
      core::cpu::Operation::RSV,
      core::cpu::Operation::SBB,
      core::cpu::Operation::SET,
      core::cpu::Operation::STSP,
      core::cpu::Operation::SUB,
      core::cpu::Operation::WAIT,
      core::cpu::Operation::XOR,
    };

    for (const core::cpu::Operation &operation : ALL_OPERATIONS)
    {
      this->add(boost::lexical_cast<std::string>(operation), operation);
    }
  }
} mnemonic;

/// Symbol table for CPU registers
struct Register : x3::symbols<core::cpu::Register>
{
  Register()
  {
    static const std::vector<core::cpu::Register> ALL_REGISTERS = {
      core::cpu::Register::R0,
      core::cpu::Register::R1,
      core::cpu::Register::R2,
      core::cpu::Register::R3,
      core::cpu::Register::R4,
      core::cpu::Register::R5,
      core::cpu::Register::R6,
      core::cpu::Register::R7
    };

    for (const core::cpu::Register &reg : ALL_REGISTERS)
    {
      this->add(boost::lexical_cast<std::string>(reg), reg);
    }
  }
} reg;

/// Symbol table for CPU special-purpose registers
struct SpecialPurposeRegister : x3::symbols<core::cpu::SpecialPurposeRegister>
{
  SpecialPurposeRegister()
  {
    static const std::vector<core::cpu::SpecialPurposeRegister> ALL_REGISTERS = {
      core::cpu::SpecialPurposeRegister::SP,
      core::cpu::SpecialPurposeRegister::SR,
    };

    for (const core::cpu::SpecialPurposeRegister &reg : ALL_REGISTERS)
    {
      this->add(boost::lexical_cast<std::string>(reg), reg);
    }
  }
} spReg;

/// Symbol table for CPU flags
struct Flag : x3::symbols<core::cpu::Flag::Bits>
{
  Flag()
  {
    static const std::vector<core::cpu::Flag::Bits> ALL_FLAGS = {
      core::cpu::Flag::NONE,
      core::cpu::Flag::CARRY,
      core::cpu::Flag::OVERFLOW,
      core::cpu::Flag::ZERO,
      core::cpu::Flag::NEGATIVE,
      core::cpu::Flag::DISABLE_IRQ,
      core::cpu::Flag::ALL_ARITHMETIC,
      core::cpu::Flag::ALL,
    };

    for (const core::cpu::Flag::Bits &flag : ALL_FLAGS)
    {
      this->add(boost::lexical_cast<std::string>(flag), flag);
    }
  }
} flag;

/// Symbol table for branch conditions
struct BranchCondition : x3::symbols<core::cpu::BranchCondition>
{
  BranchCondition()
  {
    static const std::vector<core::cpu::BranchCondition> ALL_BRANCH_CONDITIONS = {
      core::cpu::BranchCondition::Z,
      core::cpu::BranchCondition::NZ,
      core::cpu::BranchCondition::O,
      core::cpu::BranchCondition::NO,
      core::cpu::BranchCondition::C,
      core::cpu::BranchCondition::NC,
      core::cpu::BranchCondition::G,
      core::cpu::BranchCondition::GE,
      core::cpu::BranchCondition::L,
      core::cpu::BranchCondition::LE,
      core::cpu::BranchCondition::UG,
      core::cpu::BranchCondition::UGE,
      core::cpu::BranchCondition::UL,
      core::cpu::BranchCondition::ULE,
    };

    for (const core::cpu::BranchCondition &branch_condition : ALL_BRANCH_CONDITIONS)
    {
      this->add(boost::lexical_cast<std::string>(branch_condition), branch_condition);
    }
  }
} branchCondition;

/// Symbol table for label suffixes allowed in label expressions
struct LabelSuffix : x3::symbols<LabelExpression::Suffix>
{
  LabelSuffix()
  {
    this->add("segment", LabelExpression::Suffix::SEGMENT);
    this->add("offset", LabelExpression::Suffix::OFFSET);
  }
} labelSuffix;

/// Symbol table for booleans
struct Boolean : x3::symbols<bool>
{
  Boolean()
  {
    this->add("true", true);
    this->add("false", false);
  }
} boolean;

/// Symbol table for sign used in offsets
struct Sign : x3::symbols<int>
{
  Sign()
  {
    this->add("-", -1);
    this->add("+", 1);
  }
} sign;

/// Symbol table for a '0' integer literal. Seems weird we even need this as you would expect
/// to be able to just use an integer parse with a semantic action that has a pass criterium
/// that the parsed value has to be zero, but somehow this doesn't work as expected.
struct Zero: x3::symbols<int>
{
  Zero()
  {
    this->add("0", 0);
  }
} zero;

/// Symbol table for escape characters in string data
struct EscapedChar : x3::symbols<char>
{
  EscapedChar()
  {
    this->add("\\\"", '"');
    this->add("\\0", '\0');
    this->add("\\\\", '\\');
    this->add("\\n", '\n');
    this->add("\\t", '\t');
    this->add("\\|", '|');
  }
} escapedChar;

//
// Parser semantic actions
//

template<typename Context>
static ParserState &get_parser_state(Context &ctx)
{
  return x3::get<parser_state_tag>(ctx).get();
}

// Set current line number for error reporting
auto set_line = [](auto &ctx) { get_parser_state(ctx).line = x3::_attr(ctx); };
// Set current file name for error reporting
auto set_file = [](auto &ctx) { get_parser_state(ctx).file = x3::_attr(ctx); };

// Open namespace
auto open_namespace = [](auto &ctx)
{
  get_parser_state(ctx).pushNamespace(static_cast<std::string>(x3::_attr(ctx)));
};

// Open anonymous namespace
auto open_anon_namespace = [](auto &ctx)
{
  get_parser_state(ctx).pushNamespace();
};

// Close namespace
auto close_namespace = [](auto &ctx)
{
  get_parser_state(ctx).popNamespace();
};

// Qualify label reference by prefixing it with the current namespace
auto qualify_label = [](auto &ctx)
{
  const ParserState &state = get_parser_state(ctx);

  const std::string label = x3::_attr(ctx);
  const std::string qualified_label = state.getCurrentNamespace() + label;

  x3::_val(ctx) = qualified_label;
};

// Create label, using current section base and offset to resolve the label address
auto create_label = [](auto &ctx)
{
  ParserState &state = get_parser_state(ctx);

  const std::string label = x3::_attr(ctx);
  const std::string qualified_label = state.getCurrentNamespace() + label;

  state.debugInfo.setLabel(qualified_label, state.getOutputAddress());
};

// Start new section, tracking the section base address and resetting the section offset
auto start_section = [](auto &ctx)
{
  ParserState &state = get_parser_state(ctx);

  const DirectAddress section_base = boost::get<DirectAddress>(x3::_attr(ctx));

  state.createSection(section_base);

  x3::_val(ctx) = section_base;
};

// Create instruction from parsed operation and operands
auto create_instruction = [](auto &ctx)
{
  ParserState &state = get_parser_state(ctx);

  // Calculate instruction address and add file/line info to the debug information
  const core::LongPointer instruction_address = state.getOutputAddress();

  state.debugInfo.setLineInfo(instruction_address, state.debugInfo.getFileIndex(state.file), state.line);

  // Create instruction and advance section offset
  const Instruction instruction = { instruction_address, boost::fusion::at_c<0>(x3::_attr(ctx)), boost::fusion::at_c<1>(x3::_attr(ctx)) };

  state.advanceOutputAddress(core::cpu::Instruction::size());

  x3::_val(ctx) = instruction;
};

// Create data line from parsed data
auto create_data_line = [](auto &ctx)
{
  ParserState &state = get_parser_state(ctx);

  const core::LongPointer line_address = state.getOutputAddress();
  const DataLine data_line = { line_address, x3::_attr(ctx) };

  state.advanceOutputAddress(data_line.size());

  x3::_val(ctx) = data_line;
};

//
// Parser rule declarations
//

const x3::rule<class kasm, std::vector<boost::variant<CodeSection, DataSection>>> kasm = "kasm";
const x3::rule<class fileAnnotation> fileAnnotation = "fileAnnotation";
const x3::rule<class lineAnnotation> lineAnnotation = "lineAnnotation";

const x3::rule<class identifier, std::string> identifier = "identifier";
const x3::rule<class label> label = "label";
const x3::rule<class immediate, int64_t> immediate = "immediate";
const x3::rule<class integer, int64_t> integer = "integer";
const x3::rule<class decimal, int64_t> decimal = "decimal";
const x3::rule<class hexadecimal, int64_t> hexadecimal = "hexadecimal";
const x3::rule<class offset, int64_t>  offset = "offset";
const x3::rule<class flags, Flags>  flags = "flags";
const x3::rule<class directAddress, DirectAddress> directAddress = "directAddress";
const x3::rule<class indirectAddress, tools::kumitate::parser::IndirectAddress> indirectAddress = "indirectAddress";
const x3::rule<class zeroPageAddress, tools::kumitate::parser::IndirectAddress> zeroPageAddress = "zeroPageAddress";
const x3::rule<class qualifiedLabelReference, std::string> qualifiedLabelReference = "qualifiedLabelReference";
const x3::rule<class unqualifiedLabelReference, std::string> unqualifiedLabelReference = "unqualifiedLabelReference";
const x3::rule<class labelExpression, LabelExpression> labelExpression = "labelExpression";
const x3::rule<class vector, JumpVector> vector = "vector";

const x3::rule<class openNamespace> openNamespace = "openNamespace";
const x3::rule<class openAnonymousNamespace> openAnonymousNamespace = "openAnonymousNamespace";
const x3::rule<class closeNamespace> closeNamespace = "closeNamespace";

const x3::rule<class operand, Instruction::Operand> operand = "operand";
const x3::rule<class operands, std::vector<Instruction::Operand>> operands= "operands";
const x3::rule<class codeSectionHeader, DirectAddress> codeSectionHeader = "codeSectionHeader";
const x3::rule<class codeSection, CodeSection> codeSection = "codeSection";
const x3::rule<class codeChunk, std::vector<Instruction>> codeChunk = "codeChunk";
const x3::rule<class instructionLine, Instruction> instructionLine = "instructionLine";

const x3::rule<class byteData, ByteData> byteData = "byteData";
const x3::rule<class wordData, WordData> wordData = "wordData";
const x3::rule<class stringAttribute, StringData::Attribute> stringAttribute = "stringAtrribute";
const x3::rule<class stringElement, StringData::Element> stringElement = "stringElement";
const x3::rule<class stringData, StringData> stringData = "stringData";
const x3::rule<class dataSectionHeader, DirectAddress> dataSectionHeader = "dataSectionHeader";
const x3::rule<class dataSection, DataSection> dataSection = "dataSection";
const x3::rule<class dataChunk, std::vector<DataLine>> dataChunk = "dataChunk";
const x3::rule<class dataLine, DataLine> dataLine = "dataLine";

//
// Rule definitions
//

// Basic parser type rules (atoms)

const auto identifier_def = x3::lexeme[ x3::alpha >> *(x3::alnum | x3::char_('_')) ];

const auto immediate_def = '#' > integer;
const auto integer_def = hexadecimal | decimal;
const auto decimal_def = x3::int_parser<int, 10, 1>();
const auto hexadecimal_def = x3::lexeme["0x"] > x3::uint_parser<unsigned int, 16, 1, 8>();

const auto offset_def = sign [ ([](const auto &ctx) { x3::_val(ctx) = x3::_attr(ctx); }) ] > integer [ ([](const auto &ctx) { x3::_val(ctx) *= x3::_attr(ctx); }) ];
const auto flags_def = x3::lexeme['%'] > +(flag [ ([](const auto &ctx) { x3::_val(ctx) |= x3::_attr(ctx); }) ]);

const auto directAddress_def = ('(' >> integer >> ':') > integer > ')';
const auto zeroPageAddress_def = ('[' >> x3::lexeme[zero > '.' > reg]) > -offset > ']';
const auto indirectAddress_def = ('[' >> -(integer > '*') >> reg) > -offset > ']';

const auto qualifiedLabelReference_def = x3::lexeme[ +(x3::string("::") > identifier) ];
const auto unqualifiedLabelReference_def = identifier [ qualify_label ];
const auto labelExpression_def = x3::lexeme['@' > (qualifiedLabelReference | unqualifiedLabelReference) > -('.' >> labelSuffix)] > -offset;

const auto vector_def = '<' > (labelExpression | (integer >> ':' >> integer)) > '>';

// Code & data section rules

const auto fileAnnotation_def = (x3::lexeme["[="] > (+(x3::char_ - x3::char_(']'))) > ']' > x3::eol) [ set_file ];
const auto lineAnnotation_def = (x3::lexeme["[@"] > integer > ']') [ set_line ];

const auto label_def =  (lineAnnotation >> ((identifier >> ':') > x3::eol)) [ create_label ];

const auto openAnonymousNamespace_def =  ((lineAnnotation >> '{') > x3::eol) [ open_anon_namespace ];
const auto openNamespace_def =  ((lineAnnotation >> identifier >> '{') > x3::eol) [ open_namespace];
const auto closeNamespace_def = ((lineAnnotation >> '}') > x3::eol) [ close_namespace ];

// Code section rules

const auto operand_def = reg | spReg | boolean | immediate | flags | branchCondition | labelExpression | directAddress | zeroPageAddress | indirectAddress | vector;
const auto operands_def = -(operand % ',');

const auto codeSection_def = *fileAnnotation >> codeSectionHeader >> *codeChunk;
const auto codeSectionHeader_def = (lineAnnotation >> (x3::lexeme[":code"] > ((directAddress > x3::eol) | x3::eol))) [ start_section ];
const auto codeChunk_def =  ((openNamespace | openAnonymousNamespace) > *codeChunk > closeNamespace) | +(fileAnnotation | label | instructionLine);
const auto instructionLine_def = ((lineAnnotation >> (mnemonic > operands)) > x3::eol) [ create_instruction ];

// Data section rules

const auto byteData_def = x3::lexeme[".byte"] > (integer % ',');
const auto wordData_def =  x3::lexeme[".word"] > (integer % ',');
const auto stringAttribute_def = '|' > integer > '|';
const auto stringElement_def = stringAttribute | escapedChar | (x3::char_ - '"' - '\\');
const auto stringData_def = x3::lexeme[".string"] > ('"' > x3::no_skip[ *stringElement ] > '"');

const auto dataSection_def = *fileAnnotation >> dataSectionHeader >> *dataChunk;
const auto dataSectionHeader_def = (lineAnnotation >> (x3::lexeme[":data"] > directAddress > x3::eol)) [ start_section ];
const auto dataChunk_def = (openNamespace > *dataChunk > closeNamespace) | +(fileAnnotation | label | dataLine);
const auto dataLine_def = ((lineAnnotation >> (byteData | wordData | stringData)) > x3::eol) [ create_data_line ];

// Top level .kasm grammar rule

const auto kasm_def = *(codeSection | dataSection);

//
// Instantiate parser rules
//

BOOST_SPIRIT_DEFINE(kasm,
  fileAnnotation, lineAnnotation,
  identifier, label,
  immediate, integer, decimal, hexadecimal, flags, offset, qualifiedLabelReference, unqualifiedLabelReference, labelExpression, vector,
  directAddress, indirectAddress, zeroPageAddress,
  openAnonymousNamespace, openNamespace, closeNamespace,
  operand, operands, instructionLine, codeSectionHeader, codeSection, codeChunk,
  byteData, wordData, stringAttribute, stringElement, stringData, dataLine, dataSectionHeader, dataSection, dataChunk);

}
