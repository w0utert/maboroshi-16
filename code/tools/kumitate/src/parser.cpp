/// Implements the kumitate assembler parser interface.
///
/// The parse function implemented here uses the Boost.Spirit X3 parser defined in grammar.h
/// to parse a preprocessed .kasm source string, then post-processes the returned AST (currently
/// just a vector of code and data sections) to resolve label expressions in instruction operands
/// and replace them by direct addresses. After this transformation, the AST can be used for code
/// generation.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#include "parser.h"
#include "grammar.h"

#include "tools/kumitate/itf/assembler.h"

namespace tools::kumitate::parser {

// Visitor that resolves label expression operands to direct addresses or immediates
struct LabelResolver : boost::static_visitor<Instruction::Operand::Value>
{
  public:
    LabelResolver(const utils::Assembly::DebugInfo &debugInfo)
    :
      _debugInfo(&debugInfo)
    {
    }

    // Resolve label expression operand
    Instruction::Operand::Value operator()(const LabelExpression &labelExpression) const
    {
      return resolve(labelExpression);
    }

    // Resolve jump vector operand. A jump vector is a variant type that can hold  a direct
    // address (which does not need to be resolved), or a label expression (which does).
    Instruction::Operand::Value operator()(const JumpVector &jumpVector) const
    {
      // Visitor that resolves labels expressions in jump vector operands to direct addresses
      struct JumpVectorResolver : boost::static_visitor<JumpVector>
      {
        JumpVectorResolver(const LabelResolver *labelResolver) : resolver(labelResolver) {}

        JumpVector operator()(const LabelExpression &labelExpression) const
        {
          const Instruction::Operand resolved_jump_vector = { resolver->resolve(labelExpression) };

          if (resolved_jump_vector.getType() == Instruction::Operand::Type::DIRECT_ADDRESS)
          {
            return boost::get<DirectAddress>(resolved_jump_vector.value);
          }
          else
          {
            throw std::runtime_error("Invalid label expression for jump vector");
          }
        }

        JumpVector operator()(const DirectAddress &directAddress) const
        {
          return directAddress;
        }

        const LabelResolver *resolver;
      };

      // Apply the jump vector resolve visitor to the jump vector operand
      return boost::apply_visitor(JumpVectorResolver(this), jumpVector);
    }

    // Apply visitor to any operand other than label expressions or jump vectors. This just
    // returns the operand unmodified.
    template<typename T>
    Instruction::Operand::Value operator()(const T &v) const { return v; }

  private:
    Instruction::Operand::Value resolve(const LabelExpression &labelExpression) const
    {
      const core::LongPointer label_address = _debugInfo->getLabelAddress(labelExpression.symbol);

      switch (labelExpression.suffix)
      {
        case LabelExpression::Suffix::SEGMENT:
          return static_cast<int64_t>(label_address.segment + labelExpression.offset);

        case LabelExpression::Suffix::OFFSET:
          return static_cast<int64_t>(label_address.offset + labelExpression.offset);

        default:
          return DirectAddress(label_address + labelExpression.offset);
      }
    }

    const utils::Assembly::DebugInfo *_debugInfo;
};

// Push new namespace to parser state
std::string ParserState::pushNamespace(const std::optional<std::string> &ns)
{
  // Opening a namespace inside an anonymous namespace is not allowed
  if (!_currentNamespace.empty() && (*_currentNamespace.back().first.begin() == '~'))
  {
    throw std::runtime_error("Opening new namespace inside anonymous namespace is not allowed");
  }

  // If no namespace was passed, generate a unique anonymous namespace by concatenating the sequence number
  // of the current file with the current line number, prefixing it with a '~' so we can easily check if
  // the last opened (current) namespace is anonymous.
  //
  // Note:
  // The '~' character was not chosen completely arbitrarily, but because '~' sorts after all other printable
  // 7-bit ASCII codes. This has the nice property that when sorting namespaces or fully-qualified symbols that
  // point to the same address, those in the anonymous namespace are always sorted last.
  _currentNamespace.push_back({ ns.value_or(fmt::format("~{0}:{1}", debugInfo.getFileIndex(file), line)), getOutputAddress() });

  return getCurrentNamespace();
}

// Pop namespace from parser state
void ParserState::popNamespace()
{
  const std::string fully_qualified_namespace = getCurrentNamespace();

  const core::LongPointer start = _currentNamespace.back().second;
  const core::LongPointer end = getOutputAddress();

  debugInfo.setNamespace(fully_qualified_namespace, start, end);

  _currentNamespace.pop_back();
}

// Return fully-qualified namespace that is currently open
std::string ParserState::getCurrentNamespace() const
{
  std::string ns = "::";

  for (const auto &[s, address] : _currentNamespace)
  {
    ns = ns + s + "::";
  }

  return ns;
}

// Advance current address
void ParserState::advanceOutputAddress(const int words)
{
  _sectionOffset += words;
}

// Get current address
core::LongPointer ParserState::getOutputAddress() const
{
  return _sectionBase + _sectionOffset;
}

// Create new section
void ParserState::createSection(const core::LongPointer &base)
{
  _sectionBase = base;
  _sectionOffset = 0;
}

// Parse and post-process preprocessed .kasm source text
AST parse(const std::string &source)
{
  ParserState parser_state = {};

  // Parse source code to obtain a vector of code & data sections
  std::vector<boost::variant<CodeSection, DataSection>> sections;

  bool parsed = true;
  std::string msg;

  try
  {
    auto grammar = boost::spirit::x3::with<parser_state_tag>(std::ref(parser_state)) [ grammar::kasm ];

    auto f = source.begin();
    const auto l = source.end();

    const bool matched = boost::spirit::x3::phrase_parse(f, l, grammar, boost::spirit::x3::blank, sections);

    parsed = matched && (f == l);
  }
  catch (std::runtime_error &e)
  {
    msg = e.what();
    parsed = false;
  }

  if (!parsed)
  {
    throw AssemblerException("Parse error", msg, parser_state.file, parser_state.line);
  }

  // Create AST from the parsed code sections and parser state
  AST ast(sections, parser_state.debugInfo);

  // Post-process AST.
  //
  // This currently only performs label resolution (replacing label expressions by absolute addresses)
  // Apply the section post-processing visitor to the AST
  for (auto &section : ast.codeSections)
  {
    const LabelResolver label_resolver(ast.debugInfo);

    for (Instruction &instruction : section.instructions)
    {
      try
      {
        for (Instruction::Operand &operand : instruction.operands)
        {
          operand.value = boost::apply_visitor(label_resolver, operand.value);
        }
      }
      catch (const std::runtime_error &e)
      {
        const utils::Assembly::DebugInfo::LineInfo &line_info = ast.debugInfo.getLineInfo(instruction.address);

        throw AssemblerException("Symbol lookup error", e.what(), ast.debugInfo.getFile(line_info.file), line_info.line);
      }
    }
  }

  // Return AST
  return ast;
}

}