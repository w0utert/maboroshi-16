/// Defines types used for the code generation phase of the Kumitate assembler.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#pragma once

#include "tools/kumitate/itf/assembler.h"

#include "utils/itf/assembly.h"

#include "core/common/itf/types.h"
#include "core/cpu/itf/instruction.h"

#include "parser.h"

#include <memory>

namespace tools::kumitate::codegen {

/// Invalid signature exception.
struct InvalidSignature : std::runtime_error
{
  /// Constructor.
  ///
  /// \param msg exception message
  /// \param instruction instruction that triggered the invalid signature exception
  InvalidSignature(const std::string &msg, const parser::Instruction &instruction)
  :
    std::runtime_error(msg),
    instruction(instruction)
  {
  }

  /// Instruction on which the exception occurred
  parser::Instruction instruction;
};

/// Code generation functor.
///
/// This structure will generate a CPU instruction bitfield when applied to a parsed instruction.
struct CodeGen
{
  /// Apply functor.
  ///
  /// This will generate the CPU instruction bitfield encoding the passed parsed \p instruction.
  ///
  /// \param instruction parsed instruction for which to generate the cpu instruction bitfield
  /// \return cpu instruction bitfield for the passed \a instruction
  /// \throw std::runtime_error if the parsed \p instruction has an invalid combination of operation and operands
  core::cpu::Instruction operator()(const parser::Instruction &instruction) const;
};

/// Data generation functor.
///
/// When applied to a data line, this will return a vector of machine words that represents
/// the assembled/in-memory format of the data line.
///
/// Data types with a granularity smaller than a machine word (byte data, for example) are
/// zero-padded to a multiple of the machine word size.
struct DataGen
{
  /// Apply functor to a line of byte data.
  ///
  /// \param dataLine ByteData line for which to generate byte byteData
  /// \return assembled byteData for the passed byteData line
  std::vector<core::WORD> operator()(const parser::ByteData &byteData) const;

  /// Apply functor to a line of word data.
  ///
  /// \param dataLine WordData line for which to generate byte wordData
  /// \return assembled data for the passed wordData line
  std::vector<core::WORD> operator()(const parser::WordData &wordData) const;

  /// Apply functor to a line of string data.
  ///
  /// \note string data is always prefixed with a single machine word to indicate the length of
  ///   the string (in characters)
  /// \note strings are stored using a single machine word per character, where the low byte
  ///   encodes the character and the high byte is used to store an 8-bit character attribute.
  ///
  /// \param dataLine StringData line for which to generate string data
  /// \return assembled data for the passed string-type \p dataLine
  std::vector<core::WORD> operator()(const parser::StringData &stringData) const;
};

/// Generate assembly from the passed AST.
///
/// This will perform semantic checks on the AST (e.g. checking instruction signatures and
/// operand value ranges), and generate code and data chunks from the parsed AST sections,
/// to produce an assembly that can be loaded directly into system memory.
///
/// \param ast AST from which to generate the assembly
/// \para, options assembler options
/// \return generated assembly
/// \throw AssemblerException if code generation failed, e.g. because an instruction with an
///   invalid signature was encountered.
std::unique_ptr<utils::Assembly> generate(const parser::AST &ast, const AssemblerOptions &options);

}
