/// Header file that defines the opcode signature table used for code generation and validation.
///
/// The assembler uses this table to lookup and map instruction signatures (operation + operands) to
/// CPU opcodes, for semantic checks on parsed instructions and to generate CPU instruction bitfields.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#pragma once

#include "parser.h"

#include "core/cpu/itf/opcodes.h"

#include <map>

namespace tools::kumitate::codegen {

using Operand = parser::Instruction::Operand;
using Signature = parser::Instruction::Signature;

/// Table that maps instruction (operation, signature) pairs to CPU opcodes. This tells the code
/// generator that e.g. a 'mov' instruction with operands 'reg, effective address' translates
/// to a MOVE opcode. This table is also used by the semantic pass to identify invalid combinations
/// of operations and operands.
inline const std::map<std::pair<core::cpu::Operation, uint64_t>, core::cpu::Opcode> OPCODE_SIGNATURES = {
  { { core::cpu::Operation::NOP,   Signature() },                                                                               core::cpu::Opcode::NOP },
  { { core::cpu::Operation::RET,   Signature() },                                                                               core::cpu::Opcode::RET },
  { { core::cpu::Operation::RTI,   Signature() },                                                                               core::cpu::Opcode::RTI },
  { { core::cpu::Operation::CLR,   Signature(Operand::Type::IMMEDIATE) },                                                       core::cpu::Opcode::CLR },
  { { core::cpu::Operation::CLR,   Signature(Operand::Type::FLAGS) },                                                           core::cpu::Opcode::CLR },
  { { core::cpu::Operation::SET,   Signature(Operand::Type::IMMEDIATE) },                                                       core::cpu::Opcode::SET },
  { { core::cpu::Operation::SET,   Signature(Operand::Type::FLAGS) },                                                           core::cpu::Opcode::SET },
  { { core::cpu::Operation::NEG,   Signature(Operand::Type::REG) },                                                             core::cpu::Opcode::NEG},
  { { core::cpu::Operation::NOT,   Signature(Operand::Type::REG) },                                                             core::cpu::Opcode::NOT},
  { { core::cpu::Operation::PUSH,  Signature(Operand::Type::REG) },                                                             core::cpu::Opcode::PUSH },
  { { core::cpu::Operation::POP,   Signature(Operand::Type::REG) },                                                             core::cpu::Opcode::POP },
  { { core::cpu::Operation::PUSHS, Signature() },                                                                               core::cpu::Opcode::PUSHS },
  { { core::cpu::Operation::POPS,  Signature() },                                                                               core::cpu::Opcode::POPS },
  { { core::cpu::Operation::B,     Signature(Operand::Type::BRANCH_CONDITION, Operand::Type::DIRECT_ADDRESS) },                 core::cpu::Opcode::B },
  { { core::cpu::Operation::DIV,   Signature(Operand::Type::REG, Operand::Type::REG, Operand::Type::REG, Operand::Type::REG) }, core::cpu::Opcode::DIV },
  { { core::cpu::Operation::MUL,   Signature(Operand::Type::REG, Operand::Type::REG, Operand::Type::REG, Operand::Type::REG) }, core::cpu::Opcode::MUL },
  { { core::cpu::Operation::DIVS,  Signature(Operand::Type::REG, Operand::Type::REG, Operand::Type::REG, Operand::Type::REG) }, core::cpu::Opcode::DIVS },
  { { core::cpu::Operation::MULS,  Signature(Operand::Type::REG, Operand::Type::REG, Operand::Type::REG, Operand::Type::REG) }, core::cpu::Opcode::MULS },
  { { core::cpu::Operation::LDSP,  Signature(Operand::Type::REG, Operand::Type::SP_REG) },                                      core::cpu::Opcode::LDSP },
  { { core::cpu::Operation::STSP,  Signature(Operand::Type::SP_REG, Operand::Type::REG) },                                      core::cpu::Opcode::STSP },
  { { core::cpu::Operation::RSV,   Signature(Operand::Type::IMMEDIATE) },                                                       core::cpu::Opcode::RSV },
  { { core::cpu::Operation::BANK,  Signature(Operand::Type::REG) },                                                             core::cpu::Opcode::BANK },
  { { core::cpu::Operation::WAIT,  Signature(Operand::Type::IMMEDIATE) },                                                       core::cpu::Opcode::WAIT },
  { { core::cpu::Operation::HALT,  Signature() },                                                                               core::cpu::Opcode::HALT },

  { { core::cpu::Operation::CALL,  Signature(Operand::Type::DIRECT_ADDRESS) },                                                  core::cpu::Opcode::CALLM },
  { { core::cpu::Operation::CALL,  Signature(Operand::Type::VECTOR) },                                                          core::cpu::Opcode::CALLV },
  { { core::cpu::Operation::J,     Signature(Operand::Type::DIRECT_ADDRESS) },                                                  core::cpu::Opcode::JM },
  { { core::cpu::Operation::J,     Signature(Operand::Type::VECTOR) },                                                          core::cpu::Opcode::JV },

  { { core::cpu::Operation::MOV,   Signature(Operand::Type::REG, Operand::Type::REG) },                                         core::cpu::Opcode::MOV },
  { { core::cpu::Operation::MOV,   Signature(Operand::Type::REG, Operand::Type::IMMEDIATE) },                                   core::cpu::Opcode::MOVI },
  { { core::cpu::Operation::MOV,   Signature(Operand::Type::REG, Operand::Type::DIRECT_ADDRESS) },                              core::cpu::Opcode::MOVM },
  { { core::cpu::Operation::MOV,   Signature(Operand::Type::REG, Operand::Type::INDIRECT_ADDRESS) },                            core::cpu::Opcode::MOVE },
  { { core::cpu::Operation::MOV,   Signature(Operand::Type::DIRECT_ADDRESS, Operand::Type::REG) },                              core::cpu::Opcode::MOVM },
  { { core::cpu::Operation::MOV,   Signature(Operand::Type::INDIRECT_ADDRESS, Operand::Type::REG) },                            core::cpu::Opcode::MOVE },

  { { core::cpu::Operation::CMP,   Signature(Operand::Type::REG, Operand::Type::REG) },                                         core::cpu::Opcode::CMP },
  { { core::cpu::Operation::CMP,   Signature(Operand::Type::REG, Operand::Type::IMMEDIATE) },                                   core::cpu::Opcode::CMPI },
  { { core::cpu::Operation::CMP,   Signature(Operand::Type::REG, Operand::Type::DIRECT_ADDRESS) },                              core::cpu::Opcode::CMPM },
  { { core::cpu::Operation::CMP,   Signature(Operand::Type::REG, Operand::Type::INDIRECT_ADDRESS) },                            core::cpu::Opcode::CMPE },
  { { core::cpu::Operation::CMP,   Signature(Operand::Type::DIRECT_ADDRESS, Operand::Type::REG) },                              core::cpu::Opcode::CMPM },
  { { core::cpu::Operation::CMP,   Signature(Operand::Type::INDIRECT_ADDRESS, Operand::Type::REG) },                            core::cpu::Opcode::CMPE },

  { { core::cpu::Operation::ADD,   Signature(Operand::Type::REG, Operand::Type::REG) },                                         core::cpu::Opcode::ADD },
  { { core::cpu::Operation::ADD,   Signature(Operand::Type::REG, Operand::Type::IMMEDIATE) },                                   core::cpu::Opcode::ADDI },
  { { core::cpu::Operation::ADD,   Signature(Operand::Type::REG, Operand::Type::DIRECT_ADDRESS) },                              core::cpu::Opcode::ADDM },
  { { core::cpu::Operation::ADD,   Signature(Operand::Type::REG, Operand::Type::INDIRECT_ADDRESS) },                            core::cpu::Opcode::ADDE },
  { { core::cpu::Operation::ADD,   Signature(Operand::Type::DIRECT_ADDRESS, Operand::Type::REG) },                              core::cpu::Opcode::ADDM },
  { { core::cpu::Operation::ADD,   Signature(Operand::Type::INDIRECT_ADDRESS, Operand::Type::REG) },                            core::cpu::Opcode::ADDE },

  { { core::cpu::Operation::ADC,   Signature(Operand::Type::REG, Operand::Type::REG) },                                         core::cpu::Opcode::ADC },
  { { core::cpu::Operation::ADC,   Signature(Operand::Type::REG, Operand::Type::IMMEDIATE) },                                   core::cpu::Opcode::ADCI },
  { { core::cpu::Operation::ADC,   Signature(Operand::Type::REG, Operand::Type::DIRECT_ADDRESS) },                              core::cpu::Opcode::ADCM },
  { { core::cpu::Operation::ADC,   Signature(Operand::Type::REG, Operand::Type::INDIRECT_ADDRESS) },                            core::cpu::Opcode::ADCE },
  { { core::cpu::Operation::ADC,   Signature(Operand::Type::DIRECT_ADDRESS, Operand::Type::REG) },                              core::cpu::Opcode::ADCM },
  { { core::cpu::Operation::ADC,   Signature(Operand::Type::INDIRECT_ADDRESS, Operand::Type::REG) },                            core::cpu::Opcode::ADCE },

  { { core::cpu::Operation::SUB,   Signature(Operand::Type::REG, Operand::Type::REG) },                                         core::cpu::Opcode::SUB },
  { { core::cpu::Operation::SUB,   Signature(Operand::Type::REG, Operand::Type::IMMEDIATE) },                                   core::cpu::Opcode::SUBI },
  { { core::cpu::Operation::SUB,   Signature(Operand::Type::REG, Operand::Type::DIRECT_ADDRESS) },                              core::cpu::Opcode::SUBM },
  { { core::cpu::Operation::SUB,   Signature(Operand::Type::REG, Operand::Type::INDIRECT_ADDRESS) },                            core::cpu::Opcode::SUBE },
  { { core::cpu::Operation::SUB,   Signature(Operand::Type::DIRECT_ADDRESS, Operand::Type::REG) },                              core::cpu::Opcode::SUBM },
  { { core::cpu::Operation::SUB,   Signature(Operand::Type::INDIRECT_ADDRESS, Operand::Type::REG) },                            core::cpu::Opcode::SUBE },

  { { core::cpu::Operation::SBB,   Signature(Operand::Type::REG, Operand::Type::REG) },                                         core::cpu::Opcode::SBB },
  { { core::cpu::Operation::SBB,   Signature(Operand::Type::REG, Operand::Type::IMMEDIATE) },                                   core::cpu::Opcode::SBBI },
  { { core::cpu::Operation::SBB,   Signature(Operand::Type::REG, Operand::Type::DIRECT_ADDRESS) },                              core::cpu::Opcode::SBBM },
  { { core::cpu::Operation::SBB,   Signature(Operand::Type::REG, Operand::Type::INDIRECT_ADDRESS) },                            core::cpu::Opcode::SBBE },
  { { core::cpu::Operation::SBB,   Signature(Operand::Type::DIRECT_ADDRESS, Operand::Type::REG) },                              core::cpu::Opcode::SBBM },
  { { core::cpu::Operation::SBB,   Signature(Operand::Type::INDIRECT_ADDRESS, Operand::Type::REG) },                            core::cpu::Opcode::SBBE },

  { { core::cpu::Operation::ASR,   Signature(Operand::Type::REG, Operand::Type::REG) },                                         core::cpu::Opcode::ASR },
  { { core::cpu::Operation::ASR,   Signature(Operand::Type::REG, Operand::Type::IMMEDIATE) },                                   core::cpu::Opcode::ASRI },
  { { core::cpu::Operation::ASR,   Signature(Operand::Type::REG, Operand::Type::DIRECT_ADDRESS) },                              core::cpu::Opcode::ASRM },
  { { core::cpu::Operation::ASR,   Signature(Operand::Type::REG, Operand::Type::INDIRECT_ADDRESS) },                            core::cpu::Opcode::ASRE },
  { { core::cpu::Operation::ASR,   Signature(Operand::Type::DIRECT_ADDRESS, Operand::Type::REG) },                              core::cpu::Opcode::ASRM },
  { { core::cpu::Operation::ASR,   Signature(Operand::Type::INDIRECT_ADDRESS, Operand::Type::REG) },                            core::cpu::Opcode::ASRE },

  { { core::cpu::Operation::AND,   Signature(Operand::Type::REG, Operand::Type::REG) },                                         core::cpu::Opcode::AND },
  { { core::cpu::Operation::AND,   Signature(Operand::Type::REG, Operand::Type::IMMEDIATE) },                                   core::cpu::Opcode::ANDI },
  { { core::cpu::Operation::AND,   Signature(Operand::Type::REG, Operand::Type::DIRECT_ADDRESS) },                              core::cpu::Opcode::ANDM },
  { { core::cpu::Operation::AND,   Signature(Operand::Type::REG, Operand::Type::INDIRECT_ADDRESS) },                            core::cpu::Opcode::ANDE },
  { { core::cpu::Operation::AND,   Signature(Operand::Type::DIRECT_ADDRESS, Operand::Type::REG) },                              core::cpu::Opcode::ANDM },
  { { core::cpu::Operation::AND,   Signature(Operand::Type::INDIRECT_ADDRESS, Operand::Type::REG) },                            core::cpu::Opcode::ANDE },

  { { core::cpu::Operation::OR,    Signature(Operand::Type::REG, Operand::Type::REG) },                                         core::cpu::Opcode::OR },
  { { core::cpu::Operation::OR,    Signature(Operand::Type::REG, Operand::Type::IMMEDIATE) },                                   core::cpu::Opcode::ORI },
  { { core::cpu::Operation::OR,    Signature(Operand::Type::REG, Operand::Type::DIRECT_ADDRESS) },                              core::cpu::Opcode::ORM },
  { { core::cpu::Operation::OR,    Signature(Operand::Type::REG, Operand::Type::INDIRECT_ADDRESS) },                            core::cpu::Opcode::ORE },
  { { core::cpu::Operation::OR,    Signature(Operand::Type::DIRECT_ADDRESS, Operand::Type::REG) },                              core::cpu::Opcode::ORM },
  { { core::cpu::Operation::OR,    Signature(Operand::Type::INDIRECT_ADDRESS, Operand::Type::REG) },                            core::cpu::Opcode::ORE },

  { { core::cpu::Operation::XOR,   Signature(Operand::Type::REG, Operand::Type::REG) },                                         core::cpu::Opcode::XOR },
  { { core::cpu::Operation::XOR,   Signature(Operand::Type::REG, Operand::Type::IMMEDIATE) },                                   core::cpu::Opcode::XORI },
  { { core::cpu::Operation::XOR,   Signature(Operand::Type::REG, Operand::Type::DIRECT_ADDRESS) },                              core::cpu::Opcode::XORM },
  { { core::cpu::Operation::XOR,   Signature(Operand::Type::REG, Operand::Type::INDIRECT_ADDRESS) },                            core::cpu::Opcode::XORE },
  { { core::cpu::Operation::XOR,   Signature(Operand::Type::DIRECT_ADDRESS, Operand::Type::REG) },                              core::cpu::Opcode::XORM },
  { { core::cpu::Operation::XOR,   Signature(Operand::Type::INDIRECT_ADDRESS, Operand::Type::REG) },                            core::cpu::Opcode::XORE },

  { { core::cpu::Operation::LSL,   Signature(Operand::Type::REG, Operand::Type::REG) },                                         core::cpu::Opcode::LSL },
  { { core::cpu::Operation::LSL,   Signature(Operand::Type::REG, Operand::Type::IMMEDIATE) },                                   core::cpu::Opcode::LSLI },
  { { core::cpu::Operation::LSL,   Signature(Operand::Type::REG, Operand::Type::DIRECT_ADDRESS) },                              core::cpu::Opcode::LSLM },
  { { core::cpu::Operation::LSL,   Signature(Operand::Type::REG, Operand::Type::INDIRECT_ADDRESS) },                            core::cpu::Opcode::LSLE },
  { { core::cpu::Operation::LSL,   Signature(Operand::Type::DIRECT_ADDRESS, Operand::Type::REG) },                              core::cpu::Opcode::LSLM },
  { { core::cpu::Operation::LSL,   Signature(Operand::Type::INDIRECT_ADDRESS, Operand::Type::REG) },                            core::cpu::Opcode::LSLE },

  { { core::cpu::Operation::LSR,   Signature(Operand::Type::REG, Operand::Type::REG) },                                         core::cpu::Opcode::LSR },
  { { core::cpu::Operation::LSR,   Signature(Operand::Type::REG, Operand::Type::IMMEDIATE) },                                   core::cpu::Opcode::LSRI },
  { { core::cpu::Operation::LSR,   Signature(Operand::Type::REG, Operand::Type::DIRECT_ADDRESS) },                              core::cpu::Opcode::LSRM },
  { { core::cpu::Operation::LSR,   Signature(Operand::Type::REG, Operand::Type::INDIRECT_ADDRESS) },                            core::cpu::Opcode::LSRE },
  { { core::cpu::Operation::LSR,   Signature(Operand::Type::DIRECT_ADDRESS, Operand::Type::REG) },                              core::cpu::Opcode::LSRM },
  { { core::cpu::Operation::LSR,   Signature(Operand::Type::INDIRECT_ADDRESS, Operand::Type::REG) },                            core::cpu::Opcode::LSRE },

  { { core::cpu::Operation::INC,   Signature(Operand::Type::REG) },                                                             core::cpu::Opcode::INC },
  { { core::cpu::Operation::INC,   Signature(Operand::Type::DIRECT_ADDRESS) },                                                  core::cpu::Opcode::INCM },
  { { core::cpu::Operation::INC,   Signature(Operand::Type::INDIRECT_ADDRESS) },                                                core::cpu::Opcode::INCE },

  { { core::cpu::Operation::DEC,   Signature(Operand::Type::REG) },                                                             core::cpu::Opcode::DEC },
  { { core::cpu::Operation::DEC,   Signature(Operand::Type::DIRECT_ADDRESS) },                                                  core::cpu::Opcode::DECM },
  { { core::cpu::Operation::DEC,   Signature(Operand::Type::INDIRECT_ADDRESS) },                                                core::cpu::Opcode::DECE },

  { { core::cpu::Operation::MOVL,  Signature(Operand::Type::REG, Operand::Type::REG, Operand::Type::BOOLEAN) },                 core::cpu::Opcode::MOVL },
  { { core::cpu::Operation::MOVL,  Signature(Operand::Type::REG, Operand::Type::IMMEDIATE, Operand::Type::BOOLEAN) },           core::cpu::Opcode::MOVLI },
  { { core::cpu::Operation::MOVL,  Signature(Operand::Type::REG, Operand::Type::DIRECT_ADDRESS, Operand::Type::BOOLEAN) },      core::cpu::Opcode::MOVLM },
  { { core::cpu::Operation::MOVL,  Signature(Operand::Type::REG, Operand::Type::INDIRECT_ADDRESS, Operand::Type::BOOLEAN) },    core::cpu::Opcode::MOVLE },
  { { core::cpu::Operation::MOVL,  Signature(Operand::Type::DIRECT_ADDRESS, Operand::Type::REG, Operand::Type::BOOLEAN) },      core::cpu::Opcode::MOVLM },
  { { core::cpu::Operation::MOVL,  Signature(Operand::Type::INDIRECT_ADDRESS, Operand::Type::REG, Operand::Type::BOOLEAN) },    core::cpu::Opcode::MOVLE },

  { { core::cpu::Operation::MOVH,  Signature(Operand::Type::REG, Operand::Type::REG, Operand::Type::BOOLEAN) },                 core::cpu::Opcode::MOVH },
  { { core::cpu::Operation::MOVH,  Signature(Operand::Type::REG, Operand::Type::IMMEDIATE, Operand::Type::BOOLEAN) },           core::cpu::Opcode::MOVHI },
  { { core::cpu::Operation::MOVH,  Signature(Operand::Type::REG, Operand::Type::DIRECT_ADDRESS, Operand::Type::BOOLEAN) },      core::cpu::Opcode::MOVHM },
  { { core::cpu::Operation::MOVH,  Signature(Operand::Type::REG, Operand::Type::INDIRECT_ADDRESS, Operand::Type::BOOLEAN) },    core::cpu::Opcode::MOVHE },
  { { core::cpu::Operation::MOVH,  Signature(Operand::Type::DIRECT_ADDRESS, Operand::Type::REG, Operand::Type::BOOLEAN) },      core::cpu::Opcode::MOVHM },
  { { core::cpu::Operation::MOVH,  Signature(Operand::Type::INDIRECT_ADDRESS, Operand::Type::REG, Operand::Type::BOOLEAN) },    core::cpu::Opcode::MOVHE },

  { { core::cpu::Operation::OUT,   Signature(Operand::Type::IMMEDIATE, Operand::Type::IMMEDIATE, Operand::Type::REG) },         core::cpu::Opcode::OUTI },
  { { core::cpu::Operation::OUT,   Signature(Operand::Type::IMMEDIATE, Operand::Type::REG, Operand::Type::REG) },               core::cpu::Opcode::OUTR },
  { { core::cpu::Operation::IN,    Signature(Operand::Type::IMMEDIATE, Operand::Type::IMMEDIATE, Operand::Type::REG) },         core::cpu::Opcode::INI },
  { { core::cpu::Operation::IN,    Signature(Operand::Type::IMMEDIATE, Operand::Type::REG, Operand::Type::REG) },               core::cpu::Opcode::INR },
};

}
