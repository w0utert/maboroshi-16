/// Implements main assembler interface for the kumitate assembler.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#include "itf/assembler.h"

#include "tools/kumitate/itf/preprocessor.h"

#include "parser.h"
#include "code_gen.h"

namespace tools::kumitate {

// Assemble source code stream
std::unique_ptr<utils::Assembly> assemble(std::istream &source, const AssemblerOptions &options,  const std::vector<std::string> &includePaths, const std::string &name)
{
  // Preprocess source file to expand includes, resolve defines, etc
  const std::string preprocessed_source = preprocessor::preprocess(source, includePaths, name);

  // Parse preprocessed source file to get a (very rudimentary) AST
  parser::AST ast = parser::parse(preprocessed_source);

  // Generate code from the AST, producing an assembly that can be loaded into system memory directly
  return codegen::generate(ast, options);
}

}
