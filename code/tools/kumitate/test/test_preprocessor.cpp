/// Implements tets cases for the preprocessor functions of the kumitate assembler.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#include "tools/kumitate/itf/preprocessor.h"
#include "tools/kumitate/itf/assembler.h"

#include <boost/algorithm/string/replace.hpp>

#include <fmt/format.h>

#include <gtest/gtest.h>

#include <fstream>
#include <algorithm>
#include <filesystem>

using namespace tools::kumitate::preprocessor;

namespace test {

/// Preprocess input .kasm source file and compare with benchmark file
static void test_preprocess(const std::string &testCase)
{
  const std::string bin_root = std::filesystem::canonical(std::filesystem::current_path());
  const std::string test_root = fmt::format("{0}/tools/kumitate/test/", bin_root);

  std::ifstream input_stream(fmt::format("{0}/input/{1}.kasm", test_root, testCase));

  ASSERT_TRUE(input_stream.is_open());

  std::string preprocessed = preprocess(input_stream, { fmt::format("{0}/input", test_root) });

  boost::replace_all(preprocessed, bin_root, "${MABOROSHI_ROOT}");

  std::ofstream result_file(fmt::format("{0}/tools/kumitate/test/result/{1}.kpasm", bin_root, testCase));

  result_file << preprocessed;

  std::ifstream bmk_stream(fmt::format("{0}/tools/kumitate/test/benchmark/{1}.kpasm", bin_root, testCase));

  ASSERT_TRUE(bmk_stream.is_open());

  ASSERT_TRUE(std::equal(std::istreambuf_iterator<char>(bmk_stream), std::istreambuf_iterator<char>(), preprocessed.begin()));
}

/// Tests resolution of #include directives
TEST(TestPreprocessor, testIncludes)
{
  test_preprocess("TestPreprocessor.testIncludes");
}

/// Tests #including the same file twice (should only expand it once)
TEST(TestPreprocessor, testDuplicateIncludes)
{
  test_preprocess("TestPreprocessor.testDuplicateIncludes");
}

/// Test include that cannot be resolved
TEST(TestPreprocessor, testUnresolvedInclude)
{
  ASSERT_THROW(test_preprocess("TestPreprocessor.testUnresolvedInclude"), tools::kumitate::AssemblerException);
}

/// Test expanding defines
TEST(TestAssembler, testDefines)
{
  test_preprocess("TestPreprocessor.testDefines");
}

/// Test unresolved define
TEST(TestPreprocessor, testUnresolvedDefine)
{
  ASSERT_THROW(test_preprocess("TestPreprocessor.testUnresolvedDefine"), tools::kumitate::AssemblerException);
}

/// Test expanding tile set .png images to byte data
TEST(TestPreprocessor, testTiles)
{
  test_preprocess("TestPreprocessor.testTiles");
}

/// Test preprocessor syntax errors
TEST(TestPreprocessor, testSyntaxErrors)
{
  const auto preprocess_string = [](const std::string &source)
  {
    std::istringstream source_stream(source);
    preprocess(source_stream);
  };

  ASSERT_THROW(preprocess_string("#bad_directive"), tools::kumitate::AssemblerException);
  ASSERT_THROW(preprocess_string("#include \"no_closing_quote"), tools::kumitate::AssemblerException);
  ASSERT_THROW(preprocess_string("#define NO_VALUE_FOR_DEFINE"), tools::kumitate::AssemblerException);
}

}
