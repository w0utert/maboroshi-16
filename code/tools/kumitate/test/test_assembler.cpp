/// Test cases covering basic assembler functionality.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#include "itf/assembler.h"

#include "test/utils.h"

#include <gtest/gtest.h>

#include <cstdlib>
#include <sstream>
#include <memory>

namespace test {

static void test_assembly(const std::string &testCase, const std::string &bmk_sha, const bool strip = true)
{
  const std::string filename = fmt::format("./tools/kumitate/test/input/{0}.kasm", testCase);

  std::ifstream source_file(filename);

  ASSERT_TRUE(source_file.is_open());

  std::stringstream source_text;
  source_text << source_file.rdbuf();

  tools::kumitate::AssemblerOptions options;
  options.strip = strip;

  const std::unique_ptr<const utils::Assembly> assembly(tools::kumitate::assemble(source_text, options, {}, filename));

  const std::string res_filename = fmt::format("./tools/kumitate/test/result/{0}.mabo", testCase);
  const std::string bmk_filename = fmt::format("./tools/kumitate/test/benchmark/{0}.mabo", testCase);

  std::ofstream result_file(res_filename, std::ios_base::binary | std::ios_base::trunc);

  ASSERT_TRUE(result_file.is_open());

  assembly->write(result_file);
  result_file.close();

  const std::string res_sha = test::sha256(res_filename);

  ASSERT_EQ(res_sha, bmk_sha) << fmt::format("\nTest files:\n  res:\t{0}\n  bmk:\t{1}\n\n", res_filename, bmk_filename);
}

/// Tests parsing a very basic assembly source file that contains only a single code section
TEST(TestAssembler, testSingleSection)
{
  test_assembly("TestAssembler.testSingleSection", "ad70d812b31cb8f0d2bef1ac1de320220254b9cd5bb28936a6d5ce3e38307035");
}

/// Tests parsing an assembly source file that contains labels and vectors
TEST(TestAssembler, testLabels)
{
  test_assembly("TestAssembler.testLabels", "cd12501c31afd0651e8fe7d45be17b4b9568a971859073689f35af9ff8a3c37d");
}

/// Tests parsing an assembly source file that contains symbols defined inside namespaces. This test case also
/// covers writing debug info to the assembly (ie: not stripping it).
TEST(TestAssembler, testNamespaces)
{
  test_assembly("TestAssembler.testNamespaces", "12874a44f5796259283a31240a7fd38e03a4483a07228753207ea082d01861cb", false);
}

/// Tests parsing data sections
TEST(TestAssembler, testDataSections)
{
  test_assembly("TestAssembler.testDataSections", "4fa459b05053f0d0bf7d308ebf01bbdc027db7dc55086ad0327f099fc26c8985");
}

/// Tests parsing a 'real' program (calculating fibonacci sequence)
TEST(TestAssembler, testFibonacci)
{
  test_assembly("TestAssembler.testFibonacci", "7e7de808f4baf4b96add45327358254493096e7fc05fd3ac55266de39053ea6c");
}

/// Round-trip test: assemble and write source file, read it back, then save again and check whether the
/// second file written is identical to the first file written
TEST(TestAssembler, testRoundtrip)
{
  // Read and assemble source
  const std::string source_streamname = fmt::format("./tools/kumitate/test/input/TestAssembler.testFibonacci.kasm");

  std::ifstream source_stream(source_streamname);

  ASSERT_TRUE(source_stream.is_open());

  const std::unique_ptr<const utils::Assembly> assembly(tools::kumitate::assemble(source_stream, {}, {}, source_streamname));

  // Write to binary
  const std::string result_filename = fmt::format("./tools/kumitate/test/result/{0}", "TestAssembler.testRoundtrip.1.mabo");

  std::ofstream result_file(result_filename, std::ios_base::binary | std::ios_base::trunc);

  ASSERT_TRUE(result_file.is_open());

  assembly->write(result_file);
  result_file.close();

  // Read back result file
  std::ifstream result_file_in(result_filename, std::ios_base::binary);

  ASSERT_TRUE(result_file_in.is_open());

  const auto roundtrip_assembly = utils::Assembly::fromInputStream(result_file_in);

  // Write to roundtrip binary
  const std::string roundtrip_filename = fmt::format("./tools/kumitate/test/result/{0}", "TestAssembler.testRoundtrip.2.mabo");

  std::ofstream roundtrip_file(roundtrip_filename, std::ios_base::binary | std::ios_base::trunc);

  ASSERT_TRUE(roundtrip_file.is_open());

  roundtrip_assembly.write(roundtrip_file);
  roundtrip_file.close();

  // Verify both assemblies written have the same shasum
  ASSERT_EQ(test::sha256(result_filename), test::sha256(roundtrip_filename));
}

/// Test case that assembles a source file that contains all supported CPU instructions,
/// with all valid combinations of operands
TEST(testAssembler, testAllInstructions)
{
  test_assembly("TestAssembler.testAllInstructions", "5314763f8da6f5e2e766300ac350a97a664133d762d832534141c67e250a8869");
}

/// Test assembler syntax errors
TEST(TestAssembler, testSyntaxErrors)
{
  const auto assemble_string = [](const std::string &source)
  {
    std::istringstream source_stream(source);
    tools::kumitate::assemble(source_stream, {});
  };

  // \todo NOTE: these should all throw some kind of ParseException that has more info about e.g. file and line number
  ASSERT_THROW(assemble_string(":invalid_section"), tools::kumitate::AssemblerException);
  ASSERT_THROW(assemble_string(":code\n invalid_opcode r0, #0x123"), tools::kumitate::AssemblerException);
  ASSERT_THROW(assemble_string(":code\n mov r0, #0xbad_number"), tools::kumitate::AssemblerException);
  ASSERT_THROW(assemble_string(":code\n push #0x1234 ; bad operand type"), tools::kumitate::AssemblerException);
  ASSERT_THROW(assemble_string(":code\n move r8, #0x1234 ; bad register"), tools::kumitate::AssemblerException);
  ASSERT_THROW(assemble_string(":code\n bad_label*:"), tools::kumitate::AssemblerException);
  ASSERT_THROW(assemble_string(":code\n label:\n mov r0, @label.bad_suffix"), tools::kumitate::AssemblerException);
  ASSERT_THROW(assemble_string(":code\n label:\n mov r0, @label.offset + bad_offset"), tools::kumitate::AssemblerException);
  ASSERT_THROW(assemble_string(":data (0x02:0x0000)\n.bytezzz 0x10, 0x00"), tools::kumitate::AssemblerException);
  ASSERT_THROW(assemble_string(":data ; data section without address"), tools::kumitate::AssemblerException);
  ASSERT_THROW(assemble_string(":data (0x99:0xF) ; bad data section address"), std::logic_error);
}

}
