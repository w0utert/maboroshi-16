/// Declares public interfaces for the main assembler functions of the kumitate assembler.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#pragma once

#include "core/common/itf/types.h"
#include "core/cpu/itf/instruction.h"

#include "utils/itf/assembly.h"

#include <string>
#include <vector>
#include <istream>
#include <memory>

namespace tools::kumitate {

/// Assembler exception with file & line number information.
class AssemblerException : public std::runtime_error
{
  public:
    /// Constructor.
    ///
    /// \param what reason for the exception
    /// \param msg error message
    /// \param file name of the file that contained an error
    /// \param line line number that contained the error
    AssemblerException(const std::string &what, const std::string &msg, const std::string &file, const int line)
    :
      std::runtime_error(what), msg(msg), file(file), line(line)
    {
    }

    /// Error message
    std::string msg;
    /// Name of the file that contained an error
    std::string file;
    /// Line number that contained the error
    int line;
};

/// Assembler options
struct AssemblerOptions
{
  /// Construct with default options
  AssemblerOptions() : strip(false) {}

  bool strip;     ///< Strip debug information
};

/// Assemble \p source code stream.
///
/// \param source text to assemble
/// \param includePaths search paths for resolving #includes
/// \param name by which the parser will reference the source stream in error/warning messages
/// \return assembled source code stream
/// \throw AssemblerException if the source stream could not be assembled
std::unique_ptr<utils::Assembly> assemble(
  std::istream &source,
  const AssemblerOptions &options,
  const std::vector<std::string> &includePaths = {},
  const std::string &name = "(stdin)");

}
