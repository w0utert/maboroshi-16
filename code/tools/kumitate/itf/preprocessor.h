/// Header file for the preprocessor functions of the kumitate assembler.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#pragma once

#include <string>
#include <istream>

#include <vector>
#include <set>

namespace tools::kumitate::preprocessor {

/// Preprocess .kasm source text.
///
/// The returned string can be considered an amalgamation of all source code required
/// to assemble the program in the input .kasm source. It is itself a valid .kasm source
/// text, but free of any preprocessor directives, and annotated to e.g. simplify assembler
/// error reporting.
///
/// The preprocessor will currently perform the following transformations:
///
///   - Resolve and expand #includes
///   - Parse and expand #defines
///   - Extract tile & palette data and expand to .byte and .word data (#tiles and #palette directives)
///   - Add filename & line number annotations
///   - Strip comments and redundant newlines
///
/// \param source source text to preprocess
/// \param includePaths search paths for resolving #includes
/// \param name by which the parser will reference the preprocessed source in error/warning messages
/// \return preprocessed amalgamation source text
/// \throw AssemblerException if the source text contains preprocessor syntax- and/or semantic errors
std::string preprocess(std::istream &source, const std::vector<std::string> &includePaths = {}, const std::string &name = "(stdin)");

}