/// Implements test cases related to the basic types used throughout the codebase.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#include "core/common/itf/types.h"
#include "core/common/itf/bit_range.h"

#include "core/cpu/itf/cpu.h"

#include <gtest/gtest.h>

using namespace core;
using namespace core::cpu;

namespace test {

/// Tests the BitField type
TEST(TestTypes, testBitField)
{
  union TestBitField
  {
     BitRange<WORD, int, 0, 5> x;
     BitRange<WORD, SBYTE, 5, 5> y;
     BitRange<WORD, BYTE, 10, 5> z;
     BitRange<WORD, bool, 15, 1> b;

     WORD bits;

    TestBitField() : bits(0) {}
  } __attribute__((packed));

  TestBitField t;

  ASSERT_EQ(sizeof(WORD), sizeof(TestBitField));

  // Test base case (assigning values that fit in each fields width)
  t.x = -3;
  t.y = -8;
  t.z = 10;

  ASSERT_EQ(0b0010101100011101, t.bits);
  EXPECT_EQ(-3, static_cast<int>(t.x));
  EXPECT_EQ(-8, static_cast<SBYTE>(t.y));
  EXPECT_EQ(10, static_cast<BYTE>(t.z));
  EXPECT_EQ(false, static_cast<bool>(t.b));

  // Test setting individual bitfield: should not affect other bitfields
  t.y = 0;

  ASSERT_EQ(0b0010100000011101, t.bits);
  EXPECT_EQ(-3, static_cast<int>(t.x));
  EXPECT_EQ(0, static_cast<SBYTE>(t.y));
  EXPECT_EQ(10, static_cast<BYTE>(t.z));
  EXPECT_EQ(false, static_cast<bool>(t.b));

  // Test setting/getting unsigned value that will be narrowed. This should simply discard the high bits
  t.z = 0xFF;

  ASSERT_EQ(0b0111110000011101, t.bits);
  EXPECT_EQ(-3, static_cast<int>(t.x));
  EXPECT_EQ(0, static_cast<SBYTE>(t.y));
  EXPECT_EQ(0b11111, static_cast<BYTE>(t.z));
  EXPECT_EQ(false, static_cast<bool>(t.b));

  // Test setting/getting signed value. This should be narrowed when assigning, then sign-extended when getting.
  t.y = -4;

  ASSERT_EQ(0b0111111110011101, t.bits);
  EXPECT_EQ(-3, static_cast<int>(t.x));
  EXPECT_EQ(-4, static_cast<SBYTE>(t.y));
  EXPECT_EQ(0b11111, static_cast<BYTE>(t.z));
  EXPECT_EQ(false, static_cast<bool>(t.b));

  // Compile-time tests: the following test cases merely verify that the tested use cases actually compile
  t.b = true;
  const bool b = t.b;
  EXPECT_TRUE(b);

  if (!t.b)
  {
    GTEST_FAIL() << "Failed implicit cast from BitField::BitRange to bool";
  }
}

/// Tests long pointer type
TEST(TestTypes, testLongPointer)
{
  EXPECT_EQ(LongPointer(0x01, 0x0000).linear(), 0x010000);

  EXPECT_EQ((LongPointer(0x01, 0x0000) - 2).linear(), 0x00FFFE);
  EXPECT_EQ((LongPointer(0x01, 0x0000) - 2).segment, 0);
  EXPECT_EQ((LongPointer(0x01, 0x0000) - 2).offset, 0xFFFE);

  EXPECT_EQ((LongPointer(0x01, 0x0000) + 2).linear(), 0x010002);
  EXPECT_EQ((LongPointer(0x01, 0x0000) + 2).segment, 0x01);
  EXPECT_EQ((LongPointer(0x01, 0x0000) + 2).offset, 0x0002);

  EXPECT_EQ((LongPointer(0x03, 0x0000) - 0x12345).linear(), 0x01DCBB);
  EXPECT_EQ((LongPointer(0x03, 0x0000) - 0x12345).segment, 0x01);
  EXPECT_EQ((LongPointer(0x03, 0x0000) - 0x12345).offset, 0xDCBB);

  EXPECT_EQ((LongPointer(0x01, 0x0000) + 0x12345).linear(), 0x022345);
  EXPECT_EQ((LongPointer(0x01, 0x0000) + 0x12345).segment, 0x02);
  EXPECT_EQ((LongPointer(0x01, 0x0000) + 0x12345).offset, 0x2345);

  EXPECT_THROW(LongPointer(0x09, 0x0000), std::logic_error);
  EXPECT_THROW(LongPointer(0x01, 0x0000) - 0x20000, std::logic_error);
  EXPECT_THROW(LongPointer(0x07, 0xFFFF) + 1, std::logic_error);
}

/// Tests effective address type
TEST(TestTypes, testIndirectAddress)
{
  {
    const IndirectAddress ia(3, Register::R3, 0x0FF);

    EXPECT_EQ(ia.multiplier, 3);
    EXPECT_EQ(ia.base, Register::R3);
    EXPECT_EQ(ia.offset, 0x0FF);
    EXPECT_EQ(ia.backward, false);
  }

  {
    const IndirectAddress ia(2, Register::R7, -((1 << 9) - 1));

    EXPECT_EQ(ia.multiplier, 2);
    EXPECT_EQ(ia.base, Register::R7);
    EXPECT_EQ(ia.offset, 0x1FF);
    EXPECT_EQ(ia.backward, true);
  }

  {
    const IndirectAddress ia(Register::R5, 0x099);

    EXPECT_EQ(ia.multiplier, 0);
    EXPECT_EQ(ia.base, Register::R5);
    EXPECT_EQ(ia.offset, 0x099);
    EXPECT_EQ(ia.backward, false);
  }

  EXPECT_THROW(IndirectAddress(9, Register::R4, 0), std::logic_error);
  EXPECT_THROW(IndirectAddress(3, Register::R4, (1 << 9)), std::logic_error);
}

}
