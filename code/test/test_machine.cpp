/// Implements test cases related to the the Machine class, which ties all the components
/// of the system together and drives the machine main loop.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#include "platform/itf/machine.h"

#include <gtest/gtest.h>

using namespace core;
using namespace core::cpu;

namespace test {

/// Tests the machine main loop scheduler
TEST(TestMachine, testScheduler)
{
  using Scheduler = platform::Machine::Scheduler;
  using Event = Scheduler::Event;

  const uint64_t frame_ticks = 2000;

  const auto simulate_ticks = [](Scheduler &scheduler, const std::vector<uint64_t> &intervals) -> std::vector<Event>
  {
    std::vector<Event> events;

    for (const uint64_t interval : intervals)
    {
      uint64_t remaining = interval;

      while (remaining > 0)
      {
        const auto [ spent, interval_events ] = scheduler.tick(remaining);

        std::copy(interval_events.begin(), interval_events.end(), std::back_inserter(events));

        remaining -= spent;
      }
    }

    return events;
  };


  const std::vector<Instruction> CODE = {
    { Opcode::JM, Cpu::IP_0 },
  };

  Cpu cpu;

  utils::load(cpu.getRam(), Cpu::IP_0, CODE);

  // Simulate stepping exact single frame
  {
    Scheduler scheduler(&cpu, frame_ticks);

    const std::vector<Event> events = simulate_ticks(scheduler, { frame_ticks });
    const std::vector<Event> expected_events = { Event::FRAME_START, Event::VBLANK_START, Event::SCANOUT_START, Event::FRAME_END };

    EXPECT_EQ(events, expected_events);
  }

  // Simulate stepping in sub-frametime intervals
  {
    Scheduler scheduler(&cpu, frame_ticks);

    // First step 1.5 frame in 1/4 frame intervals
    const std::vector<uint64_t> intervals_0(6, frame_ticks / 4);

    const std::vector<Event> expected_events_0 = {
      Event::FRAME_START, Event::VBLANK_START, Event::SCANOUT_START, Event::FRAME_END,
      Event::FRAME_START, Event::VBLANK_START, Event::SCANOUT_START
    };

    const std::vector<Event> events_0 = simulate_ticks(scheduler, intervals_0);

    EXPECT_EQ(events_0, expected_events_0);

    // Now step another 0.5 frame in 1/8 intervals
    const std::vector<uint64_t> intervals_1(4, frame_ticks / 8);

    const std::vector<Event> expected_events_1 = { Event::FRAME_END };
    const std::vector<Event> events_1 = simulate_ticks(scheduler, intervals_1);

    EXPECT_EQ(events_1, expected_events_1);
  }

  // Simulate stepping in increments that are not a divisor of the total frame time, to simulate
  // driving the main loop at a framerate higher (e.g. 144 Hz) than the simulation framerate (60 Hz)
  {
    Scheduler scheduler(&cpu, frame_ticks);

    // This steps 5*(60/144) ~= 2.08 frames, which should end up right in the middle of the 3rd v-blank
    const std::vector<uint64_t> intervals(5, (frame_ticks * 60) / 144);

    const std::vector<Event> expected_events = {
      Event::FRAME_START, Event::VBLANK_START, Event::SCANOUT_START, Event::FRAME_END,
      Event::FRAME_START, Event::VBLANK_START, Event::SCANOUT_START, Event::FRAME_END,
      Event::FRAME_START, Event::VBLANK_START
    };

    const std::vector<Event> events = simulate_ticks(scheduler, intervals);

    EXPECT_EQ(events, expected_events);
  }
}

}

