/// Implements test cases related to the GPU device.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#include "core/gpu/itf/gpu.h"

#include "utils/itf/utils.h"

#include <gtest/gtest.h>

#include <cstdlib>
#include <random>
#include <algorithm>
#include <optional>

using namespace core;
using namespace core::gpu;

namespace test {

constexpr BYTE TILE_MAP_0_PAGE = 2;
constexpr WORD TILE_MAP_0_OFFSET = 0x4000;

constexpr BYTE COLOR_MAP_0_PAGE = 2;
constexpr WORD COLOR_MAP_0_OFFSET = 0x0000;

constexpr BYTE SPRITE_ATTRIBUTES_PAGE = 2;
constexpr WORD SPRITE_ATTRIBUTES_OFFSET = 0x8000;

constexpr BYTE SPRITE_TILES_PAGE = 2;
constexpr WORD SPRITE_TILES_OFFSET = 0xA000;

constexpr BYTE MULTICOLOR_TILES_PAGE = 2;
constexpr WORD MULTICOLOR_TILES_OFFSET = 0xC000;


// iCompare image against stored benchmark, saving the XOR-image in case of differences
static void benchmark_image(const std::string &testCase, const utils::ImageRgba &image)
{
  // Save input image for review/inspection
  utils::savePng(fmt::format("./test/result/{0}.res.png", testCase), image);

  // Load benchmark image
  const utils::ImageRgba bmk_image = utils::loadPng(fmt::format("./test/benchmark/{0}.bmk.png", testCase));
  const utils::ImageRgba diff_image = utils::imageDiff(bmk_image, image);

  const bool is_same = std::all_of(diff_image.data.begin(), diff_image.data.end(), [](const utils::ImageRgba::RGBA8888 &rgba)
  {
    return (rgba.rgba == 0);
  });

  if (!is_same)
  {
    utils::savePng(fmt::format("./test/result/{0}.xor.png", testCase), diff_image);

    FAIL() << fmt::format("result image for test case {0} does not match benchmark image", testCase);
  }
}

// Copy a few glyphs from the built-in ROM font to sprites, expanding them from monochrome to multicolor tiles
static void copy_multicolor_tiles(Gpu &gpu, BYTE * const spriteData, const std::optional<BYTE> bgIndex = std::nullopt)
{
  const auto font_data = gpu.getMemory()->getAddress<WORD>(Gpu::ROM_PAGE, Gpu::DEFAULT_FONT_8x8_OFFSET);

  std::mt19937 rnd;

  for (int i = 0; i < 10; i++)
  {
    // We use the 10 digit characters, which start at offset 48 in character ROM
    const int character_index = i + 48;

    // Use different colors for even/odd characters
    const BYTE bg = bgIndex.value_or(rnd() & 0xFF);
    const BYTE fg = (rnd() & 0x07) + 248;

    const WORD * const character_data = &font_data[character_index * 4];
    BYTE * const sprite_data = &spriteData[i*64];

    for (int row = 0; row < 8; row++)
    {
      const WORD row_word = character_data[row / 2];
      const BYTE row_byte = (row % 2 == 0 ? row_word & 0xFF : row_word >> 8);

      for (int pixel = 0; pixel < 8; pixel++)
      {
        sprite_data[row*8 + pixel] = (row_byte & (1 << (7 - pixel)) ? fg : bg);
      }
    }
  }
}

/// Tests basic sprite layer functions
TEST(TestGpu, testSpriteLayer)
{
  Gpu gpu(0, 320, 240, 0);

  copy_multicolor_tiles(gpu, gpu.getMemory()->getAddress<BYTE>(SPRITE_TILES_PAGE, SPRITE_TILES_OFFSET));

  // Configure layer 0 as sprite layer
  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_MODE), LayerMode::SPRITE);

  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_TILE_ATLAS_PAGE), SPRITE_TILES_PAGE);
  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_TILE_ATLAS_OFFSET), SPRITE_TILES_OFFSET);

  gpu.out(Gpu::SPRITE_ATTRIBUTES_PAGE, SPRITE_ATTRIBUTES_PAGE);
  gpu.out(Gpu::SPRITE_ATTRIBUTES_OFFSET, SPRITE_ATTRIBUTES_OFFSET);

  // Set sprite attributes for the sprites we want to display
  const auto sprite_attributes = gpu.getMemory()->getAddress<Sprite>(SPRITE_ATTRIBUTES_PAGE, SPRITE_ATTRIBUTES_OFFSET);

  // Basic 1x1 tile sprite
  sprite_attributes[0].x = 160;
  sprite_attributes[0].y = 120;
  sprite_attributes[0].tiles.index = 0;
  sprite_attributes[0].tiles.width = Sprite::SIZE_8_PIXELS;
  sprite_attributes[0].tiles.height = Sprite::SIZE_8_PIXELS;
  sprite_attributes[0].control.enable = true;

  // Multi-tile (4x4) sprite
  sprite_attributes[1].x = 10;
  sprite_attributes[1].y = 30;
  sprite_attributes[1].tiles.index = 0;
  sprite_attributes[1].tiles.width = Sprite::SIZE_16_PIXELS;
  sprite_attributes[1].tiles.height = Sprite::SIZE_16_PIXELS;
  sprite_attributes[1].control.enable = true;

  // Scaled & rotated & flipped 1x2 tile sprite
  sprite_attributes[2].x = 10;
  sprite_attributes[2].y = 60;
  sprite_attributes[2].tiles.index = 4;
  sprite_attributes[2].tiles.width = Sprite::SIZE_16_PIXELS;
  sprite_attributes[2].tiles.height = Sprite::SIZE_8_PIXELS;
  sprite_attributes[2].control.scale = Sprite::SCALE_4X;
  sprite_attributes[2].control.rotate = true;
  sprite_attributes[2].control.flipX = true;
  sprite_attributes[2].control.enable = true;

  // Scaled 2x2 tile sprite
  sprite_attributes[3].x = 10;
  sprite_attributes[3].y = 160;
  sprite_attributes[3].tiles.index = 5;
  sprite_attributes[3].tiles.width = Sprite::SIZE_16_PIXELS;
  sprite_attributes[3].tiles.height = Sprite::SIZE_16_PIXELS;
  sprite_attributes[3].control.scale = Sprite::SCALE_2X;
  sprite_attributes[3].control.enable = true;

  // Clipped sprites
  sprite_attributes[4].x = 304;
  sprite_attributes[4].y = 120;
  sprite_attributes[4].tiles.index = 9;
  sprite_attributes[4].tiles.width = Sprite::SIZE_8_PIXELS;
  sprite_attributes[4].tiles.height = Sprite::SIZE_8_PIXELS;
  sprite_attributes[4].control.scale = Sprite::SCALE_4X;
  sprite_attributes[4].control.enable = true;

  sprite_attributes[5].x = 160;
  sprite_attributes[5].y = 220;
  sprite_attributes[5].tiles.index = 9;
  sprite_attributes[5].tiles.width = Sprite::SIZE_8_PIXELS;
  sprite_attributes[5].tiles.height = Sprite::SIZE_8_PIXELS;
  sprite_attributes[5].control.scale = Sprite::SCALE_4X;
  sprite_attributes[5].control.enable = true;

  // Wrapped sprite
  sprite_attributes[6].x = -16;
  sprite_attributes[6].y = -16;
  sprite_attributes[6].tiles.index = 1;
  sprite_attributes[6].tiles.width = Sprite::SIZE_16_PIXELS;
  sprite_attributes[6].tiles.height = Sprite::SIZE_16_PIXELS;
  sprite_attributes[6].control.scale = Sprite::SCALE_2X;
  sprite_attributes[6].control.wrapX = true;
  sprite_attributes[6].control.wrapY = true;
  sprite_attributes[6].control.enable = true;

  // Render frame
  gpu.frame();

  const gpu::Frame frame = gpu.getFrame();

  benchmark_image("TestGpu.testSpriteLayer", frame.image);
}

/// Test monochrome tile layer using 8x8 tiles
TEST(TestGpu, testMonochromeTileLayer8x8)
{
  constexpr int NUM_TILES_X = 40;
  constexpr int NUM_TILES_Y = 30;

  Gpu gpu(0, 320, 240, 0);

  std::mt19937 rnd;

  // Configure layer 0 as tile layer. In its default state the GPU is configured to
  // use the built-in ROM font as tile atlas, we just use that for the test
  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_MODE), LayerMode::MONOCHROME_TILE);
  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_FOREGROUND_COLOR), 250);
  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_BACKGROUND_COLOR), 0);
  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_TILE_MODE), TileMode(Tile::SIZE_8_PIXELS).bits);
  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_NUM_TILES_X), NUM_TILES_X);
  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_NUM_TILES_Y), NUM_TILES_Y);

  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_TILE_MAP_PAGE), TILE_MAP_0_PAGE);
  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_TILE_MAP_OFFSET), TILE_MAP_0_OFFSET);

  // Fill tile map with some random tiles
  const auto tile_map = gpu.getMemory()->getAddress<Tile>(TILE_MAP_0_PAGE, TILE_MAP_0_OFFSET);

  for (int i = 0; i < NUM_TILES_Y; i++)
  {
    for (int j = 0; j < NUM_TILES_X; j++)
    {
      Tile &tile = tile_map[i*NUM_TILES_X + j];

      tile.tile = (rnd() % 10) + 48;
      tile.invert = tile.tile & 0b1;
    }
  }

  // Render frame
  gpu.frame();

  const gpu::Frame frame = gpu.getFrame();

  benchmark_image("TestGpu.testMonochromeTileLayer8x8", frame.image);
}

/// Test monochrome tile layer using 8x8 tiles
TEST(TestGpu, testColorMappedTileLayer8x8)
{
  constexpr int NUM_TILES_X = 40;
  constexpr int NUM_TILES_Y = 30;

  Gpu gpu(0, 320, 240, 0);

  std::mt19937 rnd;

  // Configure layer 0 as tile layer. In its default state the GPU is configured to
  // use the built-in ROM font as tile atlas, we just use that for the test
  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_MODE), LayerMode::COLOR_MAPPED_TILE);
  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_BACKGROUND_COLOR), 0);
  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_TILE_MODE), TileMode(Tile::SIZE_8_PIXELS).bits);
  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_NUM_TILES_X), NUM_TILES_X);
  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_NUM_TILES_Y), NUM_TILES_Y);

  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_TILE_MAP_PAGE), TILE_MAP_0_PAGE);
  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_TILE_MAP_OFFSET), TILE_MAP_0_OFFSET);

  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_COLOR_MAP_PAGE), COLOR_MAP_0_PAGE);
  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_COLOR_MAP_OFFSET), COLOR_MAP_0_OFFSET);

  // Fill tile map with some random tiles
  const auto tile_map = gpu.getMemory()->getAddress<Tile>(TILE_MAP_0_PAGE, TILE_MAP_0_OFFSET);

  for (int i = 0; i < NUM_TILES_Y; i++)
  {
    for (int j = 0; j < NUM_TILES_X; j++)
    {
      Tile &tile = tile_map[i*NUM_TILES_X + j];

      tile.tile = (rnd() % 10) + 48;
      tile.invert = tile.tile & 0b1;
    }
  }

  // Fill color map with some random colors
  const auto color_map = gpu.getMemory()->getAddress<WORD>(COLOR_MAP_0_PAGE, COLOR_MAP_0_OFFSET);

  for (int i = 0; i < NUM_TILES_Y; i++)
  {
    for (int j = 0; j < NUM_TILES_X; j++)
    {
      const BYTE r = rnd() & 0xFF;

      color_map[i*NUM_TILES_X + j] = ((r & 0xF0) << 4) | (r & 0x0F);
    }
  }

  // Render frame
  gpu.frame();

  const gpu::Frame frame = gpu.getFrame();

  benchmark_image("TestGpu.testColorMappedTileLayer8x8", frame.image);
}

/// Test indexed-color tile layer using 8x8 tiles
TEST(TestGpu, testIndexedTileLayer8x8)
{
  constexpr int NUM_TILES_X = 40;
  constexpr int NUM_TILES_Y = 30;

  std::mt19937 rnd;

  Gpu gpu(0, 320, 240, 0);

  // Copy the default ROM font (which are monochrome tiles) to an indexed-color tile atlas
  copy_multicolor_tiles(gpu, gpu.getMemory()->getAddress<BYTE>(MULTICOLOR_TILES_PAGE, MULTICOLOR_TILES_OFFSET));

  // Configure layer 0 as indexed-color tile layer.
  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_MODE), LayerMode::INDEXED_TILE);
  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_TILE_MODE), TileMode(Tile::SIZE_8_PIXELS).bits);
  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_NUM_TILES_X), NUM_TILES_X);
  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_NUM_TILES_Y), NUM_TILES_Y);

  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_TILE_ATLAS_PAGE), MULTICOLOR_TILES_PAGE);
  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_TILE_ATLAS_OFFSET), MULTICOLOR_TILES_OFFSET);

  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_TILE_MAP_PAGE), TILE_MAP_0_PAGE);
  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_TILE_MAP_OFFSET), TILE_MAP_0_OFFSET);

  // Fill tile map with some random tiles
  const auto tile_map = gpu.getMemory()->getAddress<Tile>(TILE_MAP_0_PAGE, TILE_MAP_0_OFFSET);

  for (int i = 0; i < NUM_TILES_Y; i++)
  {
    for (int j = 0; j < NUM_TILES_X; j++)
    {
      tile_map[i*NUM_TILES_X + j].tile = rnd() % 10;
    }
  }

  // Render frame
  gpu.frame();

  const gpu::Frame frame = gpu.getFrame();

  benchmark_image("TestGpu.testIndexedTileLayer8x8", frame.image);
}

/// Test scaled tiles
TEST(TestGpu, testScaledTiles)
{
  constexpr int NUM_TILES_X = 3;
  constexpr int NUM_TILES_Y = 3;

  Gpu gpu(0, 320, 240, 0);

  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_MODE), LayerMode::MONOCHROME_TILE);
  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_FOREGROUND_COLOR), 231);
  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_BACKGROUND_COLOR), 43);
  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_TILE_MODE), TileMode(Tile::SIZE_8_PIXELS, TileWrapMode::PERIODIC, TileWrapMode::PERIODIC, Tile::SCALE_4X).bits);
  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_NUM_TILES_X), NUM_TILES_X);
  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_NUM_TILES_Y), NUM_TILES_Y);

  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_TILE_MAP_PAGE), TILE_MAP_0_PAGE);
  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_TILE_MAP_OFFSET), TILE_MAP_0_OFFSET);

  const auto tile_map = gpu.getMemory()->getAddress<Tile>(TILE_MAP_0_PAGE, TILE_MAP_0_OFFSET);

  for (int n = 0; n < NUM_TILES_X * NUM_TILES_Y; n++)
  {
    Tile &tile = tile_map[n];

    tile.tile = n + '0';
    tile.invert = tile.tile & 0b1;
  }

  // Render frame
  gpu.frame();

  const gpu::Frame frame = gpu.getFrame();

  benchmark_image("TestGpu.testScaledTiles", frame.image);
}

/// Test tile layer wrap/repeat properties, as well as setting layer offsets
///
/// These two things (wrap/repeat and offsets) are tested together as the latter directly affects the
/// operation of the former, and testing them independently doesn't have any benefits
TEST(TestGpu, testWrapRepeatOffsetTileLayer)
{
  constexpr int SCREEN_WIDTH = 320;
  constexpr int SCREEN_HEIGHT = 240;

  constexpr int NUM_TILES_X = 3;
  constexpr int NUM_TILES_Y = 3;

  Gpu gpu(0, SCREEN_WIDTH, SCREEN_HEIGHT, 0);

  // Configure layer 0 as tile layer. In its default state the GPU is configured to
  // use the built-in ROM font as tile atlas, we just use that for the test
  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_MODE), LayerMode::MONOCHROME_TILE);
  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_CLEAR_MODE), ClearMode::CLEAR_BACKGROUND);
  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_BACKGROUND_COLOR), 0);
  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_NUM_TILES_X), NUM_TILES_X);
  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_NUM_TILES_Y), NUM_TILES_Y);

  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_TILE_MAP_PAGE), TILE_MAP_0_PAGE);
  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_TILE_MAP_OFFSET), TILE_MAP_0_OFFSET);

  // Apply layer offset
  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_OFFSET_X), -5);
  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_OFFSET_Y), -9);

  // The tile map is configured as 3x3, but before initializing it with the tiles we want
  // to render, fill a much larger tile map with a 'background character' that should not show
  // up in the rendering since it is out-of-bounds for the configured tile map size.
  const auto tile_map = gpu.getMemory()->getAddress<Tile>(TILE_MAP_0_PAGE, TILE_MAP_0_OFFSET);

  std::fill(tile_map, tile_map + NUM_TILES_X*NUM_TILES_Y, Tile(1, false));

  for (int i = 0; i < 9; i++)
  {
    tile_map[i].tile = i + 48;
    tile_map[i].invert = (i*3) & 0b1;
  }

  // The test renders the tile layer using four different layer offsets, covering all possible offset/clip directions
  const std::array<std::pair<int, int>, 4> LAYER_OFFSETS = {{
    { -6, -2 },
    {  SCREEN_WIDTH - 10, -10 },
    { -8,  SCREEN_HEIGHT - 10 },
    {  SCREEN_WIDTH - 2,  SCREEN_HEIGHT - 16 }
  }};

  // Case 1:
  // Cropped tile layer. Only pixels covered by the exact dimensions of the tile map should be rendered.
  {
    gpu.out(gpu.getLayerPort(0, Gpu::LAYER_BACKGROUND_COLOR), 3);
    gpu.out(gpu.getLayerPort(0, Gpu::LAYER_FOREGROUND_COLOR), 12);

    gpu.out(gpu.getLayerPort(0, Gpu::LAYER_TILE_MODE), TileMode(Tile::SIZE_8_PIXELS, TileWrapMode::NONE, TileWrapMode::NONE).bits);

    for (size_t i = 0; i < LAYER_OFFSETS.size(); i++)
    {
      gpu.out(gpu.getLayerPort(0, Gpu::LAYER_OFFSET_X), LAYER_OFFSETS[i].first);
      gpu.out(gpu.getLayerPort(0, Gpu::LAYER_OFFSET_Y), LAYER_OFFSETS[i].second);

      gpu.frame();

      const gpu::Frame frame = gpu.getFrame();

      const std::string test_name = fmt::format("TestGpu.testWrapRepeatTileLayer.cropped.{0}", i);

      benchmark_image(test_name, frame.image);
    }
  }

  // Case 2:
  // Wrapped/repeated tile layer. This should use periodic extension of the tile map to fill the whole screen
  {
    gpu.out(gpu.getLayerPort(0, Gpu::LAYER_BACKGROUND_COLOR), 1);
    gpu.out(gpu.getLayerPort(0, Gpu::LAYER_FOREGROUND_COLOR), 2);

    gpu.out(gpu.getLayerPort(0, Gpu::LAYER_TILE_MODE), TileMode(Tile::SIZE_8_PIXELS, TileWrapMode::PERIODIC, TileWrapMode::PERIODIC).bits);

    for (size_t i = 0; i < LAYER_OFFSETS.size(); i++)
    {
      gpu.out(gpu.getLayerPort(0, Gpu::LAYER_OFFSET_X), LAYER_OFFSETS[i].first);
      gpu.out(gpu.getLayerPort(0, Gpu::LAYER_OFFSET_Y), LAYER_OFFSETS[i].second);

      gpu.frame();

      const gpu::Frame frame = gpu.getFrame();

      const std::string test_name = fmt::format("TestGpu.testWrapRepeatTileLayer.periodic.{0}", i);

      benchmark_image(test_name, frame.image);
    }
  }
}

/// Test tile maps with rotated/flipped tiles.
///
/// This test case uses 'large tiles' composed of multiple 8x8 subtiles, as rotating
/// and flipping needs special treatment to correctly render them.
TEST(TestGpu, testRotatedFlippedTiles)
{
  constexpr int NUM_TILES_X = 8;
  constexpr int NUM_TILES_Y = 4;

  Gpu gpu(0, 320, 240, 0);

  // Copy the default ROM font (which are monochrome tiles) to an indexed-color tile atlas
  copy_multicolor_tiles(gpu, gpu.getMemory()->getAddress<BYTE>(MULTICOLOR_TILES_PAGE, MULTICOLOR_TILES_OFFSET));

  // Configure layer 0 as indexed-color tile layer.
  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_MODE), LayerMode::INDEXED_TILE);
  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_TILE_MODE), TileMode(Tile::SIZE_16_PIXELS, TileWrapMode::NONE, TileWrapMode::NONE, Tile::SCALE_2X).bits);
  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_NUM_TILES_X), NUM_TILES_X);
  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_NUM_TILES_Y), NUM_TILES_Y);

  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_TILE_ATLAS_PAGE), MULTICOLOR_TILES_PAGE);
  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_TILE_ATLAS_OFFSET), MULTICOLOR_TILES_OFFSET);

  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_TILE_MAP_PAGE), TILE_MAP_0_PAGE);
  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_TILE_MAP_OFFSET), TILE_MAP_0_OFFSET);

  // Fill tile map with a grid that contains all rotate/flip combinations. We use
  // a map that is twice as big as necessary to hold all combinations, skipping
  // odd rows and columns to make it easier to interpret the output image.
  const auto tile_map = gpu.getMemory()->getAddress<Tile>(TILE_MAP_0_PAGE, TILE_MAP_0_OFFSET);

  for (int i = 0; i < NUM_TILES_Y; i++)
  {
    for (int j = 0; j < NUM_TILES_X; j++)
    {
      const int n = i*8 + j;

      Tile * const tile = &tile_map[n];

      tile->tile = 0;
      tile->flipX = (j / 2) & 0b01;
      tile->flipY = (j / 2) & 0b10;
      tile->rotate = (i / 2) & 0b01;
      tile->skip = (i % 2) || (j % 2);
    }
  }

  // Render frame
  gpu.frame();

  const gpu::Frame frame = gpu.getFrame();

  benchmark_image("TestGpu.testRotatedFlippedTiles", frame.image);
}

// Test tiles that contain transparent colors. This should also cover rendering sprites
// with transparent colors, as it shares the same rendering code.
TEST(TestGpu, testTransparentTiles)
{
  Gpu gpu(0, 320, 240, 0);

  copy_multicolor_tiles(gpu, gpu.getMemory()->getAddress<BYTE>(SPRITE_TILES_PAGE, SPRITE_TILES_OFFSET), 0);

  // Configure layer 0 as sprite layer with light-grey background
  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_MODE), LayerMode::SPRITE);
  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_BACKGROUND_COLOR), 240);
  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_TILE_ATLAS_PAGE), SPRITE_TILES_PAGE);
  gpu.out(gpu.getLayerPort(0, Gpu::LAYER_TILE_ATLAS_OFFSET), SPRITE_TILES_OFFSET);
  gpu.out(Gpu::SPRITE_ATTRIBUTES_PAGE, SPRITE_ATTRIBUTES_PAGE);
  gpu.out(Gpu::SPRITE_ATTRIBUTES_OFFSET, SPRITE_ATTRIBUTES_OFFSET);

  // Set transparency bit on color 0 (the background color that will be used for all sprites)
  const auto palette = gpu.getMemory()->getAddress(Gpu::ROM_PAGE, Gpu::DEFAULT_PALETTE_ROM_OFFSET);

  palette[0] = (1 << 15);

  // Set sprite attributes for the sprites we want to display
  const auto sprite_attributes = gpu.getMemory()->getAddress<Sprite>(SPRITE_ATTRIBUTES_PAGE, SPRITE_ATTRIBUTES_OFFSET);

  // Initialization data for test sprites
  struct TestSprite
  {
    WORD x;
    WORD y;
    BYTE scale;
  };

  const std::vector<TestSprite> TEST_SPRITES = {
    { 10, 10, Sprite::SCALE_1X },
    { 28, 10, Sprite::SCALE_2X },
    { 54, 10, Sprite::SCALE_4X },

    { 316, 10, Sprite::SCALE_1X },
    { 312, 28, Sprite::SCALE_2X },
    { 304, 54, Sprite::SCALE_4X },
  };

  for (size_t i = 0; i < TEST_SPRITES.size(); i++)
  {
    sprite_attributes[i].x = TEST_SPRITES[i].x;
    sprite_attributes[i].y = TEST_SPRITES[i].y;
    sprite_attributes[i].tiles.index = 0;
    sprite_attributes[i].tiles.width = Sprite::SIZE_8_PIXELS;
    sprite_attributes[i].tiles.height = Sprite::SIZE_8_PIXELS;
    sprite_attributes[i].control.scale = TEST_SPRITES[i].scale;
    sprite_attributes[i].control.enable = true;
  }

  // Render frame
  gpu.frame();

  const gpu::Frame frame = gpu.getFrame();

  benchmark_image("TestGpu.testTransparentTiles", frame.image);
}

/// Test rendering multiple active layers
TEST(TestGpu, testMultipleLayers)
{
  constexpr WORD NUM_TILES_X = 2;
  constexpr WORD NUM_TILES_Y = 2;

  Gpu gpu(0, 320, 240, 0);

  // Setup 3 tile layers with increasing tile scale factor and tiles with transparent background colors
  for (int layer = 0; layer < 3; layer++)
  {
    gpu.out(gpu.getLayerPort(layer, Gpu::LAYER_MODE), LayerMode::MONOCHROME_TILE);
    gpu.out(gpu.getLayerPort(layer, Gpu::LAYER_CLEAR_MODE), ClearMode::CLEAR_BACKGROUND);
    gpu.out(gpu.getLayerPort(layer, Gpu::LAYER_BACKGROUND_COLOR), 0);
    gpu.out(gpu.getLayerPort(layer, Gpu::LAYER_NUM_TILES_X), NUM_TILES_X);
    gpu.out(gpu.getLayerPort(layer, Gpu::LAYER_NUM_TILES_Y), NUM_TILES_Y);
    gpu.out(gpu.getLayerPort(layer, Gpu::LAYER_TILE_MAP_PAGE), TILE_MAP_0_PAGE);
    gpu.out(gpu.getLayerPort(layer, Gpu::LAYER_BACKGROUND_COLOR), 0);
    gpu.out(gpu.getLayerPort(layer, Gpu::LAYER_FOREGROUND_COLOR), 2*layer + 1);

    gpu.out(gpu.getLayerPort(layer, Gpu::LAYER_TILE_MODE), TileMode(Tile::SIZE_8_PIXELS, TileWrapMode::PERIODIC, TileWrapMode::PERIODIC, layer).bits);

    // Set transparency bit on color 0 (the background color)
    const auto palette = gpu.getMemory()->getAddress(Gpu::ROM_PAGE, Gpu::DEFAULT_PALETTE_ROM_OFFSET);

    palette[0] = (1 << 15);

    // Setup 1x1 tile map containing the layer number
    const WORD tile_map_offset = 0x10*layer;

    gpu.out(gpu.getLayerPort(layer, Gpu::LAYER_TILE_MAP_OFFSET), tile_map_offset);

    const auto tile_map = gpu.getMemory()->getAddress<Tile>(TILE_MAP_0_PAGE, tile_map_offset);

    for (int tile_index = 0; tile_index < 4; tile_index++)
    {
      tile_map[tile_index].tile = 48 + layer;
      tile_map[tile_index].invert = (tile_index & 0b01) ^ ((tile_index / 2) & 0b01);
    }
  }

  gpu.frame();

  const gpu::Frame frame = gpu.getFrame();

  benchmark_image("TestGpu.testMultipleLayers", frame.image);
}



}