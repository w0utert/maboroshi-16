/// Implements test cases for functionality related to kumitate assemblies.
///
/// The test cases here cover loading assemblies into system memory, resolving
/// external symbols, etc. They do not cover creating the the assemblies themselves,
/// which is covered by the test cases implemented for the kumitate assembler.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#include "core/cpu/itf/cpu.h"
#include "core/cpu/itf/exceptions.h"

#include "utils/itf/utils.h"
#include "utils/itf/assembly.h"

#include "test/utils.h"

#include <gtest/gtest.h>

#include <fstream>

using namespace core;
using namespace core::cpu;

namespace test {

/// Test loading a single standalone assembly into system memory
TEST(TestAssemblies, testLoadStandalone)
{
  static constexpr char INPUT_FILE[] = "./test/input/fibonacci.mabo";
  static constexpr char MEMORY_DUMP_FILE[] = "./test/result/memory.bin";

  std::ifstream assembly_file(INPUT_FILE);

  ASSERT_TRUE(assembly_file.is_open());

  const auto assembly = utils::Assembly::fromInputStream(assembly_file);

  Cpu cpu;

  utils::load(cpu.getRam(), assembly);

  // Dump all memory and calculate SHASUM for benchmarking
  utils::memoryDump(MEMORY_DUMP_FILE, cpu.getRam());

  const std::string res_sha = test::sha256(MEMORY_DUMP_FILE);

  EXPECT_EQ(res_sha, "69ec2b9388b4ec7e2ff7c6599524779b68d65a332d3c72bf35291b4a01c51ed8");

  // For good measure, also execute the program loaded from the assembly and verify it works as expected
  WORD result = 0;

  try
  {
    cpu.tick(1000000);
  }
  catch (core::cpu::Halted &e)
  {
    result = cpu.getRegister(core::cpu::Register::R7);
  }

  ASSERT_EQ(4181, result);
}

// Tests disassembling a loaded assembly
TEST(TestAssemblies, testDisassemble)
{
  static constexpr char INPUT_FILE[] = "./test/input/all_instructions.mabo";
  static constexpr char RES_FILE[] = "./test/result/TestAssemblies.testDisassemble.all_instructions_disassembly";
  static constexpr char BMK_FILE[] = "./test/benchmark/TestAssemblies.testDisassemble.all_instructions_disassembly";

  // Load assembly file that contains all currently implemented instruction signatures
  std::ifstream assembly_file(INPUT_FILE);

  ASSERT_TRUE(assembly_file.is_open());

  const auto assembly = utils::Assembly::fromInputStream(assembly_file);

  // Disassemble and write disassembly to disk
  const utils::Assembly::CodeChunk &code_chunk = assembly.getCodeChunks().front();

  utils::Disassembly disassembly = utils::disassemble(code_chunk);

  std::ofstream res_file(RES_FILE);

  ASSERT_TRUE(res_file.is_open());

  for (const auto &[p, s] : disassembly)
  {
    res_file << fmt::format("{0} {1}", p, s) << std::endl;
  }

  res_file.close();

  // Benchmark disassembly
  test::benchmark_files(RES_FILE, BMK_FILE);
}

// Tests reading debug info from assembly
TEST(TestAssemblies, testDebugInfo)
{
  static constexpr char INPUT_FILE[] = "./test/input/namespaces.mabo";

  // Load assembly file that contains all currently implemented instruction signatures
  std::ifstream assembly_file(INPUT_FILE);

  ASSERT_TRUE(assembly_file.is_open());

  const auto assembly = utils::Assembly::fromInputStream(assembly_file);

  ASSERT_TRUE(assembly.getDebugInfo() != nullptr);

  EXPECT_EQ(assembly.getDebugInfo()->getFileList(), std::vector<std::string>({ "TestAssembler.testNamespaces.kasm" }));

  EXPECT_EQ(assembly.getDebugInfo()->getLabels().size(), 10);
  ASSERT_NO_THROW(assembly.getDebugInfo()->getLabelAddress("::namespace_1::nested_namespace::*1:35::abc"));
  EXPECT_EQ(assembly.getDebugInfo()->getLabelAddress("::namespace_1::nested_namespace::*1:35::abc"), core::LongPointer(0x01, 0x001c));

  EXPECT_EQ(assembly.getDebugInfo()->getLineInfo().size(), 29);
  ASSERT_NO_THROW(assembly.getDebugInfo()->getLineInfo(core::LongPointer(0x01, 0x001c)));
  EXPECT_EQ(assembly.getDebugInfo()->getLineInfo(core::LongPointer(0x01, 0x001c)).file, 0);
  EXPECT_EQ(assembly.getDebugInfo()->getLineInfo(core::LongPointer(0x01, 0x001c)).line, 37);
}

}

