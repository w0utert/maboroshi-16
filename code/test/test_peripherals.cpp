/// Implements test cases related to the peripheral device functions.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#include "core/cpu/itf/cpu.h"

#include "dummy_device.h"

#include <gtest/gtest.h>

using namespace core;
using namespace core::cpu;
using namespace core::peripheral;

namespace test {

/// Tests peripheral I/O functions
TEST(TestPeripherals, testIo)
{
  constexpr peripheral::Device::ID DUMMY_DEVICE_ID = 1;

  DummyDevice dummy_device(DUMMY_DEVICE_ID);

  core::cpu::Cpu cpu;
  cpu.connectDevice(&dummy_device);

  const WORD *device_io = dummy_device.getIo();

  ASSERT_EQ(0, dummy_device.in(DummyDevice::PORT_0));
  ASSERT_EQ(0, dummy_device.in(DummyDevice::PORT_1));

  ASSERT_EQ(0, device_io[DummyDevice::PORT_0]);
  ASSERT_EQ(0, device_io[DummyDevice::PORT_1]);

  ASSERT_NO_THROW(dummy_device.out(DummyDevice::PORT_0, 0x1234));

  ASSERT_EQ(0x1234, dummy_device.in(DummyDevice::PORT_0));
  ASSERT_EQ(0, dummy_device.in(DummyDevice::PORT_1));

  ASSERT_EQ(0x1234, device_io[DummyDevice::PORT_0]);
  ASSERT_EQ(0, device_io[DummyDevice::PORT_1]);

  ASSERT_NO_THROW(dummy_device.out(DummyDevice::PORT_1, 0x5678));

  ASSERT_EQ(0x1234, dummy_device.in(DummyDevice::PORT_0));
  ASSERT_EQ(0x5678, dummy_device.in(DummyDevice::PORT_1));

  ASSERT_EQ(0x1234, device_io[DummyDevice::PORT_0]);
  ASSERT_EQ(0x5678, device_io[DummyDevice::PORT_1]);
}

/// Test peripheral device bank switching.
TEST(TestPeripherals, testDeviceRam)
{
  constexpr peripheral::Device::ID DUMMY_DEVICE_ID = 1;
  constexpr BYTE DST_PAGE = 4;

  DummyDevice dummy_device(DUMMY_DEVICE_ID);

  core::cpu::Cpu cpu;
  cpu.connectDevice(&dummy_device);

  // Test mapping and writing/reading valid device pages. Note that this only tests writes/reads from the
  // device perspective (ie: device maps it RAM into CPU visible address space, writes to its own RAM, then
  // verifies that the CPU sees the written value at the expected mapped address.
  {
    for (BYTE page = 0; page < DummyDevice::NUM_DEVICE_RAM_PAGES; page++)
    {
      ASSERT_NO_THROW(cpu.switchBank(DUMMY_DEVICE_ID, page, DST_PAGE));

      WORD *mem = dummy_device.getMemory()->getPage(page);

      for (size_t n = 0; n < 100; n++)
      {
        const WORD offset = rand() & 0xFFFF;
        const WORD value = rand() & 0xFFFF;

        mem[offset] = value;

        ASSERT_EQ(cpu.getMemory(LongPointer(DST_PAGE, offset)), value);
      }
    }
  }

  // Test mapping invalid device page
  {
    ASSERT_THROW(cpu.switchBank(DUMMY_DEVICE_ID, DummyDevice::NUM_DEVICE_RAM_PAGES + 1, DST_PAGE), std::runtime_error);
  }
}

}
