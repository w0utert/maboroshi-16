/// Header file for test utility functions used by unit tests.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#pragma once

namespace test {

/// Return SHA256 hash of the input file, as hexadecimal string
///
/// \param filename file to get the SHA256 hash for
/// \return file SHA256 hash
std::string sha256(const std::string &filename);

/// Return whether the two passed files are identical
///
/// \param res result file name
/// \param benchmark benchmark file name
/// \return whether the two passed files are identical
bool benchmark_files(const std::string &res, const std::string &bmk);

}
