/// Implements test cases for all CPU functions
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#include "core/cpu/itf/cpu.h"
#include "core/cpu/itf/opcodes.h"
#include "core/cpu/itf/instruction.h"
#include "core/cpu/itf/exceptions.h"

#include "utils/itf/utils.h"

#include "dummy_device.h"

#include <gtest/gtest.h>

using namespace core;
using namespace core::cpu;

namespace test {

/// Tests CPU reset
TEST(TestCpu, testReset)
{
  Cpu cpu;

  EXPECT_EQ(cpu.getRegister(Register::R0), 0);
  EXPECT_EQ(cpu.getRegister(Register::R1), 0);
  EXPECT_EQ(cpu.getRegister(Register::R2), 0);
  EXPECT_EQ(cpu.getRegister(Register::R3), 0);
  EXPECT_EQ(cpu.getRegister(Register::R4), 0);
  EXPECT_EQ(cpu.getRegister(Register::R5), 0);
  EXPECT_EQ(cpu.getRegister(Register::R6), 0);
  EXPECT_EQ(cpu.getRegister(Register::R7), 0);

  EXPECT_EQ(cpu.getFlags(), 0);
  EXPECT_EQ(cpu.getFlag(Flag::OVERFLOW), 0);
  EXPECT_EQ(cpu.getFlag(Flag::NEGATIVE), 0);
  EXPECT_EQ(cpu.getFlag(Flag::CARRY), 0);
  EXPECT_EQ(cpu.getFlag(Flag::ZERO), 0);

  EXPECT_EQ(cpu.getInstructionPointer(), Cpu::IP_0);
  EXPECT_EQ(cpu.getStackPointer(), Cpu::STACK_OFFSET);

  EXPECT_EQ(cpu.getRam().size(), Cpu::RAM_SIZE);
}

/// Tests creating CPU instructions from opcodes + operands. Note: this test does not cover *all* allowed
/// instructions (opcode/address mode combinations) as there are way too many. Instead all valid variations
/// are tested with a decent sampling of opcodes, ensuring every opcode occurs at least once.
TEST(TestCpu, testCreateInstruction)
{
  EXPECT_EQ(sizeof(Instruction), 4);

  // Test creating instructions without arguments
  EXPECT_EQ(Instruction(Opcode::NOP).bits, 0x00000000);
  EXPECT_EQ(Instruction(Opcode::RET).bits, 0x02000000);
  EXPECT_EQ(Instruction(Opcode::PUSHS).bits, 0x0A000000);
  EXPECT_EQ(Instruction(Opcode::POPS).bits, 0x0B000000);

  // Test creating short instructions with CPU flags
  EXPECT_EQ(Instruction(Opcode::CLR, cpu::Flag::CARRY).bits, 0x04010000);

  // Test creating instructions with one register operand
  EXPECT_EQ(Instruction(Opcode::NEG, Register::R7).bits, 0x06070000);
  EXPECT_EQ(Instruction(Opcode::NOT, Register::R1).bits, 0x07010000);
  EXPECT_EQ(Instruction(Opcode::INC, Register::R4).bits, 0xB8040000);
  EXPECT_EQ(Instruction(Opcode::DEC, Register::R5).bits, 0xBC050000);

  // Test creating instructions with two register operands
  EXPECT_EQ(Instruction(Opcode::MOV, Register::R2, Register::R4).bits, 0x80220000);
  EXPECT_EQ(Instruction(Opcode::CMP, Register::R4, Register::R2).bits, 0x88140000);
  EXPECT_EQ(Instruction(Opcode::ADD, Register::R0, Register::R0).bits, 0x8C000000);
  EXPECT_EQ(Instruction(Opcode::ADC, Register::R7, Register::R1).bits, 0x900F0000);
  EXPECT_EQ(Instruction(Opcode::SUB, Register::R0, Register::R1).bits, 0x94080000);
  EXPECT_EQ(Instruction(Opcode::SBB, Register::R2, Register::R4).bits, 0x98220000);
  EXPECT_EQ(Instruction(Opcode::ASR, Register::R2, Register::R4).bits, 0xA0220000);
  EXPECT_EQ(Instruction(Opcode::AND, Register::R2, Register::R4).bits, 0xA4220000);
  EXPECT_EQ(Instruction(Opcode::OR, Register::R2, Register::R4).bits, 0xA8220000);
  EXPECT_EQ(Instruction(Opcode::XOR, Register::R2, Register::R4).bits, 0xAC220000);
  EXPECT_EQ(Instruction(Opcode::LSL, Register::R2, Register::R4).bits, 0xB0220000);
  EXPECT_EQ(Instruction(Opcode::LSR, Register::R2, Register::R4).bits, 0xB4220000);

  // Test creating instructions with relative address
  EXPECT_EQ(Instruction(Opcode::B, cpu::BranchCondition::Z, +128).bits, 0x10000080);
  EXPECT_EQ(Instruction(Opcode::B, cpu::BranchCondition::Z, -127).bits, 0x1000FF81);
  EXPECT_EQ(Instruction(Opcode::B, cpu::BranchCondition::LE, -32768).bits, 0x10098000);
  EXPECT_EQ(Instruction(Opcode::B, cpu::BranchCondition::NC, 32767).bits, 0x10057FFF);

  // Test creating instructions with only a long-pointer address
  EXPECT_EQ(Instruction(Opcode::CALLM, LongPointer(0x1, 0x1234)).bits, 0xF4081234);
  EXPECT_EQ(Instruction(Opcode::JM, LongPointer(0x5, 0x5566)).bits, 0xF8285566);
  EXPECT_EQ(Instruction(Opcode::DECM, LongPointer(0x1, 0x1234)).bits, 0xBF081234);
  EXPECT_EQ(Instruction(Opcode::INCM, LongPointer(0x5, 0x5566)).bits, 0xBB285566);

  // Test creating instructions with only an indirect address
  EXPECT_EQ(Instruction(Opcode::DECE, Register::R0, IndirectAddress(0x03, Register::R1, -256)).bits, 0xBE00C00B);
  EXPECT_EQ(Instruction(Opcode::INCE, Register::R2, IndirectAddress(0x04, Register::R4, 4)).bits, 0xBA020124);

  // Test creating instructions with destination register and immediate
  EXPECT_EQ(Instruction(Opcode::MOVI, Register::R1, 0x1234).bits, 0x81011234);
  EXPECT_EQ(Instruction(Opcode::XORI, Register::R7, 0x5566).bits, 0xAD075566);
  EXPECT_EQ(Instruction(Opcode::MOVLI, Register::R2, 0x0088).bits, 0xC9020088);

  // Test creating instructions with destination register and source long-pointer
  EXPECT_EQ(Instruction(Opcode::MOVM, Register::R3, LongPointer(0x1, 0x1234)).bits, 0x830B1234);
  EXPECT_EQ(Instruction(Opcode::MOVHM, Register::R1, LongPointer(0x4, 0x8800)).bits, 0xD3218800);

  // Test creating instructions with source register and destination long-pointer address
  EXPECT_EQ(Instruction(Opcode::MOVM, LongPointer(0x1, 0x1234), Register::R3).bits, 0x834B1234);

  // Test creating instructions with destination register and source effective address
  EXPECT_EQ(Instruction(Opcode::MOVE, Register::R5, IndirectAddress(0x00, Register::R7, -256)).bits, 0x8205C038);

  // Test creating instructions with destination effective address and source register
  EXPECT_EQ(Instruction(Opcode::MOVE, IndirectAddress(0x07, Register::R0, 0x100), Register::R5).bits, 0x82454007);

  // Test creating instructions with 4 register operands encoded in the instructions high-word
  EXPECT_EQ(Instruction(Opcode::MUL, Register::R0, Register::R1, Register::R2, Register::R3).bits, 0x21000688);
  EXPECT_EQ(Instruction(Opcode::DIV, Register::R4, Register::R5, Register::R6, Register::R7).bits, 0x20000FAC);

  // Test creating instructions with port-move operands
  EXPECT_EQ(Instruction(Opcode::INI, PortMove(0x02, 100, Register::R1)).bits, 0xE1002642);
  EXPECT_EQ(Instruction(Opcode::OUTR, PortMove(0x03, Register::R2, Register::R3)).bits, 0xE4006023);
}

/// Tests executing instructions. This does not test the actual operation of any instructions,
/// only that the CPU behaves according to specification when stepping through instructions.
TEST(TestCpu, testExecuteInstructions)
{
  Cpu cpu;

  utils::load(cpu.getRam(), Cpu::IP_0, { Opcode::NOP, Opcode::NOP, Opcode::NOP });

  EXPECT_EQ(cpu.getInstructionPointer(), Cpu::IP_0);

  cpu.tick();

  EXPECT_EQ(cpu.getInstructionPointer(), Cpu::IP_0 + 2);

  cpu.tick(2);

  EXPECT_EQ(cpu.getInstructionPointer(), Cpu::IP_0 + 6);
}

/// Test bank switching (mapping peripheral device RAM into CPU visible address space. This does
/// not test actually reading/writing to mapped device RAM pages, which is covered by the peripheral
/// and CPU instruction test cases.
TEST(TestCpu, testBankSwitching)
{
  constexpr BYTE DST_PAGE = 4;
  constexpr BYTE UNMAPPED_DST_PAGE = 5;
  constexpr BYTE DEVICE_PAGE = DummyDevice::NUM_DEVICE_RAM_PAGES - 1;
  constexpr peripheral::Device::ID DUMMY_DEVICE_ID = 1;
  constexpr peripheral::Device::ID INVALID_DEVICE_ID = DUMMY_DEVICE_ID + 1;
  constexpr BYTE INVALID_DEVICE_PAGE = DummyDevice::NUM_DEVICE_RAM_PAGES;
  constexpr BYTE INVALID_DST_PAGE = 0;

  DummyDevice dummy_device(DUMMY_DEVICE_ID);

  Cpu cpu;
  cpu.connectDevice(&dummy_device);

  // Base case: map valid device RAM page
  {
    ASSERT_NO_THROW(cpu.switchBank(DUMMY_DEVICE_ID, DEVICE_PAGE, DST_PAGE));

    // The memory address at the mapped page in CPU address space should match the physical
    // address of the device RAM page that was banked in.
    ASSERT_EQ(&cpu.getMemory(LongPointer(DST_PAGE, 0)), dummy_device.getMemory()->getPage(DEVICE_PAGE));
  }

  // Test invalid cases
  {
    ASSERT_THROW(cpu.switchBank(INVALID_DEVICE_ID, DEVICE_PAGE, DST_PAGE), std::runtime_error);
    ASSERT_THROW(cpu.switchBank(DUMMY_DEVICE_ID, INVALID_DEVICE_PAGE, DST_PAGE), std::runtime_error);
    ASSERT_THROW(cpu.switchBank(DUMMY_DEVICE_ID, DEVICE_PAGE, INVALID_DST_PAGE), std::runtime_error);
    ASSERT_THROW(cpu.getMemory({ UNMAPPED_DST_PAGE, 0 }), std::runtime_error);
  }
}

/// Test triggering and servicing interrupts.
TEST(TestCpu, testInterrupts)
{
  constexpr peripheral::Device::ID DUMMY_DEVICE_1 = 1;
  constexpr peripheral::Device::ID DUMMY_DEVICE_2 = 2;

  constexpr LongPointer INFINITE_LOOP = Cpu::IP_0 + 16;
  constexpr LongPointer IRQ_HANDLER_1 = Cpu::IP_0 + 18;
  constexpr LongPointer IRQ_HANDLER_2 = Cpu::IP_0 + 34;
  constexpr LongPointer TEST_VAR = Cpu::IP_0 + 0x100;
  constexpr BYTE FLAGS_BEFORE = Flag::CARRY | Flag::NEGATIVE;
  constexpr WORD TEST_VALUE = 0x1234;

  DummyDevice dummy_device_1(DUMMY_DEVICE_1);
  DummyDevice dummy_device_2(DUMMY_DEVICE_2);

  Cpu cpu;
  cpu.connectDevice(&dummy_device_1);
  cpu.connectDevice(&dummy_device_2);

  // The test program will set up interrupt handlers for the 2 created dummy devices, then
  // put the CPU in an infinite loop. The test case will then make the devices trigger an
  // interrupt request, which should result in both their interrupt handlers being called,
  // starting with the handler for the device with the lowest numbered IRQ line (dummy_device_1)
  const std::vector<Instruction> CODE = {
    { Opcode::MOVI, Register::R0, IRQ_HANDLER_1.segment },
    { Opcode::MOVI, Register::R1, IRQ_HANDLER_1.offset },
    { Opcode::MOVM, LongPointer(Cpu::IRQ_TABLE_OFFSET + 2), Register::R0 },
    { Opcode::MOVM, Cpu::IRQ_TABLE_OFFSET + 3, Register::R1 },
    { Opcode::MOVI, Register::R1, IRQ_HANDLER_2.offset },
    { Opcode::MOVM, Cpu::IRQ_TABLE_OFFSET + 4, Register::R0 },
    { Opcode::MOVM, Cpu::IRQ_TABLE_OFFSET + 5, Register::R1 },
    { Opcode::SET, FLAGS_BEFORE },

    // INFINITE_LOOP:
    { Opcode::JM, INFINITE_LOOP },

    // IRQ_HANDLER_1:
    { Opcode::CLR, Flag::ALL_ARITHMETIC },
    { Opcode::PUSH, Register::R0 },
    // Acknowledge interrupt
    { Opcode::MOVI, Register::R0, static_cast<WORD>(0) },
    { Opcode::OUTI, PortMove(DUMMY_DEVICE_1, DummyDevice::IRQ_CONTROL, Register::R0) },
    // Assign TEST_VAR
    { Opcode::MOVI, Register::R0, TEST_VALUE },
    { Opcode::MOVM, TEST_VAR, Register::R0 },
    // Restore R0 and return from interrupt
    { Opcode::POP, Register::R0 },
    { Opcode::RTI },

    // IRQ_HANDLER_2:
    { Opcode::CLR, Flag::ALL_ARITHMETIC },
    { Opcode::MOVI, Register::R0, static_cast<WORD>(0) },
    { Opcode::OUTI, PortMove(DUMMY_DEVICE_2, DummyDevice::IRQ_CONTROL, Register::R0) },
    { Opcode::RTI },
  };

  utils::load(cpu.getRam(), Cpu::IP_0, CODE);

  // Step a few cycles which should trap the CPU in the infinite loop for some iterations
  cpu.tick(100);

  ASSERT_EQ(cpu.getInstructionPointer(), INFINITE_LOOP);
  ASSERT_EQ(cpu.getFlags(), FLAGS_BEFORE);

  // Now let both dummy devices assert an irq and step one instruction. This should trigger the interrupt
  // service routine for the device with the lowest IRQ number (dummy_device_1). Note that the instruction
  // pointer will already have executed the first instruction of the handler after cpu.tick(1) call returns.
  dummy_device_1.assertIrq();
  dummy_device_2.assertIrq();

  cpu.tick(1);

  ASSERT_EQ(cpu.getInstructionPointer(), IRQ_HANDLER_1 + 2);
  ASSERT_EQ(cpu.getFlags(), Flag::DISABLE_IRQ);

  // Execute instructions up to and including the return-from-interrupt. This also checks the DISABLE_IRQ
  // flag (which is set automatically when an interrupt occurs) is honored, otherwise the interrupt would
  // re-trigger itself on every next instruction that is executed.
  cpu.tick(7);

  ASSERT_EQ(cpu.getInstructionPointer(), INFINITE_LOOP);
  ASSERT_EQ(cpu.getFlags(), FLAGS_BEFORE);
  ASSERT_EQ(cpu.getMemory(TEST_VAR), TEST_VALUE);
  ASSERT_FALSE(dummy_device_1.pendingInterrupts());
  ASSERT_TRUE(dummy_device_2.pendingInterrupts());

  // Now tick another instruction. Since dummy_device_2 also had a pending interrupt, the CPU should now
  // be one instruction passed the start of the second interrupt handler
  cpu.tick(1);

  ASSERT_EQ(cpu.getInstructionPointer(), IRQ_HANDLER_2 + 2);
  ASSERT_EQ(cpu.getFlags(), Flag::DISABLE_IRQ);

  // Execute instructions up to and including the return-from interrupt
  cpu.tick(3);

  ASSERT_EQ(cpu.getInstructionPointer(), INFINITE_LOOP);
  ASSERT_EQ(cpu.getFlags(), FLAGS_BEFORE);
  ASSERT_FALSE(dummy_device_2.pendingInterrupts());

  // Now no pending interrupts remain, and stepping one instruction should just do one iteration of the infinite loop
  cpu.tick(3);

  ASSERT_EQ(cpu.getInstructionPointer(), INFINITE_LOOP);
}

/// Test debugging facilities
TEST(TestCpu, testDebugger)
{
  constexpr LongPointer LOOP_START = Cpu::IP_0 + 2*Instruction::size();

  const std::vector<Instruction> CODE = {
    { Opcode::MOVI, Register::R0, static_cast<WORD>(1) },
    { Opcode::MOVI, Register::R1, static_cast<WORD>(1) },
    { Opcode::MOV, Register::R2, Register::R0 },
    { Opcode::MOV, Register::R0, Register::R1 },
    { Opcode::ADD, Register::R1, Register::R2 },
    { Opcode::JM, LOOP_START }
  };

  Cpu cpu;

  utils::load(cpu.getRam(), Cpu::IP_0, CODE);

  // Firs run one iteration of the program while the debugger is inactive
  ASSERT_NO_THROW(cpu.tick(6));

  ASSERT_EQ(cpu.getInstructionPointer(), LOOP_START);
  ASSERT_EQ(cpu.getRegister(Register::R0), 1);
  ASSERT_EQ(cpu.getRegister(Register::R1), 2);

  // Now enable single step mode. First tick should trigger a breakpoint without
  // executing anything, as 'single-step' breakpoint event for the current IP has
  // not fired yet.
  cpu.getDebugger().setMode(Debugger::Mode::SINGLE_STEP);

  ASSERT_THROW(cpu.tick(2), Break);

  // Each of the following tick() invocations should step exactly one instruction
  ASSERT_THROW(cpu.tick(3), Break);
  ASSERT_THROW(cpu.tick(4), Break);
  ASSERT_THROW(cpu.tick(5), Break);
  ASSERT_THROW(cpu.tick(6), Break);

  ASSERT_EQ(cpu.getInstructionPointer(), LOOP_START);
  ASSERT_EQ(cpu.getRegister(Register::R0), 2);
  ASSERT_EQ(cpu.getRegister(Register::R1), 3);

  // Now enable breakpoint mode and run for another full iteration while no breakpoints are set
  cpu.getDebugger().setMode(Debugger::Mode::RUN_TO_BREAKPOINT);

  ASSERT_NO_THROW(cpu.tick(4));

  ASSERT_EQ(cpu.getInstructionPointer(), LOOP_START);
  ASSERT_EQ(cpu.getRegister(Register::R0), 3);
  ASSERT_EQ(cpu.getRegister(Register::R1), 5);

  // Set breakpoint at the start of the loop and continue running, which should hit the breakpoint at the next iteration
  cpu.getDebugger().setBreakpoint(LOOP_START);

  ASSERT_THROW(cpu.tick(1000), Break);
  ASSERT_THROW(cpu.tick(1000), Break);

  ASSERT_EQ(cpu.getInstructionPointer(), LOOP_START);
  ASSERT_EQ(cpu.getRegister(Register::R0), 8);
  ASSERT_EQ(cpu.getRegister(Register::R1), 13);

  // Now remove breakpoint again and run for another two iterations, which should not break
  cpu.getDebugger().deleteBreakpoint(LOOP_START);

  ASSERT_NO_THROW(cpu.tick(8));

  ASSERT_EQ(cpu.getInstructionPointer(), LOOP_START);
  ASSERT_EQ(cpu.getRegister(Register::R0), 21);
  ASSERT_EQ(cpu.getRegister(Register::R1), 34);
}

}
