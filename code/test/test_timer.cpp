/// Implements test cases related to the hardware timer device.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#include "core/timer/itf/timer.h"

#include <gtest/gtest.h>

using namespace core;
using namespace core::timer;

namespace test {

/// Tests powerup state
TEST(TestTimer, testPowerupState)
{
  Timer timer(0, 0);

  EXPECT_EQ(timer.in(Timer::IRQ_CONTROL), 0);

  for (int i = 0; i < Timer::NUM_TIMERS; i++)
  {
    EXPECT_EQ(timer.in(Timer::TIMER_0_CONTROL + i*Timer::TIMER_NUM_PORTS) & Timer::TIMER_CONTROL_MODE_BITS, Timer::TIMER_CONTROL_MODE_DISABLED);
    EXPECT_EQ(timer.in(Timer::TIMER_0_INTERVAL + i*Timer::TIMER_NUM_PORTS), 0);
    EXPECT_EQ(timer.in(Timer::TIMER_0_COUNTER + i*Timer::TIMER_NUM_PORTS), 0);
  }
}

/// Tests basic timer device functions
TEST(TestTimer, testBasicTimers)
{
  constexpr WORD TIMER_DEVICE_ID = 0;
  constexpr WORD TIMER_DEVICE_IRQ = 2;

  Timer timer(TIMER_DEVICE_ID, TIMER_DEVICE_IRQ);

  // Setup one one-shot timer, one repeating timer, one configured but disabled timer
  timer.out(Timer::TIMER_0_CONTROL, Timer::TIMER_CONTROL_MODE_ONE_SHOT);
  timer.out(Timer::TIMER_0_INTERVAL, 10);
  timer.out(Timer::TIMER_1_CONTROL, Timer::TIMER_CONTROL_MODE_REPEAT);
  timer.out(Timer::TIMER_1_INTERVAL, 30);
  timer.out(Timer::TIMER_3_CONTROL, Timer::TIMER_CONTROL_MODE_DISABLED);
  timer.out(Timer::TIMER_3_INTERVAL, 2);

  timer.out(Timer::IRQ_CONTROL, 0x0F00);

  // Step a few frames but not enough to trigger any timer
  for (int i = 0; i < 9; i++)
  {
    timer.frame();
  }

  EXPECT_EQ(timer.pendingInterrupts(), 0);
  EXPECT_EQ(timer.in(Timer::TIMER_0_COUNTER), 9);
  EXPECT_EQ(timer.in(Timer::TIMER_1_COUNTER), 9);
  EXPECT_EQ(timer.in(Timer::TIMER_2_COUNTER), 0);
  EXPECT_EQ(timer.in(Timer::TIMER_3_COUNTER), 0);

  // Now step enough to trigger the one-shot timer, and verify that it was automatically disabled
  timer.frame();

  EXPECT_EQ(timer.pendingInterrupts(),  (1 << TIMER_DEVICE_IRQ));
  EXPECT_EQ(timer.in(Timer::IRQ_CONTROL) & Timer::IRQ_CONTROL_TIMERS_PENDING, (1 << 0));
  EXPECT_EQ(timer.in(Timer::TIMER_0_CONTROL) & Timer::TIMER_CONTROL_MODE_BITS, Timer::TIMER_CONTROL_MODE_DISABLED);
  EXPECT_EQ(timer.in(Timer::TIMER_0_COUNTER), 0);
  EXPECT_EQ(timer.in(Timer::TIMER_0_INTERVAL), 10);
  EXPECT_EQ(timer.in(Timer::TIMER_1_CONTROL) & Timer::TIMER_CONTROL_MODE_BITS, Timer::TIMER_CONTROL_MODE_REPEAT);

  timer.out(Timer::IRQ_CONTROL, timer.in(Timer::IRQ_CONTROL) & ~Timer::IRQ_CONTROL_TIMERS_PENDING);

  // Step enough to trigger the repeating timer, and verify that it is still active
  for (int i = 0; i < 21; i++)
  {
    timer.frame();
  }

  EXPECT_EQ(timer.pendingInterrupts(), (1 << TIMER_DEVICE_IRQ));
  EXPECT_EQ(timer.in(Timer::IRQ_CONTROL) & Timer::IRQ_CONTROL_TIMERS_PENDING, (1 << 1));
  EXPECT_EQ(timer.in(Timer::TIMER_1_CONTROL) & Timer::TIMER_CONTROL_MODE_BITS, Timer::TIMER_CONTROL_MODE_REPEAT);
  EXPECT_EQ(timer.in(Timer::TIMER_0_COUNTER), 0);
  EXPECT_EQ(timer.in(Timer::TIMER_1_COUNTER), 1);

  timer.out(Timer::IRQ_CONTROL, timer.in(Timer::IRQ_CONTROL) & ~Timer::IRQ_CONTROL_TIMERS_PENDING);

  // Step enough frames to trigger the repeating timer again
  for (int i = 0; i < 29; i++)
  {
    timer.frame();
  }

  EXPECT_EQ(timer.pendingInterrupts(), (1 << TIMER_DEVICE_IRQ));
  EXPECT_EQ(timer.in(Timer::IRQ_CONTROL) & Timer::IRQ_CONTROL_TIMERS_PENDING, (1 << 1));
  EXPECT_EQ(timer.in(Timer::TIMER_1_CONTROL) & Timer::TIMER_CONTROL_MODE_BITS, Timer::TIMER_CONTROL_MODE_REPEAT);
  EXPECT_EQ(timer.in(Timer::TIMER_1_COUNTER), 0);

  timer.out(Timer::IRQ_CONTROL, timer.in(Timer::IRQ_CONTROL) & ~Timer::IRQ_CONTROL_TIMERS_PENDING);

  // Keep the timer enabled but mask its interrupt. This should still update the timer counter but no interrupt should be generated
  timer.out(Timer::IRQ_CONTROL, timer.in(Timer::IRQ_CONTROL) & ~(0x0100 << 1));

  for (int i = 0; i < 31; i++)
  {
    timer.frame();
  }

  EXPECT_EQ(timer.in(Timer::TIMER_1_COUNTER), 1);
  EXPECT_EQ(timer.pendingInterrupts(), 0);
  EXPECT_EQ(timer.in(Timer::IRQ_CONTROL) & Timer::IRQ_CONTROL_TIMERS_PENDING, 1 << 1);

  // Unmasking the interrupt before clearing the 'timer pending' bit for the previously masked interrupt should still fire the interrupt (in
  // other words: if a program does not want to get these delayed interrupts, it should always write 0's to the 'timer pending' bits
  // of the IRQ_CONTROL register along with the bits to unmask the interrupts).
  timer.out(Timer::IRQ_CONTROL, timer.in(Timer::IRQ_CONTROL) | (0x0100 << 1));

  EXPECT_EQ(timer.pendingInterrupts(), (1 << TIMER_DEVICE_IRQ));
  EXPECT_EQ(timer.in(Timer::IRQ_CONTROL) & Timer::IRQ_CONTROL_TIMERS_PENDING, 1 << 1);

  timer.out(Timer::IRQ_CONTROL, timer.in(Timer::IRQ_CONTROL) & ~Timer::IRQ_CONTROL_TIMERS_PENDING);

  // Now disable the repeating timer as well and verify that no more interrupts are fired and the timer frame counters are not being updated
  timer.out(Timer::TIMER_1_CONTROL, Timer::TIMER_CONTROL_MODE_DISABLED);

  for (int i = 0; i < 30; i++)
  {
    timer.frame();
  }

  EXPECT_EQ(timer.pendingInterrupts(), 0);
  EXPECT_EQ(timer.in(Timer::IRQ_CONTROL) & Timer::IRQ_CONTROL_TIMERS_PENDING, 0);
  EXPECT_EQ(timer.in(Timer::TIMER_0_COUNTER), 0);
  EXPECT_EQ(timer.in(Timer::TIMER_1_COUNTER), 1);
  EXPECT_EQ(timer.in(Timer::TIMER_2_COUNTER), 0);
  EXPECT_EQ(timer.in(Timer::TIMER_3_COUNTER), 0);
}

}
