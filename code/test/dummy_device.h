/// Defines a dummy device class that is used by multiple test cases (peripheral I/O
/// and CPU IN and OUT instructions.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#pragma once

#include "core/peripheral/itf/device.h"

/// \cond

namespace test {

// Dummy device class used for testing
class DummyDevice : public core::peripheral::Device
{
  public:
    static constexpr Port PORT_0 = 0x00;
    static constexpr Port PORT_1 = 0x01;
    static constexpr Port IRQ_CONTROL = 0x02;

    static constexpr Port NUM_DEVICE_RAM_PAGES = 2;

    inline static const std::map<Port, PortDescription> PORTS = {
      { PORT_0, { "port_0", "port 0 of the dummy device" } },
      { PORT_1, { "port_1", "port 1 of the dummy device" } },
      { IRQ_CONTROL, { "irq_ctrl", "interrupt control register for the dummy device" } },
    };

    DummyDevice(const ID id)
    :
      Device(id, PORTS, (1 << id), NUM_DEVICE_RAM_PAGES)
    {
    }

    void assertIrq() { out(IRQ_CONTROL, 1); }

    const core::WORD *getIo() const { return _io.data(); }

    void reset() override {};
    core::WORD pendingInterrupts() const override { return (in(IRQ_CONTROL) != 0 ? _irqMask : 0); }
};

}

/// \endcond