/// Implements test utility functions used by unit tests.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#include "third_party/PicoSHA2/picosha2.h"
#include "third_party/fmt/format.h"

#include <gtest/gtest.h>

#include <fstream>

namespace test {

// Return SHA256 of passed file
std::string sha256(const std::string &filename)
{
  std::ifstream in(filename, std::ios_base::binary | std::ios_base::ate);

  if (!in.is_open())
  {
    throw std::runtime_error(fmt::format("Failed to calculate SHA256 of file '{0}': cannot open file", filename));
  }

  const size_t file_size = in.tellg();

  std::vector<char> bytes(file_size);

  in.seekg(0);
  in.read(bytes.data(), file_size);

  return picosha2::hash256_hex_string(bytes);
}

// Compare files for equality
bool benchmark_files(const std::string &res, const std::string &bmk)
{
  return sha256(res) == sha256(bmk);
}

}
