/// Implements test cases for each of the maboroshi-16 CPU opcodes.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#include "core/common/itf/types.h"

#include "core/cpu/itf/cpu.h"
#include "core/cpu/itf/instruction.h"
#include "core/cpu/itf/exceptions.h"

#include "utils/itf/utils.h"

#include "dummy_device.h"

#include <boost/lexical_cast.hpp>

#include <gtest/gtest.h>

#include <stdexcept>
#include <utility>
#include <algorithm>
#include <unordered_map>

using namespace core;
using namespace core::cpu;

namespace test {

/// Test ADD opcode
TEST(TestInstructions, testAdd)
{
  using AddTest = std::tuple<WORD, WORD, WORD, BYTE>;

  const std::vector<AddTest> ADD_TESTS = {
    { 0x7FFF, 0x0000, 0x7FFF, 0 },
    { 0xFFFF, 0x7FFF, 0x7FFE, Flag::CARRY },
    { 0x0000, 0x0000, 0x0000, Flag::ZERO },
    { 0xFFFF, 0x0001, 0x0000, Flag::ZERO|Flag::CARRY },
    { 0xFFFF, 0x0000, 0xFFFF, Flag::NEGATIVE },
    { 0xFFFF, 0xFFFF, 0xFFFE, Flag::NEGATIVE|Flag::CARRY },
    { 0xFFFF, 0x8000, 0x7FFF, Flag::OVERFLOW|Flag::CARRY },
    { 0x8000, 0x8000, 0x0000, Flag::OVERFLOW|Flag::ZERO|Flag::CARRY },
    { 0x7FFF, 0x7FFF, 0xFFFE, Flag::OVERFLOW|Flag::NEGATIVE },
  };

  for (const AddTest &test : ADD_TESTS)
  {
    const WORD a = std::get<0>(test);
    const WORD b = std::get<1>(test);
    const WORD expected_result = std::get<2>(test);
    const BYTE expected_flags = std::get<3>(test);

    const std::vector<Instruction> CODE = {
      { Opcode::MOVI, Register::R0, a },
      { Opcode::MOVI, Register::R1, b },
      { Opcode::ADD, Register::R0, Register::R1 }
    };

    Cpu cpu;

    utils::load(cpu.getRam(), Cpu::IP_0, CODE);

    cpu.tick(3);

    const WORD result = cpu.getRegister(Register::R0);

    EXPECT_EQ(result, expected_result) << fmt::format(
      "ADD operation result test failed, lhs: {0:#06x}, rhs: {1:#06x}, expected result: {2:#04x}, got result: {3:#04x}",
      a, b, expected_result, result);;

    EXPECT_EQ(cpu.getFlags(), expected_flags) << fmt::format(
      "ADD operation CPU flags test failed, lhs: {0:#06x}, rhs: {1:#06x}, expected flags: {2:#04x}, got flags: {3:#04x}",
      a, b, expected_flags, cpu.getFlags());
  }
}

/// Test ADC opcode
TEST(TestInstructions, testAdc)
{
  // Test results and flags for all combinations of operand signs & carry flag
  {
    using AdcTest = std::tuple<WORD, WORD, bool, WORD, BYTE>;

    const std::vector<AdcTest> ADC_TESTS = {
      { 0x7FFF, 0x0000, false, 0x7FFF, 0 },
      { 0xFFFF, 0x7FFF, false, 0x7FFE, Flag::CARRY },
      { 0x0000, 0x0000, false, 0x0000, Flag::ZERO },
      { 0xFFFF, 0x0001, false, 0x0000, Flag::ZERO | Flag::CARRY },
      { 0xFFFF, 0x0000, false, 0xFFFF, Flag::NEGATIVE },
      { 0xFFFF, 0xFFFF, false, 0xFFFE, Flag::NEGATIVE | Flag::CARRY },
      { 0xFFFF, 0x8000, false, 0x7FFF, Flag::OVERFLOW | Flag::CARRY },
      { 0x8000, 0x8000, false, 0x0000, Flag::OVERFLOW | Flag::ZERO | Flag::CARRY },
      { 0x7FFF, 0x7FFF, false, 0xFFFE, Flag::OVERFLOW | Flag::NEGATIVE },
      { 0x0000, 0x0000, true,  0x0001, 0 },
      { 0xFFFF, 0x0000, true,  0x0000, Flag::ZERO | Flag::CARRY },
      { 0xFFFF, 0x7FFF, true,  0x7FFF, Flag::CARRY },
      { 0x7FFF, 0x7FFF, true,  0xFFFF, Flag::OVERFLOW | Flag::NEGATIVE },
    };

    for (const AdcTest &test : ADC_TESTS)
    {
      const WORD a = std::get<0>(test);
      const WORD b = std::get<1>(test);
      const BYTE carry_flag = (std::get<2>(test) ? Flag::CARRY : 0);
      const WORD expected_result = std::get<3>(test);
      const BYTE expected_flags = std::get<4>(test);

      const std::vector<Instruction> CODE = {
        { Opcode::MOVI, Register::R0, a },
        { Opcode::MOVI, Register::R1, b },
        { Opcode::SET,  carry_flag },
        { Opcode::ADC,  Register::R0, Register::R1 }
      };

      Cpu cpu;

      utils::load(cpu.getRam(), Cpu::IP_0, CODE);

      cpu.tick(4);

      const WORD result = cpu.getRegister(Register::R0);

      EXPECT_EQ(result, expected_result) << fmt::format(
        "ADC operation result test failed, lhs: {0:#06x}, rhs: {1:#06x}, carry: {2}, expected result: {3:#04x}, got result: {4:#04x}",
        a, b, carry_flag, expected_result, result);;

      EXPECT_EQ(cpu.getFlags(), expected_flags) << fmt::format(
        "ADC operation CPU flags test failed, lhs: {0:#06x}, rhs: {1:#06x}, carry: {2}, expected flags: {3:#04x}, got flags: {4:#04x}",
        a, b, carry_flag, expected_flags, cpu.getFlags());
    }
  }

  // Test multi-word addition, implementing 32-bit subtraction using ADD followed by ADC
  {
    // The addiution calculated here is 0x1234ABCD + 0x00005678 = 0x12350245, left-hand side & result stored in R1:R0
    const std::vector<Instruction> CODE = {
      { Opcode::MOVI, Register::R0, 0xABCD },
      { Opcode::MOVI, Register::R1, 0x1234 },
      { Opcode::ADDI, Register::R0, 0x5678 },
      { Opcode::ADCI, Register::R1, 0x0000 }
    };

    Cpu cpu;

    utils::load(cpu.getRam(), Cpu::IP_0, CODE);

    cpu.tick(4);

    ASSERT_EQ(0x1235, cpu.getRegister(Register::R1));
    ASSERT_EQ(0x0245, cpu.getRegister(Register::R0));
  }
}

/// Test AND instruction
TEST(TestInstructions, testAnd)
{
  const std::vector<Instruction> CODE = {
    { Opcode::SET, static_cast<BYTE>(0x0F) },
    { Opcode::MOVI, Register::R0, 0xFFFF },
    { Opcode::ANDI, Register::R0, 0x1234 },
    { Opcode::SET, static_cast<BYTE>(0x0F) },
    { Opcode::MOVI, Register::R1, 0xFFFF },
    { Opcode::ANDI, Register::R1, 0x8000 },
    { Opcode::SET, static_cast<BYTE>(0x0F) },
    { Opcode::MOVI, Register::R2, 0x0000 },
    { Opcode::ANDI, Register::R2, 0xFFFF },
  };

  Cpu cpu;

  utils::load(cpu.getRam(), Cpu::IP_0, CODE);

  cpu.tick(3);

  ASSERT_EQ(cpu.getRegister(Register::R0), 0x1234);
  ASSERT_EQ(cpu.getFlags(), 0);

  cpu.tick(3);

  ASSERT_EQ(cpu.getRegister(Register::R1), 0x8000);
  ASSERT_EQ(cpu.getFlags(), Flag::NEGATIVE);

  cpu.tick(3);

  ASSERT_EQ(cpu.getRegister(Register::R2), 0x0000);
  ASSERT_EQ(cpu.getFlags(), Flag::ZERO);
}


/// Tests ASR instruction.
TEST(TestInstructions, testAsr)
{
  const std::vector<Instruction> CODE = {
    { Opcode::MOVI, Register::R0, static_cast<WORD>(-10) },
    { Opcode::SET, static_cast<BYTE>(Flag::ALL_ARITHMETIC) },
    { Opcode::ASRI, Register::R0, 2 },
    { Opcode::MOVI, Register::R1, 10 },
    { Opcode::SET, static_cast<BYTE>(Flag::ALL_ARITHMETIC) },
    { Opcode::ASRI, Register::R1, 2 },
    { Opcode::MOVI, Register::R2, static_cast<WORD>(-1) },
    { Opcode::SET, static_cast<BYTE>(Flag::ALL_ARITHMETIC) },
    { Opcode::ASRI, Register::R2, 20 },
    { Opcode::MOVI, Register::R3, 1234 },
    { Opcode::SET, static_cast<BYTE>(Flag::ALL_ARITHMETIC) },
    { Opcode::ASRI, Register::R3, 20 },
    // Test shifting by a distance of 0 does not affect CPU flags
    { Opcode::MOVI, Register::R4, 1234 },
    { Opcode::SET, static_cast<BYTE>(Flag::ALL_ARITHMETIC) },
    { Opcode::ASRI, Register::R4, 0 },
  };

  Cpu cpu;

  utils::load(cpu.getRam(), Cpu::IP_0, CODE);

  cpu.tick(3);

  ASSERT_EQ(cpu.getRegister(Register::R0), static_cast<WORD>(-3));
  ASSERT_EQ(cpu.getFlags(), Flag::NEGATIVE);

  cpu.tick(3);

  ASSERT_EQ(cpu.getRegister(Register::R1), 2);
  ASSERT_EQ(cpu.getFlags(), 0);

  cpu.tick(3);

  ASSERT_EQ(cpu.getRegister(Register::R2), static_cast<WORD>(-1));
  ASSERT_EQ(cpu.getFlags(), Flag::NEGATIVE);

  cpu.tick(3);

  ASSERT_EQ(cpu.getRegister(Register::R3), 0);
  ASSERT_EQ(cpu.getFlags(), Flag::ZERO);

  cpu.tick(3);

  ASSERT_EQ(cpu.getRegister(Register::R4), 1234);
  ASSERT_EQ(cpu.getFlags(), Flag::ALL_ARITHMETIC);
}

/// Test B (branch) opcodes
TEST(TestInstructions, testB)
{
  {
    using BranchConditionTest = std::pair<BYTE, BYTE>;

    const std::unordered_map<BranchCondition, std::vector<BranchConditionTest>> BRANCH_CONDITION_TESTS = {
      { BranchCondition::Z, { { Flag::ZERO, 0 } } },
      { BranchCondition::NZ, { { 0, Flag::ZERO } } },
      { BranchCondition::O, { { Flag::OVERFLOW, 0 } } },
      { BranchCondition::NO, { { 0, Flag::OVERFLOW} } },
      { BranchCondition::C, { { Flag::CARRY, 0 } } },
      { BranchCondition::NC, { { 0, Flag::CARRY} } },
      { BranchCondition::G, { { Flag::OVERFLOW | Flag::NEGATIVE, Flag::ZERO }, { 0, Flag::OVERFLOW | Flag::NEGATIVE | Flag::ZERO } } },
      { BranchCondition::GE, { { Flag::OVERFLOW | Flag::NEGATIVE, 0 }, { 0, Flag::OVERFLOW | Flag::NEGATIVE } } },
      { BranchCondition::L, { { Flag::NEGATIVE, Flag::OVERFLOW }, { Flag::OVERFLOW, Flag::NEGATIVE } } },
      { BranchCondition::LE, { { Flag::NEGATIVE, Flag::OVERFLOW }, { Flag::OVERFLOW, Flag::NEGATIVE }, { Flag::ZERO, 0 } } },
      { BranchCondition::UG, { { 0, Flag::CARRY | Flag::ZERO } } },
      { BranchCondition::UGE, { { 0, Flag::CARRY } } },
      { BranchCondition::UL, { { Flag::CARRY, 0 } } },
      { BranchCondition::ULE, { { Flag::CARRY, 0 }, { Flag::ZERO, 0 } } },
    };

    for (const auto &branch_condition_test : BRANCH_CONDITION_TESTS)
    {
      const BranchCondition branch_condition = branch_condition_test.first;
      const std::vector<std::pair<BYTE, BYTE> > test = branch_condition_test.second;

      for (BYTE flags = 0; flags < 16; flags++)
      {
        const bool should_branch = std::any_of(test.begin(), test.end(), [flags](const BranchConditionTest &test)
        {
          return ((flags & test.first) == test.first) && ((flags & test.second) == 0);
        });

        const std::vector<Instruction> test_program =
        {
          { Opcode::SET, flags },
          { Opcode::B, branch_condition, -2 }
        };

        Cpu cpu;

        utils::load(cpu.getRam(), Cpu::IP_0, test_program);

        cpu.tick(2);

        const bool did_branch = (cpu.getInstructionPointer() == Cpu::IP_0);

        EXPECT_EQ(did_branch, should_branch) << fmt::format(
          "Branch condition test failed, condition: {0}, flags: {1:#04x}, should branch: {2}, did branch: {3}",
          boost::lexical_cast<std::string>(branch_condition), flags, should_branch, did_branch);

        EXPECT_EQ(cpu.getFlags(), flags);
      }
    }
  }
}

/// Test BANK opcode
TEST(TestInstructions, testBank)
{
  constexpr BYTE DEVICE_PAGE_0 = 0;
  constexpr BYTE DEVICE_PAGE_1 = 1;
  constexpr peripheral::Device::ID DUMMY_DEVICE_ID = 1;
  constexpr BYTE INVALID_DEVICE_PAGE = DummyDevice::NUM_DEVICE_RAM_PAGES;
  constexpr BYTE DST_PAGE = 5;
  constexpr BYTE INVALID_DST_PAGE = (1 << Cpu::ADDRESS_PAGE_BITS);
  constexpr LongPointer MAPPED_PAGE_ADDRESS = { DST_PAGE, 0};

  const std::vector<Instruction> CODE = {
    // Map device page 0
    Instruction(Opcode::MOVI, Register::R0, static_cast<WORD>((DUMMY_DEVICE_ID << 8) | (DEVICE_PAGE_0 << 4) | DST_PAGE)),
    Instruction(Opcode::BANK, Register::R0),
    // Map device page 1
    Instruction(Opcode::MOVI, Register::R1, static_cast<WORD>((DUMMY_DEVICE_ID << 8) | (DEVICE_PAGE_1 << 4) | DST_PAGE)),
    Instruction(Opcode::BANK, Register::R1),
    // Try to map invalid destination page
    Instruction(Opcode::MOVI, Register::R0, static_cast<WORD>((DUMMY_DEVICE_ID << 8) | (DEVICE_PAGE_0 << 4) | INVALID_DST_PAGE)),
    Instruction(Opcode::BANK, Register::R0),
    // Try to map invalid device page
    Instruction(Opcode::MOVI, Register::R0, static_cast<WORD>((DUMMY_DEVICE_ID << 8) | (INVALID_DEVICE_PAGE << 4) | DST_PAGE)),
    Instruction(Opcode::BANK, Register::R0),
    // Try to map invalid device
    Instruction(Opcode::MOVI, Register::R0, static_cast<WORD>((INVALID_DEVICE_PAGE << 8) | (DEVICE_PAGE_0 << 4) | DST_PAGE)),
    Instruction(Opcode::BANK, Register::R0),
  };

  DummyDevice dummy_device(DUMMY_DEVICE_ID);

  Cpu cpu;
  cpu.connectDevice(&dummy_device);

  utils::load(cpu.getRam(), Cpu::IP_0, CODE);

  ASSERT_THROW(cpu.getMemory(MAPPED_PAGE_ADDRESS), std::runtime_error);

  cpu.tick(2);

  ASSERT_EQ(&cpu.getMemory(MAPPED_PAGE_ADDRESS), dummy_device.getMemory()->getPage(DEVICE_PAGE_0));

  cpu.tick(2);

  ASSERT_EQ(&cpu.getMemory(MAPPED_PAGE_ADDRESS), dummy_device.getMemory()->getPage(DEVICE_PAGE_1));

  // Test invalid cases
  ASSERT_THROW(cpu.tick(2), std::runtime_error);
  ASSERT_THROW(cpu.tick(2), std::runtime_error);
  ASSERT_THROW(cpu.tick(2), std::runtime_error);
}

/// Test CALL opcode
TEST(TestInstructions, testCall)
{
  // Test calling direct memory address
  {
    constexpr LongPointer RETURN_ADDRESS = Cpu::IP_0 + 4;
    constexpr LongPointer SUBROUTINE_ADDRESS = Cpu::IP_0 + 10;

    const std::vector<Instruction> CODE = {
      { Opcode::NOP },
      { Opcode::CALLM, SUBROUTINE_ADDRESS },
      // RETURN_ADDRESS:
      { Opcode::NOP },
      { Opcode::NOP },
      { Opcode::NOP },
      // SUBROUTINE_ADDRESS:
      { Opcode::NOP }
    };

    Cpu cpu;

    utils::load(cpu.getRam(), Cpu::IP_0, CODE);

    cpu.tick(2);

    ASSERT_EQ(cpu.getInstructionPointer(), SUBROUTINE_ADDRESS);
    ASSERT_EQ(cpu.getStackPointer(), Cpu::STACK_OFFSET - 2);
    EXPECT_EQ(cpu.getMemory(Cpu::STACK_OFFSET - 1), RETURN_ADDRESS.segment);
    EXPECT_EQ(cpu.getMemory(Cpu::STACK_OFFSET), RETURN_ADDRESS.offset);
  }

  // Test calling vector address
  {
    constexpr LongPointer RETURN_ADDRESS = Cpu::IP_0 + 10;
    constexpr LongPointer VECTOR_ADDRESS = Cpu::IP_0 + 200;
    constexpr LongPointer SUBROUTINE_ADDRESS = Cpu::IP_0 + 12;

    const std::vector<Instruction> CODE = {
      { Opcode::MOVI, Register::R0, SUBROUTINE_ADDRESS.segment },
      { Opcode::MOVI, Register::R1, SUBROUTINE_ADDRESS.offset },
      { Opcode::MOVM, VECTOR_ADDRESS, Register::R0 },
      { Opcode::MOVM, VECTOR_ADDRESS + 1, Register::R1 },
      { Opcode::CALLV, VECTOR_ADDRESS },
      // SUBROUTINE_ADDRESS:
      { Opcode::NOP },
      // RETURN_ADDRESS:
      { Opcode::NOP }
    };

    Cpu cpu;

    utils::load(cpu.getRam(), Cpu::IP_0, CODE);

    cpu.tick(5);

    ASSERT_EQ(cpu.getInstructionPointer(), SUBROUTINE_ADDRESS);
    ASSERT_EQ(cpu.getStackPointer(), Cpu::STACK_OFFSET - 2);
    EXPECT_EQ(cpu.getMemory(Cpu::STACK_OFFSET - 1), RETURN_ADDRESS.segment);
    EXPECT_EQ(cpu.getMemory(Cpu::STACK_OFFSET), RETURN_ADDRESS.offset);
  }

  // Test stack overflow
  {
    std::vector<Instruction> code(Cpu::STACK_SIZE);

    std::fill(code.begin(), code.end() - 1, Instruction(Opcode::PUSH, Register::R0));
    code.back() = { Opcode::CALLM, Cpu::IP_0 };

    Cpu cpu;

    utils::load(cpu.getRam(), Cpu::IP_0, code);

    cpu.tick(Cpu::STACK_SIZE - 1);

    EXPECT_THROW(cpu.tick(), std::runtime_error);
  }
}

/// Test CMP opcode
TEST(TestInstructions, testCmp)
{
  using CmpTest = std::tuple<WORD, WORD, BYTE>;

  const std::vector<CmpTest> CMP_TESTS = {
    // lhs == rhs
    { 0x1234, 0x1234, Flag::ZERO },
    { 0xFFFF, 0xFFFF, Flag::ZERO },
    // lhs > rhs
    { 0x1234, 0x0001, 0 },
    { 0xFFFF, 0xFFFE, 0 },
    // lhs > rhs, signed overflow
    { 0x8000, 0x7FFF, Flag::OVERFLOW },
    // lhs < rhs
    { 0x1, 0x1234, Flag::NEGATIVE|Flag::CARRY },
    { 0xFFFE, 0xFFFF, Flag::NEGATIVE|Flag::CARRY },
    // lhs < rhs, signed overflow
    { 0x7FFF, 0x8000, Flag::NEGATIVE|Flag::CARRY|Flag::OVERFLOW },
  };

  for (const CmpTest &test : CMP_TESTS)
  {
    const WORD lhs = std::get<0>(test);
    const WORD rhs = std::get<1>(test);
    const BYTE flags = std::get<2>(test);

    const std::vector<Instruction> CODE = {
      { Opcode::MOVI, Register::R0, lhs },
      { Opcode::MOVI, Register::R1, rhs },
      { Opcode::CMP, Register::R0, Register::R1 }
    };

    Cpu cpu;

    utils::load(cpu.getRam(), Cpu::IP_0, CODE);

    cpu.tick(3);

    EXPECT_EQ(cpu.getFlags(), flags) << fmt::format(
      "CMP operation CPU flags test failed, lhs: {0:#06x}, rhs: {1:#06x}, expected flags: {2:#04x}, got flags: {3:#04x}",
      lhs, rhs, flags, cpu.getFlags());
  }
}

/// Test CLR opcode
TEST(TestInstructions, testClr)
{
  const std::vector<Instruction> CODE = {
    { Opcode::SET, Flag::ALL },
    { Opcode::CLR, Flag::CARRY },
    { Opcode::CLR, Flag::NEGATIVE },
    { Opcode::CLR, Flag::OVERFLOW },
    { Opcode::CLR, Flag::ZERO },
    { Opcode::CLR, Flag::DISABLE_IRQ },
    { Opcode::SET, Flag::ALL },
    { Opcode::CLR, static_cast<BYTE>(Flag::ZERO | Flag::NEGATIVE | Flag::DISABLE_IRQ) },
  };

  Cpu cpu;

  utils::load(cpu.getRam(), Cpu::IP_0, CODE);

  cpu.tick(1);

  ASSERT_EQ(cpu.getFlag(Flag::CARRY), true);
  ASSERT_EQ(cpu.getFlag(Flag::NEGATIVE), true);
  ASSERT_EQ(cpu.getFlag(Flag::OVERFLOW), true);
  ASSERT_EQ(cpu.getFlag(Flag::ZERO), true);
  ASSERT_EQ(cpu.getFlag(Flag::DISABLE_IRQ), true);

  cpu.tick(1);

  ASSERT_EQ(cpu.getFlag(Flag::CARRY), false);
  ASSERT_EQ(cpu.getFlag(Flag::NEGATIVE), true);
  ASSERT_EQ(cpu.getFlag(Flag::OVERFLOW), true);
  ASSERT_EQ(cpu.getFlag(Flag::ZERO), true);
  ASSERT_EQ(cpu.getFlag(Flag::DISABLE_IRQ), true);

  cpu.tick(1);

  ASSERT_EQ(cpu.getFlag(Flag::CARRY), false);
  ASSERT_EQ(cpu.getFlag(Flag::NEGATIVE), false);
  ASSERT_EQ(cpu.getFlag(Flag::OVERFLOW), true);
  ASSERT_EQ(cpu.getFlag(Flag::ZERO), true);
  ASSERT_EQ(cpu.getFlag(Flag::DISABLE_IRQ), true);

  cpu.tick(3);

  ASSERT_EQ(cpu.getFlags(), 0);

  cpu.tick(2);

  ASSERT_EQ(cpu.getFlags(), Flag::CARRY | Flag::OVERFLOW);
}

/// Test DEC opcode
TEST(TestInstructions, testDec)
{
  const LongPointer VARIABLE_ADDRESS = Cpu::IP_0 + 100;
  const IndirectAddress VARIABLE_ADDRESS_INDIRECT = { 1, cpu::Register::R5, 0 };

  const std::vector<Instruction> CODE = {
    // Decrementing a register
    { Opcode::MOVI, Register::R0, 0x0002 },
    { Opcode::DEC, Register::R0 },
    { Opcode::DEC, Register::R0 },
    { Opcode::DEC, Register::R0 },
    { Opcode::MOVI, Register::R1, 0x8001 },
    { Opcode::DEC, Register::R1 },
    { Opcode::DEC, Register::R1 },
    { Opcode::DEC, Register::R1 },
    // Decrementing value at memory location
    { Opcode::MOVI, Register::R2, 0x1234 },
    { Opcode::MOVM, VARIABLE_ADDRESS, Register::R2 },
    { Opcode::DECM, VARIABLE_ADDRESS },
    { Opcode::DECM, VARIABLE_ADDRESS },
    // Decrementing value at effective address
    { Opcode::MOVI, Register::R5, 100 },
    { Opcode::MOVI, Register::R6, 0x3456 },
    { Opcode::MOVE, VARIABLE_ADDRESS_INDIRECT, Register::R6 },
    { Opcode::DECE, VARIABLE_ADDRESS_INDIRECT },
    { Opcode::DECE, VARIABLE_ADDRESS_INDIRECT },
  };

  Cpu cpu;

  utils::load(cpu.getRam(), Cpu::IP_0, CODE);

  // Decrementing register
  cpu.tick(2);

  ASSERT_EQ(cpu.getRegister(Register::R0), 0x0001);
  ASSERT_EQ(cpu.getFlags(), 0);

  cpu.tick(1);

  ASSERT_EQ(cpu.getRegister(Register::R0), 0x0000);
  ASSERT_EQ(cpu.getFlags(), Flag::ZERO);

  cpu.tick(1);

  ASSERT_EQ(cpu.getRegister(Register::R0), 0xFFFF);
  ASSERT_EQ(cpu.getFlags(), Flag::NEGATIVE);

  cpu.tick(2);

  ASSERT_EQ(cpu.getRegister(Register::R1), 0x8000);
  ASSERT_EQ(cpu.getFlags(), Flag::NEGATIVE);

  cpu.tick(1);

  ASSERT_EQ(cpu.getRegister(Register::R1), 0x7FFF);
  ASSERT_EQ(cpu.getFlags(), Flag::OVERFLOW);

  cpu.tick(1);

  ASSERT_EQ(cpu.getRegister(Register::R1), 0x7FFE);
  ASSERT_EQ(cpu.getFlags(), 0);

  // Decrementing value at direct memory location
  cpu.tick(4);

  ASSERT_EQ(cpu.getMemory(VARIABLE_ADDRESS), 0x1232);
  ASSERT_EQ(cpu.getFlags(), 0);

  // Decrementing value at indirect address
  cpu.tick(5);

  ASSERT_EQ(cpu.getMemory(VARIABLE_ADDRESS_INDIRECT, OperandSegment::DST), 0x3454);
  ASSERT_EQ(cpu.getFlags(), 0);
}

/// Test DIV opcode
TEST(TestInstructions, testDiv)
{
  // Test base case
  {
    using DivTest = std::tuple<DWORD, WORD, DWORD, WORD, BYTE>;

    const std::vector<DivTest> DIV_TESTS = {
      { 100, 3, 33, 1, 0 },
      { 100, 1, 100, 0, 0 },
      { 0, 100, 0, 0, Flag::ZERO },
      { 0x4000, 0x8000, 0, 0x4000, Flag::ZERO },
      { 0x80000001, 2, 0x40000000, 1, Flag::CARRY }
    };

    for (const DivTest &test : DIV_TESTS)
    {
      const DWORD numerator = std::get<0>(test);
      const WORD denominator = std::get<1>(test);
      const DWORD expected_quotient = std::get<2>(test);
      const WORD expected_remainder = std::get<3>(test);
      const BYTE expected_flags = std::get<4>(test);

      const std::vector<Instruction> CODE = {
        { Opcode::MOVI, Register::R0, static_cast<WORD>(numerator & 0xFFFF) },
        { Opcode::MOVI, Register::R1, static_cast<WORD>(numerator >> 16) },
        { Opcode::MOVI, Register::R2, denominator },
        { Opcode::DIV,  Register::R0, Register::R1, Register::R2, Register::R3 }
      };

      Cpu cpu;

      utils::load(cpu.getRam(), Cpu::IP_0, CODE);

      cpu.tick(4);

      const DWORD quotient = ((cpu.getRegister(Register::R1) << 16 | cpu.getRegister(Register::R0)));
      const WORD remainder = cpu.getRegister(Register::R3);

      EXPECT_EQ(quotient, expected_quotient) << fmt::format(
        "DIV operation quotient test failed, numerator: {0}, denominator: {1}, expected quotient: {2}, got quotient: {3}",
        numerator, denominator, expected_quotient, quotient);
      EXPECT_EQ(remainder, expected_remainder)<< fmt::format(
        "DIV operation remainder test failed, numerator: {0}, denominator: {1}, expected remainder: {2}, got remainder: {3}",
        numerator, denominator, expected_remainder, remainder);;
      EXPECT_EQ(cpu.getFlags(), expected_flags) << fmt::format(
        "DIV operation flags test failed, numerator: {0}, denominator: {1}, expected flags: {2:#04x}, got flags: {3:#04x}",
        numerator, denominator, expected_flags, cpu.getFlags());
      EXPECT_EQ(cpu.getRegister(Register::R2), denominator) <<
        "DIV operation test failed: denominator input register was changed by the operation";
    }
  }

  // Test division by zero
  {
    const std::vector<Instruction> CODE = {
      { Opcode::MOVI, Register::R0, 0x8000 },
      { Opcode::MOVI, Register::R1, 0 },
      { Opcode::MOVI, Register::R2, 0 },
      { Opcode::DIV,  Register::R0, Register::R1, Register::R2, Register::R3 }
    };

    Cpu cpu;

    utils::load(cpu.getRam(), Cpu::IP_0, CODE);

    EXPECT_THROW(cpu.tick(4), std::runtime_error);
  }
}

/// Test DIVS opcode
TEST(TestInstructions, testDivs)
{
  // Test base case
  {
    using DivTest = std::tuple<SDWORD, SWORD, SDWORD, SWORD, BYTE>;

    const std::vector<DivTest> DIV_TESTS = {
      { 100, 2,    50,  0, 0 },
      { 0,   100,  0,   0, Flag::ZERO },
      { 0,   -100, 0,   0, Flag::ZERO },
      { 100, -2,   -50, 0, Flag::NEGATIVE },
      { 0x80000000, 2, 0xC0000000, 0, Flag::NEGATIVE|Flag::CARRY },
      { 0x80000000, -2, 0x40000000, 0, Flag::CARRY },
      { 10, 3, 3, 1, 0 },
      { 10, -3, -3, 1, Flag::NEGATIVE },
      { -10, 3, -3, -1, Flag::NEGATIVE },
      { -10, -3, 3, -1, 0 },
      { 0xEFFFFFFF, 2, 0xF8000000, -1, Flag::NEGATIVE|Flag::CARRY },
      { 0x40000001, -2, 0xE0000000, 1, Flag::NEGATIVE|Flag::CARRY }
    };

    for (const DivTest &test : DIV_TESTS)
    {
      const SDWORD numerator = std::get<0>(test);
      const SWORD denominator = std::get<1>(test);
      const SDWORD expected_quotient = std::get<2>(test);
      const SWORD expected_remainder = std::get<3>(test);
      const BYTE expected_flags = std::get<4>(test);

      const std::vector<Instruction> CODE = {
        { Opcode::MOVI, Register::R0, static_cast<WORD>(numerator & 0xFFFF) },
        { Opcode::MOVI, Register::R1, static_cast<WORD>(numerator >> 16) },
        { Opcode::MOVI, Register::R2, static_cast<WORD>(denominator) },
        { Opcode::DIVS, Register::R0, Register::R1, Register::R2, Register::R3 }
      };

      Cpu cpu;

      utils::load(cpu.getRam(), Cpu::IP_0, CODE);

      cpu.tick(4);

      const SDWORD quotient = static_cast<SDWORD>((cpu.getRegister(Register::R1) << 16 | cpu.getRegister(Register::R0)));
      const SWORD remainder = static_cast<SWORD>(cpu.getRegister(Register::R3));

      EXPECT_EQ(quotient, expected_quotient) << fmt::format(
        "DIVS operation quotient test failed, numerator: {0}, denominator: {1}, expected quotient: {2}, got quotient: {3}",
        numerator, denominator, expected_quotient, quotient);
      EXPECT_EQ(remainder, expected_remainder)<< fmt::format(
        "DIVS operation remainder test failed, numerator: {0}, denominator: {1}, expected remainder: {2}, got remainder: {3}",
        numerator, denominator, expected_remainder, remainder);;
      EXPECT_EQ(cpu.getFlags(), expected_flags) << fmt::format(
        "DIVS operation flags test failed, numerator: {0}, denominator: {1}, expected flags: {2:#04x}, got flags: {3:#04x}",
        numerator, denominator, expected_flags, cpu.getFlags());
      EXPECT_EQ(static_cast<SWORD>(cpu.getRegister(Register::R2)), denominator) <<
        "DIVS operation test failed: denominator input register was changed by the operation";
    }
  }

  // Test division by zero
  {
    const std::vector<Instruction> CODE = {
      { Opcode::MOVI, Register::R0, 0x8000 },
      { Opcode::MOVI, Register::R1, 0 },
      { Opcode::MOVI, Register::R2, 0 },
      { Opcode::DIV,  Register::R0, Register::R1, Register::R2, Register::R3 }
    };

    Cpu cpu;

    utils::load(cpu.getRam(), Cpu::IP_0, CODE);

    EXPECT_THROW(cpu.tick(4), std::runtime_error);
  }
}

/// Test HALT opcode
TEST(TestInstructions, testHalt)
{
  const std::vector<Instruction> CODE = {
    { Opcode::MOVI, Register::R0, 0x0001 },
    { Opcode::HALT },
    { Opcode::INC, Register::R0 },
  };

  Cpu cpu;

  utils::load(cpu.getRam(), Cpu::IP_0, CODE);

  // Execute arbitrary number of ticks. This should only execute up to and including the HALT instruction
  ASSERT_THROW(cpu.tick(20), Halted);

  ASSERT_EQ(cpu.getInstructionPointer(), Cpu::IP_0 + 4);

  // Test that the CPU can be resumed after halting
  cpu.tick(1);

  ASSERT_EQ(cpu.getRegister(Register::R0), 0x0002);
}

/// Test IN opcode
TEST(TestInstructions, testIn)
{
  constexpr peripheral::Device::ID DUMMY_DEVICE_ID = 1;
  constexpr peripheral::Device::ID INVALID_DEVICE_ID = DUMMY_DEVICE_ID + 1;
  constexpr WORD PORT_0 = DummyDevice::PORT_0;
  constexpr WORD PORT_1 = DummyDevice::PORT_1;
  constexpr WORD INVALID_PORT = (1 << Cpu::PORT_BITS);
  constexpr WORD VALUE = 0x1234;

  const std::vector<Instruction> CODE = {
    // Test reading port specified as immediate
    { Opcode::SET, Flag::ALL_ARITHMETIC },
    { Opcode::INI, PortMove(DUMMY_DEVICE_ID, PORT_0, Register::R0) },
    // Test reading port specified as register
    { Opcode::MOVI, Register::R1, PORT_1 },
    { Opcode::SET, Flag::ALL_ARITHMETIC },
    { Opcode::INR, PortMove(DUMMY_DEVICE_ID, Register::R1, Register::R2) },
    // Test reading port from invalid device
    { Opcode::INI, PortMove(INVALID_DEVICE_ID, PORT_0, Register::R2) },
    // Test reading invalid port number
    { Opcode::MOVI, Register::R3, INVALID_PORT },
    { Opcode::INR, PortMove(INVALID_DEVICE_ID, Register::R3, Register::R2) },
  };

  Cpu cpu;

  DummyDevice dummy_device(DUMMY_DEVICE_ID);

  cpu.connectDevice(&dummy_device);

  dummy_device.out(PORT_0, VALUE);
  dummy_device.out(PORT_1, VALUE);

  utils::load(cpu.getRam(), Cpu::IP_0, CODE);

  // Test base case: read valid port specified as immediate
  cpu.tick(2);

  ASSERT_EQ(cpu.getRegister(Register::R0), VALUE);
  ASSERT_EQ(cpu.getFlags(), 0);

  // Test base case: read valid port specified as register
  cpu.tick(3);

  ASSERT_EQ(cpu.getRegister(Register::R2), VALUE);
  ASSERT_EQ(cpu.getFlags(), 0);

  // Test fail modes: read non-existing port of valid device, read port from invalid device
  ASSERT_THROW(cpu.tick(1), std::runtime_error);
  ASSERT_THROW(cpu.tick(2), std::runtime_error);
}

/// Test INC opcode
TEST(TestInstructions, testInc)
{
  const LongPointer VARIABLE_ADDRESS = Cpu::IP_0 + 100;
  const IndirectAddress VARIABLE_ADDRESS_INDIRECT = { 1, Register::R5, 0 };

  const std::vector<Instruction> CODE = {
    // Incrementing a register
    { Opcode::MOVI, Register::R0, 0xFFFE },
    { Opcode::INC, Register::R0 },
    { Opcode::INC, Register::R0 },
    { Opcode::INC, Register::R0 },
    { Opcode::MOVI, Register::R1, 0x7FFE },
    { Opcode::INC, Register::R1 },
    { Opcode::INC, Register::R1 },
    { Opcode::INC, Register::R1 },
    // Incrementing value at memory location
    { Opcode::MOVI, Register::R2, 0x1234 },
    { Opcode::MOVM, VARIABLE_ADDRESS, Register::R2 },
    { Opcode::INCM, VARIABLE_ADDRESS },
    { Opcode::INCM, VARIABLE_ADDRESS },
    // Incrementing value at effective address
    { Opcode::MOVI, Register::R5, 100 },
    { Opcode::MOVI, Register::R6, 0x3456 },
    { Opcode::MOVE, VARIABLE_ADDRESS_INDIRECT, Register::R6 },
    { Opcode::INCE, VARIABLE_ADDRESS_INDIRECT },
    { Opcode::INCE, VARIABLE_ADDRESS_INDIRECT },
  };

  Cpu cpu;

  utils::load(cpu.getRam(), Cpu::IP_0, CODE);

  // Increasing register
  cpu.tick(2);

  ASSERT_EQ(cpu.getRegister(Register::R0), 0xFFFF);
  ASSERT_EQ(cpu.getFlags(), Flag::NEGATIVE);

  cpu.tick(1);

  ASSERT_EQ(cpu.getRegister(Register::R0), 0x0000);
  ASSERT_EQ(cpu.getFlags(), Flag::ZERO);

  cpu.tick(1);

  ASSERT_EQ(cpu.getRegister(Register::R0), 0x0001);
  ASSERT_EQ(cpu.getFlags(), 0);

  cpu.tick(2);

  ASSERT_EQ(cpu.getRegister(Register::R1), 0x7FFF);
  ASSERT_EQ(cpu.getFlags(), 0);

  cpu.tick(1);

  ASSERT_EQ(cpu.getRegister(Register::R1), 0x8000);
  ASSERT_EQ(cpu.getFlags(), Flag::NEGATIVE|Flag::OVERFLOW);

  cpu.tick(1);

  ASSERT_EQ(cpu.getRegister(Register::R1), 0x8001);
  ASSERT_EQ(cpu.getFlags(), Flag::NEGATIVE);

  // Increasing value at direct memory location
  cpu.tick(4);

  ASSERT_EQ(cpu.getMemory(VARIABLE_ADDRESS), 0x1236);
  ASSERT_EQ(cpu.getFlags(), 0);

  // Increasing value at effective address
  cpu.tick(5);

  ASSERT_EQ(cpu.getMemory(VARIABLE_ADDRESS_INDIRECT, OperandSegment::DST), 0x3458);
  ASSERT_EQ(cpu.getFlags(), 0);
}

/// Test JMP (jump) opcodes
TEST(TestInstructions, testJ)
{
  // Test jump to direct memory address.
  {
    constexpr LongPointer JUMP_ADDRESS = Cpu::IP_0 + 8;

    const std::vector<Instruction> CODE = {
      { Opcode::MOVI, Register::R0, 0x1234 },
      // Jump over the MOV instructions that resets R0. Both R0 and the CPU flags should be unaffected
      { Opcode::CLR, Flag::ALL_ARITHMETIC },
      { Opcode::JM, JUMP_ADDRESS },
      { Opcode::MOVI, Register::R0, 0 },
      // JUMP_ADDRESS:
      { Opcode::NOP },
      { Opcode::SET, Flag::ALL_ARITHMETIC },
      // Jump back to IP_0. CPU flags should again be unaffected
      { Opcode::JM, Cpu::IP_0 }
    };

    Cpu cpu;

    utils::load(cpu.getRam(), Cpu::IP_0, CODE);

    cpu.tick(4);

    ASSERT_EQ(cpu.getInstructionPointer(), JUMP_ADDRESS + 2);
    EXPECT_EQ(cpu.getRegister(Register::R0), 0x1234);
    EXPECT_EQ(cpu.getFlags(), 0);

    cpu.tick(2);

    ASSERT_EQ(cpu.getInstructionPointer(), Cpu::IP_0);
    EXPECT_EQ(cpu.getFlags(), Flag::ALL_ARITHMETIC);
  }

  // Test jump to vector
  {
    constexpr LongPointer VECTOR_ADDRESS = Cpu::IP_0 + 200;
    constexpr LongPointer JUMP_ADDRESS = Cpu::IP_0 + 16;

    const std::vector<Instruction> CODE = {
      { Opcode::MOVI, Register::R0, 0x1234 },
      { Opcode::MOVI, Register::R1, JUMP_ADDRESS.offset },
      { Opcode::MOVI, Register::R2, JUMP_ADDRESS.segment },
      { Opcode::MOVM, VECTOR_ADDRESS, Register::R1 },
      { Opcode::MOVM, VECTOR_ADDRESS + 1, Register::R2 },
      { Opcode::CLR, Flag::ALL_ARITHMETIC },
      { Opcode::JV, VECTOR_ADDRESS },
      { Opcode::MOVI, Register::R0, 0 },
      // JUMP_ADDRESS:
      { Opcode::NOP },
      { Opcode::MOVI, Register::R1, Cpu::IP_0.offset },
      { Opcode::MOVI, Register::R2, Cpu::IP_0.segment },
      { Opcode::MOVM, VECTOR_ADDRESS, Register::R1 },
      { Opcode::MOVM, VECTOR_ADDRESS + 1, Register::R2 },
      { Opcode::SET, Flag::ALL_ARITHMETIC },
      { Opcode::JV, VECTOR_ADDRESS },
    };

    Cpu cpu;

    utils::load(cpu.getRam(), Cpu::IP_0, CODE);

    cpu.tick(8);

    ASSERT_EQ(cpu.getInstructionPointer(), JUMP_ADDRESS + 2);
    EXPECT_EQ(cpu.getRegister(Register::R0), 0x1234);
    EXPECT_EQ(cpu.getFlags(), 0);

    cpu.tick(6);

    ASSERT_EQ(cpu.getInstructionPointer(), Cpu::IP_0);
    EXPECT_EQ(cpu.getFlags(), Flag::ALL_ARITHMETIC);
  }
}

/// Tests LDSP opcode.
TEST(TestInstructions, testLdsp)
{
  const std::vector<Instruction> CODE = {
    // Load stack pointer (before and after pushing)
    { Opcode::LDSP, Register::R1, SpecialPurposeRegister::SP },
    { Opcode::PUSH, Register::R1 },
    { Opcode::PUSH, Register::R1 },
    { Opcode::LDSP, Register::R2, SpecialPurposeRegister::SP },
    // Load segment register. The segment register can only be written by the STSP
    // instruction, but we don't want to do that here as we just want to cover the
    // functionality of the LDSP opcode. Instead, load the SR register, and see that
    // it is set to the default value of the segment register after a CPU reset (0x0101)
    { Opcode::LDSP, Register::R3, SpecialPurposeRegister::SR },
  };

  Cpu cpu;

  utils::load(cpu.getRam(), Cpu::IP_0, CODE);

  cpu.tick(1);

  EXPECT_EQ(cpu.getRegister(Register::R1), Cpu::STACK_OFFSET);

  cpu.tick(3);

  EXPECT_EQ(cpu.getRegister(Register::R2), Cpu::STACK_OFFSET - 2);

  cpu.tick(1);

  EXPECT_EQ(cpu.getRegister(Register::R3), Cpu::SR_0);
}

/// Tests LSL instruction.
TEST(TestInstructions, testLsl)
{
  constexpr WORD SHIFT_VALUE = 0b0001011011101111;

  const std::vector<Instruction> CODE = {
    { Opcode::MOVI, Register::R0, SHIFT_VALUE },
    { Opcode::LSLI, Register::R0, 3 },
    { Opcode::MOVI, Register::R1, SHIFT_VALUE },
    { Opcode::LSLI, Register::R1, 4 },
    { Opcode::MOVI, Register::R2, SHIFT_VALUE },
    { Opcode::LSLI, Register::R2, 5 },
    { Opcode::MOVI, Register::R3, SHIFT_VALUE },
    { Opcode::LSLI, Register::R3, 20 },
    // Test shifting by a distance of 0 does not affect CPU flags
    { Opcode::MOVI, Register::R4, SHIFT_VALUE },
    { Opcode::SET, static_cast<BYTE>(Flag::ALL_ARITHMETIC) },
    { Opcode::LSLI, Register::R4, 0 },
  };

  Cpu cpu;

  utils::load(cpu.getRam(), Cpu::IP_0, CODE);

  cpu.tick(2);

  ASSERT_EQ(cpu.getRegister(Register::R0), 0b1011011101111000);
  ASSERT_EQ(cpu.getFlags(), Flag::NEGATIVE);

  cpu.tick(2);

  ASSERT_EQ(cpu.getRegister(Register::R1), 0b0110111011110000);
  ASSERT_EQ(cpu.getFlags(), Flag::CARRY);

  cpu.tick(2);

  ASSERT_EQ(cpu.getRegister(Register::R2), 0b1101110111100000);
  ASSERT_EQ(cpu.getFlags(), Flag::NEGATIVE);

  cpu.tick(2);

  ASSERT_EQ(cpu.getRegister(Register::R3), 0);
  ASSERT_EQ(cpu.getFlags(), Flag::ZERO);

  cpu.tick(3);

  ASSERT_EQ(cpu.getRegister(Register::R4), SHIFT_VALUE);
  ASSERT_EQ(cpu.getFlags(), Flag::ALL_ARITHMETIC);
}

/// Tests LSR instruction.
TEST(TestInstructions, testLsr)
{
  constexpr WORD SHIFT_VALUE = 0b0001011011101111;

  const std::vector<Instruction> CODE = {
    { Opcode::MOVI, Register::R0, SHIFT_VALUE },
    { Opcode::SET, static_cast<BYTE>(Flag::ALL_ARITHMETIC) },
    { Opcode::LSRI, Register::R0, 3 },
    { Opcode::MOVI, Register::R1, SHIFT_VALUE },
    { Opcode::SET, static_cast<BYTE>(Flag::ALL_ARITHMETIC) },
    { Opcode::LSRI, Register::R1, 20 },
    // Test shifting by a distance of 0 does not affect CPU flags
    { Opcode::MOVI, Register::R2, SHIFT_VALUE },
    { Opcode::SET, static_cast<BYTE>(Flag::ALL_ARITHMETIC) },
    { Opcode::LSRI, Register::R2, 0 },
  };

  Cpu cpu;

  utils::load(cpu.getRam(), Cpu::IP_0, CODE);

  cpu.tick(3);

  ASSERT_EQ(cpu.getRegister(Register::R0), 0b0000001011011101);
  ASSERT_EQ(cpu.getFlags(), 0);

  cpu.tick(3);

  ASSERT_EQ(cpu.getRegister(Register::R1), 0);
  ASSERT_EQ(cpu.getFlags(), Flag::ZERO);

  cpu.tick(3);

  ASSERT_EQ(cpu.getRegister(Register::R2), SHIFT_VALUE);
  ASSERT_EQ(cpu.getFlags(), Flag::ALL_ARITHMETIC);
}

/// Tests MOV opcodes.
///
/// This test case also covers the logic that handles the various addressing modes,
/// which is shared by all instructions that support multiple addressing modes. We
/// don't repeat these checks in the test cases for any of the other instructions,
/// and only use the DST_SRC REGISTER addressing mode there.
TEST(TestInstructions, testMov)
{
  // Test supported addressing modes
  {
    const std::vector<Instruction> CODE = {
      // Test register-immediate move
      { Opcode::MOVI, Register::R0, 0x1234 },
      // Test register-register move
      { Opcode::MOV, Register::R1, Register::R0 },
      // Test direct memory address move
      { Opcode::MOVM, LongPointer(0x02, 0x0000), Register::R1 },
      { Opcode::MOVM, Register::R2, LongPointer(0x02, 0x0000) },
      // Test indirect address move to/from 0x01:0x1031, represented as indirect address with
      // forward and sign offset ([2*r3 + 0x0031] and [1*r4 - 0x00CF])
      { Opcode::MOVI, Register::R3, 0x0800 },
      { Opcode::MOVI, Register::R4, 0x1100 },
      { Opcode::MOVE, IndirectAddress(2, Register::R3, 0x0031), Register::R0 },
      { Opcode::MOVE, Register::R6, IndirectAddress(1, Register::R4, -0x00CF) },
      // Test zero-page move
      { Opcode::MOVI, Register::R3, 0x0800 },
      { Opcode::MOVE, IndirectAddress(0, Register::R3, 0x0100), Register::R0 }
    };

    Cpu cpu;

    utils::load(cpu.getRam(), Cpu::IP_0, CODE);

    cpu.tick(CODE.size());

    EXPECT_EQ(cpu.getRegister(Register::R0), 0x1234);
    EXPECT_EQ(cpu.getRegister(Register::R1), 0x1234);
    EXPECT_EQ(cpu.getMemory(LongPointer(0x02, 0x0000)), 0x1234);
    EXPECT_EQ(cpu.getRegister(Register::R2), 0x1234);
    EXPECT_EQ(cpu.getMemory(IndirectAddress(2, Register::R3, 0x0031), OperandSegment::DST), 0x1234);
    EXPECT_EQ(cpu.getMemory(LongPointer(0x01, 0x1031)), 0x1234);
    EXPECT_EQ(cpu.getRegister(Register::R6), 0x1234);
    EXPECT_EQ(cpu.getMemory(LongPointer(0x0900)), 0x1234);
  }

  // Test CPU flags
  {
    const std::vector<Instruction> CODE = {
      { Opcode::MOVI, Register::R0, 0 },
      { Opcode::MOVI, Register::R1, 0xFFFF },
      { Opcode::SET, Flag::ALL_ARITHMETIC },
      { Opcode::MOV, Register::R2, Register::R1 },
      { Opcode::SET, Flag::ALL_ARITHMETIC },
      { Opcode::MOVM, LongPointer(0x02, 0x0000), Register::R0 },
      { Opcode::SET, Flag::ALL_ARITHMETIC },
      { Opcode::MOVE, IndirectAddress(1, Register::R0, 0x0100), Register::R1 },
    };

    Cpu cpu;

    utils::load(cpu.getRam(), cpu::Cpu::IP_0, CODE);

    cpu.tick(1);

    EXPECT_EQ(cpu.getFlag(Flag::ZERO), true);
    EXPECT_EQ(cpu.getFlag(Flag::NEGATIVE), false);
    EXPECT_EQ(cpu.getFlag(Flag::OVERFLOW), false);
    EXPECT_EQ(cpu.getFlag(Flag::CARRY), false);

    cpu.tick(1);

    EXPECT_EQ(cpu.getFlag(Flag::ZERO), false);
    EXPECT_EQ(cpu.getFlag(Flag::NEGATIVE), true);
    EXPECT_EQ(cpu.getFlag(Flag::OVERFLOW), false);
    EXPECT_EQ(cpu.getFlag(Flag::CARRY), false);

    cpu.tick(2);

    EXPECT_EQ(cpu.getFlag(Flag::ZERO), false);
    EXPECT_EQ(cpu.getFlag(Flag::NEGATIVE), true);
    EXPECT_EQ(cpu.getFlag(Flag::OVERFLOW), false);
    EXPECT_EQ(cpu.getFlag(Flag::CARRY), false);

    cpu.tick(2);

    EXPECT_EQ(cpu.getFlag(Flag::ZERO), true);
    EXPECT_EQ(cpu.getFlag(Flag::NEGATIVE), false);
    EXPECT_EQ(cpu.getFlag(Flag::OVERFLOW), false);
    EXPECT_EQ(cpu.getFlag(Flag::CARRY), false);

    cpu.tick(2);

    EXPECT_EQ(cpu.getFlag(Flag::ZERO), false);
    EXPECT_EQ(cpu.getFlag(Flag::NEGATIVE), true);
    EXPECT_EQ(cpu.getFlag(Flag::OVERFLOW), false);
    EXPECT_EQ(cpu.getFlag(Flag::CARRY), false);
  }
}

/// Test MOVH opcode
TEST(TestInstructions, testMovh)
{
  const std::vector<Instruction> CODE = {
    { Opcode::MOVI, Register::R0, 0x9999 },
    { Opcode::MOVHI, Register::R0, 0x1234, false },
    { Opcode::MOVI, Register::R1, 0x9999 },
    { Opcode::MOVHI, Register::R1, 0x1234, true },
    { Opcode::MOVI, Register::R2, 0x0099 },
    { Opcode::MOVI, Register::R3, 0x0099 },
    { Opcode::MOVH, Register::R2, Register::R3, true },
  };

  Cpu cpu;

  utils::load(cpu.getRam(), Cpu::IP_0, CODE);

  cpu.tick(2);

  ASSERT_EQ(cpu.getRegister(Register::R0), 0x1299);
  ASSERT_EQ(cpu.getFlags(), 0);

  cpu.tick(2);

  ASSERT_EQ(cpu.getRegister(Register::R1), 0x9912);
  ASSERT_EQ(cpu.getFlags(), Flag::NEGATIVE);

  cpu.tick(3);

  ASSERT_EQ(cpu.getRegister(Register::R2), 0x0000);
  ASSERT_EQ(cpu.getRegister(Register::R3), 0x0099);
  ASSERT_EQ(cpu.getFlags(), Flag::ZERO);
}

/// Test MOVL opcode
TEST(TestInstructions, testMovl)
{
  const std::vector<Instruction> CODE = {
    { Opcode::MOVI, Register::R0, 0x9999 },
    { Opcode::MOVLI, Register::R0, 0x1234, false },
    { Opcode::MOVI, Register::R1, 0x9999 },
    { Opcode::MOVLI, Register::R1, 0x1234, true },
    { Opcode::MOVI, Register::R2, 0x9900 },
    { Opcode::MOVI, Register::R3, 0x9900 },
    { Opcode::MOVL, Register::R2, Register::R3, true },
  };

  Cpu cpu;

  utils::load(cpu.getRam(), Cpu::IP_0, CODE);

  cpu.tick(2);

  ASSERT_EQ(cpu.getRegister(Register::R0), 0x9934);
  ASSERT_EQ(cpu.getFlags(), Flag::NEGATIVE);

  cpu.tick(2);

  ASSERT_EQ(cpu.getRegister(Register::R1), 0x3499);
  ASSERT_EQ(cpu.getFlags(), 0);

  cpu.tick(3);

  ASSERT_EQ(cpu.getRegister(Register::R2), 0x0000);
  ASSERT_EQ(cpu.getRegister(Register::R3), 0x9900);
  ASSERT_EQ(cpu.getFlags(), Flag::ZERO);
}

/// Test MUL opcode
TEST(TestInstructions, testMul)
{
  using MulTest = std::tuple<WORD, WORD, DWORD, BYTE>;

  const std::vector<MulTest> MUL_TESTS = {
    { 1, 4, 4, 0 },
    { 0, 4, 0, Flag::ZERO },
    { 0xF000, 16, 0xF0000, Flag::CARRY },
    { 0xFFFF, 0xFFFF, 0xFFFE0001, Flag::CARRY }
  };

  for (const MulTest &test : MUL_TESTS)
  {
    const WORD lhs = std::get<0>(test);
    const WORD rhs = std::get<1>(test);
    const DWORD expected_result = std::get<2>(test);
    const BYTE expected_flags = std::get<3>(test);

    const std::vector<Instruction> CODE = {
      { Opcode::MOVI, Register::R0, lhs },
      { Opcode::MOVI, Register::R1, rhs },
      { Opcode::MUL, Register::R0, Register::R1, Register::R2, Register::R3 }
    };

    Cpu cpu;

    utils::load(cpu.getRam(), Cpu::IP_0, CODE);

    cpu.tick(3);

    const DWORD result = ((cpu.getRegister(Register::R3) << 16 | cpu.getRegister(Register::R2)));

    EXPECT_EQ(result, expected_result) << fmt::format(
      "MUL operation result test failed, lhs: {0}, rhs: {1}, expected result: {2}, got result: {3}",
      lhs, rhs, expected_result, result);

    EXPECT_EQ(cpu.getFlags(), expected_flags) << fmt::format(
      "MUL operation flags test failed, lhs: {0}, rhs: {1}, expected flags: {2:#04x}, got flags: {3:#04x}",
      lhs, rhs, expected_flags, cpu.getFlags());
  }
}

/// Test MULS opcode
TEST(TestInstructions, testMuls)
{
  using MulTest = std::tuple<WORD, WORD, DWORD, BYTE>;

  const std::vector<MulTest> MUL_TESTS = {
    { 1, 4, 4, 0 },
    { -1, 4, -4, Flag::NEGATIVE },
    { 0, -4, 0, Flag::ZERO },
    { -4, -4, 16, 0 },
    { 0x1000, 16, 0x10000, Flag::CARRY },
    { 0xF000, 16, 0xFFFF0000, Flag::CARRY | Flag::NEGATIVE },
    { 0xFF00, 16, 0xFFFFF000, Flag::NEGATIVE },
  };

  for (const MulTest &test : MUL_TESTS)
  {
    const WORD lhs = std::get<0>(test);
    const WORD rhs = std::get<1>(test);
    const DWORD expected_result = std::get<2>(test);
    const BYTE expected_flags = std::get<3>(test);

    const std::vector<Instruction> CODE = {
      { Opcode::MOVI, Register::R0, lhs },
      { Opcode::MOVI, Register::R1, rhs },
      { Opcode::MULS, Register::R0, Register::R1, Register::R2, Register::R3 }
    };

    Cpu cpu;

    utils::load(cpu.getRam(), Cpu::IP_0, CODE);

    cpu.tick(3);

    const DWORD result = ((cpu.getRegister(Register::R3) << 16 | cpu.getRegister(Register::R2)));

    EXPECT_EQ(result, expected_result) << fmt::format(
      "MULS operation result test failed, lhs: {0}, rhs: {1}, expected result: {2}, got result: {3}",
      lhs, rhs, expected_result, result);

    EXPECT_EQ(cpu.getFlags(), expected_flags) << fmt::format(
      "MULS operation flags test failed, lhs: {0}, rhs: {1}, expected flags: {2:#04x}, got flags: {3:#04x}",
      lhs, rhs, expected_flags, cpu.getFlags());
  }
}

/// Test NEG opcode
TEST(TestInstructions, testNeg)
{
  const std::vector<Instruction> CODE = {
    { Opcode::MOVI, Register::R0, 3000 },
    { Opcode::NEG, Register::R0 },
    { Opcode::MOVI, Register::R1, static_cast<WORD>(-1234) },
    { Opcode::NEG, Register::R1 },
    { Opcode::MOVI, Register::R2, 0 },
    { Opcode::NEG, Register::R2 },
    { Opcode::MOVI, Register::R3, 0x7FFF },
    { Opcode::NEG, Register::R3 },
    { Opcode::MOVI, Register::R4, 0x8000 },
    { Opcode::NEG, Register::R4 },
  };

  Cpu cpu;

  utils::load(cpu.getRam(), Cpu::IP_0, CODE);

  cpu.tick(2);

  ASSERT_EQ(static_cast<SWORD>(cpu.getRegister(Register::R0)), -3000);
  ASSERT_EQ(cpu.getFlags(), Flag::NEGATIVE);

  cpu.tick(2);

  ASSERT_EQ(static_cast<SWORD>(cpu.getRegister(Register::R1)), 1234);
  ASSERT_EQ(cpu.getFlags(), 0);

  cpu.tick(2);

  ASSERT_EQ(static_cast<SWORD>(cpu.getRegister(Register::R2)), 0);
  ASSERT_EQ(cpu.getFlags(), Flag::ZERO);

  cpu.tick(2);

  ASSERT_EQ(static_cast<SWORD>(cpu.getRegister(Register::R3)), -32767);
  ASSERT_EQ(cpu.getFlags(), Flag::NEGATIVE);

  cpu.tick(2);

  ASSERT_EQ(cpu.getRegister(Register::R4), 0x8000);
  ASSERT_EQ(cpu.getFlags(),Flag::OVERFLOW|Flag::NEGATIVE);
}

/// Test NOT opcode
TEST(TestInstructions, testNot)
{
  const std::vector<Instruction> CODE = {
    { Opcode::MOVI, Register::R0, 0x3000 },
    { Opcode::NOT, Register::R0 },
    { Opcode::MOVI, Register::R1, 0xF000 },
    { Opcode::NOT, Register::R1 },
    { Opcode::MOVI, Register::R2, 0xFFFF },
    { Opcode::NOT, Register::R2 },
    { Opcode::MOVI, Register::R3, 0 },
    { Opcode::NOT, Register::R3 }
  };

  Cpu cpu;

  utils::load(cpu.getRam(), Cpu::IP_0, CODE);

  cpu.tick(2);

  ASSERT_EQ(cpu.getRegister(Register::R0), static_cast<WORD>(~0x3000));
  ASSERT_EQ(cpu.getFlags(), Flag::NEGATIVE);

  cpu.tick(2);

  ASSERT_EQ(cpu.getRegister(Register::R1), static_cast<WORD>(~0xF000));
  ASSERT_EQ(cpu.getFlags(), 0);

  cpu.tick(2);

  ASSERT_EQ(cpu.getRegister(Register::R2), 0);
  ASSERT_EQ(cpu.getFlags(), Flag::ZERO);

  cpu.tick(2);

  ASSERT_EQ(cpu.getRegister(Register::R3), static_cast<WORD>(0xFFFF));
  ASSERT_EQ(cpu.getFlags(), Flag::NEGATIVE);
}

/// Test NOP opcode
TEST(TestInstructions, testNop)
{
  const std::vector<Instruction> CODE = {
    { Opcode::SET, static_cast<BYTE>(0) },
    { Opcode::NOP },
    { Opcode::SET, Flag::ALL },
    { Opcode::NOP }
  };

  Cpu cpu;

  utils::load(cpu.getRam(), Cpu::IP_0, CODE);

  cpu.tick(2);

  EXPECT_EQ(cpu.getFlags(), 0);

  cpu.tick(2);

  EXPECT_EQ(cpu.getFlags(), Flag::ALL);
}

/// Test OR instruction
TEST(TestInstructions, testOr)
{
  const std::vector<Instruction> CODE = {
    { Opcode::SET, static_cast<BYTE>(0x0F) },
    { Opcode::MOVI, Register::R0, 0x0000 },
    { Opcode::ORI, Register::R0, 0x1234 },
    { Opcode::SET, static_cast<BYTE>(0x0F) },
    { Opcode::MOVI, Register::R1, 0x0000 },
    { Opcode::ORI, Register::R1, 0x8000 },
    { Opcode::SET, static_cast<BYTE>(0x0F) },
    { Opcode::MOVI, Register::R2, 0x0000 },
    { Opcode::ORI, Register::R2, 0x0000 },
  };

  Cpu cpu;

  utils::load(cpu.getRam(), Cpu::IP_0, CODE);

  cpu.tick(3);

  ASSERT_EQ(cpu.getRegister(Register::R0), 0x1234);
  ASSERT_EQ(cpu.getFlags(), 0);

  cpu.tick(3);

  ASSERT_EQ(cpu.getRegister(Register::R1), 0x8000);
  ASSERT_EQ(cpu.getFlags(), Flag::NEGATIVE);

  cpu.tick(3);

  ASSERT_EQ(cpu.getRegister(Register::R2), 0x0000);
  ASSERT_EQ(cpu.getFlags(), Flag::ZERO);
}

/// Test OUT opcode
TEST(TestInstructions, testOut)
{
  constexpr peripheral::Device::ID DUMMY_DEVICE_ID = 1;
  constexpr peripheral::Device::ID INVALID_DEVICE_ID = DUMMY_DEVICE_ID + 1;
  constexpr WORD PORT_0 = DummyDevice::PORT_0;
  constexpr WORD PORT_1 = DummyDevice::PORT_1;
  constexpr WORD INVALID_PORT = 0xF000;
  constexpr WORD VALUE = 0x1234;

  const std::vector<Instruction> CODE = {
    // Write to port specified as immediate
    { Opcode::MOVI, Register::R0, VALUE },
    { Opcode::SET, Flag::Bits::ALL },
    { Opcode::OUTI, PortMove(DUMMY_DEVICE_ID, PORT_0, Register::R0) },
    // Write to port specified as register
    { Opcode::MOVI, Register::R1, PORT_1 },
    { Opcode::SET, Flag::Bits::ALL },
    { Opcode::OUTR, PortMove(DUMMY_DEVICE_ID, Register::R1, Register::R0) },
    // Write to invalid device
    { Opcode::OUTI, PortMove(INVALID_DEVICE_ID, PORT_0, Register::R0) },
    // Write to invalid port
    { Opcode::MOVI, Register::R2, INVALID_PORT},
    { Opcode::OUTR, PortMove(DUMMY_DEVICE_ID, Register::R2, Register::R0) },
  };

  Cpu cpu;

  DummyDevice dummy_device(DUMMY_DEVICE_ID);

  cpu.connectDevice(&dummy_device);

  utils::load(cpu.getRam(), Cpu::IP_0, CODE);

  // Write valid port specified as immediate
  cpu.tick(3);

  ASSERT_EQ(dummy_device.in(PORT_0), VALUE);
  ASSERT_EQ(cpu.getFlags(), Flag::Bits::ALL);

  // Write valid port specified as register
  cpu.tick(3);

  ASSERT_EQ(dummy_device.in(PORT_1), VALUE);
  ASSERT_EQ(cpu.getFlags(), Flag::Bits::ALL);

  // Test fail modes: write port for invalid device, write to invalid port number
  ASSERT_THROW(cpu.tick(1), std::runtime_error);
  ASSERT_THROW(cpu.tick(2), std::runtime_error);
}

/// Test POP opcode
TEST(TestInstructions, testPop)
{
  // Test popping values
  const std::vector<Instruction> CODE = {
    { Opcode::MOVI, Register::R0, 0x1234 },
    { Opcode::MOVI, Register::R1, 0x5678 },
    { Opcode::PUSH, Register::R0 },
    { Opcode::PUSH, Register::R1 },
    { Opcode::POP, Register::R6 },
    { Opcode::POP, Register::R7 },
    { Opcode::POP, Register::R0 }
  };

  Cpu cpu;

  utils::load(cpu.getRam(), Cpu::IP_0, CODE);

  cpu.tick(4);

  ASSERT_EQ(cpu.getStackPointer(), Cpu::STACK_OFFSET - 2);

  cpu.tick(2);

  ASSERT_EQ(cpu.getStackPointer(), Cpu::STACK_OFFSET);
  EXPECT_EQ(cpu.getRegister(Register::R6), 0x5678);
  EXPECT_EQ(cpu.getRegister(Register::R7), 0x1234);

  // Test underflow
  EXPECT_THROW(cpu.tick(), std::runtime_error);
}

/// Test POPS opcode
TEST(TestInstructions, testPops)
{
  // Test base case usage
  {
    const std::vector<Instruction> CODE = {
      { Opcode::MOVI, Register::R1, Cpu::STACK_OFFSET - 9 },
      { Opcode::STSP, SpecialPurposeRegister::SP, Register::R1 },
      { Opcode::POPS },
    };

    Cpu cpu;

    utils::load(cpu.getRam(), Cpu::IP_0, CODE);

    // Copy values to stack
    const std::array<WORD, 9> STATE_VALUES = {
      static_cast<WORD>(Flag::ALL), 0x9907, 0x9906, 0x9905, 0x9904, 0x9903, 0x9902, 0x9901, 0x9900,
    };

    std::copy(STATE_VALUES.begin(), STATE_VALUES.end(), cpu.getRam().begin() + Cpu::STACK_OFFSET - 8);

    // Run the test program
    cpu.tick(3);

    ASSERT_EQ(cpu.getFlags(), static_cast<WORD>(Flag::ALL));
    ASSERT_EQ(cpu.getRegister(Register::R0), 0x9900);
    ASSERT_EQ(cpu.getRegister(Register::R1), 0x9901);
    ASSERT_EQ(cpu.getRegister(Register::R2), 0x9902);
    ASSERT_EQ(cpu.getRegister(Register::R3), 0x9903);
    ASSERT_EQ(cpu.getRegister(Register::R4), 0x9904);
    ASSERT_EQ(cpu.getRegister(Register::R5), 0x9905);
    ASSERT_EQ(cpu.getRegister(Register::R6), 0x9906);
    ASSERT_EQ(cpu.getRegister(Register::R7), 0x9907);
  }

  // Test stack underflow
  {
    const std::vector<Instruction> CODE = {
      { Opcode::MOVI, Register::R0, Cpu::STACK_OFFSET - 8 },
      { Opcode::STSP, SpecialPurposeRegister::SP, Register::R0 },
      { Opcode::POPS },
    };

    Cpu cpu;

    utils::load(cpu.getRam(), Cpu::IP_0, CODE);

    EXPECT_THROW(cpu.tick(3), std::runtime_error);
  }
}

/// Test PUSH opcode
TEST(TestInstructions, testPush)
{
  // Test pushing values
  {
    const std::vector<Instruction> CODE = {
      { Opcode::MOVI, Register::R4, 0x1234 },
      { Opcode::MOVI, Register::R5, 0x5678 },
      { Opcode::PUSH, Register::R4 },
      { Opcode::PUSH, Register::R5 }
    };

    Cpu cpu;

    utils::load(cpu.getRam(), Cpu::IP_0, CODE);

    cpu.tick(4);

    EXPECT_EQ(cpu.getStackPointer(), Cpu::STACK_OFFSET - 2);
    EXPECT_EQ(cpu.getMemory(Cpu::STACK_OFFSET), 0x1234);
    EXPECT_EQ(cpu.getMemory(Cpu::STACK_OFFSET - 1), 0x5678);
  }

  // Test stack overflow
  {
    const std::vector<Instruction> CODE = {
      { Opcode::PUSH, Register::R0 },
      { Opcode::JM, Cpu::IP_0 }
    };

    Cpu cpu;

    utils::load(cpu.getRam(), Cpu::IP_0, CODE);

    cpu.tick(Cpu::STACK_SIZE * 2);

    EXPECT_THROW(cpu.tick(2), std::runtime_error);
  }
}

/// Test PUSHS opcode
TEST(TestInstructions, testPushs)
{
  // Test pushing values
  {
    const std::vector<Instruction> CODE = {
      { Opcode::MOVI, Register::R0, 0x9900 },
      { Opcode::MOVI, Register::R1, 0x9901 },
      { Opcode::MOVI, Register::R2, 0x9902 },
      { Opcode::MOVI, Register::R3, 0x9903 },
      { Opcode::MOVI, Register::R4, 0x9904 },
      { Opcode::MOVI, Register::R5, 0x9905 },
      { Opcode::MOVI, Register::R6, 0x9906 },
      { Opcode::MOVI, Register::R7, 0x9907 },
      { Opcode::SET, static_cast<BYTE>(Flag::ALL) },
      { Opcode::PUSHS },
    };

    Cpu cpu;

    utils::load(cpu.getRam(), Cpu::IP_0, CODE);

    cpu.tick(10);

    EXPECT_EQ(cpu.getStackPointer(), Cpu::STACK_OFFSET - 9);
    EXPECT_EQ(cpu.getMemory(Cpu::STACK_OFFSET - 0), 0x9900);
    EXPECT_EQ(cpu.getMemory(Cpu::STACK_OFFSET - 1), 0x9901);
    EXPECT_EQ(cpu.getMemory(Cpu::STACK_OFFSET - 2), 0x9902);
    EXPECT_EQ(cpu.getMemory(Cpu::STACK_OFFSET - 3), 0x9903);
    EXPECT_EQ(cpu.getMemory(Cpu::STACK_OFFSET - 4), 0x9904);
    EXPECT_EQ(cpu.getMemory(Cpu::STACK_OFFSET - 5), 0x9905);
    EXPECT_EQ(cpu.getMemory(Cpu::STACK_OFFSET - 6), 0x9906);
    EXPECT_EQ(cpu.getMemory(Cpu::STACK_OFFSET - 7), 0x9907);
    EXPECT_EQ(cpu.getMemory(Cpu::STACK_OFFSET - 8), static_cast<WORD>(Flag::ALL));
  }

  // Test stack overflow
  {
    const std::vector<Instruction> CODE = {
      { Opcode::MOVI, Register::R0, Cpu::STACK_OFFSET - Cpu::STACK_SIZE + 8 },
      { Opcode::STSP, SpecialPurposeRegister::SP, Register::R0 },
      { Opcode::PUSHS }
    };

    Cpu cpu;

    utils::load(cpu.getRam(), Cpu::IP_0, CODE);

    EXPECT_THROW(cpu.tick(3), std::runtime_error);
  }
}

/// Test RET opcode
TEST(TestInstructions, testRet)
{
  // Test base case
  {
    constexpr LongPointer RETURN_ADDRESS = Cpu::IP_0 + 4;
    constexpr LongPointer SUBROUTINE_ADDRESS = Cpu::IP_0 + 10;

    const std::vector<Instruction> CODE = {
      { Opcode::NOP },
      { Opcode::CALLM, SUBROUTINE_ADDRESS },
      // IP_0 + 4:
      { Opcode::NOP },
      { Opcode::NOP },
      { Opcode::NOP },
      // IP_0 + 10:
      { Opcode::RET }
    };

    Cpu cpu;

    utils::load(cpu.getRam(), Cpu::IP_0, CODE);

    cpu.tick(2);

    ASSERT_EQ(cpu.getInstructionPointer(), SUBROUTINE_ADDRESS);

    cpu.tick(1);

    EXPECT_EQ(cpu.getInstructionPointer(), RETURN_ADDRESS);
    EXPECT_EQ(cpu.getStackPointer(), Cpu::STACK_OFFSET);
  }

  // Test stack underflow
  {
    const std::vector<Instruction> CODE = {
      { Opcode::RET }
    };

    Cpu cpu;

    utils::load(cpu.getRam(), Cpu::IP_0, CODE);

    EXPECT_THROW(cpu.tick(1), std::runtime_error);
  }
}

/// Test RSV opcode
TEST(TestInstructions, testRsv)
{
  // Test base case
  {
    constexpr WORD STACK_FRAME_SIZE = 0x10;

    const std::vector<Instruction> CODE = {
      { Opcode::RSV, STACK_FRAME_SIZE }
    };

    Cpu cpu;

    utils::load(cpu.getRam(), Cpu::IP_0, CODE);

    cpu.tick(1);

    EXPECT_EQ(cpu.getStackPointer(), Cpu::STACK_OFFSET - STACK_FRAME_SIZE);
  }

  // Test stack overflow
  {
    const std::vector<Instruction> CODE = {
      { Opcode::RSV, static_cast<WORD>(Cpu::STACK_SIZE) },
      { Opcode::RSV, static_cast<WORD>(1) }
    };

    Cpu cpu;

    utils::load(cpu.getRam(), Cpu::IP_0, CODE);

    EXPECT_NO_THROW(cpu.tick(1));
    EXPECT_THROW(cpu.tick(1), std::runtime_error);
  }
}

/// Test RTI opcode
TEST(TestInstructions, testRti)
{
  constexpr LongPointer INTERRUPT_HANDLER = Cpu::IP_0 + 10;
  constexpr WORD FLAGS = 0x0005;

  // Test base case
  {
    // The test program emulates calling an interrupt request handler by pushing an explicit
    // value as the CPU flags that should be restored by the RTI instruction, followed by a
    // regular subroutine call (which will push the return address).
    const std::vector<Instruction> CODE = {
      { Opcode::MOVI, Register::R0, FLAGS },
      { Opcode::PUSH, Register::R0 },
      { Opcode::CALLM, INTERRUPT_HANDLER },
      { Opcode::NOP },
      { Opcode::NOP },
      // INTERRUPT_HANDLER:
      { Opcode::CLR, static_cast<BYTE>(Flag::ALL) },
      { Opcode::RTI },
    };

    Cpu cpu;

    utils::load(cpu.getRam(), Cpu::IP_0, CODE);

    cpu.tick(3);

    EXPECT_EQ(cpu.getStackPointer(), Cpu::STACK_OFFSET - 3);
    EXPECT_EQ(cpu.getInstructionPointer(), INTERRUPT_HANDLER);

    cpu.tick(2);

    EXPECT_EQ(cpu.getStackPointer(), Cpu::STACK_OFFSET);
    EXPECT_EQ(cpu.getInstructionPointer(), Cpu::IP_0 + 6);
    EXPECT_EQ(cpu.getFlags(), FLAGS);
  }

  // Test stack underflow
  {
    const std::vector<Instruction> CODE = {
      { Opcode::MOVI, Register::R0, FLAGS },
      { Opcode::PUSH, Register::R0 },
      { Opcode::CALLM, INTERRUPT_HANDLER },
      { Opcode::NOP },
      { Opcode::NOP },
      // INTERRUPT_HANDLER:
      { Opcode::POP, Register::R0 },
      { Opcode::RTI },
    };

    Cpu cpu;

    utils::load(cpu.getRam(), Cpu::IP_0, CODE);

    ASSERT_THROW(cpu.tick(5), std::runtime_error);
  }
}

/// Test SBB opcode
TEST(TestInstructions, testSbb)
{
  // Test SBB results and flags for all possible combinations of operand signs and carry flag
  {
    using SbbTest = std::tuple<WORD, WORD, bool, WORD, BYTE>;

    const std::vector<SbbTest> SBB_TESTS = {
      { 0xFFFF, 0xFFFE, false, 0x0001, 0 },
      { 0x7FFE, 0xFFFF, false, 0x7FFF, Flag::CARRY },
      { 0xFFFF, 0xFFFF, false, 0x0000, Flag::ZERO },
      { 0xFFFF, 0x7FFF, false, 0x8000, Flag::NEGATIVE },
      { 0xFFFE, 0xFFFF, false, 0xFFFF, Flag::NEGATIVE | Flag::CARRY },
      { 0xFFFE, 0x7FFF, false, 0x7FFF, Flag::OVERFLOW },
      { 0x7FFF, 0xFFFF, false, 0x8000, Flag::OVERFLOW | Flag::NEGATIVE | Flag::CARRY },
      { 0x0001, 0x0000, true,  0x0000, Flag::ZERO },
      { 0x7FFE, 0xFFFE, true,  0x7FFF, Flag::CARRY },
      { 0xFFFF, 0xFFFF, true,  0xFFFF, Flag::NEGATIVE | Flag::CARRY },
      { 0x7FFF, 0xFFFE, true,  0x8000, Flag::OVERFLOW | Flag::NEGATIVE | Flag::CARRY },
      { 0x8000, 0xFFFF, true,  0x8000, Flag::NEGATIVE | Flag::CARRY },
    };

    for (const SbbTest &test : SBB_TESTS)
    {
      const WORD a = std::get<0>(test);
      const WORD b = std::get<1>(test);
      const BYTE carry_flag = (std::get<2>(test) ? Flag::CARRY : 0);
      const WORD expected_result = std::get<3>(test);
      const BYTE expected_flags = std::get<4>(test);

      const std::vector<Instruction> CODE = {
        { Opcode::MOVI, Register::R0, a },
        { Opcode::MOVI, Register::R1, b },
        { Opcode::SET,  carry_flag },
        { Opcode::SBB,  Register::R0, Register::R1 }
      };

      Cpu cpu;

      utils::load(cpu.getRam(), Cpu::IP_0, CODE);

      cpu.tick(4);

      const WORD result = cpu.getRegister(Register::R0);

      EXPECT_EQ(result, expected_result) << fmt::format(
        "SBB operation result test failed, lhs: {0:#06x}, rhs: {1:#06x}, carry flag: {3}, expected result: {2:#04x}, got result: {3:#04x}",
        a, b, expected_result, result);;

      EXPECT_EQ(cpu.getFlags(), expected_flags) << fmt::format(
        "SBB operation CPU flags test failed, lhs: {0:#06x}, rhs: {1:#06x}, carry flag: {3}, expected flags: {2:#04x}, got flags: {3:#04x}",
        a, b, expected_flags, cpu.getFlags());
    }
  }

  // Test multi-word subtraction, implementing 32-bit subtraction using SUB followed by SBB
  {
    // The subtraction calculated here is 0x12341234 - 0x00005678 = 0x1233BBBC, left-hand side & result stored in R1:R0
    const std::vector<Instruction> CODE = {
      { Opcode::MOVI, Register::R0, 0x1234 },
      { Opcode::MOVI, Register::R1, 0x1234 },
      { Opcode::SUBI, Register::R0, 0x5678 },
      { Opcode::SBBI, Register::R1, 0x0000 }
    };

    Cpu cpu;

    utils::load(cpu.getRam(), Cpu::IP_0, CODE);

    cpu.tick(4);

    ASSERT_EQ(0x1233, cpu.getRegister(Register::R1));
    ASSERT_EQ(0xBBBC, cpu.getRegister(Register::R0));
  }
}

/// Test SET opcode
TEST(TestInstructions, testSet)
{
  const std::vector<Instruction> CODE = {
    { Opcode::SET, Flag::CARRY },
    { Opcode::SET, Flag::ZERO },
    { Opcode::SET, Flag::NEGATIVE },
    { Opcode::SET, Flag::OVERFLOW },
    { Opcode::SET, Flag::DISABLE_IRQ },
    { Opcode::CLR, Flag::ALL },
    { Opcode::SET, static_cast<BYTE>(Flag::CARRY | Flag::OVERFLOW | Flag::ZERO) }
  };

  Cpu cpu;

  utils::load(cpu.getRam(), cpu::Cpu::IP_0, CODE);

  constexpr int NUM_FLAGS = 5;

  std::array<std::pair<Flag::Bits, BYTE>, NUM_FLAGS> FLAG_BITS = {{
    { Flag::CARRY, 0b00000001 },
    { Flag::ZERO, 0b00000011 },
    { Flag::NEGATIVE, 0b00000111 },
    { Flag::OVERFLOW, 0b00001111 },
    { Flag::DISABLE_IRQ, 0b10001111 },
  }};

  for (int i = 0; i < NUM_FLAGS; i++)
  {
    cpu.tick(1);

    for (int flags_on = i; flags_on < i+1; flags_on++)
    {
      EXPECT_EQ(cpu.getFlag(FLAG_BITS[flags_on].first), true);
    }

    for (int flags_off = i+1; flags_off < NUM_FLAGS; flags_off++)
    {
      EXPECT_EQ(cpu.getFlag(FLAG_BITS[flags_off].first), false);
    }

    EXPECT_EQ(cpu.getFlags(), FLAG_BITS[i].second);
  }

  cpu.tick(1);

  ASSERT_EQ(cpu.getFlags(), 0);

  cpu.tick(1);

  EXPECT_EQ(cpu.getFlags(), Flag::CARRY | Flag::OVERFLOW | Flag::ZERO);
}

/// Test STSP opcode
TEST(TestInstructions, testStsp)
{
  // Test base case
  {
    constexpr WORD NEW_SP = Cpu::STACK_OFFSET - 100;

    constexpr BYTE IA_MULTIPLIER = 3;
    constexpr BYTE IA_BASE = 5;
    constexpr Register IA_BASE_REG = Register::R0;
    constexpr BYTE IA_OFFSET = 0x10;

    const IndirectAddress IA = { IA_MULTIPLIER, IA_BASE_REG, IA_OFFSET };

    const std::vector<Instruction> CODE = {
      // Store stack pointer
      { Opcode::MOVI, Register::R1, NEW_SP },
      { Opcode::STSP, SpecialPurposeRegister::SP, Register::R1 },
      // Store segment register
      { Opcode::MOVI, Register::R2, 0x0203 },
      { Opcode::STSP, SpecialPurposeRegister::SR, Register::R2 },
      // Test moving data to indirect address in new dst segment
      { Opcode::MOVI, Register::R0, IA_BASE },
      { Opcode::MOVI, Register::R3, 0x1234 },
      { Opcode::MOVE, IA, Register::R3 },
      // Test moving date from indirect address in new src segment
      { Opcode::MOVM, LongPointer(0x03, IA_MULTIPLIER * IA_BASE + IA_OFFSET), Register::R3 },
      { Opcode::MOVE, Register::R4, IA },
    };

    Cpu cpu;

    utils::load(cpu.getRam(), Cpu::IP_0, CODE);

    cpu.tick(2);

    EXPECT_EQ(cpu.getStackPointer(), NEW_SP);

    cpu.tick(2);

    EXPECT_EQ(cpu.getSegmentRegister(), std::make_tuple(0x03, 0x02));

    cpu.tick(3);

    EXPECT_EQ(cpu.getMemory(LongPointer(0x02, IA_MULTIPLIER*IA_BASE + IA_OFFSET)), 0x1234);

    cpu.tick(2);

    EXPECT_EQ(cpu.getRegister(Register::R4), 0x1234);
  }

  // Test underflow when restoring stack pointer
  {
    constexpr WORD NEW_SP = Cpu::STACK_OFFSET - Cpu::STACK_SIZE - 1;

    const std::vector<Instruction> CODE = {
      { Opcode::MOVI, Register::R0, NEW_SP },
      { Opcode::STSP, SpecialPurposeRegister::SP, Register::R0 }
    };

    Cpu cpu;

    utils::load(cpu.getRam(), Cpu::IP_0, CODE);

    EXPECT_THROW(cpu.tick(2), std::runtime_error);
  }
}

/// Test SUB opcode
TEST(TestInstructions, testSub)
{
  using SubTest = std::tuple<WORD, WORD, WORD, BYTE>;

  const std::vector<SubTest> SUB_TESTS = {
    { 0xFFFF, 0xFFFE, 0x0001, 0 },
    { 0x7FFE, 0xFFFF, 0x7FFF, Flag::CARRY },
    { 0xFFFF, 0xFFFF, 0x0000, Flag::ZERO },
    { 0xFFFF, 0x7FFF, 0x8000, Flag::NEGATIVE },
    { 0xFFFE, 0xFFFF, 0xFFFF, Flag::NEGATIVE|Flag::CARRY },
    { 0xFFFE, 0x7FFF, 0x7FFF, Flag::OVERFLOW },
    { 0x7FFF, 0xFFFF, 0x8000, Flag::OVERFLOW|Flag::NEGATIVE|Flag::CARRY },
  };

  for (const SubTest &test : SUB_TESTS)
  {
    const WORD a = std::get<0>(test);
    const WORD b = std::get<1>(test);
    const WORD expected_result = std::get<2>(test);
    const BYTE expected_flags = std::get<3>(test);

    const std::vector<Instruction> CODE = {
      { Opcode::MOVI, Register::R0, a },
      { Opcode::MOVI, Register::R1, b },
      { Opcode::SUB, Register::R0, Register::R1 }
    };

    Cpu cpu;

    utils::load(cpu.getRam(), Cpu::IP_0, CODE);

    cpu.tick(3);

    const WORD result = cpu.getRegister(Register::R0);

    EXPECT_EQ(result, expected_result) << fmt::format(
      "SUB operation result test failed, lhs: {0:#06x}, rhs: {1:#06x}, expected result: {2:#04x}, got result: {3:#04x}",
      a, b, expected_result, result);;

    EXPECT_EQ(cpu.getFlags(), expected_flags) << fmt::format(
      "SUB operation CPU flags test failed, lhs: {0:#06x}, rhs: {1:#06x}, expected flags: {2:#04x}, got flags: {3:#04x}",
       a, b, expected_flags, cpu.getFlags());
  }
}

/// Test WAIT instruction
TEST(TestInstructions, testWait)
{
  constexpr peripheral::Device::ID DUMMY_DEVICE_ID = 0x01;
  constexpr LongPointer WAIT_1 = Cpu::IP_0 + 8;
  constexpr LongPointer WAIT_2 = Cpu::IP_0 + 12;
  constexpr LongPointer IRQ_HANDLER = Cpu::IP_0 + 14;
  constexpr WORD DUMMY_DEVICE_IRQ = (1 << DUMMY_DEVICE_ID);
  constexpr WORD OTHER_IRQ = 0x04;
  constexpr WORD TEST_VALUE = 0x1234;

  const std::vector<Instruction> CODE = {
    { Opcode::MOVI, Register::R0, IRQ_HANDLER.segment },
    { Opcode::MOVI, Register::R1, IRQ_HANDLER.offset},
    { Opcode::MOVM, Cpu::IRQ_TABLE_OFFSET + 2, Register::R0 },
    { Opcode::MOVM, Cpu::IRQ_TABLE_OFFSET + 3, Register::R1 },
    // WAIT_1:
    { Opcode::WAIT, static_cast<WORD>(DUMMY_DEVICE_IRQ | OTHER_IRQ) },
    { Opcode::MOVI, Register::R3, TEST_VALUE },
    // WAIT_2:
    { Opcode::WAIT, static_cast<WORD>(OTHER_IRQ) },
    // IRQ_HANDLER:
    { Opcode::MOVI, Register::R0, 0 },
    { Opcode::OUTI, PortMove(DUMMY_DEVICE_ID, DummyDevice::IRQ_CONTROL, Register::R0) },
    { Opcode::RTI },
  };

  Cpu cpu;

  DummyDevice dummy_device(DUMMY_DEVICE_ID);
  cpu.connectDevice(&dummy_device);

  utils::load(cpu.getRam(), Cpu::IP_0, CODE);

  // First tick away for a few cycles without any interrupts pending. This should block the CPU at the WAIT instruction
  cpu.tick(100);

  ASSERT_EQ(cpu.getInstructionPointer(), WAIT_1);
  ASSERT_EQ(cpu.getRegister(Register::R3), 0);

  // Now assert an IRQ, step three instructions. This should call the IRQ service routine, return, and step over
  // the WAIT instruction.
  dummy_device.assertIrq();

  cpu.tick(4);

  ASSERT_EQ(cpu.getInstructionPointer(), WAIT_1 + 4);
  ASSERT_EQ(cpu.getRegister(Register::R3), TEST_VALUE);
  ASSERT_FALSE(dummy_device.pendingInterrupts());

  // Now block again on the second WAIT instruction, which has a different IRQ operand than the one the dummy
  // device asserts. The CPU should remain blocked.
  cpu.tick(1);

  dummy_device.assertIrq();

  cpu.tick(10);

  ASSERT_EQ(cpu.getInstructionPointer(), WAIT_2);
  ASSERT_FALSE(dummy_device.pendingInterrupts());
}

/// Test XOR instruction
TEST(TestInstructions, testXor)
{
  const std::vector<Instruction> CODE = {
    { Opcode::SET, static_cast<BYTE>(0x0F) },
    { Opcode::MOVI, Register::R0, 0b0101010101010101 },
    { Opcode::XORI, Register::R0, 0b1010101010101010 },
    { Opcode::SET, static_cast<BYTE>(0x0F) },
    { Opcode::MOVI, Register::R1, 0b1111111111111111 },
    { Opcode::XORI, Register::R1, 0b1111111111111111 },
  };

  Cpu cpu;

  utils::load(cpu.getRam(), Cpu::IP_0, CODE);

  cpu.tick(3);

  ASSERT_EQ(cpu.getRegister(Register::R0), 0b1111111111111111);
  ASSERT_EQ(cpu.getFlags(), Flag::NEGATIVE);

  cpu.tick(3);

  ASSERT_EQ(cpu.getRegister(Register::R1), 0);
  ASSERT_EQ(cpu.getFlags(), Flag::ZERO);
}

}
