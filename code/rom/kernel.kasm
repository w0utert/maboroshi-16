#include "defines.kasm"
#include "lib.kasm"

:code (0x00:0xa000)

kernel {
  init:
  {
    ; Map GPU page 1 to system page 5
    mov r0, #0x0215
    bank r0

    ; Set source segment to 0 (kernel data is in zero-page), destination segment to 5 (mapped GPU memory)
    mov r0, #0x0500
    stsp sr, r0

    ; Save some useful constant GPU state to simplify accessing it
    in $GPU.DEVICE, $GPU.LAYER_0_NUM_TILES_X, r0
    mov @::kernel::tile_map_width, r0

    in $GPU.DEVICE, $GPU.LAYER_0_NUM_TILES_Y, r0
    mov @::kernel::tile_map_height, r0

    ; Setup console tile map
    mov r0, #3
    out $GPU.DEVICE, $GPU.LAYER_0_FOREGROUND_COLOR, r0

    mov r0, #0x20
    out $GPU.DEVICE, $GPU.LAYER_0_TILE_MODE, r0

    ; Write some text to console
    mov r1, #0

    mov r0, @::kernel::welcome_0.offset
    mov r2, #0x0118
    call @::kernel::lib::puts

    mov r0, @::kernel::welcome_1.offset
    mov r2, #0x030A
    call @::kernel::lib::puts

    ; Set v-blank interrupt handler and enable GPU v-blank
    mov r0, @::kernel::vblank::entry.segment
    mov r1, @::kernel::vblank::entry.offset
    mov ($SYS.IRQ_2_VECTOR_SEGMENT), r0
    mov ($SYS.IRQ_2_VECTOR_OFFSET), r1

    mov r0, #0x0100
    out $GPU.DEVICE, $GPU.IRQ_CONTROL, r0

    ; Set timer interrupt handler and configure timer device. We setup 3 timers 
    ; with 1 minute (3600 frames), 1 second (60 frames) and one-tenth of a second intervals
    mov r0, @::kernel::timer::entry.segment
    mov r1, @::kernel::timer::entry.offset
    mov ($SYS.IRQ_3_VECTOR_SEGMENT), r0
    mov ($SYS.IRQ_3_VECTOR_OFFSET), r1

    mov r0, #3600
    out $TIMER.DEVICE, $TIMER.TIMER_0_INTERVAL, r0
    mov r0, $TIMER.TIMER_CONTROL_MODE_REPEAT
    out $TIMER.DEVICE, $TIMER.TIMER_0_CONTROL, r0

    mov r0, #60
    out $TIMER.DEVICE, $TIMER.TIMER_1_INTERVAL, r0
    mov r0, $TIMER.TIMER_CONTROL_MODE_REPEAT
    out $TIMER.DEVICE, $TIMER.TIMER_1_CONTROL, r0

    mov r0, #6
    out $TIMER.DEVICE, $TIMER.TIMER_2_INTERVAL, r0
    mov r0, $TIMER.TIMER_CONTROL_MODE_REPEAT
    out $TIMER.DEVICE, $TIMER.TIMER_2_CONTROL, r0

    mov r0, #0x0700
    out $TIMER.DEVICE, $TIMER.IRQ_CONTROL, r0

    ret
  }

  main:
  {
  loop:
    wait $GPU.VBLANK_IRQ_BIT
    j @loop
  }

  ; V-Blank interrupt handler
  vblank {
    entry:
      push r0
      push r1

      ;
      in $GPU.DEVICE, $GPU.LAYER_0_TILE_MAP_OFFSET, r0
      add r0, #512

      mov r1, @::timers::minutes
      add r1, #48
      mov [r0], r1

      mov r1, @::timers::seconds
      add r1, #48
      mov [r0 + 1], r1

      mov r1, @::timers::tenths
      add r1, #48
      mov [r0 + 2], r1

      mov r0, @::kernel::foreground_color
      out $GPU.DEVICE, $GPU.LAYER_0_FOREGROUND_COLOR, r0
      ; mov r0, @::kernel::background_color

      in $CONTROLLERS.DEVICE, $CONTROLLERS.CONTROLLER_0_STATE, r0
      out $GPU.DEVICE, $GPU.LAYER_0_BACKGROUND_COLOR, r0

      ; Acknowledge interrupt
      in $GPU.DEVICE, $GPU.IRQ_CONTROL, r0
      and r0, #0xFF00
      out $GPU.DEVICE, $GPU.IRQ_CONTROL, r0

      pop r1
      pop r0
      rti
  }

  ; Timer interrupt handler
  timer {
    entry:
      push r0
      push r1

      ; Handle pending timers, one-by-by one
    minutes:
      in $TIMER.DEVICE, $TIMER.IRQ_CONTROL, r0
      and r0, #1
      b z, @seconds

      mov r0, @::timers::minutes
      inc r0
      and r0, #0x3F
      mov @::timers::minutes, r0

    seconds:
      in $TIMER.DEVICE, $TIMER.IRQ_CONTROL, r0
      and r0, #2
      b z, @tenths

      mov r0, @::timers::seconds
      inc r0
      and r0, #0x3F
      mov @::timers::seconds, r0

      mov r0, @::kernel::foreground_color
      inc r0
      and r0, #0x0F
      mov @::kernel::foreground_color, r0

    tenths:
      in $TIMER.DEVICE, $TIMER.IRQ_CONTROL, r0
      and r0, #4
      b z, @acknowledge

      mov r0, @::timers::tenths
      inc r0
      and r0, #0x3F
      mov @::timers::tenths, r0
      
    acknowledge:
      in  $TIMER.DEVICE, $TIMER.IRQ_CONTROL, r0
      and r0, #0xFF00
      out $TIMER.DEVICE, $TIMER.IRQ_CONTROL, r0

      pop r1
      pop r0
      rti
  }
}

:code 

; Kernel code entry point
main:
{
  call @::kernel::init
  call @::kernel::main

  halt
}

:data (0x00:0xd000)

kernel {
  tile_map_width:
    .word 0x0
  tile_map_height:
    .word 0x0

  welcome_0:
    .string "MABOROSHI-16!"

  welcome_1:
    .string "not much you can do from here yet :-("

  foreground_color:
    .word 0x0
  background_color:
    .word 0x0
}

timers {
  minutes:
    .word 0x0
  seconds:
    .word 0x0
  tenths: 
    .word 0x0
}

