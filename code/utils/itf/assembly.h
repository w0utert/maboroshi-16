/// Header file for the Assembly class, which is used to hold assembled code and data chunks.
///
/// Assemblies are produced by the kumitate assembler, and can be loaded into system
/// memory directly, using the load() function implemented in the utils namespace.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#pragma once

#include "core/common/itf/types.h"

#include "core/cpu/itf/instruction.h"

#include <boost/bimap.hpp>
#include <boost/bimap/multiset_of.hpp>

#include <optional>
#include <iostream>
#include <fstream>
#include <vector>

namespace utils {

/// Type alias for disassemblies
using Disassembly = std::vector<std::pair<core::LongPointer, std::string>>;

/// Class to hold a collection of assembled code and data chunks.
///
/// The Assembly is produced by assembling a source stream comprises one or more chunks,
/// where each chunk contains the binary representation of a sequence of instructions
/// or binary data, plus the offset in RAM the section should be loaded at.
///
/// In addition to the generated code/data section, an assembly can optionally hold
/// debug information such as label addresses, file/line number information for the
/// addresses covered by the assembly, namespace start/end ranges. When reading/writing
/// the assembly, this debug information is persisted, for example to improve disassembly.
class Assembly
{
  public:
    /// Structure type to represent a chunk of consecutive assembled instructions
    struct CodeChunk
    {
      /// Base address for code chunk
      core::LongPointer base;
      /// Assembled instructions
      std::vector<core::cpu::Instruction> instructions;
    };

    /// Structure type to represent a chunk of assembled byte data
    struct DataChunk
    {
      /// Base address for data chunk
      core::LongPointer base;
      /// Assembled data
      std::vector<core::WORD> words;
    };

    /// Structure to hold debug information
    class DebugInfo
    {
      public:
        /// Structure to hold line info
        struct LineInfo
        {
          /// File index into the file list
          int file;
          /// Line number
          int line;
        };

        /// Get file list entry for the passed \p index.
        ///
        /// \param index file list entry to get
        /// \return file list for the passed \p index
        /// \throw std::runtime_error if the passed \p index is out of range
        std::string getFile(const unsigned int index) const;

        /// Get index of file in file list, adding a new entry if it is not yet in there.
        ///
        /// \param filename file name for which to get index
        /// \return index of file with the passed name
        int getFileIndex(const std::string &filename);

        /// \return file list
        const std::vector<std::string> &getFileList() const { return _fileList; }

        /// Set \p label \p address.
        ///
        /// The label is required to be unique, setting the same label multiple times is not allowed.
        ///
        /// \param label label
        /// \param address label address
        /// \throw std::runtime_error if the \p label is duplicate
        void setLabel(const std::string &label, const core::LongPointer &address);

        /// \return address associated with the passed label
        /// \throw std::runtime_error if the debug info for this assembly does not contain the passed \p label
        core::LongPointer getLabelAddress(const std::string &label) const;

        /// Lookup labels associated with the passed \p address.
        ///
        /// Note that multiple labels can be associated with the same address!
        ///
        /// \param address address for which to look up labels
        /// \return labels associated with the passed \p address, if any.
        std::vector<std::string> getLabels(const core::LongPointer &address) const;

        /// Get all registered labels.
        ///
        /// \note the labels are returned as a multiset mapping addresses to labels, as multiple
        ///   labels may be associated with the same address.
        ///
        /// \return addresses of all registered labels
        std::multimap<core::LongPointer, std::string> getLabels() const;

        /// Set line info.
        ///
        /// \param line address
        /// \param fileIndex index of the file at the time the line was parsed
        /// \param lineNumber line number into the parsed file at the time the line was parsed
        void setLineInfo(const core::LongPointer &address, const int fileIndex, const int lineNumber);

        /// Return line info for the passed \p address.
        ///
        /// \param address addrees for which to get line info
        /// \return line info for the passed \p address
        /// \throw std::runime_error if there is no line info for the passed \p address
        LineInfo getLineInfo(const core::LongPointer &address) const;

        /// \return line info for all known addresses
        const std::map<core::LongPointer, LineInfo> &getLineInfo() const { return _lineInfo; }

        /// Set namespace start/end address range.
        ///
        /// \param ns namespace name
        /// \param start start address
        /// \param end end address (exclusive)
        void setNamespace(const std::string &ns, const core::LongPointer &start, const core::LongPointer &end);

        /// Return the smallest namespace that contains the passed \p address.
        ///
        /// \return smallest namespace that contains the passed \p address, or \a nullopt if the
        ///   address is in the global namespace or not covered by the assembly debug information at all
        std::optional<std::string> getNamespace(const core::LongPointer &address) const;

        /// \return namespace start/end addresses
        std::map<std::string, std::pair<core::LongPointer, core::LongPointer>> getNamespaces() const;

      private:
        using LabelMap = boost::bimap<boost::bimaps::set_of<std::string>, boost::bimaps::multiset_of<core::LongPointer>>;

        std::vector<std::string> _fileList;
        LabelMap _labels;
        std::map<core::LongPointer, LineInfo> _lineInfo;
        std::vector<std::tuple<std::string, core::LongPointer, core::LongPointer>> _namespaces;
    };

    /// Constructor
    ///
    /// \param codeChunks assembly code chunks
    /// \param dataChunks assembly data chunks
    /// \param debugInfo optional debug information
    Assembly(const std::vector<CodeChunk> &codeChunks, const std::vector<DataChunk> &dataChunks, std::optional<DebugInfo> debugInfo = std::nullopt)
    :
      _codeChunks(codeChunks), _dataChunks(dataChunks), _debugInfo(debugInfo)
    {
    }

    /// Write assembly to output stream, in binary format.
    ///
    /// \param out output stream to write the assembly to
    void write(std::ostream &out) const;

    /// Get code chunk that contains the passed address
    ///
    /// \param address for which to get code chunk
    /// \return code chunk that contains the passed \p address, or nullptr if the passed \p address is not
    ///   covered by any code chunk in the
    const CodeChunk *getCodeChunk(const core::LongPointer &address) const;

    /// \return code chunks
    const std::vector<CodeChunk> &getCodeChunks() const { return _codeChunks; }

    /// \return data chunks
    const std::vector<DataChunk> &getDataChunks() const { return _dataChunks; }

    /// \return debug info
    const Assembly::DebugInfo *getDebugInfo() const { return (_debugInfo.has_value() ? &_debugInfo.value() : nullptr); }

    /// Read assembly (in binary format) from input stream
    ///
    /// \param in input stream to read the assembly from
    /// \throw std::runtime_error if reading the assembly from the stream failed
    static Assembly fromInputStream(std::istream &in);

  private:
    std::vector<CodeChunk> _codeChunks;
    std::vector<DataChunk> _dataChunks;
    std::optional<DebugInfo> _debugInfo;
};

/// Disassemble code chunk
///
/// The disassembly is currently just a list of (address, disassembly text) pairs, which
/// can be used for debugging purposes. It is not intended for (re-) building purposes at
/// this time. The disassembly will be clipped to only valid addresses (addresses covered
/// by a code chunk) of this Assembly instance.
///
/// \param chunk code chunk to disaassemble
/// \param debugInfo optional debug info for e.g. looking up labels
/// \return disassembly of instructions in the range [address - before, address + after],
Disassembly disassemble(const Assembly::CodeChunk &chunk, const Assembly::DebugInfo * const debugInfo = nullptr);

}
