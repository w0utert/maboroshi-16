/// This is the dreaded 'utils.h' that pops up in almost every project, a grab-bag of random things
/// that fall through the cracks, escaping our efforts to categorize them more sensibly.
///
/// For now, the utils namespace is completely flat, once the diversity of things that end up here
/// grows unwieldy, it should be considered to split it and add some hierarchy.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#pragma once

#include "assembly.h"

#include "core/common/itf/types.h"
#include "core/cpu/itf/instruction.h"

#include "tools/kumitate/itf/assembler.h"

#include <optional>

namespace utils {

/// Structure used for RGBA image data.
///
/// \note even though the name of this class implies the pixel data is layed out in memory in RGBA
///   format, this is not actually true. The in-memory pixel format is actually ARGB, ie: the alpha
///   channel occupies the most-significant byte of a pixel.
struct ImageRgba
{
  /// 32-bit RGBA color tuple
  struct RGBA8888
  {
    /// Zero-construct color tuple
    constexpr RGBA8888()
    :
      rgba(0)
    {
    }

    /// Construct color tuple from 8-bit RGBA channels.
    ///
    /// \param r red channel
    /// \param g green channel
    /// \param b blue channel
    /// \param a alpha channel
    constexpr RGBA8888(const core::BYTE &r, const core::BYTE &g, const core::BYTE &b, const core::BYTE &a = 0xFF)
    :
      rgba((a << 24) | (r << 16) | (g << 8) | b)
    {
      /// \return alpha channel (bits 24-31 of the color tuple)
    }

    constexpr core::BYTE a() const { return (rgba & 0xFF000000) >> 24; }
    /// \return red channel (bits 16-23 of the color tuple)
    constexpr core::BYTE r() const { return (rgba & 0x00FF0000) >> 16; }
    /// \return green channel (bits 8-15 of the color tuple)
    constexpr core::BYTE g() const { return (rgba & 0x0000FF00) >>  8; }
    /// \return blue channel (bits 0-7 of the color tuple)
    constexpr core::BYTE b() const { return (rgba & 0x000000FF) >>  0; }

    /// Color tuple as 32-bit word, in ARGB order (alpha channel occupies MSB)
    core::DWORD rgba;
  };

  /// Construct empty image of specified dimensions.
  ///
  /// \param width image width in pixels
  /// \param height image height in pixels
  ImageRgba(const unsigned int width, const unsigned int height)
  :
    width(width), height(height), data(std::vector<RGBA8888>(width * height))
  {
  }

  /// Image width in pixels
  unsigned int width;
  /// Image height in pixels
  unsigned int height;

  /// Image pixel data
  std::vector<RGBA8888> data;
};

/// Load \p code specified as a vector of core::Cpu::Instruction into \p memory at \p address.
///
/// \param memory memory to load the code into
/// \param address address into the passed memory at which to load the code
/// \param code code to load
/// \throw std::out_of_range if the assembly contains sections that are outside the valid
///    address range of the passed \p memory
void load(std::vector<core::WORD> &memory, const core::LongPointer address, const std::vector<core::cpu::Instruction> &code);

/// Load assembly into memory.
///
/// Loading an assembly into memory will copy its code and data chunks to their correct locations in
/// memory and resolve any external symbols the assembly may refer to.
///
/// \param memory memory to load the code into
/// \param assembly the assembly to load
/// \throw std::out_of_range if the assembly contains sections that are outside the valid
///    address range of the passed \p memory
void load(std::vector<core::WORD> &memory, const Assembly &assembly);

/// Dump memory to binary file for debugging purposes.
///
/// \note the maboroshi-16 system uses big-endian byte order, so the 16-bit machine words written
///   to the binary file by this function will be byte-swapped compared to a straight-up dump
///   of the host system memory if that uses little-endian byte order (which is almost definitely
///   the case).
///
/// \param filename file to dump to, will be overwritten
/// \param base base address to write, if not specified 0x0:0x0000 (start of memory) is assumed
/// \param numWords maximum number of 16-bit machine words to write, if not specified all memory from the
///   base address upward will be written
/// \throw std::runtime_error if the file could not be written
void memoryDump(
  const std::string &filename,
  const std::vector<core::WORD> &memory,
  const std::optional<core::LongPointer> &base = std::nullopt,
  const std::optional<size_t> numWords = std::nullopt);

/// Write RGB data to an lossless PNG file, for example to create screen dumps for debugging and benchmarking.
///
/// \note this is an 'unsafe' interface that does not do any kind of bounds checking on the input data, and
///   does not check for any unexpected libpng errors. Use at your own peril!
///
/// \note this currently completely ignores the alpha channel, writing an RGB (not RGBA) image instead, the
///  reason being that this function is currently only used to dump GPU frames, which don't use the alpha channel.
///
/// \param filename file to write to
/// \param image the RGBA
/// \throw std::runtime_error if the file could not be written
void savePng(const std::string &filename, const ImageRgba &image);

/// Load RGBA data from PNG image.
///
/// \note this is primarily intended for debugging purposes, and as such only supports the image
///   format written by the savePng function (ie: 24-bit RGB data)
///
/// \param filename PNG filename
/// \return loaded image data
/// \throw std::runtime_error if the PNG file could not be opened or does not have the expected format
ImageRgba loadPng(const std::string &filename);

/// Calculate image diff, for benchmarking and debugging purposes.
///
/// The diff is calculated by taking XOR-ing the RGBA channels of the two input images.
///
/// \param image0 first image for diff
/// \param image1 second image for diff
/// \return difference image, ie: image0 ^ image1
ImageRgba imageDiff(const ImageRgba &image0, const ImageRgba &image1);

}
