/// Header file for the TileSet class, which is used to load tiles from PNG images.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#include "core/common/itf/types.h"

#include "utils/itf/palette.h"

#include <vector>
#include <array>
#include <memory>

namespace utils {

/// Preliminary tile set interface class.
///
/// This is currently basically a wrapper around some libpng loading code to
/// read tile data from a PNG, and to provide a simple interface to get tile
/// data for e.g. writing it to byte data when preprocessing .kasm assembly
/// source files with #tiles directives.
class TileSet
{
  public:
    /// Valid tile sizes for both dimensions. Any width x height combination of these is allowed, e.g. 8x16, 16x16, 32x8, etc.
    static constexpr std::array<int, 3> VALID_TILE_SIZES = { 8, 16, 32 };

    /// Constructor.
    ///
    /// \param tileWidth tile width in pixels
    /// \param tileHeight tile height in pixels
    /// \throw std::runtime_error if the tile dimensions are invalid/unsupported
    TileSet(const uint8_t tileWidth, const uint8_t tileHeight);

    /// Return number of tiles.
    ///
    /// \return number or files.
    size_t getNumTiles() const { return _tiles.size(); }

    /// Return tile size.
    ///
    /// \return tile size.
    std::pair<uint8_t, uint8_t> getTileSize() const { return _tileSize; }

    /// Return 256-color palette for the tile set.
    ///
    /// \return 256-color palette for the tile set
    const Palette &getPalette() const { return _palette; }

    /// Get tile data for tile with the passed index.
    ///
    /// The tile data is stored in row-major order.
    ///
    /// \param tile tile index
    /// \return tile data with the passed index.
    /// \throw std::runtime_error if the tile index is out of range
    const uint8_t *getTileData(const unsigned int tile) const;

    /// Factory method that loads a tile set from a PNG file.
    ///
    /// The PNG file is required to contain an image with a size that is an exact multiple
    /// of the passed \p tileWidth and \p tileHeight. The number of tiles in the images is
    /// directly derived from the image size.
    ///
    /// \param filename PNG file containing the tiles
    /// \param tileWidth tile width
    /// \param tileHeight tile height
    /// \return tile set tile set loaded from the PNG file
    /// \throw std::runtime_error if loading tile data failed (file not found, file format
    ///   error, unsupported tile size, image size not a muliple of tile size, ...)
    static std::unique_ptr<TileSet> fromPng(const std::string &filename, const uint8_t tileWidth, const uint8_t tileHeight);

    /// Load 256-color palette from PNG image.
    ///
    /// \param filename PNG file containing the tiles
    /// \return palette read from PNG image
    /// \throw std::runtime_error if loading the palette failed (file not found, file
    ///    format error, ...)
    static Palette paletteFromPng(const std::string &filename);

  private:
    std::pair<uint8_t, uint8_t> _tileSize;
    Palette _palette;
    std::vector<std::vector<uint8_t>> _tiles;
};

}