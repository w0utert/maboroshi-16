/// Declares and defines data structures for storing palette data.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#pragma once

#include <vector>

namespace utils {

/// Simple 256-color palette structure
struct Palette
{
  /// A palette always has 256 colors, even if they are not all required/used
  static constexpr int NUM_COLORS = 256;

  /// Color type that represents a color with a 1-bit alpha channel and 8-bit RGB channels.
  struct ARGB1888
  {
    /// 1-bit alpha channel
    bool alpha;

    /// 8-bit red channel
    uint8_t red;
    /// 8-bit green channel
    uint8_t green;
    /// 8-bit blue channel
    uint8_t blue;

    /// Construct with 1-bit alpha channel, and 8 bit red, green and blue channels.
    ///
    /// \param alpha 1-bit alpha channel
    /// \param red 8-bit red channel
    /// \param green 8-bit green channel
    /// \param blue 8-bit blue channel
    ARGB1888(const bool alpha = true, const uint8_t red = 0, const uint8_t green = 0, const uint8_t blue = 0)
    :
      alpha(alpha), red(red), green(green), blue(blue)
    {
    }

    /// Pack color value to ARGB1555 format.
    ///
    /// ARGB1555 is the packed format used for all maboroshi-16 color values.
    ///
    /// \return packed ARGB1555 color
    uint16_t pack() const
    {
      return (alpha << 15) | ((red >> 3) << 9) | ((green >> 3) << 5) | (blue >> 3);
    }
  };

  /// Construct empty (zeroed) 256-color palette.
  Palette()
  {
    data.resize(NUM_COLORS);
  }

  /// Palette data for 256 colors
  std::vector<ARGB1888> data;
};

}