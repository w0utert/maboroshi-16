/// Header file that declares stream operators for streamable types.
///
/// These are separated from the header files that define the actual types, to prevent circular dependencies.
///
/// \note not all stream operators have been move here, and those that are only needed once (from a single source file) may never be.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#pragma once

#include <ostream>

namespace core {

struct LongPointer;
struct IndirectAddress;

std::ostream &operator<<(std::ostream &out, const core::LongPointer &p);
std::ostream &operator<<(std::ostream &out, const core::IndirectAddress &ia);

}

namespace core::cpu {

enum class Operation;
enum class BranchCondition;
enum class Register;
enum class SpecialPurposeRegister;

std::ostream &operator<<(std::ostream &out, const Operation operation);
std::ostream &operator<<(std::ostream &out, const BranchCondition condition);
std::ostream &operator<<(std::ostream &out, const Register &r);
std::ostream &operator<<(std::ostream &out, const SpecialPurposeRegister &r);

}

