/// Implements the Assembly class, which is used to represent collections of
/// assembled code and data chunks.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#include "core/common/itf/types.h"

#include "utils/itf/assembly.h"
#include "utils/itf/stream_operators.h"

#include <boost/endian/conversion.hpp>
#include <boost/algorithm/string.hpp>

//
// Helper functions and constants used for reading & writing assemblies
//

namespace {

// MABO file magic word
constexpr core::DWORD MABO_MAGIC_WORD = 0x4d41424f;
// Magic byte that indicates the start of a code section in a MABO file
constexpr core::BYTE CODE_CHUNK_MAGIC_BYTE = 0x01;
// Magic byte that indicates the start of a data section in a MABO file
constexpr core::BYTE DATA_CHUNK_MAGIC_BYTE = 0x02;
// Magic byte that indicates the start of a debug info section in a MABO file
constexpr core::BYTE DEBUG_INFO_MAGIC_BYTE = 0xFF;

// Write BYTE, WORD or DWORD value to stream using big-endian byte order.
template<typename T>
typename std::enable_if<std::disjunction<std::is_same<T, core::BYTE>, std::is_same<T, core::WORD>, std::is_same<T, core::DWORD>>::value, void>::type
assembly_write(std::ostream &out, const T value)
{
  const T value_big = boost::endian::native_to_big(value);

  out.write(reinterpret_cast<const char *>(&value_big), sizeof(T));
}

// Write word data to stream.
//
// Word data is written in big-endian byte order.
void assembly_write(std::ostream &out, const std::vector<core::WORD> &words)
{
  for (const core::WORD &w : words)
  {
    assembly_write(out, w);
  }
}

// Write long pointer to stream.
//
// String data is written as a (segment : BYTE, offset : WORD) pair
void assembly_write(std::ostream &out, const core::LongPointer &p)
{
  assembly_write<core::BYTE>(out, p.segment);
  assembly_write<core::WORD>(out, p.offset);
}

// Write string to stream
//
// String data is written as ASCII character data prefixed by the string length
void assembly_write(std::ostream &out, const std::string &s)
{
  assembly_write<core::WORD>(out, s.size());

  std::copy(s.begin(), s.end(), std::ostream_iterator<char>(out));
}

// Read BYTE, WORD or DWORD value in big-endian byte order from stream, and return as value in host byte order
template<typename T>
typename std::enable_if<std::disjunction<std::is_same<T, core::BYTE>, std::is_same<T, core::WORD>, std::is_same<T, core::DWORD>>::value, T>::type
assembly_read(std::istream &in)
{
  T value_big;

  in.read(reinterpret_cast<char *>(&value_big), sizeof(T));

  return boost::endian::big_to_native(value_big);
}

// Read word data from input stream.
//
// The input stream is assumed to contain word data in big-endian byte order, a
// conversion to host byte order is performed on each word received from the input
// stream.
template<typename T>
typename std::enable_if<std::is_same<T, std::vector<core::WORD>>::value, T>::type
assembly_read(std::istream &in, const size_t size)
{
  std::vector<core::WORD> words(size);

  for (size_t i = 0; i < size; i++)
  {
    words[i] = assembly_read<core::WORD>(in);
  }

  return words;
}

// Read string from input stream
template<typename T>
typename std::enable_if<std::is_same<T, std::string>::value, T>::type
assembly_read(std::istream &in)
{
  const auto len = assembly_read<core::WORD>(in);

  std::string s(len, '\0');
  in.read(s.data(), len);

  return s;
};

// Read long pointer from input stream
template<typename T>
typename std::enable_if<std::is_same<T, core::LongPointer>::value, T>::type
assembly_read(std::istream &in)
{
  const auto segment = assembly_read<core::BYTE>(in);
  const auto offset = assembly_read<core::WORD>(in);

  return { segment, offset };
};

}

namespace utils {

//
// Assembly::DebugInfo implementation
//

// Get file list entry for the passed  index.
std::string Assembly::DebugInfo::getFile(const unsigned int index) const
{
  if (index >= _fileList.size())
  {
    throw std::runtime_error(fmt::format("File list index {0} out of range", index));
  }

  return _fileList[index];
}

// Get index of file in file list, adding a new entry if it is not yet in there.
int Assembly::DebugInfo::getFileIndex(const std::string &filename)
{
  const auto file = std::find(_fileList.begin(), _fileList.end(), filename);

  if (file == _fileList.end())
  {
    _fileList.push_back(filename);
    return _fileList.size() - 1;
  }

  return std::distance(_fileList.begin(), file);
}

// Set label address.
void Assembly::DebugInfo::setLabel(const std::string &label, const core::LongPointer &address)
{
  if (_labels.left.count(label) != 0)
  {
    throw std::runtime_error(fmt::format("Duplicate symbol '{0}'", label));
  }

  _labels.insert({ label, address });
}

// Return labels associated with the passed address
std::vector<std::string> Assembly::DebugInfo::getLabels(const core::LongPointer &address) const
{
  std::vector<std::string> labels;

  const auto labels_for_address = _labels.right.equal_range(address);

  if (labels_for_address.first != _labels.right.end())
  {

    for (auto it = labels_for_address.first; it != labels_for_address.second; it++)
    {
      labels.push_back(it->get_left());
    }
  }

  return labels;
}

// Get all registered labels
std::multimap<core::LongPointer, std::string> Assembly::DebugInfo::getLabels() const
{
  std::multimap<core::LongPointer, std::string> labels;

  for (const auto &label_address : _labels)
  {
    const std::string label = label_address.get_left();
    const core::LongPointer address = label_address.get_right();

    labels.insert({ address, label });
  }

  return labels;
}

// Return address associated with the passed label, if any
core::LongPointer Assembly::DebugInfo::getLabelAddress(const std::string &label) const
{
  const auto label_address = _labels.left.find(label);

  if (label_address == _labels.left.end())
  {
    throw std::runtime_error(fmt::format("Invalid label '{0}'", label));
  }

  return label_address->get_right();
}

// Set line info.
void Assembly::DebugInfo::setLineInfo(const core::LongPointer &address, const int fileIndex, const int lineNumber)
{
  _lineInfo[address] = { fileIndex, lineNumber };
}

// Return line info for the passed address.
Assembly::DebugInfo::LineInfo Assembly::DebugInfo::getLineInfo(const core::LongPointer &address) const
{
  const auto line_info = _lineInfo.find(address);

  if (line_info == _lineInfo.end())
  {
    throw std::runtime_error(fmt::format("No line info available for address {0}", address));
  }

  return line_info->second;
}

// Set namespace start/end address range.
void Assembly::DebugInfo::setNamespace(const std::string &ns, const core::LongPointer &start, const core::LongPointer &end)
{
  _namespaces.push_back({ ns, start, end });
}

// Return the smallest namespace that contains the passed \p address.
std::optional<std::string> Assembly::DebugInfo::getNamespace(const core::LongPointer &address) const
{
  // Search backward to find last-opened namespace that contains the address. By construction,
  // this will also be the smallest namespace that contains the address, since nameespaces
  // are not allowed to partially overlap.
  //
  // Note:
  // Not a huge fan of the linear search here, but the typical number of namespaces is expected
  // to stay relatively low, and the range test itself is very fast, so it is not expected a much
  // more complicated data structure like an interval tree would actually outperform this for
  // real-world cases.
  const auto ns = std::find_if(_namespaces.rbegin(), _namespaces.rend(), [address](const auto &t)
  {
    const core::LongPointer &start = std::get<1>(t);
    const core::LongPointer &end= std::get<2>(t);

    return (start <= address) && (address <= end);
  });

  return (ns != _namespaces.rend() ? std::make_optional<std::string>(std::get<0>(*ns)) : std::nullopt);
}

// Return namespace start/end addresses
std::map<std::string, std::pair<core::LongPointer, core::LongPointer>> Assembly::DebugInfo::getNamespaces() const
{
  std::map<std::string, std::pair<core::LongPointer, core::LongPointer>> namespaces;

  for (const auto &[ns, start, end] : _namespaces)
  {
    namespaces[ns] = { start, end };
  }

  return namespaces;
}


//
// Assembly class implementation
//

// Write assembly to output stream
void Assembly::write(std::ostream &out) const
{
  // Write assembly header magic word
  assembly_write<core::DWORD>(out, MABO_MAGIC_WORD);

  // Write code chunks
  for (const CodeChunk &code_chunk : _codeChunks)
  {
    assembly_write<core::BYTE>(out, CODE_CHUNK_MAGIC_BYTE);

    assembly_write<core::BYTE>(out, code_chunk.base.segment);
    assembly_write<core::WORD>(out, code_chunk.base.offset);

    assembly_write<core::DWORD>(out, code_chunk.instructions.size());

    for (const core::cpu::Instruction &instruction : code_chunk.instructions)
    {
      assembly_write<core::DWORD>(out, instruction.bits);
    };
  }

  // Write data chunks
  for (const DataChunk &data_chunk : _dataChunks)
  {
    assembly_write<core::BYTE>(out, DATA_CHUNK_MAGIC_BYTE);

    assembly_write<core::BYTE>(out, data_chunk.base.segment);
    assembly_write<core::WORD>(out, data_chunk.base.offset);

    assembly_write<core::DWORD>(out, data_chunk.words.size());

    assembly_write(out, data_chunk.words);
  }

  // Write debug info
  if (_debugInfo)
  {
    assembly_write<core::BYTE>(out, DEBUG_INFO_MAGIC_BYTE);

    // File list
    assembly_write<core::WORD>(out, _debugInfo->getFileList().size());

    for (const std::string &file : _debugInfo->getFileList())
    {
      assembly_write(out, file);
    }

    // Labels
    const std::multimap<core::LongPointer, std::string> labels = _debugInfo->getLabels();

    assembly_write<core::DWORD>(out, labels.size());

    for (const auto &[address, label] : labels)
    {
      assembly_write(out, label);
      assembly_write(out, address);
    }

    // Line info
    const std::map<core::LongPointer, DebugInfo::LineInfo> line_info = _debugInfo->getLineInfo();

    assembly_write<core::DWORD>(out, line_info.size());

    for (const auto &[address, line_info] : line_info)
    {
      assembly_write(out, address);
      assembly_write<core::WORD>(out, line_info.file);
      assembly_write<core::WORD>(out, line_info.line);
    }

    // Namespace start/end addresses
    const std::map<std::string, std::pair<core::LongPointer, core::LongPointer>> namespaces = _debugInfo->getNamespaces();

    assembly_write<core::WORD>(out, namespaces.size());

    for (const auto &[ns, range] : namespaces)
    {
      assembly_write(out, ns);
      assembly_write(out, range.first);
      assembly_write(out, range.second);
    }
  }
}

// Get code chunk covering the passed address
const Assembly::CodeChunk *Assembly::getCodeChunk(const core::LongPointer &address) const
{
  const auto chunk = std::find_if(_codeChunks.begin(), _codeChunks.end(), [address](const CodeChunk &chunk)
  {
    return (chunk.base <= address) && (address < (chunk.base + chunk.instructions.size() * core::cpu::Instruction::size()));
  });

  return (chunk != _codeChunks.end() ? &*chunk : nullptr);
}

// Read assembly from input stream
Assembly Assembly::fromInputStream(std::istream &in)
{
  const auto mabo_magic_word = assembly_read<core::DWORD>(in);

  if (mabo_magic_word != MABO_MAGIC_WORD)
  {
    throw std::runtime_error("did not get MABO file header magic word from stream");
  }

  // Parse chunks & other sections
  std::vector<CodeChunk> code_chunks;
  std::vector<DataChunk> data_chunks;
  std::optional<DebugInfo> debug_info;

  while (in.peek() != EOF)
  {
    const auto section_magic_byte = assembly_read<core::BYTE>(in);

    switch (section_magic_byte)
    {
      case CODE_CHUNK_MAGIC_BYTE:
      {
        const auto segment = assembly_read<core::BYTE>(in);
        const auto offset = assembly_read<core::WORD>(in);

        const auto num_instructions = assembly_read<core::DWORD>(in);

        std::vector<core::cpu::Instruction> instructions(num_instructions);
        std::generate_n(instructions.begin(), num_instructions, [&in]() -> core::cpu::Instruction
        {
          return core::cpu::Instruction(assembly_read<core::DWORD>(in));
        });

        code_chunks.push_back({ core::LongPointer(segment, offset), instructions });

        break;
      }

      case DATA_CHUNK_MAGIC_BYTE:
      {
        const auto segment = assembly_read<core::BYTE>(in);
        const auto offset = assembly_read<core::WORD>(in);

        const auto size = assembly_read<core::DWORD>(in);

        const auto words = assembly_read<std::vector<core::WORD>>(in, size);

        data_chunks.push_back({ core::LongPointer(segment, offset), words });

        break;
      }

      case DEBUG_INFO_MAGIC_BYTE:
      {
        debug_info = DebugInfo();

        const auto file_list_len = assembly_read<core::WORD>(in);

        for (unsigned int i = 0; i < file_list_len; i++)
        {
          debug_info->getFileIndex(assembly_read<std::string>(in));
        }

        const auto labels_len = assembly_read<core::DWORD>(in);

        for (unsigned int i = 0; i < labels_len; i++)
        {
          const auto label = assembly_read<std::string>(in);
          const auto address = assembly_read<core::LongPointer>(in);

          debug_info->setLabel(label, address);
        }

        const auto line_info_len = assembly_read<core::DWORD>(in);

        for (unsigned int i = 0; i < line_info_len; i++)
        {
          const auto address = assembly_read<core::LongPointer>(in);
          const auto file_index = assembly_read<core::WORD>(in);
          const auto line = assembly_read<core::WORD>(in);

          debug_info->setLineInfo(address, file_index, line);
        }

        const auto namespaces_len = assembly_read<core::WORD>(in);

        for (unsigned int i = 0; i < namespaces_len; i++)
        {
          const auto ns = assembly_read<std::string>(in);
          const auto start = assembly_read<core::LongPointer>(in);
          const auto end = assembly_read<core::LongPointer>(in);

          debug_info->setNamespace(ns, start, end);
        }

        break;
      }

      default:
        throw std::runtime_error("invalid chunk magic byte");
    }
  }

  return Assembly(code_chunks, data_chunks, debug_info);
}

// Disassemble instructions in passed range
Disassembly disassemble(const Assembly::CodeChunk &chunk, const Assembly::DebugInfo * const debugInfo)
{
  using Instruction = core::cpu::Instruction;
  using Layout = Instruction::Layout;
  using OperandType = Layout::OperandType;

  Disassembly disassembly;

  // Lookup possible label associated with the passed address, and if found format and return it
  const auto lookup_label = [debugInfo](const core::LongPointer &address, const core::LongPointer &fromAddress) -> std::optional<std::string>
  {
    // No debug info, no label
    if (!debugInfo)
    {
      return std::nullopt;
    }

    const std::vector<std::string> labels = debugInfo->getLabels(address);

    std::optional<std::string> label;

    if (labels.size() == 1)
    {
      // Only a single label associated with the address, just return it
      label = labels[0];
    }
    else if (labels.size() > 1)
    {
      // Multiple labels associated with the address. We need to disambiguate. The rule is: if
      // any of the resolved labels is defined in the same namespace as the instruction that
      // referred to tha associated address, pick one arbitrarily. If none of the resolved
      // labels is in the same namespace, prioritize non-anonymous namespaces over anonymous
      // namespaces.
      const std::string from_namespace = debugInfo->getNamespace(fromAddress).value_or("::");

      for (size_t i = 0; i < labels.size(); i++)
      {
        const bool same_namespace = boost::algorithm::starts_with(labels[i], from_namespace);
        const bool is_anonymous = (labels[i].find('~') != std::string::npos);

        if (label->empty() || same_namespace || !is_anonymous)
        {
          label = labels[i];
        }
      }
    }

    // Label found for address. Format and return it
    if (label)
    {
      // If the label is local (defined in an anonymous namespace), strip off the namespace part
      const bool is_local = (label->find('~') != std::string::npos);

      return fmt::format("@{0}", (is_local ? label->substr(label->rfind(':') + 1) : *label));
    }

    // No label associated with address
    return std::nullopt;
  };

  for (size_t instruction_index = 0; instruction_index < chunk.instructions.size(); instruction_index++)
  {
    const Instruction &instruction = chunk.instructions[instruction_index];
    const core::LongPointer address = chunk.base + instruction_index * Instruction::size();

    // Lookup instruction layout for the instruction opcode, and based on that extract
    // the correct operands from the instruction bitfield and format them for printing
    const auto &layout = Instruction::INSTRUCTION_LAYOUTS.find(instruction.opcode);

    std::vector<std::string> operands;

    if (layout != Instruction::INSTRUCTION_LAYOUTS.end())
    {
      switch (layout->second)
      {

        case Layout(OperandType::REG):
          operands = { fmt::format("{0}", instruction.unaryOperand.as<core::cpu::Register>()) };
          break;

        case Layout(OperandType::SP_REG):
          operands = { fmt::format("{0}", instruction.unaryOperand.as<core::cpu::SpecialPurposeRegister>()) };
          break;

        case Layout(OperandType::IMM16):
          operands = { fmt::format("#{0:#06x} ({0})", instruction.immediate.as<core::WORD>()) };
          break;

        case Layout(OperandType::FLAGS):
          operands = { fmt::format("{0:#04x}", instruction.unaryOperand.as<core::BYTE>()) };
          break;

        case Layout(OperandType::LONGPOINTER):
        {
          operands = { lookup_label(instruction.longPointer(), address).value_or(fmt::format("({0})", instruction.longPointer())) };
          break;
        }

        case Layout(OperandType::INDIRECT_ADDRESS):
          operands = { fmt::format("[{0}]", instruction.indirectAddress()) };
          break;

        case Layout(OperandType::VECTOR):
          operands = { fmt::format("<{0}>", instruction.longPointer()) };
          break;

        case Layout(OperandType::INDIRECT_PORT_MOVE):

          operands = {
            fmt::format("#{0}",instruction.portMove.device.as<core::BYTE>()),
            fmt::format("{0}", instruction.portMove.port.as<core::cpu::Register>()),
            fmt::format("{0}", instruction.portMove.reg.as<core::cpu::Register>())
          };

          break;

        case Layout(OperandType::DIRECT_PORT_MOVE):

          operands = {
            fmt::format("#{0}",instruction.portMove.device.as<core::BYTE>()),
            fmt::format("#{0:#06x}", instruction.portMove.port.as<core::WORD>()),
            fmt::format("{0}", instruction.portMove.reg.as<core::cpu::Register>())
          };

          break;

        case Layout(OperandType::BRANCH_CONDITION, OperandType::RELATIVE_ADDRESS):
        {
          const auto branch_distance = instruction.relativeAddress.as<int16_t>();
          const auto destination = (address + Instruction::size()) + branch_distance * Instruction::size();

          operands = {
            fmt::format("{0}", instruction.branchCondition.as<core::cpu::BranchCondition>()),
            lookup_label(destination, address).value_or(fmt::format("{0:#06x} ({1})", static_cast<core::WORD>(branch_distance), branch_distance))
          };

          break;
        }

        case Layout(OperandType::REG, OperandType::REG):

          operands = {
            fmt::format("{0}", instruction.binaryOperands.o0.as<core::cpu::Register>()),
            fmt::format("{0}", instruction.binaryOperands.o1.as<core::cpu::Register>()),
          };

          break;

        case Layout(OperandType::REG, OperandType::IMM16):

          operands = {
            fmt::format("{0}", instruction.binaryOperands.o0.as<core::cpu::Register>()),
            fmt::format("#{0:#06x} ({0})", instruction.immediate.as<core::WORD>())
          };

          break;

        case Layout(OperandType::REG, OperandType::SP_REG):

          operands = {
            fmt::format("{0}", instruction.binaryOperands.o0.as<core::cpu::Register>()),
            fmt::format("{0}", instruction.binaryOperands.o1.as<core::cpu::SpecialPurposeRegister>()),
          };

          break;

        case Layout(OperandType::SP_REG, OperandType::REG):

          operands = {
            fmt::format("{0}", instruction.binaryOperands.o0.as<core::cpu::SpecialPurposeRegister>()),
            fmt::format("{0}", instruction.binaryOperands.o1.as<core::cpu::Register>()),
          };

          break;

        case Layout(OperandType::REG, OperandType::IMM16, OperandType::DIRECTION):

          operands = {
            fmt::format("{0}", instruction.binaryOperands.o0.as<core::cpu::Register>()),
            fmt::format("#{0:#06x} ({0})", instruction.immediate.as<core::WORD>())
          };

          if (instruction.binaryOperands.direction != core::cpu::Instruction::Direction::DST_MEM)
          {
            std::swap(operands[0], operands[1]);
          }

          break;

        case Layout(OperandType::REG, OperandType::LONGPOINTER, OperandType::DIRECTION):

          operands = {
            fmt::format("{0}", instruction.binaryOperands.o0.as<core::cpu::Register>()),
            lookup_label(instruction.longPointer(), address).value_or(fmt::format("({0})", instruction.longPointer()))
          };

          if (instruction.binaryOperands.direction != core::cpu::Instruction::Direction::DST_MEM)
          {
            std::swap(operands[0], operands[1]);
          }

          break;

        case Layout(OperandType::REG, OperandType::INDIRECT_ADDRESS, OperandType::DIRECTION):

          operands = {
            fmt::format("{0}", instruction.binaryOperands.o0.as<core::cpu::Register>()),
            fmt::format("[{0}]", instruction.indirectAddress())
          };

          if (instruction.binaryOperands.direction != core::cpu::Instruction::Direction::DST_MEM)
          {
            std::swap(operands[0], operands[1]);
          }

          break;

        case Layout(OperandType::REG, OperandType::REG, OperandType::MODIFIER):

          operands = {
            fmt::format("{0}", instruction.binaryOperands.o0.as<core::cpu::Register>()),
            fmt::format("{0}", instruction.binaryOperands.o1.as<core::cpu::Register>()),
            fmt::format("{0}", instruction.binaryOperands.modifier.as<bool>())
          };

          break;

        case Layout(OperandType::REG, OperandType::IMM16, OperandType::MODIFIER):

          operands = {
            fmt::format("{0}", instruction.binaryOperands.o0.as<core::cpu::Register>()),
            fmt::format("#{0:06x} ({0})", instruction.immediate.as<core::WORD>()),
            fmt::format("{0}", instruction.binaryOperands.modifier.as<bool>())
          };

          break;

        case Layout(OperandType::REG, OperandType::LONGPOINTER, OperandType::MODIFIER):

          operands = {
            fmt::format("{0}", instruction.binaryOperands.o0.as<core::cpu::Register>()),
            lookup_label(instruction.longPointer(), address).value_or(fmt::format("({0})", core::LongPointer())),
            fmt::format("{0}", instruction.binaryOperands.modifier.as<bool>())
          };

          break;

        case Layout(OperandType::REG, OperandType::INDIRECT_ADDRESS, OperandType::MODIFIER):

          operands = {
            fmt::format("{0}", instruction.binaryOperands.o0.as<core::cpu::Register>()),
            fmt::format("[{0}]", instruction.indirectAddress()),
            fmt::format("{0}", instruction.binaryOperands.modifier.as<bool>())
          };

          break;

        case Layout(OperandType::REG, OperandType::REG, OperandType::REG, OperandType::REG):

          operands = {
            fmt::format("{0}", instruction.quaternaryOperands.o0.as<core::cpu::Register>()),
            fmt::format("{0}", instruction.quaternaryOperands.o1.as<core::cpu::Register>()),
            fmt::format("{0}", instruction.quaternaryOperands.o2.as<core::cpu::Register>()),
            fmt::format("{0}", instruction.quaternaryOperands.o3.as<core::cpu::Register>())
          };

          break;

        default:
          break;
      };
    }

    const std::string instruction_disassembly = fmt::format("{0}{1}{2}", instruction, (operands.empty() ? "" : " "), boost::algorithm::join(operands, ", "));

    disassembly.push_back({ chunk.base + instruction_index*core::cpu::Instruction::size(), instruction_disassembly });
  }

  return disassembly;
}

}
