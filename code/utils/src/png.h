/// Declares and defines the PngState class, which is used by the PNG-related
/// functions implemented in the utils namespace to initialize and access PNG
/// image files using libpng.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#ifndef UTILS_PNG_H_INCLUDED
#define UTILS_PNG_H_INCLUDED

#include "utils/itf/palette.h"

#include "fmt/format.h"

#include <png.h>

namespace utils {

/// Utility structure to initialize & track libpng read state.
///
/// Note that this is in no way intended to be any kind of object-oriented wrapper
/// around libpng, by itself it does not do anything useful with the PNG data after
/// opening the file and checking the PNG signature. The only purpose of this class
/// is to add RAII semantics to ensure any allocated libpng state is cleaned up after
/// use, and remove some duplication from the libpng-based functions in the utils
/// namespace. Also note that this structure is only useful for libpng state for
/// reading PNG files, not for writing them.
class PngState
{
  public:
    struct Header
    {
      uint32_t width;
      uint32_t height;
      int bitDepth;
      int colorType;
    };

    PngState(const std::string &filename)
    :
      png(nullptr), info(nullptr), header({}), _fp(nullptr)
    {
      try
      {
        // Initialize libpng
        png = png_create_read_struct(PNG_LIBPNG_VER_STRING, nullptr, nullptr, nullptr);
        info = (png != nullptr ? png_create_info_struct(png) : nullptr);

        if (!info)
        {
          throw std::string("libpng initialization error");
        }

        // Open the file
        _fp = std::fopen(filename.c_str(), "rb");

        if (!_fp)
        {
          throw std::string("Failed to open file");
        }

        // Check PNG signature
        uint8_t png_sig[8] = {};
        std::fread(png_sig, 8, 1, _fp);

        if (png_sig_cmp(png_sig, 0, 8) != 0)
        {
          throw std::runtime_error("Not a PNG file");
        }

        // Read PNG header and data
        png_init_io(png, _fp);
        png_set_sig_bytes(png, 8);

        png_read_png(png, info, PNG_TRANSFORM_PACKING, nullptr);
        png_get_IHDR(png, info, &header.width, &header.height, &header.bitDepth, &header.colorType, nullptr, nullptr, nullptr);

        // If the PNG uses an indexed-color format, read its palette data
        if (header.colorType == PNG_COLOR_TYPE_PALETTE)
        {
          png_color *png_palette;
          uint8_t *transparency;
          int num_colors;
          int num_trans;

          png_get_PLTE(png, info, reinterpret_cast<png_colorp *>(&png_palette), &num_colors);
          png_get_tRNS(png, info, reinterpret_cast<png_bytep *>(&transparency), &num_trans, nullptr);

          // Create 256-color palette structure and copy the PNG palette data into it. Note that the PNG palette may contain less then 256 colors.
          for (int i = 0; i < num_colors; i++)
          {
            const bool alpha = (i >= num_trans) || (transparency[i] != 0);

            palette.data[i] = { alpha, png_palette[i].red, png_palette[i].green, png_palette[i].blue };
          }
        }
      }
      catch (const std::string &error)
      {
        throw std::runtime_error(fmt::format("Failed to open PNG file {0}: {1}", filename, error));
      }
    }

    ~PngState()
    {
      if (info)
      {
        png_free_data(png, info, PNG_FREE_ALL, -1);
      }

      if (_fp)
      {
        fclose(_fp);
      }
    }

    png_structp png;
    png_infop info;

    Header header;
    Palette palette;

  private:
    FILE *_fp;
};


}

#endif
