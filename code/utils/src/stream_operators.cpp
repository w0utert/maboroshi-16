/// Implements stream operators for most streamable types
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#include "utils/itf/stream_operators.h"

#include "core/common/itf/types.h"

#include "core/cpu/itf/cpu.h"
#include "core/cpu/itf/opcodes.h"

#include "fmt/format.h"

namespace core {

// Output stream operator for long pointers
std::ostream &operator<<(std::ostream &out, const core::LongPointer &p)
{
  out << fmt::format("{0:#04x}:{1:#06x}", p.segment, p.offset);

  return out;
}

// Output stream operator for indirect addresses
std::ostream &operator<<(std::ostream &out, const core::IndirectAddress &ia)
{
  std::string multiplier_str;

  if (ia.multiplier == 0)
  {
    multiplier_str = fmt::format("0.");
  }
  else if (ia.multiplier != 1)
  {
    multiplier_str = fmt::format("{0}*", ia.multiplier);
  }

  if (ia.offset == 0)
  {
    out << fmt::format("{0}{1}", ia.base, multiplier_str);
  }
  else
  {
    out << fmt::format("{0}{1}{2}{3}", multiplier_str, ia.base, (ia.backward ? " - " : " + "), fmt::format("{0}", ia.offset));
  }

  return out;
}

}

namespace core::cpu {

// Output stream operator for Operation enumeration values.
std::ostream &operator<<(std::ostream &out, const Operation operation)
{
  static const std::unordered_map<Operation, std::string> OPERATIONS = {
    { Operation::ADC   ,  "adc" },
    { Operation::ADD   ,  "add" },
    { Operation::AND   ,  "and" },
    { Operation::ASR   ,  "asr" },
    { Operation::B     ,  "b" },
    { Operation::BANK  ,  "bank" },
    { Operation::CALL  ,  "call" },
    { Operation::CLR   ,  "clr" },
    { Operation::CMP   ,  "cmp" },
    { Operation::DEC   ,  "dec" },
    { Operation::DIV   ,  "div" },
    { Operation::DIVS  ,  "divs" },
    { Operation::HALT  ,  "halt" },
    { Operation::IN    ,  "in" },
    { Operation::INC   ,  "inc" },
    { Operation::J     ,  "j" },
    { Operation::LDSP  ,  "ldsp" },
    { Operation::LSL   ,  "lsl" },
    { Operation::LSR   ,  "lsr" },
    { Operation::MOV   ,  "mov" },
    { Operation::MOVH  ,  "movh" },
    { Operation::MOVL  ,  "movl" },
    { Operation::MUL   ,  "mul" },
    { Operation::MULS  ,  "muls" },
    { Operation::NEG   ,  "neg" },
    { Operation::NOP   ,  "nop" },
    { Operation::NOT   ,  "not" },
    { Operation::OR    ,  "or" },
    { Operation::OUT   ,  "out" },
    { Operation::POP   ,  "pop" },
    { Operation::POPS  ,  "pops" },
    { Operation::PUSH  ,  "push" },
    { Operation::PUSHS ,  "pushs" },
    { Operation::RET   ,  "ret" },
    { Operation::RTI   ,  "rti" },
    { Operation::RSV   ,  "rsv" },
    { Operation::SBB   ,  "sbb" },
    { Operation::SET   ,  "set" },
    { Operation::STSP  ,  "stsp" },
    { Operation::SUB   ,  "sub" },
    { Operation::WAIT  ,  "wait" },
    { Operation::XOR   ,  "xor" },
  };

  const auto mnemonic = OPERATIONS.find(operation);

  out << (mnemonic != OPERATIONS.end() ? mnemonic->second : "???");

  return out;
}

// Output stream operator for BranchCondition enumeration values.
std::ostream &operator<<(std::ostream &out, const BranchCondition condition)
{
  static const std::unordered_map<BranchCondition, std::string> BRANCH_CONDITIONS = {
    { BranchCondition::Z   , "z" },
    { BranchCondition::NZ  , "nz" },
    { BranchCondition::O   , "o" },
    { BranchCondition::NO  , "no" },
    { BranchCondition::C   , "c" },
    { BranchCondition::NC  , "nc" },
    { BranchCondition::G   , "g" },
    { BranchCondition::GE  , "ge" },
    { BranchCondition::L   , "l" },
    { BranchCondition::LE  , "le" },
    { BranchCondition::UG  , "ug" },
    { BranchCondition::UGE , "uge" },
    { BranchCondition::UL  , "ul" },
    { BranchCondition::ULE , "ule" },
  };

  out << BRANCH_CONDITIONS.find(condition)->second;

  return out;
}

// Output stream operator for CPU register enumeration values
std::ostream &operator<<(std::ostream &out, const Register &r)
{
  static const std::unordered_map<Register, std::string> REG_NAMES = {
    { Register::R0, "r0" },
    { Register::R1, "r1" },
    { Register::R2, "r2" },
    { Register::R3, "r3" },
    { Register::R4, "r4" },
    { Register::R5, "r5" },
    { Register::R6, "r6" },
    { Register::R7, "r7" },
  };

  out << REG_NAMES.find(r)->second;

  return out;
}

// Output stream operator for special-purpose CPU register enumeration values
std::ostream &operator<<(std::ostream &out, const SpecialPurposeRegister &r)
{
  static const std::unordered_map<SpecialPurposeRegister, std::string> REG_NAMES = {
    { SpecialPurposeRegister::SP, "sp" },
    { SpecialPurposeRegister::SR, "sr" },
  };

  out << REG_NAMES.find(r)->second;

  return out;
}

}
