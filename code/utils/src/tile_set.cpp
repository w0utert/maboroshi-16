/// Implements the TileSet class, which is used to load tiles from PNG images.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#include "utils/itf/tile_set.h"

#include "png.h"

#include "fmt/format.h"

#include <cstdio>

namespace utils {

// Construct empty tile set
TileSet::TileSet(const uint8_t tileWidth, const uint8_t tileHeight)
:
  _tileSize({ tileWidth, tileHeight })
{
  if (std::find(VALID_TILE_SIZES.begin(), VALID_TILE_SIZES.end(), tileWidth) == VALID_TILE_SIZES.end() ||
      std::find(VALID_TILE_SIZES.begin(), VALID_TILE_SIZES.end(), tileHeight) == VALID_TILE_SIZES.end())
  {
    throw std::runtime_error("Unsupported tile size, tile width/height can only be 8/16/32 pixels");
  }
}

// Return tile data for tile with the passed index
const uint8_t *TileSet::getTileData(const unsigned int tile) const
{
  if (tile >= _tiles.size())
  {
    throw std::runtime_error(fmt::format("Tile index {0} out of range", tile));
  }

  return _tiles[tile].data();
}

// Return palette
std::unique_ptr<TileSet> TileSet::fromPng(const std::string &filename, const uint8_t tileWidth, const uint8_t tileHeight)
{
  auto tile_set = std::make_unique<TileSet>(tileWidth, tileHeight);

  try
  {
    auto png = PngState(filename);

    // Check if the PNG file it is an 8-bit indexed color file with the expected dimensions
    if (((png.header.width % tileWidth) != 0) && ((png.header.height % tileHeight) != 0))
    {
      throw std::string("Tile image size not an exact multiple of tile size");
    }

    if ((png.header.bitDepth > 8) || (png.header.colorType != PNG_COLOR_TYPE_PALETTE))
    {
      throw std::string("Only 1/2/4/8-bit indexed color format is supported");
    }

    const int num_tile_rows = png.header.height / tileHeight;
    const int num_tile_cols = png.header.width / tileWidth;
    const int num_tiles = num_tile_rows * num_tile_cols;

    // Read image data
    tile_set->_tiles.resize(num_tiles);

    const png_bytepp row_pointers = png_get_rows(png.png, png.info);

    for (int i = 0; i < num_tile_rows; i++)
    {
      for (int j = 0; j < num_tile_cols; j++)
      {
        const int tile_index = i*num_tile_cols + j;

        std::vector<uint8_t> &tile_data = tile_set->_tiles[tile_index];

        tile_data.resize(tileWidth * tileHeight);

        // tx and ty are the tile output pixel coordinates
        for (int ty = 0; ty < tileHeight; ty++)
        {
          for (int tx = 0; tx < tileWidth; tx++)
          {
            // ix and iy are the PNG image coordinates of the current pixel
            const int ix = j * tileWidth + tx;
            const int iy = i * tileHeight + ty;

            tile_data[ty * tileWidth + tx] = row_pointers[iy][ix];
          }
        }
      }
    }
  }
  catch (const std::string &msg)
  {
    throw std::runtime_error(fmt::format("Failed to load TileSet from PNG file {0}: {1}", filename, msg));
  }

  return tile_set;
}

// Read palette data from PNG image
Palette TileSet::paletteFromPng(const std::string &filename)
{
  try
  {
    // Open PNG and move out & return the palette data
    return PngState(filename).palette;
  }
  catch (const std::string &msg)
  {
    throw std::runtime_error(fmt::format("Failed to load palette from PNG file {0}: {1}", filename, msg));
  }
}

}