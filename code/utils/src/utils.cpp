/// Implements functionality declared in the utils namespace
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#include "utils/itf/utils.h"

#include "png.h"

#include "fmt/format.h"

#include <boost/endian/conversion.hpp>

#include <stdexcept>
#include <algorithm>
#include <cstdio>

using namespace core;
using namespace core::cpu;

namespace utils {

// Load code into memory
void load(std::vector<core::WORD> &memory, const core::LongPointer address, const std::vector<core::cpu::Instruction> &code)
{
  const size_t num_instructions = code.size();
  const size_t code_size = num_instructions * cpu::Instruction::WORD_SIZE;

  if ((address.linear() + code_size) > memory.size())
  {
    throw std::out_of_range(fmt::format("Cannot load code at {0:#04x}:{1:#06x} with size of {2}W: exceeds RAM size of {2}W",
      address.segment, address.offset, code_size, memory.size()));
  }

  auto p = memory.begin() + address.linear();

  for (const Instruction &instruction : code)
  {
    *p++ = instruction.low;
    *p++ = instruction.high;
  }
}

// Load assembly into memory
void load(std::vector<core::WORD> &memory, const Assembly &assembly)
{
  // Load code sections
  for (const Assembly::CodeChunk &code_chunk : assembly.getCodeChunks())
  {
    load(memory, code_chunk.base, code_chunk.instructions);
  }

  // Load data sections
  for (const Assembly::DataChunk &data_chunk : assembly.getDataChunks())
  {
    if ((data_chunk.base.linear() + data_chunk.words.size()) >= memory.size())
    {
      throw std::out_of_range(fmt::format("Cannot load data at {0:#04x}:{1:#06x} with size of {2}W: exceeds RAM size of {2}W",
        data_chunk.base.segment, data_chunk.base.offset, data_chunk.words.size(), memory.size()));
    }

    std::copy(data_chunk.words.begin(), data_chunk.words.end(), memory.begin() + data_chunk.base.linear());
  }
}

// Dump memory to file
void memoryDump(const std::string &filename, const std::vector<core::WORD> &memory, const std::optional<core::LongPointer> &base, const std::optional<size_t> numWords)
{
  std::ofstream out_file(filename, std::ios_base::out | std::ios_base::trunc | std::ios_base::binary);

  if (!out_file.is_open())
  {
    throw std::runtime_error(fmt::format("Failed to open file '{0}' for writing", filename));
  }

  const size_t start = base.value_or(core::LongPointer(0x0, 0x0000)).linear();
  const size_t end = start + std::min(start + numWords.value_or(memory.size()), memory.size());

  std::for_each(memory.begin() + start, memory.begin() + end, [&out_file](const core::WORD &word)
  {
    const core::WORD word_big = boost::endian::native_to_big(word);

    out_file.write(reinterpret_cast<const char *>(&word_big), sizeof(core::WORD));
  });

  out_file.close();
}

// Save RGBA data to lossless PNG image
void savePng(const std::string &filename, const ImageRgba &image)
{
  FILE * const png_file = std::fopen(filename.c_str(), "wb");

  if (!png_file)
  {
    throw std::runtime_error(fmt::format("Failed to open file {0} for writing PNG", filename));
  }

  png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, nullptr, nullptr, nullptr);
  png_infop info_ptr = (png_ptr != nullptr ? png_create_info_struct(png_ptr) : nullptr);

  if (!info_ptr)
  {
    throw std::runtime_error(fmt::format("libpng error writing PNG file {0}", filename));
  }

  png_init_io(png_ptr, png_file);

  png_set_IHDR(
    png_ptr, info_ptr, image.width, image.height, 8,
    PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE,
    PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

  png_write_info(png_ptr, info_ptr);

  std::vector<png_byte> dst_row(image.width * 3);

  for (unsigned int row = 0; row < image.height; row++)
  {
    const ImageRgba::RGBA8888 *src_row = &image.data[row * image.width];

    for (unsigned int x = 0; x < image.width; x++)
    {
      dst_row[3*x + 0] = src_row[x].r();
      dst_row[3*x + 1] = src_row[x].g();
      dst_row[3*x + 2] = src_row[x].b();
    }

    png_write_row(png_ptr, dst_row.data());
  }

  png_write_end(png_ptr, nullptr);

  png_free_data(png_ptr, info_ptr, PNG_FREE_ALL, -1);
  png_destroy_write_struct(&png_ptr, &info_ptr);

  fclose(png_file);
}

// Read RGB image data from PNG file
ImageRgba loadPng(const std::string &filename)
{
  const PngState png(filename);

  const unsigned int width = png.header.width;
  const unsigned int height= png.header.height;

  const int color_type = png.header.colorType;

  if ((png.header.bitDepth != 8) || ((color_type != PNG_COLOR_TYPE_RGB) && (color_type != PNG_COLOR_TYPE_RGBA)))
  {
    throw std::runtime_error("Not an 8-bit RGB or RGBA PNG file");
  }

  const bool has_alpha = (color_type == PNG_COLOR_TYPE_RGBA);

  utils::ImageRgba image(width, height);

  const png_bytepp src_rows = png_get_rows(png.png, png.info);

  for (unsigned int row = 0; row < height; row++)
  {
    const png_bytep src_row = src_rows[row];
    ImageRgba::RGBA8888 * const dst_row = &image.data[row * width];

    png_bytep src_pixel = src_row;

    for (unsigned int col = 0; col < width; col++)
    {
      dst_row[col] = ImageRgba::RGBA8888(src_pixel[0], src_pixel[1], src_pixel[2]);
      src_pixel += (has_alpha ? 4 : 3);
    }
  }

  return image;
}

// Calculate image diff (bitwise XOR between RGBA channels)
ImageRgba imageDiff(const ImageRgba &image0, const ImageRgba &image1)
{
  if ((image0.width != image1.width) || (image0.height != image1.height))
  {
    throw std::runtime_error("Cannot create image diff: images have different width and/or height");
  }

  ImageRgba diff(image0.width, image0.height);

  std::transform(image0.data.begin(), image0.data.end(), image1.data.begin(), diff.data.begin(), [](const auto &lhs, const auto &rhs)
  {
    /// \todo temporary ignore alpha channel as it appears to be bugged in the PNG read/write code
    return ImageRgba::RGBA8888(lhs.r() ^ rhs.r(), lhs.g() ^ rhs.g(), lhs.b() ^ rhs.b(), 0); // lhs.a() ^ rhs.a());
  });

  return diff;
}

}
