/// Header file for the debugger user interface classes.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#pragma once

#include "core/common/itf/types.h"

#include "utils/itf/assembly.h"

#include <optional>

namespace ui {

/// Implements a bare-bones ImGUI memory viewer
class MemoryView
{
  public:
  static constexpr unsigned int PAGE_SIZE = 1024;

  void render(core::cpu::Cpu &cpu, const utils::Assembly &assembly);

  private:
  std::optional<uint64_t> _lastBreak;
  core::LongPointer _memoryAddress;
  std::optional<std::array<core::WORD, PAGE_SIZE>> _previousMemoryPage;
  std::optional<std::array<core::WORD, PAGE_SIZE>> _memoryPage;
};

/// Implements a disassembly & CPU state view
class DebuggerView
{
  public:
  bool render(core::cpu::Cpu &cpu, const utils::Assembly &assembly);

  std::optional<uint64_t> _lastBreak;
  std::optional<core::LongPointer> _disassemblyAddress;
  std::optional<utils::Disassembly> _disassembly;
  std::optional<core::cpu::Cpu::State> _previousCpuState;
  std::optional<core::cpu::Cpu::State> _cpuState;
};

}
