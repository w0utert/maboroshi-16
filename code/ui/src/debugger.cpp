/// Implements debugger user interface classes.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#include "ui/itf/debugger.h"

#include <imgui/imgui.h>

namespace ui {

// Render memory view
void MemoryView::render(core::cpu::Cpu &cpu, const utils::Assembly &assembly)
{
  static const ImVec4 HIGHLIGHT_COLOR = { 1.0f, 1.0f, 0.0f, 1.0f };
  static const ImVec4 DIMMED_COLOR = { 0.5f, 0.5f, 0.5f, 1.0f };

  // Disassemble when no disassembly is availble or whenever the CPU tick count changed
  if (!_memoryPage || !_lastBreak || (_lastBreak != cpu.getPerformanceCounter()))
  {
    _lastBreak = cpu.getPerformanceCounter();

    _previousMemoryPage.swap(_memoryPage);
    _memoryPage.emplace();

    const std::vector<core::WORD> &ram = cpu.getRam();

    std::copy(&ram[_memoryAddress.linear()], &ram[_memoryAddress.linear() + PAGE_SIZE], _memoryPage->begin());
  }

  ImGui::Begin("Memory viewer", nullptr, ImGuiWindowFlags_NoResize);

  if (ImGui::IsWindowCollapsed())
  {
    return;
  }

  constexpr float MEMORY_VIEW_HEIGHT = 530.0f;

  ImGui::BeginChild("words", ImVec2(MEMORY_VIEW_HEIGHT, 300.0f));

  for (unsigned int line = 0; line < PAGE_SIZE / 8; line++)
  {
    ImGui::TextColored(DIMMED_COLOR, "%s", fmt::format("{0}", core::LongPointer(_memoryAddress) + line*8).c_str());
    ImGui::SameLine(0.0f, 20.0f);

    ImGui::PushItemWidth(70.0f);

    // Current line data
    std::array<char, 16> ascii;

    for (auto word = 0; word < 8; word++)
    {
      const unsigned int idx = line*8 + word;
      const core::WORD w =  _memoryPage->at(idx);

      const bool changed = (_previousMemoryPage && (_previousMemoryPage->at(idx) != w));

      const char lo = w & 0xFF;
      const char hi = w >> 8;

      ascii[word*2] = ((hi >= ' ') && (hi <= '~') ? hi : '.');
      ascii[word*2 + 1] = ((lo >= ' ') && (lo <= '~') ? lo : '.');

      if (!changed)
      {
        ImGui::TextUnformatted(fmt::format("{0:04x}", w).c_str());
      }
      else
      {
        ImGui::TextColored(HIGHLIGHT_COLOR, "%s", fmt::format("{0:04x}", w).c_str());
      }

      ImGui::SameLine();
    }

    ImGui::PopItemWidth();

    // Ascii representation of the current line data
    ImGui::SameLine(0.0f, 20.0f);
    ImGui::TextColored(DIMMED_COLOR, "%s", std::string(ascii.begin(), ascii.begin() + 16).c_str());
  }

  ImGui::EndChild();

  ImGui::Separator();

  // Memory address input
  {
    static int segment = 0;
    static int page = 0;

    if (ImGui::Button("jump to"))
    {
      _memoryAddress = core::LongPointer(segment, page * PAGE_SIZE);
      _memoryPage.reset();
    }

    ImGui::SameLine();
    ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x / 6.0f);
    ImGui::SliderInt("##segment", &segment, 0, core::cpu::Cpu::NUM_ADDRESS_PAGES - 1, fmt::format("{0:#04x}", segment).c_str(), ImGuiSliderFlags_AlwaysClamp);
    ImGui::SameLine();
    ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x / 4.0f);
    ImGui::SliderInt("##page", &page, 0, ((1 << 16) / PAGE_SIZE) - 1, fmt::format("{0:#06x}", page * PAGE_SIZE).c_str(), ImGuiSliderFlags_AlwaysClamp);

    ImGui::SameLine();

    ImGui::SetNextItemWidth(ImGui::GetContentRegionAvail().x);

    static std::optional<std::string> selected_label;

    if (ImGui::BeginCombo("##jump-labels", (selected_label.has_value() ? selected_label->c_str() : nullptr)))
    {
      const std::multimap<core::LongPointer, std::string> labels = assembly.getDebugInfo()->getLabels();

      for (const auto &[address, label] : labels)
      {
        const bool is_local = (label.find('~') != std::string::npos);

        if (!is_local)
        {
          if (ImGui::Selectable(label.c_str()))
          {
            segment = address.segment;
            page = address.offset / PAGE_SIZE;

            selected_label = label;
          }
        }
      }

      ImGui::EndCombo();
    }
  }

  ImGui::End();
}

// Render Disassembly view
bool DebuggerView::render(core::cpu::Cpu &cpu, const utils::Assembly &assembly)
{
  static const ImVec4 HIGHLIGHT_COLOR = { 1.0f, 1.0f, 0.0f, 1.0f };
  static const ImVec4 DIMMED_COLOR = { 0.5f, 0.5f, 0.5f, 1.0f };
  static const ImVec4 ON_COLOR = { 0.0f, 1.0f, 0.0f, 1.0f };
  static const ImVec4 OFF_COLOR = { 1.0f, 0.0f, 0.0f, 1.0f };

  static const std::array<core::cpu::Register, 8> ALL_REGISTERS = {
  core::cpu::Register::R0,
  core::cpu::Register::R1,
  core::cpu::Register::R2,
  core::cpu::Register::R3,
  core::cpu::Register::R4,
  core::cpu::Register::R5,
  core::cpu::Register::R6,
  core::cpu::Register::R7,
  };

  const std::array<std::pair<char, core::cpu::Flag::Bits>, 4> ALL_FLAGS = {{
  { 'C', core::cpu::Flag::Bits::CARRY },
  { 'Z', core::cpu::Flag::Bits::ZERO },
  { 'N', core::cpu::Flag::Bits::NEGATIVE},
  { 'O', core::cpu::Flag::Bits::OVERFLOW},
  }};

  bool scroll_to_disassembly_address = false;

  // If no disassembly address was set yet, or it was cleared by stepping through the assembly, set it to the current instruction pointer.
  if (!_disassemblyAddress)
  {
    _disassemblyAddress = cpu.getInstructionPointer();
  }

  // Disassemble when no disassembly is availble or whenever the CPU tick count changed
  if (!_disassembly || !_lastBreak || (_lastBreak != cpu.getPerformanceCounter()))
  {
    _lastBreak = cpu.getPerformanceCounter();
    _disassembly = utils::disassemble(*assembly.getCodeChunk(*_disassemblyAddress), assembly.getDebugInfo());

    _previousCpuState.swap(_cpuState);
    _cpuState = cpu.getState();

    scroll_to_disassembly_address = true;
  }

  ImGui::Begin("Debugger", nullptr); //, ImGuiWindowFlags_NoResize);

  if (ImGui::IsWindowCollapsed())
  {
    return true;
  }

  const utils::Assembly::DebugInfo * const debug_info = assembly.getDebugInfo();
  const core::cpu::Cpu::State previous_state = _previousCpuState.value_or(core::cpu::Cpu::State());

  // Disassembly view
  constexpr float DISASSEMBLY_VIEW_HEIGHT = 300.0f;

  ImGui::BeginChild("disassembly", ImVec2(0.0f, 300.0f));
  {
    ImDrawList * const draw_list = ImGui::GetWindowDrawList();

    // Iterate disassembly and output to the ImGui window, highlighting the instruction under the
    // current IP, and instructions for which a breakpoint is set. Also capture mouse clicks on
    // each line, and use them to toggle breakpoints. It's almost like a real debugger ;-)
    for (size_t line = 0; line < _disassembly->size(); line++)
    {
      const core::LongPointer &p = _disassembly->at(line).first;
      const std::string &s = _disassembly->at(line).second;

      if (debug_info)
      {
        for (const std::string &label : debug_info->getLabels(p))
        {
          const bool is_local = (label.find('~') != std::string::npos);

          // Pretty-printing: for non-local labels strip off leading '::' characters. For local labels, completely strip off namespace
          const std::string label_text = (is_local ? label.substr(label.rfind(':') + 1) : label.substr(2));

          ImGui::TextColored(DIMMED_COLOR, "%s:", label_text.c_str());
        }
      }

      std::optional<ImU32> highlight_color;

      ImGui::BeginGroup();

      // Use ImGui draw list splitting to be able to 'under-draw' a highlight rectangle later
      draw_list->ChannelsSplit(2);
      draw_list->ChannelsSetCurrent(1);

      const ImVec2 line_start = ImGui::GetCursorScreenPos();

      if (p == cpu.getInstructionPointer())
      {
        ImGui::TextUnformatted(fmt::format("{0}", p).c_str());
        ImGui::SameLine(100);
        ImGui::TextUnformatted(s.c_str());

        highlight_color = IM_COL32(50, 100, 255, 160);
      }
      else
      {
        ImGui::TextUnformatted(fmt::format("{0}", p).c_str());
        ImGui::SameLine(100);
        ImGui::TextUnformatted(s.c_str());

        if (cpu.getDebugger().isBreakpoint(p))
        {
          highlight_color = IM_COL32(200, 0, 0, 160);
        }
      }

      // Scroll to the instruction under the current IP if the CPU was just stepped
      if ((p == _disassemblyAddress) && scroll_to_disassembly_address)
      {
        ImGui::SetScrollY(ImGui::GetCursorPosY() - DISASSEMBLY_VIEW_HEIGHT / 2.0f);
      }

      ImGui::EndGroup();

      // In case a highlight color was set, draw highlight rectangle under the text we've just drawn
      if (highlight_color)
      {
        draw_list->ChannelsSetCurrent(0);

        const ImVec2 rect_min = ImGui::GetItemRectMin();
        const ImVec2 rect_max = ImGui::GetItemRectMax();

        draw_list->AddRectFilled(
        { line_start.x, rect_min.y},
        { line_start.x + ImGui::GetWindowWidth(), rect_max.y + 2 }, *highlight_color, false, ImDrawFlags_None);
      }

      draw_list->ChannelsMerge();

      // If the current line was clicked, toggle its breakpoint state
      if (ImGui::IsItemClicked())
      {
        if (cpu.getDebugger().isBreakpoint(p))
        {
          cpu.getDebugger().deleteBreakpoint(p);
        }
        else
        {
          cpu.getDebugger().setBreakpoint(p);
        }
      }
    }
  }

  ImGui::EndChild();

  ImGui::Separator();

  // Label drop down + jump button
  {
    static std::optional<std::string> selected;
    static std::optional<core::LongPointer> selectedAddress;

    const std::multimap<core::LongPointer, std::string> labels = assembly.getDebugInfo()->getLabels();

    if (!selectedAddress)
    {
      ImGui::BeginDisabled();
    }

    if (ImGui::Button("jump to"))
    {
      _disassemblyAddress = *selectedAddress;
      _disassembly.reset();
    }

    if (!selectedAddress)
    {
      ImGui::EndDisabled();
    }

    ImGui::SameLine();

    if (ImGui::BeginCombo("##jump-labels", (selected ? selected->c_str() : nullptr)))
    {
      for (const auto &[address, label] : labels)
      {
        const bool is_code = (assembly.getCodeChunk(address) != nullptr);
        const bool is_local = (label.find('~') != std::string::npos);

        if (is_code && !is_local)
        {
          if (ImGui::Selectable(label.c_str()))
          {
            selected = label;
            selectedAddress = address;
          }
        }
      }

      ImGui::EndCombo();
    }
  }

  ImGui::Separator();

  // Special-purpose registers
  ImGui::BeginGroup();
  {
    // Instruction pointer
    ImGui::Text("ip    ");
    ImGui::SameLine();
    ImGui::TextColored(DIMMED_COLOR, "%s", fmt::format("{0:#04x}:{1:#06x}", cpu.getInstructionPointer().segment, cpu.getInstructionPointer().offset).c_str());

    // Stack pointer
    ImGui::Text("sp    ");
    ImGui::SameLine();
    ImGui::TextColored((previous_state.sp == cpu.getStackPointer() ? DIMMED_COLOR : HIGHLIGHT_COLOR), "%s", fmt::format("{0:#06x}", cpu.getStackPointer()).c_str());

    // Segment register
    const auto &[src_segment, dst_segment] = cpu.getSegmentRegister();
    const core::WORD segment_register = (src_segment << 8) | dst_segment;

    ImGui::Text("sr    ");
    ImGui::SameLine();
    ImGui::TextColored((previous_state.sr == segment_register ? DIMMED_COLOR : HIGHLIGHT_COLOR), "%s", fmt::format("{0:#06x}", segment_register).c_str());

    // Flags
    ImGui::Text("flags ");
    ImGui::SameLine();

    for (const auto &[flag, bits] : ALL_FLAGS)
    {
      const ImVec4 color = (cpu.getFlag(bits) != 0 ? ON_COLOR : OFF_COLOR);

      ImGui::TextColored(color, "%s", fmt::format("{0}", flag).c_str());
      ImGui::SameLine();
    }
  }

  ImGui::EndGroup();

  ImGui::SameLine(0.0f, 50.0f);

  // General-purpose registers
  ImGui::BeginGroup();
  {
    for (size_t i = 0; i < ALL_REGISTERS.size(); i++)
    {

      const int reg_index = ((i % 2) * 4) + i / 2;

      const core::WORD old_value = previous_state.registers.all[reg_index];
      const core::WORD current_value = _cpuState->registers.all[reg_index];

      const ImVec4 color = (old_value == current_value ? DIMMED_COLOR : HIGHLIGHT_COLOR);

      ImGui::TextUnformatted(fmt::format("{0:6}", ALL_REGISTERS[reg_index]).c_str());
      ImGui::SameLine();
      ImGui::TextColored(color, "%s", fmt::format("{0:#06x}", current_value).c_str());

      if ((i % 2) == 0)
      {
        ImGui::SameLine(0.0f, 30.0f);
      }
    }
  }

  ImGui::EndGroup();
  ImGui::Separator();

  // Controls & input handling
  bool keep_showing_debugger = true;

  if (ImGui::Button("step"))
  {
    cpu.getDebugger().step();

    _disassemblyAddress.reset();
  }

  ImGui::SameLine();

  if (ImGui::Button("continue"))
  {
    cpu.getDebugger().resume();

    _disassemblyAddress.reset();

    keep_showing_debugger = false;
  }

  ImGui::End();

  return keep_showing_debugger;
}

}
