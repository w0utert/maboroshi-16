/// maboroshi-16 main executable.
///
/// This implements system initialization, a run loop, SDL-based visualization and
/// input handling, and ImGUI overlays for (currently only) debugging capabilities.
///
/// \author Wouter Bijlsma <wouter@wouterbijlsma.nl>
/// \license This project is released under the GNU Public License, version 3, see
///   LICENSE in the project root directory for the license text
/// \file

#include "ui/itf/debugger.h"

#include "platform/itf/machine.h"

#include "core/cpu/itf/exceptions.h"

#include "utils/itf/utils.h"

#include "third_party/imgui/imgui.h"
#include "third_party/imgui/imgui_impl_sdl.h"
#include "third_party/imgui/imgui_impl_sdlrenderer.h"

#include <SDL2/SDL.h>

#include <cstdlib>
#include <thread>

using namespace std::chrono;

namespace {

std::string dumpCpuState(const core::cpu::Cpu *cpu)
{
  return fmt::format(
     "\tip\t\t=\t{0}\n" \
     "\tsp\t\t=\t{1:#06x}\n" \
     "\tflags\t=\t{2:#06x}\n" \
     "\tr0\t\t=\t{3:#06x}\t\tr1\t\t=\t{4:#06x}\n" \
     "\tr2\t\t=\t{5:#06x}\t\tr3\t\t=\t{6:#06x}\n" \
     "\tr4\t\t=\t{7:#06x}\t\tr5\t\t=\t{8:#06x}\n" \
     "\tr6\t\t=\t{9:#06x}\t\tr7\t\t=\t{10:#06x}\n\n",
    cpu->getInstructionPointer(),
    cpu->getStackPointer(),
    cpu->getFlags(),
    cpu->getRegister(core::cpu::Register::R0),
    cpu->getRegister(core::cpu::Register::R1),
    cpu->getRegister(core::cpu::Register::R2),
    cpu->getRegister(core::cpu::Register::R3),
    cpu->getRegister(core::cpu::Register::R4),
    cpu->getRegister(core::cpu::Register::R5),
    cpu->getRegister(core::cpu::Register::R6),
    cpu->getRegister(core::cpu::Register::R7));
}

}

int main(int argc, char **argv)
{
  constexpr unsigned int SCREEN_WIDTH = 64*8;
  constexpr unsigned int SCREEN_HEIGHT = 36*8;

  constexpr unsigned int WINDOW_WIDTH = SCREEN_WIDTH * 2;
  constexpr unsigned int WINDOW_HEIGHT = SCREEN_HEIGHT * 2;

  // Initialize SDL
  if (SDL_Init(SDL_INIT_VIDEO) < 0)
  {
    std::cout << "Failed to initialize SDL2" << std::endl;
    return EXIT_FAILURE;
  }

  // Create SDL window and renderer
  SDL_Window * const window = SDL_CreateWindow(
    "maboroshi-16",
    SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
    WINDOW_WIDTH, WINDOW_HEIGHT,
    SDL_WINDOW_ALLOW_HIGHDPI);


  SDL_Renderer * const renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_PRESENTVSYNC);

  // Check window size vs drawable size. On macOS HiDPI screens, these will be different, and
  // everything will be rendered ridiculously small unless we enable render scaling.
  {
    int window_width, window_height;
    int drawable_width, drawable_height;

    SDL_GetWindowSize(window, &window_width, &window_height);
    SDL_GetRendererOutputSize(renderer, &drawable_width, &drawable_height);

    SDL_RenderSetScale(renderer, (float) drawable_width / window_width, (float) drawable_height / window_height);
  }

  // The framebuffer will be streamed to a texture which is blitted to the SDL window at each frame
  SDL_Texture *framebuffer_texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STREAMING, SCREEN_WIDTH, SCREEN_HEIGHT);

  // Set machine constants
  platform::Machine::Constants machine_constants;

  machine_constants.screenWidth = SCREEN_WIDTH;
  machine_constants.screenHeight = SCREEN_HEIGHT;
  machine_constants.cpuHz = 1'000'000;
  machine_constants.frameRate = 60;

  // Init ImGui
  ImGui::CreateContext();

  ImGuiIO &io = ImGui::GetIO();
  (void) io;

  io.IniFilename = nullptr;

  ImGui_ImplSDL2_InitForSDLRenderer(window);
  ImGui_ImplSDLRenderer_Init(renderer);

  // Create the machine instance
  platform::Machine machine(machine_constants);
  // Load kernel ROM
  std::ifstream assembly_file("./rom/kernel.mabo");

  const utils::Assembly assembly = utils::Assembly::fromInputStream(assembly_file);

  utils::load(machine.getCpu()->getRam(), assembly);

  // Create debugger view. This is mainly to cache the disassembly shown when the
  // ImGUI debugger overlay is active, so we can re-use it between frames while the
  // CPU is stopped
  ui::DebuggerView debugger_view;
  ui::MemoryView memory_view;

  // Set breakpoint at IP after CPU reset to immediately force a CPU break
  machine.getCpu()->getDebugger().setBreakpoint(core::cpu::Cpu::IP_0);
  machine.getCpu()->getDebugger().setMode(core::cpu::Debugger::Mode::RUN_TO_BREAKPOINT);

  // Input state for controller #0
  platform::Machine::ControllerState controller_0 = {};

  // SDL main loop until window closed
  steady_clock::time_point last_refresh = steady_clock::now();
  double refresh_rate = 0.0;

  bool quit = false;
  bool show_debugger = false;

  while (!quit)
  {
    // Handle pending events
    SDL_Event e;

    while (SDL_PollEvent(&e))
    {
      ImGui_ImplSDL2_ProcessEvent(&e);

      switch (e.type)
      {
        case SDL_QUIT:
          quit = true;
          break;

        case SDL_KEYDOWN:
        case SDL_KEYUP:
        {
          const bool down = (e.type == SDL_KEYDOWN);

          switch (e.key.keysym.sym)
          {
            case SDLK_a:
              controller_0.a = down;
              break;

            case SDLK_b:
              controller_0.b = down;
              break;

            case SDLK_x:
              controller_0.x = down;
              break;

            case SDLK_y:
              controller_0.y = down;
              break;

            case SDLK_RETURN:
              controller_0.start = down;
              break;

            case SDLK_SLASH:
              controller_0.select = down;
              break;
          }

          break;
        }

        default:
          break;
      }
    }

    // Set controller state
    machine.setControllerState(0, controller_0);

    // Emulate as much machine time as is spent in a single iteration through the host display
    // framerate. While this may mean frames could be dropped or duplicated if the host refresh
    // rate is not the same as the emulated display rate, this makes sure the emulator does not
    // run faster or slower depending on the host refresh rate.
    try
    {
      machine.tick(microseconds(static_cast<long>(1'000'000.0 / refresh_rate)));
    }
    catch (core::cpu::Break &e)
    {
      show_debugger = true;
    }
    catch (core::cpu::Halted &e)
    {
      std::cout << "CPU halted\n\n" << dumpCpuState(machine.getCpu()) << std::endl;

      std::exit(EXIT_FAILURE);
    }

    // Get video frame and blit to the window surface
    const core::gpu::Frame frame = machine.getFrame();

    SDL_UpdateTexture(framebuffer_texture, nullptr, (void *) frame.image.data.data(), frame.image.width*4);

    SDL_RenderClear(renderer);
    SDL_RenderCopy(renderer, framebuffer_texture, nullptr, nullptr);

    // Add enabled ImGui overlays
    ImGui_ImplSDLRenderer_NewFrame();
    ImGui_ImplSDL2_NewFrame();
    ImGui::NewFrame();

    if (show_debugger)
    {
      memory_view.render(*machine.getCpu(), assembly);
      show_debugger = debugger_view.render(*machine.getCpu(), assembly);
    }

//    ImGui::ShowDemoWindow();

    ImGui::Render();

    ImGui_ImplSDLRenderer_RenderDrawData(ImGui::GetDrawData());

    // Done with the frame, present it and update frame rate counters for main loop throttling
    SDL_RenderPresent(renderer);

    steady_clock::time_point current_refresh = steady_clock::now();

    refresh_rate = 1'000'000.0 / duration_cast<microseconds>(current_refresh - last_refresh).count();

    last_refresh = current_refresh;
  }

  std::cerr << "WINDOW CLOSE" << std::endl;

  SDL_DestroyTexture(framebuffer_texture);
  SDL_DestroyWindow(window);
  SDL_Quit();

  return EXIT_SUCCESS;
}


