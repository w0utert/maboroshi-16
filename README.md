maboroshi-16 is an effort to implement a 16-bit fantasy console from scratch, inspired by the great gaming systems from the past, such as the C64, NES, SNES, etc. The goal is to implement a full virtual hardware platform, firmware, development tools and shell to interact with the console.
This project is at this point purely for fun & learning, and since I have literally zero prior experience writing virtual hardware plaforms or emulators, I don't expect development to follow a straight line. 

I'm thinking of implementing at least the following:

*  A virtual CPU with a relatively small and straightforward instruction set \[mostly done\]
*  A virtual GPU that implements character & sprite-based rendering \[mostly done\]
*  A virtual sound chip that implements a waveform synthesizer \[future\]
*  A platform component that ties all the virtual hardware components together, sets up a fixed memory map, IO regions, configures interrupts, etc \[very preliminary\]
*  A capable assembler to write binaries on the emulator host \[mostly done\]
*  A firmware kernel that implements platform services and a standard library for client programs \[in progress\]
*  A debugger to be able to single-step the emulator and inspect its complete state \[mostly done\]
*  ~~A simple BASIC compiler/interpreter (either host-based, or running directly on the emulator)~~ \[scrapped, too much work, it will be 'game console' only\]
*  ~~A shell that is loaded when the console starts, to interact with the system and the BASIC interpreter~~ \[scrapped, too much work, it will be 'game console' only\]

This is a lot of work, and considering my leasury pace working at side-projects, it could take a while before this thing can even run a Hello World ;-) \[update: the Hello World milestone has been reached quite a while ago :-)\]

The specifications of the maboroshi-16 console will evolve during development. Now that development is advancing (slowly, but surely), system specs are converging to something like this:

*  ~4-8 Mhz CPU with 16-bit registers, using a fixed 32-bit instruction word
* 512 KB RAM, 512 KB of VRAM
*  A GPU that supports 256-color output at up to 512x288 pixel resolution, a sprite engine that supports up to 64 sprites, up to 4 independent sprite-, or character/tile-based layers
*  A sound chip similar in capabilities to the C64 MOS 6581 (SID) or something resembling the later Yamaha FM synthesizers

#### Update January 2022

By now, most basic system features have been implemented. Specifically, the follwing components are in full working order:

* The CPU, including a pretty rounded and capable instruction set and advanced features such as periphal memory banking, port-based I/O, IRQ handling
* The GPU, capable of compositing up to 4 layers in either monochrome-, or color-mapped tile mode, or full 8-bit color sprite mode, built-in ROM font & palette
* A fully functional symbolic assembler, including a preprocessor, binary output with optional debug info
* An SDL-based simulator binary to configure, initialize and run the maboroshi-16 emulator
* An integrated debugger with disassembly/CPU state and memory debugger views
* Infrastructure to load system ROM, with an extremely basic proof-of-concept ROM that just shows a 'hello-world' like message

Still missing and planned next main features:

* Clock/timer peripheral device for programmable timer IRQ's
* Input device peripheral devices for ~~keyboard and~~ gamepad \[keyboard support scrapped, retro game consoles don't have keyboards ;-)\]
* Sound device
* CPU improvements to update the execution model to be more realistic (in particular, variable instruction timings)

#### Screenshot

![screenshot](docs/screenshot.png "Hello world!")

