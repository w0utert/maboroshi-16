\page instruction_set_reference Instruction set reference

\tableofcontents

# ADD
\copydoc core::cpu::Cpu::add()

<hr>

# ADC
\copydoc core::cpu::Cpu::adc()

<hr>

# ASR
\copydoc core::cpu::Cpu::asr()

<hr>

# AND
\copydoc core::cpu::Cpu::and_()

<hr>

# B
\copydoc core::cpu::Cpu::b()

<hr>

# BANK
\copydoc core::cpu::Cpu::bank()

<hr>

# CALL
\copydoc core::cpu::Cpu::call()

<hr>

# CLR
\copydoc core::cpu::Cpu::clr()

<hr>

# CMP
\copydoc core::cpu::Cpu::cmp()

<hr>

# DEC
\copydoc core::cpu::Cpu::dec()

<hr>

# DIV
\copydoc core::cpu::Cpu::div()

<hr>

# DIVS
\copydoc core::cpu::Cpu::divs()

<hr>

# HALT
\copydoc core::cpu::Cpu::halt()

<hr>

# IN
\copydoc core::cpu::Cpu::in()

<hr>

# INC
\copydoc core::cpu::Cpu::inc()

<hr>

# J
\copydoc core::cpu::Cpu::j()

<hr>

# LDSP
\copydoc core::cpu::Cpu::ldsp()

<hr>

# LSL
\copydoc core::cpu::Cpu::lsl()

<hr>

# LS$
\copydoc core::cpu::Cpu::lsr()

<hr>

# MOV
\copydoc core::cpu::Cpu::mov()

<hr>

# MOVH
\copydoc core::cpu::Cpu::movh()

<hr>

# MOVL
\copydoc core::cpu::Cpu::movl()

<hr>

# MUL
\copydoc core::cpu::Cpu::mul()

<hr>

# MULS
\copydoc core::cpu::Cpu::muls()

<hr>

# NEG
\copydoc core::cpu::Cpu::neg()

<hr>

# NOT
\copydoc core::cpu::Cpu::not_()

<hr>

# OR
\copydoc core::cpu::Cpu::or_()

<hr>

# OUT
\copydoc core::cpu::Cpu::out()

<hr>

# POP
\copydoc core::cpu::Cpu::pop()

<hr>

# POPS
\copydoc core::cpu::Cpu::pops()

<hr>

# PUSH
\copydoc core::cpu::Cpu::push()

<hr>

# PUSHS
\copydoc core::cpu::Cpu::pushs()

<hr>

# RET
\copydoc core::cpu::Cpu::ret()

<hr>

# RSV
\copydoc core::cpu::Cpu::rsv()

<hr>

# RTI
\copydoc core::cpu::Cpu::rti()

<hr>

# SBB
\copydoc core::cpu::Cpu::sbb()

<hr>

# SET
\copydoc core::cpu::Cpu::set()

<hr>

# STSP
\copydoc core::cpu::Cpu::stsp()

<hr>

# SUB
\copydoc core::cpu::Cpu::sub()

<hr>

# WAIT
\copydoc core::cpu::Cpu::wait()

<hr>

# XOR 
\copydoc core::cpu::Cpu::xor_()

<hr>
